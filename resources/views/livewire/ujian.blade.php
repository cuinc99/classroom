@php
$jawabanTugas = \App\Models\JawabanTugas::firstOrCreate(
    [
        'tugas_id' => $tugas->id,
        'mahasiswa_id' => $mahasiswa->id,
        'pertanyaan_id' => $pertanyaan->id,
    ],
    [
        'poin' => 0,
        'mahasiswa_id' => $mahasiswa->id,
        'tugas_id' => $tugas->id,
        'pertanyaan_id' => $pertanyaan->id,
        'jawaban_id' => NULL,
    ]
);
@endphp
<label wire:click='updateJawaban({{ $pertanyaan->id }}, {{ $jawaban->id }}, {{ $jawaban->poin }})'
    class="btn btn-outline btn-outline-dashed d-flex flex-stack text-start p-6 mb-5" for="jawaban{{ $jawaban->id }}">
    <!--end::Description-->
    <div class="d-flex align-items-center me-2">
        <!--begin::Radio-->
        <div class="form-check form-check-custom form-check-solid form-check-primary me-6">
            <input class="form-check-input" type="radio" name="jawaban{{ $pertanyaan->id }}" id="jawaban{{ $jawaban->id }}"
                {{ $jawaban->id == $jawabanTugas->jawaban_id ? 'checked' : '' }} />
        </div>
        <!--end::Radio-->

        <!--begin::Info-->
        <div class="flex-grow-1">
            <div class="fw-bold">
                <span class="fs-1 opacity-50">
                    {{ chr(64 + $index) }}.
                </span>
                {{ $jawaban->jawaban ?? '-' }}
            </div>
        </div>
        <!--end::Info-->
    </div>
    <!--end::Description-->

    <!--begin::Price-->
    <!--end::Price-->
</label>
