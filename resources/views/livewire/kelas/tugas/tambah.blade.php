<form wire:submit.prevent='save' enctype="multipart/form-data">
    <div class="d-flex flex-column gap-7 gap-lg-10">
        <!--begin::Inventory-->
        <div class="card card-flush py-4">
            <!--begin::Card body-->
            <div class="card-body pt-5">
                <!--begin::Input group-->
                <div class="mb-10 fv-row">
                    <!--begin::Label-->
                    <label class="required form-label">Judul</label>
                    <!--end::Label-->
                    <div class="input-group input-group-solid mb-5">
                        <span class="input-group-text" id="basic-addon2">
                            <span class="svg-icon svg-icon-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none">
                                    <path opacity="0.3"
                                        d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                                        fill="black" />
                                    <path
                                        d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                                        fill="black" />
                                </svg>
                            </span>
                        </span>
                        <input type="text" wire:model="tugas.judul" class="form-control" placeholder="Judul"
                            required />
                    </div>
                    <!--begin::Input-->
                    <!--end::Input-->
                </div>
                <!--end::Input group-->
                <!--begin::Input group-->
                <div class="mb-10 fv-row">
                    <!--begin::Label-->
                    <label class="form-label">Instruksi</label>
                    <!--end::Label-->
                    <div id="isi_editor" wire:ignore></div>
                    <input type="hidden" id="isi_html" wire:model="tugas.instruksi" name="isi"></input>
                    {{-- <div class="input-group input-group-solid mb-5">
                        <span class="input-group-text" id="basic-addon2">
                            <span class="svg-icon svg-icon-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none">
                                    <path opacity="0.3"
                                        d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                                        fill="black" />
                                    <path
                                        d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                                        fill="black" />
                                </svg>
                            </span>
                        </span>
                        <textarea wire:model="tugas.instruksi" class="form-control"></textarea>
                    </div> --}}
                    <!--begin::Input-->
                    <!--end::Input-->
                </div>
                <div class="mb-10 fv-row">
                    <!--begin::Label-->
                    <label class="form-label">Dokumen Instruksi</label>
                    <!--end::Label-->
                    <div class="input-group input-group-solid mb-5">
                        <span class="input-group-text" id="basic-addon2">
                            <span class="svg-icon svg-icon-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none">
                                    <path opacity="0.3"
                                        d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                                        fill="black" />
                                    <path
                                        d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                                        fill="black" />
                                </svg>
                            </span>
                        </span>
                        <input type="file" wire:model="tugas.dokumen_instruksi" class="form-control" />
                    </div>
                    <!--begin::Input-->
                    <!--end::Input-->
                </div>
                <!--end::Input group-->
                <!--begin::Input group-->
                {{-- <div class="mb-10 fv-row">
                    <!--begin::Label-->
                    <label class="form-label">Maksimal Poin</label>
                    <!--end::Label-->
                    <!--begin::Input-->
                    <input type="number" wire:model="tugas.maks_poin" class="form-control" placeholder="Maksimal Poin" />
                    <!--end::Input-->
                </div> --}}
                <!--end::Input group-->
                <!--begin::Input group-->
                {{-- <div class="mb-10 fv-row">
                    <!--begin::Label-->
                    <label class="form-label">Batas Akhir Tugas</label>
                    <!--end::Label-->
                    <!--begin::Input-->
                    <div class="input-group input-group-solid mb-5">
                        <span class="input-group-text" id="basic-addon2">
                            <span class="svg-icon svg-icon-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.3" d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" fill="black"/>
                                    <path d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" fill="black"/>
                                    </svg>
                            </span>
                        </span>
                        <input type="date" wire:model="tugas.batas_akhir" class="form-control" placeholder="Batas Akhir Tugas" />
                    </div>
                    <!--end::Input-->
                </div> --}}
                <!--end::Input group-->
                <!--begin::Input group-->
                <div x-data="{ open: false }">
                    <div class="fv-row mb-10">
                        <!--begin::Label-->
                        <label class="fs-6 fw-bold mb-2 required">Tipe Tugas
                            <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip"
                                title="lorem ipsum"></i></label>
                        <!--End::Label-->
                        <!--begin::Row-->
                        <div class="row row-cols-1 row-cols-md-3 row-cols-lg-1 row-cols-xl-3 g-9" data-kt-buttons="true"
                            data-kt-buttons-target="[data-kt-button='true']">
                            <!--begin::Col-->
                            @if (auth()->user()->isAdmin())
                                <div class="col">
                                    <!--begin::Option-->
                                    <label
                                        class="btn btn-outline btn-outline-dashed btn-outline-default  d-flex text-start p-6"
                                        data-kt-button="true" x-on:click="open = false">
                                        <!--begin::Radio-->
                                        <span
                                            class="form-check form-check-custom form-check-solid form-check-sm align-items-start mt-1">
                                            <input class="form-check-input" type="radio" wire:model="tugas.tipe"
                                                value="input" checked="checked" />
                                        </span>
                                        <!--end::Radio-->
                                        <!--begin::Info-->
                                        <span class="ms-5">
                                            <span class="fs-4 fw-bolder text-gray-800 d-block">Input Nilai</span>
                                        </span>
                                        <!--end::Info-->
                                    </label>
                                    <!--end::Option-->
                                </div>
                            @endif
                            <!--end::Col-->
                            <!--begin::Col-->
                            <div class="col">
                                <!--begin::Option-->
                                <label
                                    class="btn btn-outline btn-outline-dashed btn-outline-default  d-flex text-start p-6"
                                    data-kt-button="true" x-on:click="open = false">
                                    <!--begin::Radio-->
                                    <span
                                        class="form-check form-check-custom form-check-solid form-check-sm align-items-start mt-1">
                                        <input class="form-check-input" type="radio" wire:model="tugas.tipe"
                                            value="rubrik" />
                                    </span>
                                    <!--end::Radio-->
                                    <!--begin::Info-->
                                    <span class="ms-5">
                                        <span class="fs-4 fw-bolder text-gray-800 d-block">Rubrik</span>
                                    </span>
                                    <!--end::Info-->
                                </label>
                                <!--end::Option-->
                            </div>
                            <!--end::Col-->
                            <!--begin::Col-->
                            <div class="col">
                                <!--begin::Option-->
                                <label
                                    class="btn btn-outline btn-outline-dashed btn-outline-default d-flex text-start p-6"
                                    data-kt-button="true" x-on:click="open = true">
                                    <!--begin::Radio-->
                                    <span
                                        class="form-check form-check-custom form-check-solid form-check-sm align-items-start mt-1">
                                        <input class="form-check-input" type="radio" wire:model="tugas.tipe"
                                            value="kuis" />
                                    </span>
                                    <!--end::Radio-->
                                    <!--begin::Info-->
                                    <span class="ms-5">
                                        <span class="fs-4 fw-bolder text-gray-800 d-block">Kuis</span>
                                    </span>
                                    <!--end::Info-->
                                </label>
                                <!--end::Option-->
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Row-->
                    </div>
                    <!--end::Input group-->
                    <!--begin::Input group-->
                    <div x-bind:class="! open ? 'd-none' : ''" x-cloak>
                        <div class="fv-row mb-10">
                            <!--begin::Label-->
                            <label class="form-label">Acak Soal</label>
                            <!--end::Label-->
                            <!--begin::Input-->
                            <div class="form-check form-check-custom form-check-solid mb-2">
                                <input class="form-check-input" type="radio" id="acak_soal_ya" name="acak_soal"
                                    wire:model='tugas.acak_soal' value="1" />
                                <label class="form-check-label" for="acak_soal_ya">Ya</label>
                            </div>
                            <div class="form-check form-check-custom form-check-solid mb-2">
                                <input class="form-check-input" type="radio" id="acak_soal_tidak" name="acak_soal"
                                    wire:model='tugas.acak_soal' value="0" />
                                <label class="form-check-label" for="acak_soal_tidak">Tidak</label>
                            </div>
                            <!--end::Input-->
                            <!--begin::Description-->
                            <div class="text-muted fs-7">Centang YA untuk mengacak soal.</div>
                            <!--end::Description-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="fv-row">
                            <!--begin::Label-->
                            <label class="form-label">Acak Jawaban</label>
                            <!--end::Label-->
                            <!--begin::Input-->
                            <div class="form-check form-check-custom form-check-solid mb-2">
                                <input class="form-check-input" type="radio" id="acak_jawaban_ya"
                                    name="tugas.acak_jawaban" wire:model='tugas.acak_jawaban' value="1" />
                                <label class="form-check-label" for="acak_jawaban_ya">Ya</label>
                            </div>
                            <div class="form-check form-check-custom form-check-solid mb-2">
                                <input class="form-check-input" type="radio" id="acak_jawaban_tidak"
                                    name="tugas.acak_jawaban" wire:model='tugas.acak_jawaban' value="0" checked />
                                <label class="form-check-label" for="acak_jawaban_tidak">Tidak</label>
                            </div>
                            <!--end::Input-->
                            <!--begin::Description-->
                            <div class="text-muted fs-7">Centang YA untuk mengacak jawaban.</div>
                            <!--end::Description-->
                        </div>
                        <!--end::Input group-->
                    </div>
                </div>
            </div>
            <!--end::Card header-->
        </div>
        <!--end::Inventory-->
    </div>
    <div class="d-flex justify-content-end mt-10">
        <!--begin::Button-->
        <a href="{{ route('kelas.penilaianSumatif', $kelas->kode) }}" class="btn btn-light me-5">Batal</a>
        <!--end::Button-->
        <!--begin::Button-->
        <button type="submit" class="btn btn-primary">
            <span class="indicator-label">Simpan</span>
        </button>
        <!--end::Button-->
    </div>
</form>

@push('scripts')
<script src="/assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js"></script>
<script src="/js/quill/image-resize.min.js"></script>
<script src="/js/quill/image-drop.min.js"></script>
<script>
    var toolbarOptions = [
        ['bold', 'italic', 'underline', 'strike'], // toggled buttons
        ['blockquote', 'code-block'],

        [{
            'header': 1
        }, {
            'header': 2
        }], // custom button values
        [{
            'list': 'ordered'
        }, {
            'list': 'bullet'
        }],
        [{
            'script': 'sub'
        }, {
            'script': 'super'
        }], // superscript/subscript
        [{
            'indent': '-1'
        }, {
            'indent': '+1'
        }], // outdent/indent
        [{
            'direction': 'rtl'
        }], // text direction

        [{
            'size': ['small', false, 'large', 'huge']
        }], // custom dropdown
        [{
            'header': [1, 2, 3, 4, 5, 6, false]
        }],

        [{
            'color': []
        }, {
            'background': []
        }], // dropdown with defaults from theme
        [{
            'font': []
        }],
        [{
            'align': []
        }],
        ['image', 'video', 'link'],

        ['clean'] // remove formatting button
    ];
    var quill = new Quill('#isi_editor', {
        theme: 'snow',
        placeholder: 'Tulis pengumuman disini...',
        modules: {
            toolbar: toolbarOptions,
            imageResize: {
                displaySize: true
            },
            imageDrop: true
        },
    });
    quill.on('text-change', function(delta, oldDelta, source) {
        document.getElementById("isi_html").value = quill.root.innerHTML;
    });
</script>
@endpush
