<div>
    <div class="d-flex flex-column gap-7 gap-lg-10">
        <form wire:submit.prevent='save'>
            <!--begin::Inventory-->
            <div class="card card-flush py-4">
                <!--begin::Card body-->
                <div class="card-body pt-5">
                    <!--begin::Input group-->
                    <div class="fv-row">
                        <!--begin::Label-->
                        <label class="required form-label">Pilih Kelompok Pertanyaan</label>
                        <!--end::Label-->
                        <div class="input-group input-group-solid flex-nowrap">
                            <div class="overflow-hidden flex-grow-1">
                                <select id="pertanyaans" wire:model='kelompok_pertanyaan_id'
                                    class="form-select form-select-solid rounded-0 border-start border-end"
                                    data-control="select2" data-placeholder="Cari kelompok pertanyaan" required>
                                    <option></option>
                                    @foreach ($kelompokPertanyaans as $item)
                                        @if ($item->pertanyaans()->count() > 0)
                                            <option value="{{ $item->id }}">{!! $item->nama ?? '-' !!}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <button type="submit" class="btn btn-icon btn-primary">
                                <!--begin::Svg Icon | path: icons/duotune/general/gen035.svg-->
                                <span class="svg-icon svg-icon-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none">
                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black" />
                                        <rect x="10.8891" y="17.8033" width="12" height="2" rx="1"
                                            transform="rotate(-90 10.8891 17.8033)" fill="black" />
                                        <rect x="6.01041" y="10.9247" width="12" height="2" rx="1" fill="black" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                            </button>
                        </div>
                        <!--begin::Input-->

                        <!--end::Input-->
                    </div>
                    <!--end::Input group-->

                </div>
                <!--end::Card header-->
            </div>
        </form>
        <!--end::Inventory-->
    </div>

    @if ($pertanyaans->count() > 0)
        <div class="card-body pt-0 mt-10">
            <!--begin::Table-->
            <table class="table align-middle table-row-dashed fs-6 gy-5">
                <thead>
                    <!--begin::Table row-->
                    <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                        <th width="10">#</th>
                        <th class="min-w-100px">Pertanyaan</th>
                        <th class="text-end"></th>
                    </tr>
                    <!--end::Table row-->
                </thead>
                <!--begin::Table body-->
                <tbody class="fw-bold text-gray-600">
                    <!--begin::Table row-->
                    @foreach ($pertanyaans as $pertanyaanItem)
                        <tr>
                            <td>
                                {{ $loop->iteration }}
                            </td>
                            <!--begin::Customer-->
                            <td>
                                <a href="#"
                                    class="text-gray-800 fw-bolder text-hover-primary mb-1 fs-6 text-start pe-0">{!! $pertanyaanItem->pertanyaan ?? '-' !!}</a>
                                <div class="d-flex flex-column flex-grow-1 pe-8 mt-5">
                                    <!--begin::Stats-->
                                    <div class="d-flex flex-wrap">
                                        @foreach ($pertanyaanItem->jawabans as $jawabanItem)
                                            <!--begin::Stat-->
                                            <div
                                                class="border {{ $jawabanItem->poin > 0 ? 'text-primary border-primary shadow' : 'border-gray-300' }} border rounded min-w-125px py-3 px-4 me-6 mb-3">
                                                <!--begin::Number-->
                                                <div class="d-flex align-items-center">
                                                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
                                                    <span
                                                        class="svg-icon svg-icon-3 {{ $jawabanItem->poin > 0 ? ' svg-icon-primary' : '' }} me-2">
                                                        @if ($jawabanItem->poin > 0)
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                height="24" viewBox="0 0 24 24" fill="none">
                                                                <path opacity="0.3"
                                                                    d="M10.3 14.3L11 13.6L7.70002 10.3C7.30002 9.9 6.7 9.9 6.3 10.3C5.9 10.7 5.9 11.3 6.3 11.7L10.3 15.7C9.9 15.3 9.9 14.7 10.3 14.3Z"
                                                                    fill="black" />
                                                                <path
                                                                    d="M22 12C22 17.5 17.5 22 12 22C6.5 22 2 17.5 2 12C2 6.5 6.5 2 12 2C17.5 2 22 6.5 22 12ZM11.7 15.7L17.7 9.70001C18.1 9.30001 18.1 8.69999 17.7 8.29999C17.3 7.89999 16.7 7.89999 16.3 8.29999L11 13.6L7.70001 10.3C7.30001 9.89999 6.69999 9.89999 6.29999 10.3C5.89999 10.7 5.89999 11.3 6.29999 11.7L10.3 15.7C10.5 15.9 10.8 16 11 16C11.2 16 11.5 15.9 11.7 15.7Z"
                                                                    fill="black" />
                                                            </svg>
                                                        @else
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                height="24" viewBox="0 0 24 24" fill="none">
                                                                <path opacity="0.3"
                                                                    d="M12 10.6L14.8 7.8C15.2 7.4 15.8 7.4 16.2 7.8C16.6 8.2 16.6 8.80002 16.2 9.20002L13.4 12L12 10.6ZM10.6 12L7.8 14.8C7.4 15.2 7.4 15.8 7.8 16.2C8 16.4 8.3 16.5 8.5 16.5C8.7 16.5 8.99999 16.4 9.19999 16.2L12 13.4L10.6 12Z"
                                                                    fill="black" />
                                                                <path
                                                                    d="M22 12C22 17.5 17.5 22 12 22C6.5 22 2 17.5 2 12C2 6.5 6.5 2 12 2C17.5 2 22 6.5 22 12ZM13.4 12L16.2 9.20001C16.6 8.80001 16.6 8.19999 16.2 7.79999C15.8 7.39999 15.2 7.39999 14.8 7.79999L12 10.6L9.2 7.79999C8.8 7.39999 8.2 7.39999 7.8 7.79999C7.4 8.19999 7.4 8.80001 7.8 9.20001L10.6 12L7.8 14.8C7.4 15.2 7.4 15.8 7.8 16.2C8 16.4 8.3 16.5 8.5 16.5C8.7 16.5 9 16.4 9.2 16.2L12 13.4L14.8 16.2C15 16.4 15.3 16.5 15.5 16.5C15.7 16.5 16 16.4 16.2 16.2C16.6 15.8 16.6 15.2 16.2 14.8L13.4 12Z"
                                                                    fill="black" />
                                                            </svg>
                                                        @endif
                                                    </span>
                                                    <!--end::Svg Icon-->
                                                    <div class="fs-2 fw-bolder">{{ $jawabanItem->poin ?? '0' }}</div>
                                                </div>
                                                <!--end::Number-->
                                                <!--begin::Label-->
                                                <div class="fw-bold fs-6 text-gray-400">
                                                    {{ $jawabanItem->jawaban ?? '-' }}</div>
                                                <!--end::Label-->
                                            </div>
                                            <!--end::Stat-->
                                        @endforeach
                                    </div>
                                    <!--end::Stats-->
                                </div>
                            </td>
                            <!--end::Customer-->
                            <!--begin::Action-->
                            <td class="text-end">
                                <a wire:click.prevent="deletePertanyaan({{ $pertanyaanItem->pivot ? $pertanyaanItem->pivot->id : '' }})"
                                    onclick="confirm('Yakin akan menghapus?') || event.stopImmediatePropagation()"
                                    data-bs-toggle="tooltip" data-bs-custom-class="tooltip-dark"
                                    data-bs-placement="right" title="Hapus">
                                    <span class="svg-icon svg-icon-danger svg-icon-2hx">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10"
                                                fill="black" />
                                            <rect x="7" y="15.3137" width="12" height="2" rx="1"
                                                transform="rotate(-45 7 15.3137)" fill="black" />
                                            <rect x="8.41422" y="7" width="12" height="2" rx="1"
                                                transform="rotate(45 8.41422 7)" fill="black" />
                                        </svg>
                                    </span>
                                </a>
                            </td>
                            <!--end::Action-->
                        </tr>
                    @endforeach
                    <!--end::Table row-->
                </tbody>
                <!--end::Table body-->
            </table>
            <!--end::Table-->
        </div>
    @endif
</div>

@push('scripts')
    <script>
        document.addEventListener("livewire:load", () => {
            let el = $('#pertanyaans')
            initSelect()
            Livewire.hook('message.processed', (message, component) => {
                initSelect()
            })
            Livewire.on('setUsersSelect', values => {
                el.val(values).trigger('change.select2')
            })
            el.on('change', function(e) {
                @this.set('kelompok_pertanyaan_id', el.select2("val"))
            })

            function initSelect() {
                el.select2({
                    placeholder: '{{ __('Pilih kelompok pertanyaan') }}',
                    allowClear: !el.attr('required'),
                })
            }
        })
    </script>
@endpush
