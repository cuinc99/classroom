<form wire:submit.prevent='saveRubrik'>
    <div class="d-flex flex-column gap-7 gap-lg-10">
        <!--begin::Inventory-->
        <div class="card card-flush py-4">
            <!--begin::Card body-->
            <div class="card-body pt-5">
                <!--begin::Input group-->
                <div class="mb-10 fv-row">
                    <!--begin::Label-->
                    <label
                        class="required form-label">{{ \App\Models\Tugas::TIPE_OPTION[$tugas->tipe]['judul'] }}</label>
                    <!--end::Label-->
                    <!--begin::Input-->
                    <div class="input-group input-group-solid mb-5">
                        <span class="input-group-text" id="basic-addon2">
                            <span class="svg-icon svg-icon-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.3" d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" fill="black"/>
                                    <path d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" fill="black"/>
                                    </svg>
                            </span>
                        </span>
                        <input type="text" wire:model="pertanyaan.pertanyaan" class="form-control"
                        placeholder="Tulis {{ \App\Models\Tugas::TIPE_OPTION[$tugas->tipe]['judul'] }}" required />
                    </div>

                    <!--end::Input-->
                </div>
                <!--end::Input group-->
                <div class="mb-10 fv-row">
                    <!--begin::Label-->
                    <label
                        class="required form-label">Maksimal Poin</label>
                    <!--end::Label-->
                    <!--begin::Input-->
                    <div class="input-group input-group-solid mb-5">
                        <span class="input-group-text" id="basic-addon2">
                            <span class="svg-icon svg-icon-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.3" d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" fill="black"/>
                                    <path d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" fill="black"/>
                                    </svg>
                            </span>
                        </span>
                        <input type="number" wire:model="pertanyaan.maks_poin" class="form-control"
                        placeholder="Maksismal Poin" required />
                    </div>

                    <!--end::Input-->
                </div>
                <!--begin::Input group-->
                <div class="mb-10 fv-row">
                    <!--begin::Label-->
                    <label class="form-label">Deskripsi</label>
                    <!--end::Label-->
                    <div class="input-group input-group-solid mb-5">
                        <span class="input-group-text" id="basic-addon2">
                            <span class="svg-icon svg-icon-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.3" d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" fill="black"/>
                                    <path d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" fill="black"/>
                                    </svg>
                            </span>
                        </span>
                        <textarea wire:model="pertanyaan.deskripsi" class="form-control form-control-solid mb-2" placeholder="Deskripsi"></textarea>
                    </div>
                    <!--begin::Input-->
                    <!--end::Input-->
                </div>
                <!--end::Input group-->

            </div>
            <!--end::Card header-->
        </div>
        <!--end::Inventory-->
    </div>
    <div class="d-flex justify-content-end mt-10">
        <!--begin::Button-->
        <a href="{{ route('kelas.penilaianSumatif', $kelas->kode) }}" class="btn btn-light me-5">Batal</a>
        <!--end::Button-->
        <!--begin::Button-->
        <button type="submit" class="btn btn-primary">
            <span class="indicator-label">Simpan</span>
        </button>
        <!--end::Button-->
    </div>

    @if ($pertanyaans->count() > 0)
        <div class="card-body pt-0 mt-10">
            <!--begin::Table-->
            <table class="table align-middle table-row-dashed fs-6 gy-5">
                <thead>
                    <!--begin::Table row-->
                    <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                        <th class="min-w-100px">Kriteria</th>
                        <th class="text-end">Maksimal Poin</th>
                        <th class="text-end">Tanggal Buat</th>
                        <th class="text-end"></th>
                    </tr>
                    <!--end::Table row-->
                </thead>
                <!--begin::Table body-->
                <tbody class="fw-bold text-gray-600">
                    <!--begin::Table row-->
                    @foreach ($pertanyaans as $pertanyaanItem)
                        <tr>
                            <!--begin::Customer-->
                            <td>
                                <a href="#"
                                    class="text-gray-800 fw-bolder text-hover-primary mb-1 fs-6 text-start pe-0">{!! $pertanyaanItem->pertanyaan ?? '-' !!}</a>
                            </td>
                            <td class="text-end">
                                <span
                                    class="fw-bolder">{{ $pertanyaanItem->maks_poin }}</span>
                            </td>
                            <!--end::Customer-->
                            <td class="text-end">
                                <span
                                    class="fw-bolder">{{ Date::parse($pertanyaanItem->created_at)->diffForHumans() }}</span>
                            </td>
                            <!--begin::Action-->
                            <td class="text-end">
                                {{-- <a href="{{ route('pertanyaan.daftar', [$kelas->kode, $kategori->kode, $tugas->kode]) }}" class="btn btn-sm btn-light-{{ \App\Models\Tugas::TIPE_OPTION[$tugas->tipe]['warna'] }}"><i class="bi bi-plus fs-4 me-2"></i> Buat {{ Str::ucfirst($tugas->tipe) }}</a> --}}
                                <a href="#" class="btn btn-sm btn-light btn-active-light-primary"
                                    data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Aksi
                                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
                                    <span class="svg-icon svg-icon-5 m-0">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none">
                                            <path
                                                d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                                                fill="black" />
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->
                                </a>
                                <!--begin::Menu-->
                                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4"
                                    data-kt-menu="true">
                                    <!--begin::Menu item-->
                                    {{-- <div class="menu-item px-3">
                                        <a href="#" class="menu-link px-3">Edit</a>
                                    </div> --}}
                                    <!--end::Menu item-->
                                    <!--begin::Menu item-->
                                    <div class="menu-item px-3">
                                        <a wire:click.prevent="deletePertanyaan({{ $pertanyaanItem->id }})"
                                            onclick="confirm('Yakin akan menghapus?') || event.stopImmediatePropagation()"
                                            class="menu-link px-3">Hapus</a>
                                    </div>
                                    <!--end::Menu item-->
                                </div>
                                <!--end::Menu-->
                            </td>
                            <!--end::Action-->
                        </tr>
                    @endforeach
                    <!--end::Table row-->
                </tbody>
                <!--end::Table body-->
            </table>
            <!--end::Table-->
        </div>
    @endif
</form>
