<div class="row">
    <div class="col-12 mb-5">
        <form method="POST" action="{{ route('kuliah.simpan') }}">
            @csrf
            <div class="input-group">
                <span class="input-group-text" id="basic-addon3" style="border: none">
                    <i class="las la-wallet fs-1"></i>
                </span>
                <input type="hidden" name="kelas_id" value="{{ $kelas->id }}">
                <input type="text" name="nama" class="form-control form-control-solid"
                    placeholder="Nama perkuliahan" required/>
                <button type="submit" class="btn btn-icon btn-primary">
                    <!--begin::Svg Icon | path: icons/duotune/general/gen035.svg-->
                    <span class="svg-icon svg-icon-2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                            fill="none">
                            <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black" />
                            <rect x="10.8891" y="17.8033" width="12" height="2" rx="1"
                                transform="rotate(-90 10.8891 17.8033)" fill="black" />
                            <rect x="6.01041" y="10.9247" width="12" height="2" rx="1" fill="black" />
                        </svg>
                    </span>
                    <!--end::Svg Icon-->
                </button>
            </div>
        </form>
    </div>

    <!--begin::Content-->
    <div class="col-12">

        @foreach ($kategoriItems as $kategori)
            <!--begin::Kategori-->
            <div class="card card-flush mb-5 crd-shadow bg-light">
                <!--begin::Card header-->
                <div class="card-header align-items-center py-5 gap-2 gap-md-5" style="background: #1c285e;">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label text-white fw-bolder fs-3 mb-1">{{ $kategori->nama ?? '-' }}</span>
                        <span class="text-muted mt-1 fw-bold fs-7">Persentase Nilai :
                            {{ $kategori->persentase_nilai ?? '-' }}%</span>
                    </h3>
                    <!--begin::Card toolbar-->
                    <div class="card-toolbar flex-row-fluid justify-content-end gap-5">
                        <!--begin::Add product-->
                        <a href="{{ route('tugas.tambah', [$kelas->kode, $kategori->kode]) }}"
                            class="btn btn-secondary rounded-50">
                            <span class="svg-icon svg-icon-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                    viewBox="0 0 24 24" fill="none">
                                    <rect opacity="0.3" x="2" y="2" width="20"
                                        height="20" rx="5" fill="black" />
                                    <rect x="10.8891" y="17.8033" width="12" height="2"
                                        rx="1" transform="rotate(-90 10.8891 17.8033)" fill="black" />
                                    <rect x="6.01041" y="10.9247" width="12" height="2"
                                        rx="1" fill="black" />
                                </svg>
                            </span>
                            Tambah</a>
                        <!--end::Add product-->
                    </div>
                    <!--end::Card toolbar-->
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body pt-0">
                    <!--begin::Table-->
                    <table class="table align-middle table-row-dashed fs-6 gy-5">
                        <!--begin::Table body-->
                        <tbody class="fw-bold text-gray-600">
                            <!--begin::Table row-->
                            @foreach ($kategori->tugas as $tugas)
                                <tr>
                                    <!--begin::Customer-->
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <!--begin:: Avatar -->
                                            <div class="symbol symbol-circle symbol-50px overflow-hidden me-3">
                                                <x-smart-image
                                                    src="{{ public_path(\App\Models\Tugas::TIPE_OPTION[$tugas->tipe]['icon']) }}"
                                                    height="100px" />
                                            </div>
                                            <!--end::Avatar-->
                                            <div class="ms-5">
                                                <a href="#"
                                                    class="text-gray-800 fw-bolder text-hover-primary mb-1 fs-6 text-start pe-0">{{ $tugas->judul ?? '-' }}</a>
                                                @if ($tugas->batas_akhir)
                                                    <br>
                                                    <small class="text-muted">
                                                        Deadline:
                                                        <span class="text-{{ $tugas->color }}">
                                                            {{ Date::parse($tugas->batas_akhir)->format('l, d M Y H:i') }}
                                                        </span>
                                                    </small>
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                    <!--end::Customer-->
                                    <td class="text-end pe-0">
                                        <span
                                            class="fw-bolder">{{ Date::parse($tugas->created_at)->diffForHumans() }}</span>
                                    </td>
                                    <!--begin::Action-->
                                    <td class="text-end">

                                        @if ($tugas->tipe != 'input')
                                            @if ($tugas->status_ujian)
                                                <a href="{{ route('tugas.tutup', $tugas->kode) }}"
                                                    class="btn btn-sm btn-light-danger">
                                                    <span class="svg-icon"><svg xmlns="http://www.w3.org/2000/svg"
                                                            width="24" height="24" viewBox="0 0 24 24"
                                                            fill="none">
                                                            <rect opacity="0.5" x="7.05025" y="15.5356"
                                                                width="12" height="2" rx="1"
                                                                transform="rotate(-45 7.05025 15.5356)"
                                                                fill="black" />
                                                            <rect x="8.46447" y="7.05029" width="12"
                                                                height="2" rx="1"
                                                                transform="rotate(45 8.46447 7.05029)"
                                                                fill="black" />
                                                        </svg></span>
                                                    Tutup</a>
                                            @else
                                                <a href="{{ route('tugas.buka', $tugas->kode) }}"
                                                    class="btn btn-sm btn-light-primary">
                                                    <span class="svg-icon"><svg xmlns="http://www.w3.org/2000/svg"
                                                            width="24" height="24" viewBox="0 0 24 24"
                                                            fill="none">
                                                            <path
                                                                d="M9.89557 13.4982L7.79487 11.2651C7.26967 10.7068 6.38251 10.7068 5.85731 11.2651C5.37559 11.7772 5.37559 12.5757 5.85731 13.0878L9.74989 17.2257C10.1448 17.6455 10.8118 17.6455 11.2066 17.2257L18.1427 9.85252C18.6244 9.34044 18.6244 8.54191 18.1427 8.02984C17.6175 7.47154 16.7303 7.47154 16.2051 8.02984L11.061 13.4982C10.7451 13.834 10.2115 13.834 9.89557 13.4982Z"
                                                                fill="black" />
                                                        </svg></span>
                                                    Buka</a>
                                            @endif
                                            <a href="{{ route('pertanyaan.daftar', [$kelas->kode, $kategori->kode, $tugas->kode]) }}"
                                                class="btn btn-sm btn-{{ \App\Models\Tugas::TIPE_OPTION[$tugas->tipe]['warna'] }}"><i
                                                    class="bi bi-plus fs-4 me-2"></i> Buat
                                                {{ Str::ucfirst($tugas->tipe) }}</a>
                                        @endif
                                        @if (auth()->user()->isAdmin() && $tugas->tipe == 'input')
                                            <a href="{{ route('tugas.proses', $tugas->kode) }}"
                                                class="btn btn-sm btn-success"><i class="bi bi-plus fs-4 me-2"></i>
                                                Input Nilai</a>
                                        @endif
                                        <a href="#" class="btn btn-sm btn-light btn-active-light-primary"
                                            data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Aksi
                                            <!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
                                            <span class="svg-icon svg-icon-5 m-0">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                    viewBox="0 0 24 24" fill="none">
                                                    <path
                                                        d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                                                        fill="black" />
                                                </svg>
                                            </span>
                                            <!--end::Svg Icon-->
                                        </a>
                                        <!--begin::Menu-->
                                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4"
                                            data-kt-menu="true">
                                            <div class="menu-item px-3">
                                                <a href="{{ route('tugas.detail', [$tugas->kode]) }}"
                                                    class="menu-link px-3">Detail</a>
                                            </div>
                                            <!--begin::Menu item-->
                                            <div class="menu-item px-3">
                                                <a href="{{ route('tugas.edit', [$kelas->kode, $tugas->kode]) }}"
                                                    class="menu-link px-3">Edit</a>
                                            </div>
                                            <!--end::Menu item-->
                                            <!--begin::Menu item-->
                                            <div class="menu-item px-3">
                                                <a wire:click.prevent="deleteTugas({{ $tugas->id }})"
                                                    onclick="confirm('Yakin akan menghapus?') || event.stopImmediatePropagation()"
                                                    class="menu-link px-3 text-danger">Hapus</a>
                                            </div>
                                            <!--end::Menu item-->
                                        </div>
                                        <!--end::Menu-->
                                    </td>
                                    <!--end::Action-->
                                </tr>
                            @endforeach
                            <!--end::Table row-->
                        </tbody>
                        <!--end::Table body-->
                    </table>
                    <!--end::Table-->
                </div>
                <!--end::Card body-->
            </div>
            <!--end::Kategori-->
        @endforeach


    </div>
    <!--end::Content-->
</div>
