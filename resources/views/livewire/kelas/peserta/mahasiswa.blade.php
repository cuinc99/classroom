<div x-data="{ buka: false }">
    <!--begin::mahasiswa-->
    <div class="card card-flush mb-10">
        <!--begin::Card header-->
        <div class="card-header align-items-center py-5 gap-2 gap-md-5">
            <!--begin::Card title-->
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label fw-bolder fs-3 mb-1">Mahasiswa</span>
            </h3>
            <!--begin::Card toolbar-->
            @if (auth()->user()->isAdmin())
            <div class="card-toolbar flex-row-fluid justify-content-end gap-5">
                <!--begin::Add product-->
                <button class="btn btn-primary rounded-50" x-on:click="buka = ! buka">
                    <span class="svg-icon svg-icon-2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                            fill="none">
                            <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black" />
                            <rect x="10.8891" y="17.8033" width="12" height="2" rx="1"
                                transform="rotate(-90 10.8891 17.8033)" fill="black" />
                            <rect x="6.01041" y="10.9247" width="12" height="2" rx="1" fill="black" />
                        </svg>
                    </span>
                    Tambah</button>
                <!--end::Add product-->
            </div>
            @endif
            <!--end::Card toolbar-->
        </div>
        <!--end::Card header-->
        <!--begin::Card body-->
        <div class="card-body pt-0">
            <!--begin::Table-->
            <table class="table align-middle table-row-dashed fs-6 gy-5">
                <!--begin::Table head-->
                <thead>
                    <!--begin::Table row-->
                    <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                        <th class="min-w-175px">Mahasiswa</th>
                        <th class="text-end min-w-70px">Kelompok</th>
                        @if (auth()->user()->isAdmin())
                        <th class="text-end min-w-100px"></th>
                        @endif
                    </tr>
                    <!--end::Table row-->
                </thead>
                <!--end::Table head-->
                <!--begin::Table body-->
                <tbody class="fw-bold text-gray-600">
                    <tr class="" x-bind:class="! buka ? 'd-none' : ''" x-cloak>
                        <form wire:submit.prevent='saveMahasiswa'>
                            <td>
                                <select class="form-select form-select-solid" wire:model='mahasiswa.kelompok_id'
                                    required>
                                    <option>Pilih Kelompk</option>
                                    @foreach ($kelompokItems as $kelompok)
                                        <option value="{{ $kelompok->id }}">{{ $kelompok->nama ?? '-' }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <select id="usersMahasiswa" class="form-select form-select-solid"
                                    wire:model='mahasiswa.user_ids' data-control="select2"
                                    data-placeholder="Pilih mahasiswa" data-allow-clear="true" multiple="multiple"
                                    required>
                                    <option></option>
                                    @foreach ($userItems as $user)
                                        <option value="{{ $user->id }}">({{ $user->username }})
                                            {{ $user->name ?? '-' }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td class="text-end">
                                <button type="button" class="btn btn-light" data-bs-dismiss="modal"
                                    x-on:click="buka = ! buka">Batal</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </td>
                        </form>
                    </tr>
                    <!--begin::Table row-->
                    @foreach ($mahasiswaItems as $mahasiswa)
                        <tr>
                            <!--begin::Customer-->
                            <td>
                                <div class="d-flex align-items-center">
                                    <!--begin:: Avatar -->
                                    <div class="symbol symbol-circle symbol-50px overflow-hidden me-3">
                                        <x-smart-image src="{{ public_path('/img/user.png') }}" height="100px" />
                                    </div>
                                    <!--end::Avatar-->
                                    <div class="ms-5">
                                        <a href="#"
                                            class="text-gray-800 fw-bolder text-hover-primary mb-1 fs-6 text-start pe-0">{{ $mahasiswa->user->name ?? '' }}</a>
                                            <span class="text-muted fw-bold d-block fs-7">NIM :
                                                {{ $mahasiswa->user->username ?? '-' }}</span>
                                    </div>
                                </div>
                            </td>
                            <!--end::Customer-->
                            <td class="text-end">
                                <span class="fw-bolder">{{ $mahasiswa->kelompok->nama ?? '-' }}</span>
                            </td>
                            <!--begin::Action-->
                            @if (auth()->user()->isAdmin())
                            <td class="text-end">
                                <a href="#" class="btn btn-sm btn-light btn-active-light-primary"
                                    data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Aksi
                                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
                                    <span class="svg-icon svg-icon-5 m-0">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none">
                                            <path
                                                d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                                                fill="black" />
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->
                                </a>
                                <!--begin::Menu-->
                                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4"
                                    data-kt-menu="true">
                                    <!--begin::Menu item-->
                                    {{-- <div class="menu-item px-3">
                                        <a href="#" class="menu-link px-3">Edit</a>
                                    </div> --}}
                                    <!--end::Menu item-->
                                    <!--begin::Menu item-->
                                    <div class="menu-item px-3">
                                        <a wire:click.prevent="deleteMahasiswa({{ $mahasiswa->id }})"
                                            onclick="confirm('Yakin akan menghapus?') || event.stopImmediatePropagation()"
                                            class="menu-link px-3">Hapus</a>
                                    </div>
                                    <!--end::Menu item-->
                                </div>
                                <!--end::Menu-->
                            </td>
                            @endif
                            <!--end::Action-->
                        </tr>
                    @endforeach
                    <!--end::Table row-->
                </tbody>
                <!--end::Table body-->
            </table>
            <!--end::Table-->
        </div>
        <!--end::Card body-->
    </div>
</div>

@push('scripts')
    <script>
        document.addEventListener("livewire:load", () => {
            let elm = $('#usersMahasiswa')
            initSelectM()
            Livewire.hook('message.processed', (message, component) => {
                initSelectM()
            })
            Livewire.on('setUsersMahasiswaSelect', values => {
                elm.val(values).trigger('change.select2')
            })
            elm.on('change', function(e) {
                @this.set('mahasiswa.user_ids', elm.select2("val"))
            })

            function initSelectM() {
                elm.select2({
                    placeholder: '{{ __('Select your option') }}',
                    allowClear: !elm.attr('required'),
                })
            }
        })
    </script>
@endpush
