<div>
    <!--begin::Toolbar-->
    <div class="toolbar py-5 py-lg-5" id="kt_toolbar">
        <!--begin::Container-->
        <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column me-3">
                <!--begin::Title-->
                <h1 class="d-flex text-dark fw-bolder my-1 fs-3">Folder</h1>
                <!--end::Title-->
            </div>
            <!--end::Page title-->
            <!--begin::Actions-->
            <div class="d-flex align-items-center py-2 py-md-1">
                @if ($currentFolderId)
                    <a href="{{ $currentFolder->parent_folder ? route('folder.show', $currentFolder->parent_folder?->kode) : route('home') }}"
                        class="btn btn-secondary fw-bolder me-2">
                        Kembali
                        <span class="svg-icon svg-icon-2hx svg-icon-gray-500 me-1">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                    d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3" />
                            </svg>

                        </span>
                    </a>
                @endif
                <!--begin::Button-->
                @if (auth()->user()->isAdmin())
                    <button class="btn btn-primary fw-bolder btn-hover-scale" data-bs-toggle="modal" wire:click="create"
                        data-bs-target="#modal_buat_folder" id="kt_toolbar_primary_button">
                        <!--begin::Svg Icon | path: assets/media/icons/duotune/arrows/arr087.svg-->
                        <span class="svg-icon svg-icon-muted svg-icon-2hx"><svg xmlns="http://www.w3.org/2000/svg"
                                width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <rect opacity="0.5" x="11" y="18" width="12" height="2"
                                    rx="1" transform="rotate(-90 11 18)" fill="black" />
                                <rect x="6" y="11" width="12" height="2" rx="1"
                                    fill="black" />
                            </svg></span>
                        <!--end::Svg Icon-->
                        Buat Folder
                    </button>
                @endif
                <!--end::Button-->
                <div class="modal fade" tabindex="-1" id="modal_buat_folder" wire:ignore.self>
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">{{ $folderId ? 'Edit' : 'Buat' }} Folder</h5>

                                <!--begin::Close-->
                                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                                    aria-label="Close">
                                    <span class="svg-icon svg-icon-2x"></span>
                                </div>
                                <!--end::Close-->
                            </div>

                            <form wire:submit.prevent='save'>
                                <div class="modal-body">
                                    <!--begin::Input group-->
                                    <div class="input-group input-group-solid mb-5">
                                        <span class="input-group-text" id="basic-addon2">
                                            <span class="svg-icon svg-icon-1">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                    viewBox="0 0 24 24" fill="none">
                                                    <path opacity="0.3"
                                                        d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                                                        fill="black" />
                                                    <path
                                                        d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                                                        fill="black" />
                                                </svg>
                                            </span>
                                        </span>
                                        <input type="text" wire:model='folder.nama'
                                            class="form-control form-control-solid" placeholder="Nama Folder"
                                            required />
                                    </div>
                                    <!--end::Input group-->
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-light" data-bs-dismiss="modal"
                                        wire:click='close'>Tutup</button>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Actions-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Container-->
    <div id="kt_content_container" class="flex-column-fluid align-items-start container-xxl" style="margin-top: 20px">
        <!--begin::Post-->

        <div class="row mb-10">
            <div class="col-12">
                <form action="{{ url()->current() }}" method="get">
                    <div class="input-group">
                        <input type="text" name="q" value="{{ $search }}"
                            class="form-control form-control-solid" placeholder="Cari disini..." />
                        <button type="submit" class="btn btn-icon btn-primary">
                            <!--begin::Svg Icon | path: icons/duotune/general/gen035.svg-->
                            <span class="svg-icon svg-icon-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                    viewBox="0 0 24 24" fill="none">
                                    <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546"
                                        height="2" rx="1" transform="rotate(45 17.0365 15.1223)"
                                        fill="black" />
                                    <path
                                        d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                        fill="black" />
                                </svg>
                            </span>
                            <!--end::Svg Icon-->
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <div class="row" data-masonry='{"percentPosition": true }'>

            @forelse ($folders as $item)
                <div class="col-12 col-md-6 mb-5">
                    <div class="card rounded-20 shadow"
                        style="background: #790252; border: 5px white solid; border-style: double;">
                        <!--begin::Body-->
                        <div style="top: 0; right: 0; position: absolute" data-bs-toggle="tooltip"
                            data-bs-placement="left" title="Edit">
                            <button class="btn btn-icon btn-color-gray-400 btn-active-color-primary"
                                wire:click="edit({{ $item->id }})" data-bs-toggle="modal"
                                data-bs-target="#modal_buat_folder">
                                <!--begin::Svg Icon | path: icons/duotune/general/gen023.svg-->
                                <span class="svg-icon svg-icon-2x">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                        fill="currentColor" class="bi bi-wrench-adjustable-circle-fill"
                                        viewBox="0 0 16 16">
                                        <path
                                            d="M6.705 8.139a.25.25 0 0 0-.288-.376l-1.5.5.159.474.808-.27-.595.894a.25.25 0 0 0 .287.376l.808-.27-.595.894a.25.25 0 0 0 .287.376l1.5-.5-.159-.474-.808.27.596-.894a.25.25 0 0 0-.288-.376l-.808.27.596-.894Z" />
                                        <path
                                            d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16Zm-6.202-4.751 1.988-1.657a4.5 4.5 0 0 1 7.537-4.623L7.497 6.5l1 2.5 1.333 3.11c-.56.251-1.18.39-1.833.39a4.49 4.49 0 0 1-1.592-.29L4.747 14.2a7.031 7.031 0 0 1-2.949-2.951ZM12.496 8a4.491 4.491 0 0 1-1.703 3.526L9.497 8.5l2.959-1.11c.027.2.04.403.04.61Z" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                            </button>
                        </div>
                        @if ($item->kelas()->count() == 0 && $item->child_folder()->count() == 0)
                            <div style="bottom: 0; right: 0; position: absolute">
                                <button class="btn btn-icon btn-color-danger btn-active-color-danger"
                                    wire:click.prevent="delete({{ $item->id }})" data-bs-toggle="tooltip"
                                    data-bs-placement="left" title="Hapus"
                                    onclick="confirm('Yakin akan menghapus?') || event.stopImmediatePropagation()">
                                    <!--begin::Svg Icon | path: icons/duotune/general/gen023.svg-->
                                    <span class="svg-icon svg-icon-2x">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                            fill="currentColor" class="bi bi-x-circle-fill" viewBox="0 0 16 16">
                                            <path
                                                d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293 5.354 4.646z" />
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->
                                </button>
                            </div>
                        @endif
                        <div class="card-body d-flex ps-xl-15">
                            <!--begin::Action-->
                            <div class="m-0">
                                <!--begin::Title-->
                                <div class="position-relative fs-2x z-index-2 fw-bolder text-white mb-7">
                                    {{ $item->nama ?? '' }}
                                </div>
                                <!--end::Title-->
                                <!--begin::Action-->
                                <a href='{{ route('folder.show', $item->kode) }}'
                                    class="btn btn-danger fw-bold me-2">Buka Folder</a>
                                <!--begin::Action-->
                            </div>
                            <!--begin::Action-->
                            <!--begin::Illustration-->
                            <img src="/assets/media/illustrations/unitedpalms-1/13.png"
                                class="position-absolute me-10 bottom-0 end-0 h-200px" alt="" />
                            <!--end::Illustration-->
                        </div>
                        <!--end::Body-->
                    </div>
                </div>
            @empty
                <div class="col-12 col-md-6 mb-5">
                    <span class="text-danger">{{ $search ? 'Folder tidak ditemukan' : 'Belum ada folder' }}</span>
                </div>
            @endforelse
        </div>
        @if ($folders->count() > 0)
            <div class="mt-10 mb-10">
                {{ $folders->links() }}
            </div>
        @endif

        <!--end::Post-->
    </div>
    <!--end::Container-->
</div>
