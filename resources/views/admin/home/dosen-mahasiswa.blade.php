<div id="kt_content_container" class="flex-column-fluid align-items-start container-xxl">
    <!--begin::Post-->

    <div class="row mb-5 mt-5">
        <div class="col-12">
            <form action="{{ url()->current() }}" method="get">
                <div class="input-group">
                    <input type="text" name="q" value="{{ $search }}"
                        class="form-control form-control-solid" placeholder="Cari kelas disini..." />
                    <button type="submit" class="btn btn-icon btn-primary">
                        <!--begin::Svg Icon | path: icons/duotune/general/gen035.svg-->
                        <span class="svg-icon svg-icon-2">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none">
                                <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2"
                                    rx="1" transform="rotate(45 17.0365 15.1223)" fill="black" />
                                <path
                                    d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                    fill="black" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                    </button>
                </div>
            </form>
        </div>
    </div>

    <div class="row">

        @forelse ($kelasItems as $item)
            <div class="col-12 col-md-6 mb-5">
                <div class="card rounded-20 shadow"
                    style="background: {{ $item->active ? '#1c285e' : '#4C6793' }}; border: 5px white solid; border-style: double;">
                    <!--begin::Body-->
                    <div class="card-body d-flex ps-xl-15">
                        <!--begin::Action-->
                        <div class="m-0">
                            <!--begin::Title-->
                            <small class="text-white">Blok : {{ $item->blok->nama }}</small> <br>
                            <small class="text-white">Semester : {{ $item->blok->semester }}</small>
                            <div class="position-relative fs-2x z-index-2 fw-bolder text-white mb-7">
                                {{ $item->nama ?? '' }}
                            </div>
                            <!--end::Title-->
                            <!--begin::Action-->
                            <a href='{{ route('kelas.show', $item->kode) }}' class="btn btn-danger fw-bold me-2">Lihat
                                Kelas</a>
                            @if (!$item->active)
                                <button class="btn btn-warning fw-bold">Tidak Aktif</button>
                            @endif
                            <!--begin::Action-->
                        </div>
                        <!--begin::Action-->
                        <!--begin::Illustration-->
                        <img src="/assets/media/illustrations/sigma-1/4.png"
                            class="position-absolute me-10 bottom-0 end-0 h-200px" alt="" />
                        <!--end::Illustration-->
                    </div>
                    <!--end::Body-->
                </div>
            </div>
        @empty
            <div class="col-12 col-md-6 mb-5">
                <span class="text-danger">{{ $search ? 'Kelas tidak ditemukan' : 'Belum ada kelas' }}</span>
            </div>
        @endforelse
    </div>
    @if ($kelasItems->count() > 0)
        <div class="mt-10 mb-10">
            {{ $kelasItems->links() }}
        </div>
    @endif

    <!--end::Post-->
</div>
