<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
    <div class="content flex-row-fluid" id="kt_content">
        <div class="row">
            @foreach ($bloks as $key => $blok)
                <div class="col-12 mb-5">
                    <div class="card card-flush h-xl-100">
                        <!--begin::Header-->
                        <div class="card-header pt-7 mb-3">
                            <!--begin::Title-->
                            <h3 class="card-title align-items-start flex-column">
                                <span class="text-gray-400 mt-1 fw-bold fs-6">Blok</span>
                                <span class="card-label fw-bolder text-gray-800">Semester {{ $key + 1 }}</span>
                            </h3>
                            <!--end::Title-->
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div class="card-body pt-4">
                            <!--begin::Item-->
                            <div class="row">
                                @foreach ($blok as $item)
                                    <div class="col-12 col-md-6 mb-5">
                                        <div class="card rounded-20 shadow"
                                            style="background: #1c285e; border: 5px white solid; border-style: double;">
                                            <!--begin::Body-->
                                            <div class="card-body d-flex ps-xl-15">
                                                <!--begin::Action-->
                                                <div class="m-0">
                                                    <!--begin::Title-->
                                                    <div class="position-relative fs-2 z-index-2 fw-bolder text-white mb-7">
                                                        {{ $item->nama ?? '' }}
                                                    </div>
                                                    <!--end::Title-->
                                                    <!--begin::Action-->
                                                    <a href='{{ route('blok.index', Str::lower($item->kode)) }}'
                                                        class="btn btn-danger fw-bold me-2">Isi Soal</a>
                                                    <button href='#'
                                                        class="btn btn-secondary fw-bold me-2">Total Soal
                                                        {{ $item->pertanyaans()->count() }}</button>
                                                    <!--begin::Action-->
                                                </div>
                                                <!--begin::Action-->
                                                <!--begin::Illustration-->
                                                <img src="/assets/media/illustrations/dozzy-1/4.png"
                                                    class="position-absolute me-10 bottom-0 end-0 h-200px" alt="" />
                                                <!--end::Illustration-->
                                            </div>
                                            <!--end::Body-->
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    {{-- <h3 class="fs-2hx line-height-lg mb-5">
                        <span class="fw-bold">Semester </span>
                        <span class="fw-bolder">{{ $key + 1 }}</span>
                    </h3> --}}

                </div>
            @endforeach
        </div>
    </div>
    <!--end::Post-->
</div>
