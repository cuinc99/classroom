<div id="kt_content_container" class="flex-column-fluid align-items-start container-xxl">
    <div class="d-flex flex-wrap flex-stack my-5">
        <!--begin::Heading-->
        <span class="d-inline-block position-relative">
            <!--begin::Label-->
            <span class="d-inline-block mb-2 fs-2x fw-bolder text-dark">
                Pengumuman
            </span>
            <!--end::Label-->

            <!--begin::Line-->
            <span
                class="d-inline-block position-absolute h-8px bottom-0 end-0 start-0 bg-primary translate rounded"></span>
            <!--end::Line-->
        </span>
    </div>
    @forelse ($pengumumanItems as $pengumuman)
        <div class="card bgi-no-repeat card-xl-stretch mb-xl-8"
            style="background-position: right top; background-size: 30% auto; background-image: url(/assets/media/svg/shapes/abstract-2.svg">
            <!--begin::Body-->
            <div class="card-body">
                <a href="#"
                    class="card-title fw-bolder text-dark text-hover-primary fs-4">{!! $pengumuman->isi ?? '-' !!}</a>
                <div>
                    <span
                        class="fw-bolder text-muted">{{ Date::parse($pengumuman->create_at)->diffForHumans() }}</span>
                    <span class="text-muted">dari kelas</span> <span
                        class="fw-bolder text-info">{{ $pengumuman->kelas->nama }}</span>
                        @if ($pengumuman->lampiran)
                            <a href="{{ url('storage/' . $pengumuman->lampiran ?? '') }}" class="btn btn-sm btn-primary mt-5">Unduh lampiran</a>
                            @endif
                </div>
                {{-- <p class="text-dark-75 fw-bold fs-5 m-0">Great blog posts don’t just happen Even the best bloggers need it</p> --}}
            </div>
            <!--end::Body-->
        </div>
    @empty
        Belum ada pengumuman.
    @endforelse
    <div class="d-flex flex-wrap flex-stack my-5">
        <!--begin::Heading-->
        <span class="d-inline-block position-relative">
            <!--begin::Label-->
            <span class="d-inline-block mb-2 fs-2x fw-bolder text-dark">
                Tugas
            </span>
            <!--end::Label-->

            <!--begin::Line-->
            <span
                class="d-inline-block position-absolute h-8px bottom-0 end-0 start-0 bg-primary translate rounded"></span>
            <!--end::Line-->
        </span>
    </div>
    <!--begin::Row-->
    <div class="row g-6 g-xl-9">

        @forelse ($tugasItems as $tugas)
            <div class="col-md-6 col-xl-6">
                <!--begin::Card-->
                <a href="{{ route('tugas.proses', $tugas->kode) }}"
                    class="card border-hover-{{ \App\Models\Tugas::TIPE_OPTION[$tugas->tipe]['warna'] }}">
                    <!--begin::Card header-->
                    <div class="card-header border-0 pt-9">
                        <!--begin::Card Title-->
                        <div class="card-title m-0">
                            <!--begin::Avatar-->
                            <div class="symbol symbol-50px w-50px bg-light">
                                <img src="/assets/media/svg/brand-logos/plurk.svg" alt="image" class="p-3" />
                            </div>
                            <!--end::Avatar-->
                        </div>
                        <!--end::Car Title-->
                        <!--begin::Card toolbar-->
                        <div class="card-toolbar">
                            <span
                                class="badge badge-light-{{ \App\Models\Tugas::TIPE_OPTION[$tugas->tipe]['warna'] }} fw-bolder me-auto px-4 py-3">{{ Str::ucfirst($tugas->tipe ?? '-') }}</span>
                        </div>
                        <!--end::Card toolbar-->
                    </div>
                    <!--end:: Card header-->
                    <!--begin:: Card body-->
                    <div class="card-body p-9">
                        <!--begin::Name-->
                        <div class="fs-3 fw-bolder text-dark">{{ $tugas->judul ?? '-' }}</div>
                        <!--end::Name-->
                        <!--begin::Description-->
                        <p class="text-gray-400 fw-bold fs-5 mt-1 mb-7">#{{ $tugas->kode ?? '-' }}</p>
                        <!--end::Description-->
                        <!--begin::Info-->
                        <div class="d-flex flex-wrap mb-5">
                            <!--begin::Due-->
                            <div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-7 mb-3">
                                <div class="fs-6 text-gray-800 fw-bolder">Kelas</div>
                                <div class="fw-bold text-gray-400">{{ $tugas->kelas->nama }}</div>
                            </div>
                            <!--end::Due-->
                            <!--begin::Budget-->
                            <div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-7 mb-3">
                                <div class="fs-6 text-gray-800 fw-bolder">Kategori</div>
                                <div class="fw-bold text-gray-400">{{ $tugas->kategori->nama }}</div>
                            </div>
                            <!--end::Budget-->
                            @if (auth()->user()->isMahasiswa())
                                @if ($tugas->mahasiswa_tugas->tanggal_kumpul)
                                    <!--begin::Budget-->
                                    <div class="border border-primary bg-light-primary border-dashed rounded min-w-125px py-3 px-4 mb-3">
                                        <div class="fs-6 text-gray-800 fw-bolder">Poin Anda</div>
                                        <div class="fw-bold text-gray-400">{{ $tugas->mahasiswa_tugas->total_poin }}
                                        </div>
                                    </div>
                                    <!--end::Budget-->
                                @endif
                            @endif
                        </div>
                        <!--end::Info-->
                        <!--begin::Progress-->
                        <div class="h-4px w-100 bg-light mb-5" data-bs-toggle="tooltip"
                            title="This project 50% completed">
                            <div class="bg-{{ \App\Models\Tugas::TIPE_OPTION[$tugas->tipe]['warna'] }} rounded h-4px"
                                role="progressbar" style="width: 100%" aria-valuenow="50" aria-valuemin="0"
                                aria-valuemax="100"></div>
                        </div>
                        <!--end::Progress-->
                        <div>
                            <p class="text-gray-400 fw-bold">Status:
                                @if (!$tugas->status_ujian && $tugas->mulai_pada)
                                    Ditutup
                                @elseif ($tugas->status_ujian && $tugas->mulai_pada)
                                    Dibuka
                                    {{ $tugas->mulai_pada ? Date::parse($tugas->batas_akhir)->format('l, d M Y') : '-' }}
                                @else
                                    Belum Dibuka
                                @endif
                            </p>
                        </div>
                    </div>
                    <!--end:: Card body-->
                </a>
                <!--end::Card-->
            </div>
        @empty
            <div class="col-12">
                Belum ada tugas.
            </div>
        @endforelse

    </div>
</div>
