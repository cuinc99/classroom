<table>
    <thead>
        <tr>
            <th width="10" style="text-align: center;">No</th>
            <th style="text-align: center;">NIM</th>
            <th style="text-align: center;">Nama Mahasiswa</th>
            @foreach ($kelas->kategoris as $kategori)
                @if ($kategori->tugas->count() > 1)
                    @foreach ($kategori->tugasByOldest as $tugas)
                        <th style="text-align: center;">
                            {{ $tugas->judul ?? '-' }}
                            @if ($kategori->is_persentase)
                                ({{ $tugas->persentase_nilai ?? '-' }}%)
                            @endif
                        </th>
                    @endforeach
                @endif
                <th style="text-align: center;">{{ $kategori->nama ?? '-' }}</th>
                <th style="text-align: center;">{{ $kategori->persentase_nilai ?? '-' }}%</th>
            @endforeach
            <th style="text-align: center;">Total</th>
            <th style="text-align: center;">Huruf</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($mahasiswas as $mahasiswa)
            <tr>
                <td style="text-align: center;">{{ $loop->iteration }}</td>
                <td>{{ $mahasiswa->user->username ?? '-' }}</td>
                <td align="left">{{ $mahasiswa->user->name ?? '-' }}</td>
                @php
                    $total = 0;
                @endphp
                @foreach ($kelas->kategoris as $kategori)
                    @php
                        $sum_poin = 0;
                    @endphp
                    @if ($kategori->tugas->count() > 1)
                        @foreach ($kategori->tugasByOldest as $tugas)
                            @php
                                $mahasiswaTugas = \App\Models\MahasiswaTugas::where('mahasiswa_id', $mahasiswa->id)
                                    ->where('tugas_id', $tugas->id)
                                    ->first();
                                $total_poin = $mahasiswaTugas ? $mahasiswaTugas->total_poin : 0;

                                if ($kategori->is_persentase) {
                                    $poinAsli = $total_poin;
                                    $total_poin = $tugas->persentase_nilai / 100 * $total_poin;
                                }

                                $sum_poin += $total_poin;
                            @endphp
                            <td style="text-align: center;">
                                {{ number_format($total_poin, 2) - 0 }}
                                @if ($kategori->is_persentase)
                                    <small>({{ $poinAsli }})</small>
                                @endif
                            </td>
                        @endforeach
                    @endif
                    @php
                        $tugasIds = $kategori->tugas->pluck('id');
                        if (!$kategori->is_persentase) {
                            $sum_poin = \App\Models\MahasiswaTugas::where('mahasiswa_id', $mahasiswa->id)
                            ->whereIn('tugas_id', $tugasIds)
                            ->sum('total_poin');
                        }
                        $nilai_akhir = $tugasIds->count() != 0 ? $sum_poin / $tugasIds->count() : 0;
                        $persentase_nilai_akhir = ($nilai_akhir * $kategori->persentase_nilai) / 100;
                        $total += $persentase_nilai_akhir;
                    @endphp
                    <td style="text-align: center;">{{ number_format($nilai_akhir, 2) - 0 }}</td>
                    <td style="text-align: center;">{{ number_format($persentase_nilai_akhir, 2) - 0 }}</td>
                @endforeach
                {{-- <td style="text-align: center;">{{ $totalBulat = round($total) - 0 }}</td> --}}
                <td style="text-align: center;">{{ number_format($total, 2) }}</td>
                <td style="text-align: center;">{{ \App\Services\NilaiService::getNilaiHuruf($total) }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
