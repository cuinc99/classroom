<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Absensi</title>
    <style>
        table,
        th,
        td {
            border: 1px double black;
            border-collapse: collapse;
            padding: 5px;
        }

        .page-break {
            page-break-after: always;
        }
    </style>
</head>

<body>
    <div style="text-align: center;">
        <img src="{{ public_path().'/img/cop.png' }}" style="height: 90px; width: auto;" alt="">
    </div>
    <br />
    <br />

    <div style="text-align: center;">
        <b>PRESENTASE ABSENSI KELAS {{ $kelas?->nama }}</b>
        <br />
        <b>FAKULTAS KEDOKTERAN UNIVERSITAS ISLAM AL-AZHAR MATARAM</b>
    </div>
    <br />

    <table width="100%" style="font-size: 10pt;">
        <tr>
            <th width="5">No</th>
            <th>NIM</th>
            <th>Nama Mahasiswa</th>
            <th>Total</th>
            <th>Kehadiran (%)</th>
            <th>Izin (%)</th>
            <th>Tidak Hadir (%)</th>
        </tr>
        @foreach (\App\Models\Mahasiswa::with('user')->whereKelasId($kelas->id)->get() as $mahasiswa)
        <tr>
            <td style="text-align: center;">{{ $loop->iteration }}</td>
            <td style="text-align: center;">{{ $mahasiswa?->user?->username }}</td>
            <td>{{ $mahasiswa?->user?->name }}</td>

            @php
                $absensi = \App\Models\Absensi::query()
                    ->where('mahasiswa_id', $mahasiswa?->id)
                    ->get();
                $totalAbsen = $mahasiswa->berita_acaras()->count();
            @endphp

            <td style="text-align: center;">{{ $totalAbsen }}</td>
            <td style="text-align: center;">
                <b>{{ $hadir = $absensi->where('status', 'Hadir')->count() }}</b>/{{ $totalAbsen }} ({{ $totalAbsen ? round($hadir / $totalAbsen * 100) : 0 }}%)
            </td>
            <td style="text-align: center;">
                <b>{{ $izin = $absensi->where('status', 'izin')->count() }}</b>/{{ $totalAbsen }} ({{ $totalAbsen ? round($izin / $totalAbsen * 100) : 0 }}%)
            </td>
            <td style="text-align: center;">
                <b>{{ $tidakHadir = $totalAbsen - $absensi->count() }}</b>/{{ $totalAbsen }} ({{ $totalAbsen ? round($tidakHadir / $totalAbsen * 100) : 0 }}%)
            </td>
        </tr>
        @endforeach
    </table>
    <br />
</body>

</html>
