<table>
    <thead>
        <tr>
            <th width="10" style="text-align: center;">No</th>
            <th style="text-align: center;">ID</th>
            <th style="text-align: center;">NIM</th>
            <th style="text-align: center;">Nama Mahasiswa</th>
            <th style="text-align: center;">Total Nilai</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($mahasiswas as $mahasiswa)
            <tr>
                <td style="text-align: center;">{{ $loop->iteration }}</td>
                <td>{{ $mahasiswa->id ?? '-' }}</td>
                <td>{{ $mahasiswa->user->username ?? '-' }}</td>
                <td align="left">{{ $mahasiswa->user->name ?? '-' }}</td>
                <td style="text-align: center;"></td>
            </tr>
        @endforeach
    </tbody>
</table>
