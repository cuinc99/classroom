<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Berita Acara</title>
    <style>
        table,
        th,
        td {
            border: 1px double black;
            border-collapse: collapse;
            padding: 5px;
        }

        .page-break {
            page-break-after: always;
        }
    </style>
</head>

<body>
    <div style="text-align: center;">
        <img src="{{ public_path().'/img/cop.png' }}" style="height: 90px; width: auto;" alt="">
    </div>
    <br />
    <br />

    <div style="text-align: center;">
        <b>BERITA ACARA PELAKSANAAN PEMBELAJARAN</b>
        <br />
        <b>FAKULTAS KEDOKTERAN UNIVERSITAS ISLAM AL-AZHAR MATARAM</b>
    </div>
    <br />

    <div style="text-align: left;">
        Telah diselenggarakan Metode Pembelajaran pada Program Pendidikan Dokter Fakultas Kedokteran Universitas Islam
        Al-Azhar dengan rincian:
    </div>
    <br />

    <table width="100%" style="font-weight: bold; border: none;">
        <tr>
            <td width="35%" style="border: none;">Semester</td>
            <td width="5" style="border: none;">:</td>
            <td style="border: none;">{{ $beritaAcara?->semester }}</td>
        </tr>
        <tr>
            <td width="35%" style="border: none;">Blok</td>
            <td width="5" style="border: none;">:</td>
            <td style="border: none;">{{ $beritaAcara?->kelas?->blok?->nama }}</td>
        </tr>
        <tr>
            <td width="35%" style="border: none;">Metode Pembelajaran</td>
            <td width="5" style="border: none;">:</td>
            <td style="border: none;">{{ $beritaAcara?->metode }}</td>
        </tr>
        <tr>
            <td width="35%" style="border: none;">Judul Topik</td>
            <td width="5" style="border: none;">:</td>
            <td style="border: none;">{{ $beritaAcara?->judul }}</td>
        </tr>
        <tr>
            <td width="35%" style="border: none;">Hari, Tanggal</td>
            <td width="5" style="border: none;">:</td>
            <td style="border: none;">{{ Date::parse($beritaAcara?->tanggal)?->format('l, d M Y') }}</td>
        </tr>
        <tr>
            <td width="35%" style="border: none;">Waktu</td>
            <td width="5" style="border: none;">:</td>
            <td style="border: none;">{{ $beritaAcara?->tanggal?->format('H:i') }}</td>
        </tr>
        <tr>
            <td width="35%" style="border: none;">Seharussnya Hadir</td>
            <td width="5" style="border: none;">:</td>
            <td style="border: none;">{{ $beritaAcara?->kelompok->mahasiswas->count() }}</td>
        </tr>
        <tr>
            <td width="35%" style="border: none;">Yang Hadir</td>
            <td width="5" style="border: none;">:</td>
            <td style="border: none;">{{ $beritaAcara?->absensis->where('status', 'Hadir')->count() }}</td>
        </tr>
        <tr>
            <td width="35%" style="border: none;">Dosen Pengampu</td>
            <td width="5" style="border: none;">:</td>
            <td style="border: none;">{{ $beritaAcara?->user?->name }}</td>
        </tr>
        <tr>
            <td width="35%" style="border: none;">Feedback Dosen Pengampu</td>
            <td width="5" style="border: none;">:</td>
            <td style="border: none;">{{ $beritaAcara?->feedbacks()->whereHas('user', fn($q) => $q->where('user_type', 'dosen'))->first()->feedback ?? '-' }}</td>
        </tr>
    </table>
    <br />

    @if ($beritaAcara?->feedbacks->count() > 0)
    <div style="text-align: left;">
        Berikut daftar feedback mahasiswa:
    </div>
    <br />

    <table width="100%">
        <tr>
            <th width="5">No</th>
            <th>Feedback</th>
        </tr>
        @foreach ($beritaAcara?->feedbacks()->whereHas('user', fn($q) => $q->where('user_type', 'mahasiswa'))->get() as $feedback)
        <tr>
            <td style="text-align: center;">{{ $loop->iteration }}</td>
            <td>{{ $feedback?->feedback }}</td>
        </tr>
        @endforeach
    </table>
    <br />
    @endif

    <div class="page-break"></div>

    <div style="text-align: center;">
        <img src="{{ public_path().'/img/cop.png' }}" style="height: 90px; width: auto;" alt="">
    </div>
    <br />
    <br />

    <div style="text-align: center;">
        <br />
        <b>DAFTAR HADIR MAHASISWA</b>
        <br />
        <b>{{ $beritaAcara?->semester }}</b>
    </div>
    <br />

    <table width="100%" style="font-weight: bold; border: none;">
        <tr>
            <td width="35%" style="border: none;">Blok</td>
            <td width="5" style="border: none;">:</td>
            <td style="border: none;">{{ $beritaAcara?->kelas?->blok?->nama }}</td>
        </tr>
        <tr>
            <td width="35%" style="border: none;">Kelas</td>
            <td width="5" style="border: none;">:</td>
            <td style="border: none;">{{ $beritaAcara?->kelas?->nama }}</td>
        </tr>
        <tr>
            <td width="35%" style="border: none;">Metode Pembelajaran</td>
            <td width="5" style="border: none;">:</td>
            <td style="border: none;">{{ $beritaAcara?->metode }}</td>
        </tr>
        <tr>
            <td width="35%" style="border: none;">Judul Topik</td>
            <td width="5" style="border: none;">:</td>
            <td style="border: none;">{{ $beritaAcara?->judul }}</td>
        </tr>
        <tr>
            <td width="35%" style="border: none;">Hari, Tanggal</td>
            <td width="5" style="border: none;">:</td>
            <td style="border: none;">{{ Date::parse($beritaAcara?->tanggal)?->format('l, d M Y, H:i') }}</td>
        </tr>
    </table>
    <br />

    <table width="100%">
        <tr>
            <th width="5">No</th>
            <th width="20%">NIM</th>
            <th width="30%">Nama Mahasiswa</th>
            <th width="20%">Status</th>
            <th>Ket</th>
        </tr>
        @foreach ($beritaAcara?->kelompok?->mahasiswas as $mahasiswa)
        <tr>
            <td style="text-align: center;">{{ $loop->iteration }}</td>
            <td style="text-align: center;">{{ $mahasiswa?->user?->username }}</td>
            <td>{{ $mahasiswa?->user?->name }}</td>

            @php
            $absensi = \App\Models\Absensi::query()
            ->where('berita_acara_id', $beritaAcara?->id)
            ->where('mahasiswa_id', $mahasiswa?->id)
            ->first();
            @endphp

            <td style="text-align: center;">
                @if ($absensi)
                {{ $absensi?->status }} - {{ $absensi?->created_at->format('H:i') }}
                @else
                <span style="color: red;">Belum Absen</span>
                @endif
            </td>
            <td>{{ $absensi?->keterangan }}</td>
        </tr>
        @endforeach
    </table>
    <br />
</body>

</html>
