@extends('layouts.admin')

@section('title', 'Detail Berita Acara ' . $beritaAcara->judul)

@section('content')
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
    <div class="content flex-row-fluid" id="kt_content">
        <div class="row">
            <div class="col-12">
                @if ($beritaAcara?->isBuka())
                <div class="card">
                    <div class="card-body">
                        <div id="reader" style="width: 100%"></div>
                    </div>
                </div>

                <div class="modal fade" tabindex="-1" id="modal_success" data-bs-backdrop="static"
                    data-bs-keyboard="false">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Anda Berhasil Absen!</h5>
                            </div>

                            <div class="modal-body text-center">
                                <img
                                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABmJLR0QA/wD/AP+gvaeTAAAHRklEQVR4nO2be4wV1RnAf9+Ze+feZdf17oslAhINsrs8kgoLi0tbXbAr+kcbam3T2FbRVmsMNS1p2qQxJk36IDVpSlObSBDbJq0mJqYxsS/lIS3ui0cTWxAUYQVkF3bZBe6y9zFz+k+xwD1z78zcuRsb9/fnOd/rfPOdOXPOzMA000wzzccYmSpHumdxM8pdDLoFceehJQXUcG6wE9QllIwj6hja2kvCflU+ffrfUxFXxRKgNYqBtjVo1oHuAlqNgkOHzAZidgZlHyZub6NrdLMITiXijDwBet/8JvLWBpD1wJySCl4JuBIrlidetZtY8hHpOvNO+VH+j8gSoHcvqcPOPYnwCFDtW9FPAi6jlMau7iNW+wXpOnkieJQGk+Ua0BrRva0PkMgdQvg2QQYfFNcVJi90kB46rl+v+2UUJsuqAN0zvxYV3wL6i6GNBKmAa7Gr38dJdco94ashdAXovoWLULEDZQ2+XLLpuTB8VO+q7w5rIlQCdP+CFeDuBG4K6zgynFyci+N/0jvr14dRD5wA3dfaiVY7gMYwDiuC6ygujm3V2xu+FlQ1UAJ038JFwCvAjKCOKo52hYmxbXp70z1B1HwnQO//RArcV4D6wMFNFdpRTI6/rP/aeINfFf8VkJvcykdhzpfCydroiR6/4r4SoHtbHwA+HzqoqSY7MVe/lvqFH9GSzwF695I6ErlDwMyyAzNRznNAMVTMITlrnqw5cbKoWElDdu5JKjX4SuLmLdzzL5USK1oBure1ATiGUBNZYNdSogLOZRRf7ZnN4bEYqYTLj289y53NaX+2ldLMaFhQbANVvAJEP1HRwZfA0bB251wODFtMZDWnLggP/2Mm4zmf927XFfKTzxYT8bSkNQp4MFDEEbNx/yxOnr+6SB1H87ehWv9Gcpc+pTWWV7d3Kgfa1oDM9e8pWnYMVfPS0WRBuyAsvP6Sf0NOPsaOhse9ur0ToFnn30u0XMgrvtnbBLqw75YGzcLrMsEM5jIPeXUVmUx6dTAv0fGVPbNJG8ZoxxV/6Cy6qplxsi1eXcYE6L5FswBPpUqy5Wgde4cKp6wI/KR9lOZkLrhRJ5vUuxraTF3mChB3cXAv5XM8HedHB1LGvjvm5vnSnLHwxrPOWlOz1xSY8qvvAvf9fTZ5p3Dip6qF55aHKP2rPSw3tZoT4DKvTG+B+e7+WZw6X9iulPCbztPEleGOGAStbzY1e1SADrDQXs1gWthzxuJ8gKm6a3gGL75buOQBPNyWZlkqwLLnhdbGB7qYUTjk09+WI3GeeTuOq6Euodm0NENHo1tUJ+0Ij/Y0gy68wrc0wlMLh8OEYsA1HuKUfSx+mcG0fDh4gHMZYUNfgjfPeD6EAXD/njlczBQO3o4rXgyz5AXEnADNxaCGTkyoDwd/mYwjPNFveybhX2OKgdPmvp8tH2VmIsSS54maMLaahcVwOyrO4pRDXaLwSnolYSIP39+XMNq6c16ee2eXseSZELlgajYnQHE8qP3aOGxamiFh+UvCT9+yGUwX7sZT1cKW9gqUvqijpmaPKSChjmk6Gl1+3ZGhynBrzTjCt/oT7B62eO0Diz++XyikLOG3qyJY8kwo6Tc2G4Vd9RbGrUhpljW4/LzdXAlZBzYO2Dz1T9uo+422NEuD7PSCUGX92dTseSKk+1oP4vVO3wd7RxSP9yW5lC8uN3J8FIAFjbC961hYd8WJ2ZNyd7bK1FVsN7i9HJ/LGlx+tWLSOB2uxbalskueZXtOae8EiHq5XL9+k7Cp/RxNkS5512Altnp1eU8BjaK/5VgUp0LFpsNSGWJb2RudIlixPHfnk16f2HhWgAguyLYoYvC6MS6td9nSfioKF97Eqt4o9n1R8UdhzeYwT4UmbmtyeHZlhpZal6QFn7nBYfOKSWJSgSXvMkpp4slHi4mUfjPU1/o0sDGyoK6lUm+GABLX7ZHuC6uKiZTeDLn5HwIfRBXTlGHFHEjcV0qsZAJk5TvnEfleNFFNIYmazdJ9tuQNxvdHUrq39fcIXy4vKgOVmAKJGYPSPeHrVMv/eYCVfQwwbig+Ulh2llR1h19x3wmQ9qPjuLIWiOqIJnqU5ZKs+aysOHPat0oQ+7Ly4BFc+Rzg8/XsFCJKU516UFaP/iWIWuAjMVl5sAfcLuBMUN2KoSyXqrqH5I6R3wVWDeNPVhzux3FuB3k3jH6kWHaWqlS3rBl5Pox66ENRue3IQbLurcALYW2UTaJ6kNrmm2T1yOthTUTytbjua7kf5GlgVmDlMMugFXNI1GyWNWPfCa58NdF9Lj9w8/U4iR+AfizQe4UgCRClsavfJJW8N8idvqjJKIxcyX+/K9qAsB64saSCrx8m4nliyTfQM74udw29V3aQV1DZX2Z6W27HknVoVgOLjILev8xMYtmHsBLPsXrkmf+bX2a80PvmN5GzlgALELkRTT1CDaODq1BqAi3jKPUelgxgx16VT559e6pim2aaaab52PIfKndOa3G45U4AAAAASUVORK5CYII=">
                                <p id="pesan_success"></p>
                            </div>

                            <div class="modal-footer">
                                <a href="{{ route('kelas.show', $beritaAcara?->kelas?->kode) . '#tab_berita_acara' }}"
                                    class="btn btn-primary">Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" tabindex="-1" id="modal_error">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Maaf, Terjadi Kesalahan!</h5>
                            </div>

                            <div class="modal-body text-center">
                                <img
                                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABmJLR0QA/wD/AP+gvaeTAAAHaUlEQVR4nO2aa2wcVxWAvzOzL2f9dlIcIAU3oZCmjROiRoVKqFFpeVQQCrioSZOG0kIRCPGQEBJCQlRApApKqQJtxR9AFfCD/gjhIaQCPxCvOqQY3MSxE5I4bRri+IEd73Pm8GOdeHdndnZmdtYRsJ9k2b7n3nPPPXPuuffOHWjRokWLFi1atPg/Ra5Wx3pkSzeSH0BFIX5Ktr4wezXsWHEH6Ph9u8H4HPOHB8E2loot4A8I+2Xr0V+spD0r5gAd/miczsJPMFN3A5CZgPxZt6rfY+Low3IP1krYZdSvEhFdxWeuDB4g1l2r5oNs2PjEitjECjlAx+/bjZEcqiiUmFeTj+uRjXc11aglmu4AHR1qx0g/6hDYmToNOaDD21Y1yawrND8CEun9YKx1lBfO12v5OiTz+abYVEZTk6C+uOdGkunDIIkKQXEGLv3Nj4ochm6WLceON8VAmh0B8eSTjsGrDZlxvxqS2Ma3I7erjKY5QI/f/2GM+K0OQW4S7MUgmt6hRza+LzrLKmnKFNDRoXaSveOI0V8hsLOw8Dxo4CX+DGb8BhkcuRSZkUt4rkWhSaYfdQweIDsBaqHH0uive+BcslTen0dum0UG52tpvBar8AXgS1GbGnkE6Il9N0Fq2Jn4puHSCPq7HvTZNe7GfPBfyNtqHgnymPZmGRwbi9Le6HOAxmokvgkoCvrzvtpND/VBseYzSWAZke8QI3WAju17CIm/1SG4nPguxiHn0WXWLNWpzR361zd9oGFDy4jMAXrsgQ7M5FccAjsL+TOlvxN2fUVxrVNBHtPRTe3BLXQnuggw9ZuuiS8zvpz1u4uQ9HBC0oaeQr2e1pHTL4a2s4pIHKAn925Gkvc7BMVpKF5c/l+Afo8B9ud9pmX9rB6+cWNQO92IJgKsxFOIVE7eGjs+6c/VVCNr8357TCBWJAmxYQfo8X0PY8RvcQhyZ9xPfB4OYK2HzMnteviGe4I0cKMhB+jIrh6M5CMA9O6EgW9Bz3tKA8+dcW/U7/GUvWRuiD6uf9rQGaxRJY1FQCr+DcRYTdv10H1nafnv2lFa83FPdl5hLq8OFAHkC9p/dkYeCdSoitAO0LF9N2Ou2oOY0PchrmSvhecrE181PQX3lWCVDV1F3/0XLWXyfIFMzvrE6UPrtwWzfpnwEWDEnwRidO6AxNL7DjsLZ7/q3a7WShBs/nNx1sKyQFXMXFG/oxpuWx/KATqx95MYsTdjdkLPu5YFF35Ye+6XIf1ZZ1lAByxklqNIVbZP/GzggUAKlgjsAB3Z1QNtXwag7/1gtJUE+Zfg3AF/StySnf8lEFWwrOoy4+unD13b41vJEsEjoC31OGKUTjTpLZe7h7P7QX0OwsUBAfYAiIBpVpapzZqCxr/mW8kSgRygY3u3YyTvvVIw95ulQ86zMPecbz3ymqpwFwLngPY2p+m2pQ+dOHj9zUH0+HaAKoKZ/C7lL1GmD8I/P1M/8VXTXYSBsk3SdRlIB3tL1NdtYlRZryqmpcUDQRKi74o6vufTGO2POQTZk74Sn4N5E32uNGXl9hnoCH4TNj1nMTXrbGeafGzDe0887UeHv6PH6FAvqd5xMHorBFamtO7X2PQ0G1U4fa5AvuA4Qk+taku9cd07R6fr6fA3BRLtTzgGD5AdJ9TgFfRoGv1lH/qrPvR4uAsgEbim13QTrc7msvt96ahXQV/cfQvJjt+DVPZUuACLo74Mdej8QT86XLmFl7fMIffWvS1y5dxUkflLVQ9CsBJx49aBu8b/7NXWMwJUERLppx2DVwuyJ0IZq8MdjsED6B+70JFwL3rW9DgTIopZLNh1E6L3FBjbuxsxb3KU506Vtr1hGOmoLft7OpTKmCn0dTmngq1sO3nwurtdmlzB2wFm3Lm9tDKQeymgiWUsenS5GP6aorvDJJlwPmzbEOebqjK8HWDEBiv+V4XMMRrK+us8IsdLVgcRWN3tEgWWbvdqV2cVMJYnq1qQOQrWXDgLl5C3z0Cny5rfXURua+w7qbak23DE83zgHXOFqQWgG2sBCq+En/flpC2MT02iB1ejp9pAQdZnkJ0XINXYZ0E5534AVDzvE70dcOkff0G4syGr3Lgmjzz4cqT3ctm8cv6i84WKITri1a5O1pEfg0bvAB8sLNrMzFvk8ordQMoRgx95yb1zQEqeAY6G7z4cU7MWL18oksk2NniE4xqT73tV8XSAbBrNI8Yu4N8NmBGIhUWb6bkIPhEU5mOiu9/w7gnPc3bds4BsHX0Bmx2InGzcqvrMzDc+eBE9k4gbd6zfeXK4bl2/SvW3r0/R2fYRhCFgE7C6ESNrMTGZDxX2IswIckxi+lMzbR0Y2HEqgiWrRYsW/+tEuRlLAquAVNVPG2Au9RUr+w1w+Ur98lVREdCy3xaQAbJVP4tAsNfINWjEAe3AqyitBl1Awrt65OSBOWAKeAUI9Q1hWAcMAq9toH3UKDAJeO773Qh7OVrvS6b/Ghp5gmmgn6s3BXKUtuhXZQq4USsJpqid/KqToFsyrE6AkSbB/wDBW3rdoIidSQAAAABJRU5ErkJggg==">
                                <p id="pesan_error"></p>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="/js/html5-qrcode.min.js"></script>

<script type="text/javascript">
    var mahasiswaId = "{{ $mahasiswaId }}";

    function onScanSuccess(decodedText, decodedResult) {

        console.log(`Berhasil: ${decodedText}`);

        var url = "{{ url('absensi/set-hadir') }}/" + decodedText + "/" + mahasiswaId;

        $.ajax({
            url: url,
            success: function (res) {
                $("#pesan_success").html(res.message)
                $("#modal_success").modal('show');
            },
            error: function (xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                $("#pesan_error").html(err.message)
                $("#modal_error").modal({backdrop: 'static', keyboard: false})
            }
        })
    }

    function onScanError(errorMessage) {
        console.log(`Terjadi kesalahan: ${errorMessage}`);
    }

    let config = {
        fps: 10,
        qrbox: {width: '100%', height: '100%'},
        rememberLastUsedCamera: true,
        supportedScanTypes: [Html5QrcodeScanType.SCAN_TYPE_CAMERA],
        formatsToSupport: [Html5QrcodeSupportedFormats.QR_CODE],
    };

    var html5QrcodeScanner = new Html5QrcodeScanner("reader", config, false);

    html5QrcodeScanner.render(onScanSuccess, onScanError);

</script>
@endsection
