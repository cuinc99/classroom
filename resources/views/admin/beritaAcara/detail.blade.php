@extends('layouts.admin')

@section('title', 'Detail Berita Acara ' . $beritaAcara->judul)

@section('content')
<div class="toolbar py-5 py-lg-5">
    <!--begin::Container-->
    <div class="container-xxl d-flex flex-stack flex-wrap">
        <!--begin::Page title-->
        <div class="page-title d-flex flex-column me-3">
            <!--begin::Title-->
            <h1 class="d-flex text-dark fw-bolder my-1 fs-3">Daftar Mahasiswa yang Absensi</h1>
            <!--end::Title-->
        </div>
        @if (auth()->user()->isDosen())
        <div class="d-flex align-items-center py-2 py-md-1">
            @php
            $feedback = \App\Models\Feedback::query()
            ->where('berita_acara_id', $beritaAcara?->id)
            ->where('user_id', auth()->id())
            ->first();
            @endphp

            <button class="btn btn-sm btn-info me-2" data-bs-toggle="modal" data-bs-target="#modal_feedback">
                {{ $feedback ? 'Ubah' : 'Tambah' }} Feedback & Materi
            </button>

            <div class="modal fade" tabindex="-1" id="modal_feedback">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Feedback untuk {{ $beritaAcara?->judul }}</h5>

                            <!--begin::Close-->
                            <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                                aria-label="Close">
                                <span class="svg-icon svg-icon-2x">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none">
                                        <rect opacity="0.5" x="7.05025" y="15.5356" width="12" height="2" rx="1"
                                            transform="rotate(-45 7.05025 15.5356)" fill="black" />
                                        <rect x="8.46447" y="7.05029" width="12" height="2" rx="1"
                                            transform="rotate(45 8.46447 7.05029)" fill="black" />
                                    </svg>
                                </span>
                            </div>
                            <!--end::Close-->
                        </div>

                        <form method="POST" action="{{ route('feedback.simpan', $beritaAcara?->id) }}" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                <input type="hidden" name="user_id" value="{{ auth()->id() }}">
                                <input type="hidden" name="berita_acara_id" value="{{ $beritaAcara?->id }}">

                                <div class="mb-10">
                                    <label class="form-label">Feedback</label>
                                    <textarea name="feedback" class="form-control form-control-solid">{{ $feedback?->feedback }}</textarea>
                                </div>
                                <div class="mb-10">
                                    <label class="form-label">Upload File Materi</label>
                                    <input type="file" name="file" class="form-control form-control-solid mb-5">
                                    @if ($beritaAcara?->file)
                                    <a href="{{ url('storage') . '/' .  $beritaAcara?->file }}">Lihat file</a>
                                    @endif
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div>
                <a href="{{ route('kelas.show', $beritaAcara?->kelas->kode) }}"
                    class="btn btn-sm btn-secondary fw-bolder">
                    Kembali
                    <span class="svg-icon svg-icon-1hx svg-icon-gray-500 me-1">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                            stroke="currentColor" class="w-6 h-6">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3" />
                        </svg>

                    </span>
                </a>
            </div>
        </div>
        @else
        <div class="d-flex align-items-center py-2 py-md-1">
            <a href="{{ route('beritaAcara.export', $beritaAcara?->kode) }}" class="btn btn-sm btn-primary me-2">
                Export Berita Acara
            </a>
            <div>
                <a href="{{ route('kelas.beritaAcara', $beritaAcara?->kelas->kode) }}"
                    class="btn btn-sm btn-secondary fw-bolder">
                    Kembali
                    <span class="svg-icon svg-icon-1hx svg-icon-gray-500 me-1">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                            stroke="currentColor" class="w-6 h-6">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3" />
                        </svg>

                    </span>
                </a>
            </div>
        </div>
        @endif
        <!--end::Page title-->
    </div>
    <!--end::Container-->
</div>
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
    <div class="content flex-row-fluid" id="kt_content">
        <div class="row">
            @if ($beritaAcara?->isBuka() && auth()->user()->isDosen())
            <div class="col-12">
                <div class="card card-xl-stretch mb-xl-8">
                    <div class="card-body rounded-circle d-flex flex-column align-items-center">
                        <h3 class="mb-10">Scan Qrcode untuk Absen</h3>

                        <a target="_blank"
                            href="https://api.qrserver.com/v1/create-qr-code/?size=1000x1000&data={{ $beritaAcara?->kode }}">
                            <img class="img-fluid"
                                src="https://api.qrserver.com/v1/create-qr-code/?size=300x300&data={{ $beritaAcara?->kode }}" />
                        </a>
                    </div>
                </div>
            </div>
            @endif

            {{-- @php
                $absensi = \App\Models\Absensi::where('berita_acara_id', $beritaAcara->id)->get();
            @endphp --}}
            <div class="col-xl-3 col-6">
                <a href="#" class="card bg-dark hoverable card-xl-stretch mb-8">
                    <div class="card-body">
                        <i class="ki-duotone ki-cheque text-gray-100 fs-2x ms-n1">
                            <span class="svg-icon svg-icon-2x svg-icon-secondary">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none">
                                    <path
                                        d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z"
                                        fill="currentColor" />
                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="currentColor" />
                                </svg>
                            </span>
                        </i>

                        <div class="text-gray-100 fw-bold fs-2 mb-2 mt-5">
                            {{ $beritaAcara?->kelompok?->mahasiswas->count() }}
                        </div>

                        <div class="fw-semibold text-gray-100"> Total </div>
                    </div>
                </a>
            </div>

            <div class="col-xl-3 col-6">
                <a href="#" class="card bg-success hoverable card-xl-stretch mb-8">
                    <div class="card-body">
                        <i class="ki-duotone ki-cheque text-gray-100 fs-2x ms-n1">
                            <span class="svg-icon svg-icon-2x svg-icon-secondary">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none">
                                    <path
                                        d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z"
                                        fill="currentColor" />
                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="currentColor" />
                                </svg>
                            </span>
                        </i>

                        <div class="text-gray-100 fw-bold fs-2 mb-2 mt-5">
                            {{ $beritaAcara->absensis->where('status', 'Hadir')->count() }}
                        </div>

                        <div class="fw-semibold text-gray-100"> Hadir </div>
                    </div>
                </a>
            </div>

            <div class="col-xl-3 col-6">
                <a href="#" class="card bg-warning hoverable card-xl-stretch mb-8">
                    <div class="card-body">
                        <i class="ki-duotone ki-cheque text-gray-100 fs-2x ms-n1">
                            <span class="svg-icon svg-icon-2x svg-icon-secondary">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none">
                                    <path
                                        d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z"
                                        fill="currentColor" />
                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="currentColor" />
                                </svg>
                            </span>
                        </i>

                        <div class="text-gray-100 fw-bold fs-2 mb-2 mt-5">
                            {{ $beritaAcara->absensis->where('status', 'Izin')->count() }}
                        </div>

                        <div class="fw-semibold text-gray-100"> Izin </div>
                    </div>
                </a>
            </div>

            <div class="col-xl-3 col-6">
                <a href="#" class="card bg-danger hoverable card-xl-stretch mb-8">
                    <div class="card-body">
                        <i class="ki-duotone ki-cheque text-gray-100 fs-2x ms-n1">
                            <span class="svg-icon svg-icon-2x svg-icon-secondary">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none">
                                    <path
                                        d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z"
                                        fill="currentColor" />
                                    <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="currentColor" />
                                </svg>
                            </span>
                        </i>

                        <div class="text-gray-100 fw-bold fs-2 mb-2 mt-5">
                            {{ $beritaAcara?->kelompok?->mahasiswas->count() - $beritaAcara->absensis->count() }}
                        </div>

                        <div class="fw-semibold text-gray-100"> Belum Absen </div>
                    </div>
                </a>
            </div>

            <div class="col-xl-4">
                <div class="card mb-6" data-kt-sticky="true">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped gy-7 gs-7">
                                <tr>
                                    <td width="35%">Semester</td>
                                    <td width="5">:</td>
                                    <td>{{ $beritaAcara?->semester }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Blok</td>
                                    <td width="5">:</td>
                                    <td>{{ $beritaAcara?->kelas?->blok?->nama }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Metode Pembelajaran</td>
                                    <td width="5">:</td>
                                    <td>{{ $beritaAcara?->metode }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Judul Topik</td>
                                    <td width="5">:</td>
                                    <td>{{ $beritaAcara?->judul }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Hari, Tanggal</td>
                                    <td width="5">:</td>
                                    <td>{{ Date::parse($beritaAcara?->tanggal)?->format('l, d M Y') }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Waktu</td>
                                    <td width="5">:</td>
                                    <td>{{ $beritaAcara?->tanggal?->format('H:i') }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Kelompok Peserta</td>
                                    <td width="5">:</td>
                                    <td>{{ $beritaAcara?->kelompok?->nama }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Dosen Pengampu</td>
                                    <td width="5">:</td>
                                    <td>
                                        <ul>
                                            @foreach ($beritaAcara?->dosen_pengampu_list as $item)
                                            <li>{{ $item }}</li>
                                            @endforeach
                                        </ul>
                                    </td>
                                </tr>
                                @php
                                    $fb = $beritaAcara?->feedbacks()->whereHas('user', fn($q) => $q->where('user_type', 'dosen'))->first();
                                @endphp
                                <tr>
                                    <td width="35%">Feedback Dosen Pengampu</td>
                                    <td width="5">:</td>
                                    <td>
                                        {{ $fb->feedback ?? '-' }}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="35%">File Materi</td>
                                    <td width="5">:</td>
                                    <td>
                                        @if ($beritaAcara?->file)
                                        <a href="{{ url('storage') . '/' .  $beritaAcara?->file }}" class="btn btn-sm btn-primary" target="_blank">Lihat file</a>
                                        @else
                                        -
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-8">
                <div class="card mb-6">
                    <!--begin::Body-->
                    <div class="card-body">
                        <div class="table-responsive">
                            <!--begin::Table-->
                            <table class="table table-striped gy-7 gs-7">
                                <!--begin::Table head-->
                                <thead>
                                    <tr class="fw-bold fs-6 text-gray-800 border-bottom border-gray-200"">
                                        <th class=" w-50px">
                                        </th>
                                        <th class="min-w-150px">Mahasiswa</th>
                                        <th class="text-end min-w-150px">Status Kehadiran</th>
                                        <th class="min-w-150px">Keterangan</th>
                                        @if (auth()->user()->isDosen() && $beritaAcara?->isBuka())
                                        <th class="min-w-100px"></th>
                                        @endif
                                    </tr>
                                </thead>
                                <!--end::Table head-->
                                <!--begin::Table body-->
                                <tbody>

                                    @foreach ($beritaAcara->kelompok->mahasiswas as $mahasiswa)

                                    <tr>
                                        <td>
                                            <div class="symbol symbol-50px me-2">
                                                <span class="symbol-label bg-light-primary">
                                                    <!--begin::Svg Icon | path: icons/duotune/abstract/abs027.svg-->
                                                    <span class="svg-icon svg-icon-2x svg-icon-primary">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                            viewBox="0 0 24 24" fill="none">
                                                            <path
                                                                d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z"
                                                                fill="currentColor" />
                                                            <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4"
                                                                fill="currentColor" />
                                                        </svg>
                                                    </span>
                                                    <!--end::Svg Icon-->
                                                </span>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="#" class="text-dark fw-bolder text-hover-primary mb-1 fs-6">
                                                {{ $mahasiswa->user?->name ?? '-' }}
                                            </a>
                                            <span class="text-muted fw-bold d-block">
                                                {{ $mahasiswa->user?->username ?? '-' }}
                                            </span>
                                        </td>
                                        <td class="text-end">
                                            @php
                                            $absensi = \App\Models\Absensi::query()
                                            ->where('berita_acara_id', $beritaAcara?->id)
                                            ->where('mahasiswa_id', $mahasiswa?->id)
                                            ->first();
                                            @endphp
                                            @if ($absensi)
                                            <span
                                                class="badge badge-{{ \App\Models\Absensi::STATUS_SELECT[$absensi->status]['warna'] }} fw-bolder me-auto px-4 py-3">
                                                {{ $absensi?->status }} <br><br> ({{
                                                $absensi?->created_at->format('d/M/Y H:i:s') }})
                                            </span>
                                            @else
                                            <span class="badge badge-danger fw-bolder me-auto px-4 py-3">
                                                Belum Absen
                                            </span>
                                            @endif
                                        </td>
                                        <td>
                                            {{ $absensi?->keterangan ?? '-' }}
                                        </td>
                                        @if (auth()->user()->isDosen() && $beritaAcara?->isBuka())
                                        <td class="text-end">
                                            @if ($absensi)
                                            <button class="btn btn-sm btn-light-primary" data-bs-toggle="modal"
                                                data-bs-target="#modal_absen{{ $mahasiswa?->id }}{{ $beritaAcara?->id }}">
                                                Ubah Absen
                                            </button>
                                            @else
                                            <button class="btn btn-sm btn-light-info" data-bs-toggle="modal"
                                                data-bs-target="#modal_absen{{ $mahasiswa?->id }}{{ $beritaAcara?->id }}">
                                                Buat Absen
                                            </button>
                                            @endif
                                        </td>

                                        <div class="modal fade" tabindex="-1"
                                            id="modal_absen{{ $mahasiswa?->id }}{{ $beritaAcara?->id }}">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Absen Mahasiswa {{
                                                            $mahasiswa?->user?->name
                                                            }}</h5>

                                                        <!--begin::Close-->
                                                        <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                                            data-bs-dismiss="modal" aria-label="Close">
                                                            <span class="svg-icon svg-icon-2x">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                    height="24" viewBox="0 0 24 24" fill="none">
                                                                    <rect opacity="0.5" x="7.05025" y="15.5356" width="12"
                                                                        height="2" rx="1"
                                                                        transform="rotate(-45 7.05025 15.5356)"
                                                                        fill="black" />
                                                                    <rect x="8.46447" y="7.05029" width="12" height="2"
                                                                        rx="1" transform="rotate(45 8.46447 7.05029)"
                                                                        fill="black" />
                                                                </svg>
                                                            </span>
                                                        </div>
                                                        <!--end::Close-->
                                                    </div>

                                                    <form method="POST" action="{{ route('absensi.simpan') }}"
                                                        enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="modal-body">
                                                            <input type="hidden" name="mahasiswa_id"
                                                                value="{{ $mahasiswa?->id }}">
                                                            <input type="hidden" name="berita_acara_id"
                                                                value="{{ $beritaAcara?->id }}">

                                                            <div class="mb-10">
                                                                <label class="required form-label">Status
                                                                    Kehadiran</label>
                                                                <select name="status" class="form-select form-select-solid"
                                                                    data-control="select2"
                                                                    data-dropdown-parent="#modal_absen{{ $mahasiswa?->id }}{{ $beritaAcara?->id }}"
                                                                    data-placeholder="Pilih Status" data-allow-clear="true"
                                                                    required>
                                                                    <option></option>
                                                                    <option value="Hadir" @selected($absensi?->status ==
                                                                        'Hadir')>Hadir</option>
                                                                    <option value="Izin" @selected($absensi?->status ==
                                                                        'Izin')>Izin</option>
                                                                </select>
                                                            </div>

                                                            <div class="mb-10">
                                                                <label class="form-label">Keterangan</label>
                                                                <textarea name="keterangan"
                                                                    class="form-control form-control-solid">{{ $absensi?->keterangan }}</textarea>
                                                            </div>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    </tr>
                                    @endforeach

                                </tbody>
                                <!--end::Table body-->
                            </table>
                        </div>
                    </div>
                    <!--end::Body-->
                </div>

                @if ($beritaAcara?->feedbacks()->whereHas('user', fn($q) => $q->where('user_type', 'mahasiswa'))->count() > 0)
                <div class="card">
                    <!--begin::Body-->
                    <div class="card-body">
                        <div class="table-responsive">
                            <!--begin::Table-->
                            <table class="table table-striped gy-7 gs-7">
                                <!--begin::Table head-->
                                <thead>
                                    <tr class="fw-bold fs-6 text-gray-800 border-bottom border-gray-200"">
                                        <th width="5" class="text-center">No</th>
                                        {{-- <th class="min-w-150px">User</th> --}}
                                        <th class="min-w-150px">Feedback</th>
                                        <th class="text-end">File</th>
                                    </tr>
                                </thead>
                                <!--end::Table head-->
                                <!--begin::Table body-->
                                <tbody>

                                    @foreach ($beritaAcara?->feedbacks()->whereHas('user', fn($q) => $q->where('user_type', 'mahasiswa'))->get() as $feedback)

                                    <tr>
                                        <td class="text-center">
                                            {{ $loop->iteration }}
                                        </td>
                                        {{-- <td>
                                            <span class="text-dark fw-bolder mb-1 fs-6">
                                                {{ $feedback?->user?->name ?? '-' }} ({{ $feedback?->user?->user_type ?? '-' }})
                                            </span>
                                            <span class="text-muted fw-bold d-block">
                                                {{ $feedback->user?->username ?? '-' }}
                                            </span>
                                        </td> --}}
                                        <td>
                                            <span class="text-dark fw-bolder mb-1 fs-6">
                                                {{ $feedback?->feedback ?? '-' }}
                                            </span>
                                        </td>
                                    </tr>
                                    @endforeach

                                </tbody>
                                <!--end::Table body-->
                            </table>
                        </div>
                    </div>
                    <!--end::Body-->
                </div>
            @endif
            </div>

        </div>
    </div>
    <!--end::Post-->
</div>
@endsection

@section('styles')
@endsection

@section('scripts')
@endsection
