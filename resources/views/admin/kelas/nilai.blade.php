@extends('layouts.admin')

@section('title')
    Nilai
@endsection

@section('content')
    <!--begin::Toolbar-->
    <div class="toolbar py-5 py-lg-5" id="kt_toolbar">
        <!--begin::Container-->
        <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column me-3">
                <!--begin::Title-->
                <h1 class="d-flex text-dark fw-bolder my-1 fs-3">Nilai</h1>
                <!--end::Title-->
            </div>
            <!--end::Page title-->

            <div class="d-flex align-items-center py-2 py-md-1">
                <div>
                    <a href="{{ route('kelas.nilaiExport', $kelas->kode) }}" class="btn btn-primary fw-bolder btn-hover-scale me-2">
                        <!--begin::Svg Icon | path: assets/media/icons/duotune/arrows/arr087.svg-->
                        <span class="svg-icon svg-icon-muted svg-icon-2hx">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path opacity="0.3"
                                    d="M5 16C3.3 16 2 14.7 2 13C2 11.3 3.3 10 5 10H5.1C5 9.7 5 9.3 5 9C5 6.2 7.2 4 10 4C11.9 4 13.5 5 14.3 6.5C14.8 6.2 15.4 6 16 6C17.7 6 19 7.3 19 9C19 9.4 18.9 9.7 18.8 10C18.9 10 18.9 10 19 10C20.7 10 22 11.3 22 13C22 14.7 20.7 16 19 16H5ZM8 13.6H16L12.7 10.3C12.3 9.89999 11.7 9.89999 11.3 10.3L8 13.6Z"
                                    fill="black" />
                                <path d="M11 13.6V19C11 19.6 11.4 20 12 20C12.6 20 13 19.6 13 19V13.6H11Z" fill="black" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                        Export Nilai
                    </a>
                    <a href="{{ route('folder.show', $kelas->folder->kode) }}" class="btn btn-secondary fw-bolder">
                        Kembali
                        <span class="svg-icon svg-icon-2hx svg-icon-gray-500 me-1">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                stroke="currentColor" class="w-6 h-6">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                    d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3" />
                            </svg>

                        </span>
                    </a>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Container-->
    <div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
        <div class="content flex-row-fluid" id="kt_content">

            @include('admin.kelas.partials._detail')

            @include('admin.kelas.partials._nav')

            <div class="card card-flush">
                <div class="card-header align-items-center py-5 gap-2 gap-md-5">
                    <div class="card-title">
                        <!--begin::Search-->
                        <div class="d-flex align-items-center position-relative my-1">
                            <span class="svg-icon svg-icon-1 position-absolute ms-4">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor"></rect>
                                    <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="currentColor"></path>
                                </svg>
                            </span>
                            <input type="text" data-kt-filter="search" class="form-control form-control-solid w-250px ps-14"
                                placeholder="Cari..." />
                        </div>
                        <!--end::Search-->
                    </div>

                </div>
                <div class="card-body">
                    <button class="btn btn-danger" id="deleteAllSelectedRecord">Hapus data terpilih</button>
                    <table id="kt_datatable_example_1" class="table table-striped table-row-bordered gy-5 gs-7">
                        <thead>
                            <tr class="fw-bold fs-6 text-gray-800 text-center">
                                <th width="10">
                                    <input type="checkbox" id="selectAllIds">
                                </th>
                                <th width="10"></th>
                                <th width="10">No</th>
                                <th class="min-w-150px">NIM</th>
                                <th class="min-w-300px">Nama Mahasiswa</th>
                                @foreach ($kelas->kategoris as $kategori)
                                    @if ($kategori->tugas->count() > 1)
                                        @foreach ($kategori->tugasByOldest as $tugas)
                                            <th class="min-w-100px text-muted">{{ $tugas->judul ?? '-' }}</th>
                                        @endforeach
                                    @endif
                                    <th class="min-w-100px">{{ $kategori->nama ?? '-' }}</th>
                                    <th class="min-w-100px bg-primary">{{ $kategori->persentase_nilai ?? '-' }}%</th>
                                @endforeach
                                <th class="min-w-100px">Total</th>
                                <th class="min-w-100px">Huruf</th>
                                <th class="min-w-100px">Kehadiran (%)</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($mahasiswas as $mahasiswa)
                                <tr class="text-center" id="mahasiswa_ids{{ $mahasiswa->id }}">
                                    <td>
                                        <input type="checkbox" name="ids" class="checkbox_ids" value="{{ $mahasiswa->id }}">
                                    </td>
                                    <td>
                                        <a href="{{ route('mahasiswa.hapus', $mahasiswa->kode) }}"
                                            onclick="return confirm('Yakin akan menghapus?');" data-bs-toggle="tooltip"
                                            data-bs-custom-class="tooltip-dark" data-bs-placement="left" title="Hapus">
                                            <span class="svg-icon svg-icon-danger svg-icon-1">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                    fill="none">
                                                    <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black" />
                                                    <rect x="7" y="15.3137" width="12" height="2" rx="1"
                                                        transform="rotate(-45 7 15.3137)" fill="black" />
                                                    <rect x="8.41422" y="7" width="12" height="2" rx="1"
                                                        transform="rotate(45 8.41422 7)" fill="black" />
                                                </svg>
                                            </span>
                                        </a>
                                    </td>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $mahasiswa->user->username ?? '-' }}</td>
                                    <td align="left">{{ $mahasiswa->user->name ?? '-' }}</td>
                                    @php
                                        $total = 0;
                                    @endphp
                                    @foreach ($kelas->kategoris as $kategori)
                                        @if ($kategori->tugas->count() > 1)
                                            @foreach ($kategori->tugasByOldest as $tugas)
                                                @php
                                                    $mahasiswaTugas = \App\Models\MahasiswaTugas::where('mahasiswa_id', $mahasiswa->id)
                                                        ->where('tugas_id', $tugas->id)
                                                        ->first();
                                                        $total_poin = $mahasiswaTugas ? $mahasiswaTugas->total_poin : 0;
                                                @endphp
                                                <td>{{ number_format($total_poin, 2) - 0 }}</td>
                                            @endforeach
                                        @endif
                                        @php
                                            $tugasIds = $kategori->tugas->pluck('id');
                                            $sum_poin = \App\Models\MahasiswaTugas::where('mahasiswa_id', $mahasiswa->id)
                                                ->whereIn('tugas_id', $tugasIds)
                                                ->sum('total_poin');
                                            $nilai_akhir = $tugasIds->count() != 0 ? $sum_poin / $tugasIds->count() : 0;
                                            $persentase_nilai_akhir = ($nilai_akhir * $kategori->persentase_nilai) / 100;
                                            $total += $persentase_nilai_akhir;
                                        @endphp
                                        <td>{{ number_format($nilai_akhir, 2) - 0 }}</td>
                                        <td class="text-dark fw-bolder">{{ number_format($persentase_nilai_akhir, 2) - 0 }}</td>
                                    @endforeach
                                    <td>{{ number_format($total, 2) - 0 }}</td>
                                    <td>{{ \App\Services\NilaiService::getNilaiHuruf(number_format($total, 2) - 0) }}</td>

                                    @php
                                        $absensi = \App\Models\Absensi::query()
                                            ->where('mahasiswa_id', $mahasiswa?->id)
                                            ->get();
                                        $totalAbsen = $mahasiswa->berita_acaras()->count();
                                    @endphp
                                    <td>
                                        <span class="text-dark fw-bolder">{{ $hadir = $absensi->where('status', 'Hadir')->count() }}</span>/{{ $totalAbsen }}
                                        @if ($totalAbsen > 0)
                                            ({{ round($hadir / $totalAbsen * 100) }}%)
                                            <button class="btn btn-sm btn-light-primary" data-bs-toggle="modal" data-bs-target="#modal_detail_absensi{{ $mahasiswa?->id }}">
                                                Detail
                                            </button>
                                            <div class="modal fade" tabindex="-1" id="modal_detail_absensi{{ $mahasiswa?->id }}">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Detail Absensi {{ $mahasiswa?->user?->name }}</h5>

                                                            <!--begin::Close-->
                                                            <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                                                data-bs-dismiss="modal" aria-label="Close">
                                                                <span class="svg-icon svg-icon-2x">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                        height="24" viewBox="0 0 24 24" fill="none">
                                                                        <rect opacity="0.5" x="7.05025" y="15.5356" width="12"
                                                                            height="2" rx="1"
                                                                            transform="rotate(-45 7.05025 15.5356)"
                                                                            fill="black" />
                                                                        <rect x="8.46447" y="7.05029" width="12" height="2"
                                                                            rx="1" transform="rotate(45 8.46447 7.05029)"
                                                                            fill="black" />
                                                                    </svg>
                                                                </span>
                                                            </div>
                                                            <!--end::Close-->
                                                        </div>

                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-xl-3 col-6">
                                                                    <a href="#" class="card bg-dark hoverable card-xl-stretch mb-8">
                                                                        <div class="card-body">
                                                                            <i class="ki-duotone ki-cheque text-gray-100 fs-2x ms-n1">
                                                                                <span class="svg-icon svg-icon-2x svg-icon-secondary">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                                                        fill="none">
                                                                                        <path
                                                                                            d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z"
                                                                                            fill="currentColor" />
                                                                                        <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="currentColor" />
                                                                                    </svg>
                                                                                </span>
                                                                            </i>

                                                                            <div class="text-gray-100 fw-bold fs-2 mb-2 mt-5">
                                                                                {{ $totalAbsen }}
                                                                            </div>

                                                                            <div class="fw-semibold text-gray-100"> Total </div>
                                                                        </div>
                                                                    </a>
                                                                </div>

                                                                <div class="col-xl-3 col-6">
                                                                    <a href="#" class="card bg-success hoverable card-xl-stretch mb-8">
                                                                        <div class="card-body">
                                                                            <i class="ki-duotone ki-cheque text-gray-100 fs-2x ms-n1">
                                                                                <span class="svg-icon svg-icon-2x svg-icon-secondary">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                                                        fill="none">
                                                                                        <path
                                                                                            d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z"
                                                                                            fill="currentColor" />
                                                                                        <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="currentColor" />
                                                                                    </svg>
                                                                                </span>
                                                                            </i>

                                                                            <div class="text-gray-100 fw-bold fs-2 mb-2 mt-5">
                                                                                {{ $hadir = $absensi->where('status', 'Hadir')->count() }}</span>/{{ $totalAbsen }} ({{ round($hadir / $totalAbsen * 100) }}%)
                                                                            </div>

                                                                            <div class="fw-semibold text-gray-100"> Hadir </div>
                                                                        </div>
                                                                    </a>
                                                                </div>

                                                                <div class="col-xl-3 col-6">
                                                                    <a href="#" class="card bg-warning hoverable card-xl-stretch mb-8">
                                                                        <div class="card-body">
                                                                            <i class="ki-duotone ki-cheque text-gray-100 fs-2x ms-n1">
                                                                                <span class="svg-icon svg-icon-2x svg-icon-secondary">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                                                        fill="none">
                                                                                        <path
                                                                                            d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z"
                                                                                            fill="currentColor" />
                                                                                        <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="currentColor" />
                                                                                    </svg>
                                                                                </span>
                                                                            </i>

                                                                            <div class="text-gray-100 fw-bold fs-2 mb-2 mt-5">
                                                                                {{ $izin = $absensi->where('status', 'Izin')->count() }}</span>/{{ $totalAbsen }} ({{ round($izin / $totalAbsen * 100) }}%)
                                                                            </div>

                                                                            <div class="fw-semibold text-gray-100"> Izin </div>
                                                                        </div>
                                                                    </a>
                                                                </div>

                                                                <div class="col-xl-3 col-6">
                                                                    <a href="#" class="card bg-danger hoverable card-xl-stretch mb-8">
                                                                        <div class="card-body">
                                                                            <i class="ki-duotone ki-cheque text-gray-100 fs-2x ms-n1">
                                                                                <span class="svg-icon svg-icon-2x svg-icon-secondary">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                                                        fill="none">
                                                                                        <path
                                                                                            d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z"
                                                                                            fill="currentColor" />
                                                                                        <rect opacity="0.3" x="8" y="3" width="8" height="8" rx="4" fill="currentColor" />
                                                                                    </svg>
                                                                                </span>
                                                                            </i>

                                                                            <div class="text-gray-100 fw-bold fs-2 mb-2 mt-5">
                                                                                {{ $tidakHadir = $totalAbsen - $absensi->count() }}</span>/{{ $totalAbsen }} ({{ round($tidakHadir / $totalAbsen * 100) }}%)
                                                                            </div>

                                                                            <div class="fw-semibold text-gray-100"> Belum Absen </div>
                                                                        </div>
                                                                    </a>
                                                                </div>

                                                                <div class="col-xl-12 mt-10">
                                                                    @foreach ($mahasiswa->berita_acaras() as $item)
                                                                    <div class="d-flex align-items-sm-center mb-7">
                                                                        <div class="symbol symbol-60px symbol-2by3 me-4">
                                                                            <div class="symbol-label">
                                                                                {{ $item?->metode }}
                                                                            </div>
                                                                        </div>
                                                                        <div class="d-flex flex-row-fluid flex-wrap align-items-center">
                                                                            <div class="flex-grow-1 me-2">
                                                                                <a href="#" class="text-gray-800 fw-bolder text-hover-primary fs-6">{{ $item?->judul }}</a>
                                                                                <span class="text-muted fw-bold d-block pt-1">{{ $item?->user?->name }}</span>
                                                                                <span class="text-muted fw-bold d-block pt-1">{{ $item?->tanggal->format('d/M/Y') }}</span>
                                                                            </div>
                                                                            @php
                                                                                $absen = \App\Models\Absensi::query()
                                                                                    ->where('berita_acara_id', $item?->id)
                                                                                    ->where('mahasiswa_id', $mahasiswa?->id)
                                                                                    ->first();
                                                                            @endphp
                                                                            @if ($absen)
                                                                            <span class="badge badge-{{ \App\Models\Absensi::STATUS_SELECT[$absen?->status]['warna'] }} fs-8 fw-bolder my-2">{{ $absen?->status }} <br> ({{ $absen?->created_at->format('d/M/Y H:i:s') }})</span>
                                                                            @else
                                                                            <span class="badge badge-danger fs-8 fw-bolder my-2">Belum Absen</span>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Tutup</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                Mahasiswa belum ada.
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <!--end::Post-->
    </div>
    <!--end::Container-->
@endsection

@section('styles')
    <link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
    <script>
        $(function(e) {
            $("#selectAllIds").click(function(){
                $(".checkbox_ids").prop("checked", $(this).prop("checked"))
            })

            $("#deleteAllSelectedRecord").click(function(e) {
                e.preventDefault()

                var all_ids = [];

                $("input:checkbox[name=ids]:checked").each(function() {
                    all_ids.push($(this).val())
                })

                if (all_ids === undefined || all_ids.length == 0) {
                    alert("Tidak ada data terpilih")
                } else {
                    $.ajax({
                        url: "{{ route('kelas.mahasiswaHapus') }}",
                        type: "DELETE",
                        data: {
                            ids: all_ids,
                            _token: "{{ csrf_token() }}"
                        },
                        success: function(res) {
                            $.each(all_ids, function(key, val) {
                                $("#mahasiswa_ids" + val).remove()
                            })
                        }
                    })
                }

            })
        })
    </script>
    <script src="/assets/plugins/custom/datatables/datatables.bundle.js"></script>
    <script>
        "use strict";

        // Class definition
        var KTDatatablesButtons = function() {
            // Shared variables
            var table;
            var datatable;

            // Private functions
            var initDatatable = function() {
                // Set date data order
                const tableRows = table.querySelectorAll('tbody tr');

                tableRows.forEach(row => {
                    const dateRow = row.querySelectorAll('td');
                    const realDate = moment(dateRow[3].innerHTML, "DD MMM YYYY, LT")
                .format(); // select date from 4th column in table
                    dateRow[3].setAttribute('data-order', realDate);
                });

                // Init datatable --- more info on datatables: https://datatables.net/manual/
                datatable = $(table).DataTable({
                    "info": false,
                    'order': [],
                    'pageLength': 10,
                });
            }

            // Search Datatable --- official docs reference: https://datatables.net/reference/api/search()
            var handleSearchDatatable = () => {
                const filterSearch = document.querySelector('[data-kt-filter="search"]');
                filterSearch.addEventListener('keyup', function(e) {
                    datatable.search(e.target.value).draw();
                });
            }

            // Public methods
            return {
                init: function() {
                    table = document.querySelector('#kt_datatable_example_1');

                    if (!table) {
                        return;
                    }

                    initDatatable();
                    handleSearchDatatable();
                }
            };
        }();

        // On document ready
        KTUtil.onDOMContentLoaded(function() {
            KTDatatablesButtons.init();
        });
    </script>
@endsection
