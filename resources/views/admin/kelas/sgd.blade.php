@extends('layouts.admin')

@section('title')
Pleno SGD
@endsection

@section('content')
<!--begin::Toolbar-->
<div class="toolbar py-5 py-lg-5" id="kt_toolbar">
    <!--begin::Container-->
    <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
        <!--begin::Page title-->
        <div class="page-title d-flex flex-column me-3">
            <!--begin::Title-->
            <h1 class="d-flex text-dark fw-bolder my-1 fs-3">Pleno SGD</h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <div class="d-flex align-items-center py-2 py-md-1">
            <div>
                <button class="btn btn-primary fw-bolder btn-hover-scale me-2" data-bs-toggle="modal"
                    data-bs-target="#modal_buat_sgd">
                    <!--begin::Svg Icon | path: assets/media/icons/duotune/arrows/arr087.svg-->
                    <span class="svg-icon svg-icon-muted svg-icon-2hx">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <path opacity="0.3"
                                d="M3 13V11C3 10.4 3.4 10 4 10H20C20.6 10 21 10.4 21 11V13C21 13.6 20.6 14 20 14H4C3.4 14 3 13.6 3 13Z"
                                fill="black" />
                            <path
                                d="M13 21H11C10.4 21 10 20.6 10 20V4C10 3.4 10.4 3 11 3H13C13.6 3 14 3.4 14 4V20C14 20.6 13.6 21 13 21Z"
                                fill="black" />
                        </svg>
                    </span>
                    <!--end::Svg Icon-->
                    Buat Pleno SGD
                </button>
                <a href="{{ route('folder.show', $kelas->folder->kode) }}" class="btn btn-secondary fw-bolder">
                    Kembali
                    <span class="svg-icon svg-icon-2hx svg-icon-gray-500 me-1">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                            stroke="currentColor" class="w-6 h-6">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3" />
                        </svg>

                    </span>
                </a>
            </div>
        </div>

        <div class="modal fade" tabindex="-1" id="modal_buat_sgd">
            <div class="modal-dialog modal-fullscreen">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Buat Pleno SGD</h5>

                        <!--begin::Close-->
                        <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                            aria-label="Close">
                            <span class="svg-icon svg-icon-2x">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none">
                                    <rect opacity="0.5" x="7.05025" y="15.5356" width="12" height="2" rx="1"
                                        transform="rotate(-45 7.05025 15.5356)" fill="black" />
                                    <rect x="8.46447" y="7.05029" width="12" height="2" rx="1"
                                        transform="rotate(45 8.46447 7.05029)" fill="black" />
                                </svg>
                            </span>
                        </div>
                        <!--end::Close-->
                    </div>

                    <div class="modal-body">
                        <form method="POST" action="{{ route('sgd.simpan') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-xl-6">
                                    <input type="hidden" name="kelas_id" value="{{ $kelas->id }}">

                                    <div class="mb-10">
                                        <label class="required form-label">Semester</label>
                                        <input type="text" name="semester" class="form-control form-control-solid"
                                            placeholder="cth: 1 (Satu)" required />
                                    </div>

                                    <div class="mb-10">
                                        <label class="required form-label">Judul</label>
                                        <input type="text" name="judul" class="form-control form-control-solid"
                                           placeholder="cth: Kriteria Penilaian SGD Sesi I" required />
                                    </div>

                                    <div class="mb-10">
                                        <label class="required form-label">Ruang</label>
                                        <input type="text" name="ruang" class="form-control form-control-solid"
                                            required />
                                    </div>

                                    <div class="mb-10">
                                        <label class="form-label">Skenario</label>
                                        <textarea name="skenario" class="form-control form-control-solid"></textarea>
                                    </div>

                                    <div class="mb-10">
                                        <label class="required form-label">Tanggal</label>
                                        <input type="datetime-local" name="tanggal"
                                            class="form-control form-control-solid" required />
                                    </div>

                                    <div class="mb-10">
                                        <label class="required form-label">Kelompok</label>
                                        <select name="kelompok_id" class="form-select form-select-solid"
                                            id="kelompok_id" data-control="select2"
                                            data-dropdown-parent="#modal_buat_sgd" data-placeholder="Pilih Kelompok"
                                            data-allow-clear="true" required>
                                            <option></option>
                                            @foreach ($kelas->kelompoks as $item)
                                            <option value="{{ $item->id }}">{{ $item->nama }} ({{
                                                $item->mahasiswas->count() }}
                                                Mahasiswa)</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="mb-10">
                                        <label class="required form-label">Tutor</label>
                                        <select name="user_id" class="form-select form-select-solid"
                                            data-control="select2" data-dropdown-parent="#modal_buat_sgd"
                                            data-placeholder="Pilih Tutor" data-allow-clear="true" required>
                                            <option></option>
                                            @foreach ($dosens as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div x-data="handler()">
                                        <div class="mb-5 fv-row">
                                            <a class="btn btn-primary rounded-50" @click="addNewField()">
                                                <span class="svg-icon svg-icon-2">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none">
                                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5"
                                                            fill="black" />
                                                        <rect x="10.8891" y="17.8033" width="12" height="2" rx="1"
                                                            transform="rotate(-90 10.8891 17.8033)" fill="black" />
                                                        <rect x="6.01041" y="10.9247" width="12" height="2" rx="1"
                                                            fill="black" />
                                                    </svg>
                                                </span>
                                                Tambah Kriteria</a>
                                        </div>
                                        <template x-for="(field, index) in fields" :key="index">
                                            <div class="row fv-row">
                                                <!--begin::Col-->
                                                <div class="col-10 fv-row">
                                                    <div class="input-group input-group-solid mb-5">
                                                        <span class="input-group-text" id="basic-addon2">
                                                            <span class="svg-icon svg-icon-1">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                    height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path opacity="0.3"
                                                                        d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                                                                        fill="black" />
                                                                    <path
                                                                        d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                                                                        fill="black" />
                                                                </svg>
                                                            </span>
                                                        </span>
                                                        <textarea x-model="field.kriterias" name="kriterias[]"
                                                            class="form-control form-control-solid" required></textarea>
                                                    </div>
                                                </div>
                                                <!--end::Col-->
                                                <!--begin::Col-->
                                                <div class="col-2 fv-row">
                                                    <span class="svg-icon svg-icon-danger svg-icon-3hx"
                                                        @click="removeField(index)">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                            viewBox="0 0 24 24" fill="none">
                                                            <rect opacity="0.3" x="2" y="2" width="20" height="20"
                                                                rx="10" fill="black" />
                                                            <rect x="7" y="15.3137" width="12" height="2" rx="1"
                                                                transform="rotate(-45 7 15.3137)" fill="black" />
                                                            <rect x="8.41422" y="7" width="12" height="2" rx="1"
                                                                transform="rotate(45 8.41422 7)" fill="black" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <!--end::Col-->
                                            </div>
                                        </template>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--end::Container-->
</div>
<!--end::Toolbar-->
<!--begin::Container-->
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
    <div class="content flex-row-fluid" id="kt_content">

        @include('admin.kelas.partials._detail')

        @include('admin.kelas.partials._nav')

        @include('layouts.partials.messages')

        <div class="card card-flush">
            <div class="card-header align-items-center py-5 gap-2 gap-md-5">
                <div class="card-title">
                    <!--begin::Search-->
                    <div class="d-flex align-items-center position-relative my-1">
                        <span class="svg-icon svg-icon-1 position-absolute ms-4">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none">
                                <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1"
                                    transform="rotate(45 17.0365 15.1223)" fill="currentColor"></rect>
                                <path
                                    d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                    fill="currentColor"></path>
                            </svg>
                        </span>
                        <input type="text" data-kt-filter="search" class="form-control form-control-solid w-250px ps-14"
                            placeholder="Cari..." />
                    </div>
                    <!--end::Search-->
                </div>

            </div>
            <div class="card-body">
                <table id="kt_datatable_example_1" class="table table-striped table-row-bordered gy-5 gs-7">
                    <thead>
                        <tr class="fw-bold fs-6 text-gray-800">
                            <th width="10">No</th>
                            <th class="min-w-100px">Judul</th>
                            <th class="min-w-100px">Ruang</th>
                            <th class="min-w-100px">Semester</th>
                            <th class="min-w-100px">Blok</th>
                            <th class="min-w-100px">Tutor</th>
                            <th class="min-w-100px">Hari, Tanggal, Waktu</th>
                            <th class="min-w-100px">Kelompok (Peserta)</th>
                            <th class="min-w-100px">Skenario</th>
                            <th class="min-w-100px">Status</th>
                            <th class="min-w-100px"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($sgds as $sgd)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $sgd?->judul ?? '-' }}</td>
                            <td>{{ $sgd?->ruang ?? '-' }}</td>
                            <td>{{ $sgd?->semester ?? '-' }}</td>
                            <td>{{ $sgd?->kelas?->blok?->nama ?? '-' }}</td>
                            <td>{{ $sgd?->user?->name ?? '-' }}</td>
                            <td>{{ Date::parse($sgd?->tanggal)->format('l, d F Y, H:i') ?? '-' }}</td>
                            <td>{{ $sgd?->kelompok?->nama }} ({{ $sgd?->kelompok?->mahasiswas?->count() ?? '0' }})</td>
                            <td>{{ $sgd?->skenario ?? '-' }}</td>
                            <td>
                                <span
                                    class="badge badge-{{ \App\Models\Sgd::STATUS_SELECT[$sgd?->status]['warna'] }} fs-8 fw-bolder my-2">{{
                                    $sgd?->status }}</span>
                            </td>
                            <td>
                                <a href="#" class="btn btn-sm btn-light btn-active-light-primary"
                                    data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Aksi
                                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
                                    <span class="svg-icon svg-icon-5 m-0">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none">
                                            <path
                                                d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                                                fill="black" />
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->
                                </a>
                                <!--begin::Menu-->
                                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4"
                                    data-kt-menu="true">
                                    <div class="menu-item px-3">
                                        <a href="{{ route('sgd.detail', [$sgd->kode]) }}"
                                            class="menu-link px-3">Detail</a>
                                    </div>
                                    <div class="menu-item px-3">
                                        <a href="{{ route('sgd.ubahStatus', [$sgd->kode]) }}" class="menu-link px-3">{{
                                            $sgd->status == 'Buka' ? 'Tutup' : 'Buka' }}</a>
                                    </div>
                                    <!--begin::Menu item-->
                                    <div class="menu-item px-3">
                                        <a href="{{ route('sgd.edit', $sgd?->kode) }}" class="menu-link px-3">Edit</a>
                                    </div>
                                    <!--end::Menu item-->
                                    <!--begin::Menu item-->
                                    <div class="menu-item px-3">
                                        <a href="{{ route('sgd.hapus', [$sgd?->kode]) }}"
                                            onclick="return confirm('Yakin akan menghapus?');"
                                            class="menu-link px-3 text-danger">Hapus</a>
                                    </div>
                                    <!--end::Menu item-->
                                </div>
                                <!--end::Menu-->
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <!--end::Post-->
</div>
<!--end::Container-->
@endsection

@section('styles')
<link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
<script src="/assets/plugins/custom/datatables/datatables.bundle.js"></script>
<script>
    "use strict";

        $("#metode").select2({
            tags: true,
        });

        // Class definition
        var KTDatatablesButtons = function() {
            // Shared variables
            var table;
            var datatable;

            // Private functions
            var initDatatable = function() {
                // Set date data order
                const tableRows = table.querySelectorAll('tbody tr');

                tableRows.forEach(row => {
                    const dateRow = row.querySelectorAll('td');
                    const realDate = moment(dateRow[3].innerHTML, "DD MMM YYYY, LT")
                .format(); // select date from 4th column in table
                    dateRow[3].setAttribute('data-order', realDate);
                });

                // Init datatable --- more info on datatables: https://datatables.net/manual/
                datatable = $(table).DataTable({
                    "info": false,
                    'order': [],
                    'pageLength': 10,
                });
            }

            // Search Datatable --- official docs reference: https://datatables.net/reference/api/search()
            var handleSearchDatatable = () => {
                const filterSearch = document.querySelector('[data-kt-filter="search"]');
                filterSearch.addEventListener('keyup', function(e) {
                    datatable.search(e.target.value).draw();
                });
            }

            // Public methods
            return {
                init: function() {
                    table = document.querySelector('#kt_datatable_example_1');

                    if (!table) {
                        return;
                    }

                    initDatatable();
                    handleSearchDatatable();
                }
            };
        }();

        // On document ready
        KTUtil.onDOMContentLoaded(function() {
            KTDatatablesButtons.init();
        });
</script>
<script>
    function handler() {
            return {
                fields: [],
                addNewField() {
                    this.fields.push({
                        kriterias: '',
                    });
                },
                removeField(index) {
                    this.fields.splice(index, 1);
                }
            }
        }
</script>
@endsection
