@extends('layouts.admin')

@section('title')
    Detail
@endsection

@section('content')
    <!--begin::Toolbar-->
    <div class="toolbar py-5 py-lg-5" id="kt_toolbar">
        <!--begin::Container-->
        <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column me-3">
                <!--begin::Title-->
                <h1 class="d-flex text-dark fw-bolder my-1 fs-3">Detail</h1>
                <!--end::Title-->
            </div>
            <!--end::Page title-->
            <div class="d-flex align-items-center py-2 py-md-1">
                <div>
                    <a href="{{ route('folder.show', $kelas->folder->kode) }}" class="btn btn-sm btn-secondary fw-bolder">
                        Kembali
                        <span class="svg-icon svg-icon-1hx svg-icon-gray-500 me-1">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                stroke="currentColor" class="w-6 h-6">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                    d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3" />
                            </svg>

                        </span>
                    </a>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Container-->
    <div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
        <div class="content flex-row-fluid" id="kt_content">

            @include('admin.kelas.partials._detail')

            @include('admin.kelas.partials._nav')

            <!--begin::Contacts App- Getting Started-->
            <div class="d-flex flex-wrap flex-stack my-5">
                <!--begin::Heading-->
                <span class="d-inline-block position-relative">
                    <!--begin::Label-->
                    <span class="d-inline-block mb-2 fs-2x fw-bolder text-dark">
                        Pengumuman
                    </span>
                    <!--end::Label-->

                    <!--begin::Line-->
                    <span
                        class="d-inline-block position-absolute h-8px bottom-0 end-0 start-0 bg-primary translate rounded"></span>
                    <!--end::Line-->
                </span>
            </div>
            <div class="mb-10">
                <form method="post" action="{{ route('pengumuman.simpan', $kelas->id) }}"
                    enctype='multipart/form-data'>
                    @csrf
                    <div id="isi_editor"></div>
                    <input type="hidden" id="isi_html" name="isi"></input>
                    <input type="file" name="lampiran" class="form-control form-control-solid mt-5" id="">
                    <!--begin::Submit-->
                    <div class="d-flex justify-content-end ">
                        <button type="submit" class="btn btn-primary mt-5">Kirim</button>
                    </div>
                    <!--end::Submit-->
                </form>
            </div>
            @forelse ($pengumumanItems as $pengumuman)
                <div class="card bgi-no-repeat card-xl-stretch mb-xl-8"
                    style="background-position: right top; background-size: 30% auto; background-image: url(/assets/media/svg/shapes/abstract-2.svg">
                    <!--begin::Body-->
                    <div style="top: 0; right: 0; position: absolute">
                        <a href="{{ route('pengumuman.hapus', $pengumuman->kode) }}"
                            class="btn btn-icon btn-color-danger btn-active-color-danger" data-bs-toggle="tooltip"
                            data-bs-custom-class="tooltip-dark" data-bs-placement="right" title="Hapus"
                            onclick="return confirm('Yakin akan menghapus?');">
                            <!--begin::Svg Icon | path: icons/duotune/general/gen023.svg-->
                            <span class="svg-icon svg-icon-2x">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                    fill="currentColor" class="bi bi-x-circle-fill" viewBox="0 0 16 16">
                                    <path
                                        d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293 5.354 4.646z" />
                                </svg>
                            </span>
                            <!--end::Svg Icon-->
                        </a>
                    </div>
                    <div class="card-body">
                        <a href="#"
                            class="card-title fw-bolder text-dark text-hover-primary fs-4">{!! $pengumuman->isi ?? '-' !!}</a>
                        <div>
                            <span
                                class="fw-bolder text-muted">{{ Date::parse($pengumuman->created_at)->diffForHumans() }}</span>

                        </div>
                        @if ($pengumuman->lampiran)
                            <a href="{{ url('storage/' . $pengumuman->lampiran ?? '') }}"
                                class="btn btn-sm btn-primary mt-5">Unduh lampiran</a>
                        @endif
                        {{-- <p class="text-dark-75 fw-bold fs-5 m-0">Great blog posts don’t just happen Even the best bloggers need it</p> --}}
                    </div>
                    <!--end::Body-->
                </div>
            @empty
                Belum ada pengumuman.
            @endforelse
        </div>
        <!--end::Post-->
    </div>
    <!--end::Container-->
@endsection

@section('styles')
@endsection

@section('scripts')
    <script src="/assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js"></script>
    <script src="/js/quill/image-resize.min.js"></script>
    <script src="/js/quill/image-drop.min.js"></script>
    <script>
        var toolbarOptions = [
            ['bold', 'italic', 'underline', 'strike'], // toggled buttons
            ['blockquote', 'code-block'],

            [{
                'header': 1
            }, {
                'header': 2
            }], // custom button values
            [{
                'list': 'ordered'
            }, {
                'list': 'bullet'
            }],
            [{
                'script': 'sub'
            }, {
                'script': 'super'
            }], // superscript/subscript
            [{
                'indent': '-1'
            }, {
                'indent': '+1'
            }], // outdent/indent
            [{
                'direction': 'rtl'
            }], // text direction

            [{
                'size': ['small', false, 'large', 'huge']
            }], // custom dropdown
            [{
                'header': [1, 2, 3, 4, 5, 6, false]
            }],

            [{
                'color': []
            }, {
                'background': []
            }], // dropdown with defaults from theme
            [{
                'font': []
            }],
            [{
                'align': []
            }],
            ['image', 'video', 'link'],

            ['clean'] // remove formatting button
        ];
        var quill = new Quill('#isi_editor', {
            theme: 'snow',
            placeholder: 'Tulis pengumuman disini...',
            modules: {
                toolbar: toolbarOptions,
                imageResize: {
                    displaySize: true
                },
                imageDrop: true
            },
        });
        quill.on('text-change', function(delta, oldDelta, source) {
            document.getElementById("isi_html").value = quill.root.innerHTML;
        });
    </script>
@endsection
