@extends('layouts.admin')

@section('title')
    Kehadiran
@endsection

@section('content')
    <!--begin::Toolbar-->
    <div class="toolbar py-5 py-lg-5" id="kt_toolbar">
        <!--begin::Container-->
        <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column me-3">
                <!--begin::Title-->
                <h1 class="d-flex text-dark fw-bolder my-1 fs-3">Kehadiran</h1>
                <!--end::Title-->
            </div>
            <!--end::Page title-->

            <div class="d-flex align-items-center py-2 py-md-1">
                <div>
                    <a href="{{ route('kelas.kehadiranExport', $kelas->kode) }}" class="btn btn-primary fw-bolder btn-hover-scale me-2">
                        <!--begin::Svg Icon | path: assets/media/icons/duotune/arrows/arr087.svg-->
                        <span class="svg-icon svg-icon-muted svg-icon-2hx">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path opacity="0.3" d="M19 15C20.7 15 22 13.7 22 12C22 10.3 20.7 9 19 9C18.9 9 18.9 9 18.8 9C18.9 8.7 19 8.3 19 8C19 6.3 17.7 5 16 5C15.4 5 14.8 5.2 14.3 5.5C13.4 4 11.8 3 10 3C7.2 3 5 5.2 5 8C5 8.3 5 8.7 5.1 9H5C3.3 9 2 10.3 2 12C2 13.7 3.3 15 5 15H19Z" fill="black"/>
                                <path d="M13 17.4V12C13 11.4 12.6 11 12 11C11.4 11 11 11.4 11 12V17.4H13Z" fill="black"/>
                                <path opacity="0.3" d="M8 17.4H16L12.7 20.7C12.3 21.1 11.7 21.1 11.3 20.7L8 17.4Z" fill="black"/>
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                        Export Kehadiran
                    </a>
                    <a href="{{ route('folder.show', $kelas->folder->kode) }}" class="btn btn-secondary fw-bolder">
                        Kembali
                        <span class="svg-icon svg-icon-2hx svg-icon-gray-500 me-1">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                stroke="currentColor" class="w-6 h-6">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                    d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3" />
                            </svg>

                        </span>
                    </a>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Container-->
    <div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
        <div class="content flex-row-fluid" id="kt_content">

            @include('admin.kelas.partials._detail')

            @include('admin.kelas.partials._nav')

            <div class="card card-flush">
                <div class="card-header align-items-center py-5 gap-2 gap-md-5">
                    <div class="card-title">
                        <!--begin::Search-->
                        <div class="d-flex align-items-center position-relative my-1">
                            <span class="svg-icon svg-icon-1 position-absolute ms-4">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor"></rect>
                                    <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="currentColor"></path>
                                </svg>
                            </span>
                            <input type="text" data-kt-filter="search" class="form-control form-control-solid w-250px ps-14"
                                placeholder="Cari..." />
                        </div>
                        <!--end::Search-->
                    </div>

                </div>
                <div class="card-body">
                    <button class="btn btn-danger" id="deleteAllSelectedRecord">Hapus data terpilih</button>
                    <table id="kt_datatable_example_1" class="table table-striped table-row-bordered gy-5 gs-7">
                        <thead>
                            <tr class="fw-bold fs-6 text-gray-800 text-center">
                                <th width="10">
                                    <input type="checkbox" id="selectAllIds">
                                </th>
                                <th width="10"></th>
                                <th width="10">No</th>
                                <th class="min-w-150px">NIM</th>
                                <th class="min-w-300px">Nama Mahasiswa</th>
                                <th class="min-w-100px">Total</th>
                                <th class="min-w-100px">Kehadiran (%)</th>
                                <th class="min-w-100px">Izin (%)</th>
                                <th class="min-w-100px">Tidak Hadir (%)</th>
                                <th class="min-w-100px"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($mahasiswas as $mahasiswa)
                                <tr class="text-center" id="mahasiswa_ids{{ $mahasiswa->id }}">
                                    <td>
                                        <input type="checkbox" name="ids" class="checkbox_ids" value="{{ $mahasiswa->id }}">
                                    </td>
                                    <td>
                                        <a href="{{ route('mahasiswa.hapus', $mahasiswa->kode) }}"
                                            onclick="return confirm('Yakin akan menghapus?');" data-bs-toggle="tooltip"
                                            data-bs-custom-class="tooltip-dark" data-bs-placement="left" title="Hapus">
                                            <span class="svg-icon svg-icon-danger svg-icon-1">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                    fill="none">
                                                    <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black" />
                                                    <rect x="7" y="15.3137" width="12" height="2" rx="1"
                                                        transform="rotate(-45 7 15.3137)" fill="black" />
                                                    <rect x="8.41422" y="7" width="12" height="2" rx="1"
                                                        transform="rotate(45 8.41422 7)" fill="black" />
                                                </svg>
                                            </span>
                                        </a>
                                    </td>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $mahasiswa->user->username ?? '-' }}</td>
                                    <td align="left">{{ $mahasiswa->user->name ?? '-' }}</td>

                                    @php
                                        $absensi = \App\Models\Absensi::query()
                                            ->where('mahasiswa_id', $mahasiswa?->id)
                                            ->get();
                                        $totalAbsen = $mahasiswa->berita_acaras()->count();
                                    @endphp
                                    <td>
                                        {{ $totalAbsen }}
                                    </td>
                                    <td>
                                        <span class="text-dark fw-bolder">{{ $hadir = $absensi->where('status', 'Hadir')->count() }}</span>/{{ $totalAbsen }} ({{ $totalAbsen ? round($hadir / $totalAbsen * 100) : 0 }}%)
                                    </td>
                                    <td>
                                        <span class="text-dark fw-bolder">{{ $izin = $absensi->where('status', 'izin')->count() }}</span>/{{ $totalAbsen }} ({{ $totalAbsen ? round($izin / $totalAbsen * 100) : 0 }}%)
                                    </td>
                                    <td>
                                        <span class="text-dark fw-bolder">{{ $tidakHadir = $totalAbsen - $absensi->count() }}</span>/{{ $totalAbsen }} ({{ $totalAbsen ? round($tidakHadir / $totalAbsen * 100) : 0 }}%)
                                    </td>

                                    <td>
                                        @if ($totalAbsen > 0)
                                            <button class="btn btn-sm btn-light-primary" data-bs-toggle="modal" data-bs-target="#modal_detail_absensi{{ $mahasiswa?->id }}">
                                                Detail
                                            </button>
                                            <div class="modal fade" tabindex="-1" id="modal_detail_absensi{{ $mahasiswa?->id }}">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Detail Absensi {{ $mahasiswa?->user?->name }}</h5>

                                                            <!--begin::Close-->
                                                            <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                                                data-bs-dismiss="modal" aria-label="Close">
                                                                <span class="svg-icon svg-icon-2x">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                        height="24" viewBox="0 0 24 24" fill="none">
                                                                        <rect opacity="0.5" x="7.05025" y="15.5356" width="12"
                                                                            height="2" rx="1"
                                                                            transform="rotate(-45 7.05025 15.5356)"
                                                                            fill="black" />
                                                                        <rect x="8.46447" y="7.05029" width="12" height="2"
                                                                            rx="1" transform="rotate(45 8.46447 7.05029)"
                                                                            fill="black" />
                                                                    </svg>
                                                                </span>
                                                            </div>
                                                            <!--end::Close-->
                                                        </div>

                                                        <div class="modal-body">
                                                            <div class="row">

                                                                <div class="col-xl-12 mt-10">
                                                                    @foreach ($mahasiswa->berita_acaras() as $item)
                                                                    <div class="d-flex align-items-sm-center mb-7">
                                                                        <div class="symbol symbol-60px symbol-2by3 me-4">
                                                                            <div class="symbol-label">
                                                                                {{ $item?->metode }}
                                                                            </div>
                                                                        </div>
                                                                        <div class="d-flex flex-row-fluid flex-wrap align-items-center">
                                                                            <div class="flex-grow-1 me-2">
                                                                                <a href="#" class="text-gray-800 fw-bolder text-hover-primary fs-6">{{ $item?->judul }}</a>
                                                                                <span class="text-muted fw-bold d-block pt-1">{{ $item?->user?->name }}</span>
                                                                                <span class="text-muted fw-bold d-block pt-1">{{ $item?->tanggal?->format('d/M/Y') }}</span>
                                                                            </div>
                                                                            @php
                                                                                $absen = \App\Models\Absensi::query()
                                                                                    ->where('berita_acara_id', $item?->id)
                                                                                    ->where('mahasiswa_id', $mahasiswa?->id)
                                                                                    ->first();
                                                                            @endphp
                                                                            @if ($absen)
                                                                            <span class="badge badge-{{ \App\Models\Absensi::STATUS_SELECT[$absen?->status]['warna'] }} fs-8 fw-bolder my-2">{{ $absen?->status }} <br> ({{ $absen?->created_at?->format('d/M/Y H:i:s') }})</span>
                                                                            @else
                                                                            <span class="badge badge-danger fs-8 fw-bolder my-2">Belum Absen</span>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Tutup</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                Mahasiswa belum ada.
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <!--end::Post-->
    </div>
    <!--end::Container-->
@endsection

@section('styles')
    <link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
    <script>
        $(function(e) {
            $("#selectAllIds").click(function(){
                $(".checkbox_ids").prop("checked", $(this).prop("checked"))
            })

            $("#deleteAllSelectedRecord").click(function(e) {
                e.preventDefault()

                var all_ids = [];

                $("input:checkbox[name=ids]:checked").each(function() {
                    all_ids.push($(this).val())
                })

                if (all_ids === undefined || all_ids.length == 0) {
                    alert("Tidak ada data terpilih")
                } else {
                    $.ajax({
                        url: "{{ route('kelas.mahasiswaHapus') }}",
                        type: "DELETE",
                        data: {
                            ids: all_ids,
                            _token: "{{ csrf_token() }}"
                        },
                        success: function(res) {
                            $.each(all_ids, function(key, val) {
                                $("#mahasiswa_ids" + val).remove()
                            })
                        }
                    })
                }

            })
        })
    </script>
    <script src="/assets/plugins/custom/datatables/datatables.bundle.js"></script>
    <script>
        "use strict";

        // Class definition
        var KTDatatablesButtons = function() {
            // Shared variables
            var table;
            var datatable;

            // Private functions
            var initDatatable = function() {
                // Set date data order
                const tableRows = table.querySelectorAll('tbody tr');

                tableRows.forEach(row => {
                    const dateRow = row.querySelectorAll('td');
                    const realDate = moment(dateRow[3].innerHTML, "DD MMM YYYY, LT")
                .format(); // select date from 4th column in table
                    dateRow[3].setAttribute('data-order', realDate);
                });

                // Init datatable --- more info on datatables: https://datatables.net/manual/
                datatable = $(table).DataTable({
                    "info": false,
                    'order': [],
                    'pageLength': 10,
                });
            }

            // Search Datatable --- official docs reference: https://datatables.net/reference/api/search()
            var handleSearchDatatable = () => {
                const filterSearch = document.querySelector('[data-kt-filter="search"]');
                filterSearch.addEventListener('keyup', function(e) {
                    datatable.search(e.target.value).draw();
                });
            }

            // Public methods
            return {
                init: function() {
                    table = document.querySelector('#kt_datatable_example_1');

                    if (!table) {
                        return;
                    }

                    initDatatable();
                    handleSearchDatatable();
                }
            };
        }();

        // On document ready
        KTUtil.onDOMContentLoaded(function() {
            KTDatatablesButtons.init();
        });
    </script>
@endsection
