@extends('layouts.admin')

@section('title')
Detail
@endsection

@section('content')
<!--begin::Container-->
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
    <div class="content flex-row-fluid" id="kt_content">
        <!--begin::Contacts App- Getting Started-->
        <div class="card card-flush pb-0 bgi-position-y-center bgi-no-repeat mb-10 bg-light"
            style="background-size: auto calc(100% + 10rem); background-position-x: 100%; background-image: url('/assets/media/illustrations/sketchy-1/4.png');">
            <!--begin::Card header-->
            <div class="card-header pt-10">
                <div class="d-flex align-items-center">
                    <!--begin::Icon-->
                    {{-- <div class="symbol symbol-circle me-5">
                        <div class="symbol-label bg-transparent text-primary border border-secondary border-dashed">
                            <!--begin::Svg Icon | path: icons/duotune/abstract/abs020.svg-->
                            <span class="svg-icon svg-icon-2x svg-icon-primary">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none">
                                    <path
                                        d="M17.302 11.35L12.002 20.55H21.202C21.802 20.55 22.202 19.85 21.902 19.35L17.302 11.35Z"
                                        fill="black" />
                                    <path opacity="0.3"
                                        d="M12.002 20.55H2.802C2.202 20.55 1.80202 19.85 2.10202 19.35L6.70203 11.45L12.002 20.55ZM11.302 3.45L6.70203 11.35H17.302L12.702 3.45C12.402 2.85 11.602 2.85 11.302 3.45Z"
                                        fill="black" />
                                </svg>
                            </span>
                            <!--end::Svg Icon-->
                        </div>
                    </div> --}}
                    <!--end::Icon-->
                    <!--begin::Title-->
                    <div class="d-flex flex-column">
                        <h2 class="mb-1">
                            @if (!$kelas->active)
                            <span class="badge badge-danger mb-2">Tidak Aktif</span> <br>
                            @endif
                            {{ $kelas->nama ?? '-' }}
                        </h2>
                        <div class="text-muted fw-bolder">
                            Kode Kelas #<a href="#">{{ $kelas->kode ?? '-' }}</a>
                            <span class="mx-3">|</span>
                            <a href="#">{{ $kelas->blok->nama ?? '-' }}</a>
                            <span class="mx-3">|</span>
                            <span>Semester {{ $kelas->blok->semester ?? '-' }}</a>
                                <span class="mx-3">|</span>
                                Kelompok #
                                @foreach ($kelompokItems as $kelompokItem)
                                {{ $kelompokItem->nama }}
                                @if (!$loop->last)
                                ,
                                @endif
                                @endforeach
                        </div>
                        @if ($kelas->deskripsi)
                        <br>
                        <span class="text-muted fw-bolder">Deskripsi:</span>
                        <span class="text-muted">{{ $kelas->deskripsi }}</span>
                        @endif
                    </div>
                    <!--end::Title-->
                </div>
            </div>
            <!--end::Card header-->
            <div class="card-body pb-0">
            </div>
        </div>
        <ul class="nav nav-tabs nav-pills border-0 mb-5 fs-6">
            <li class="nav-item">
                <a href="{{ route('home') }}" class="btn btn-sm btn-secondary fw-bolder">
                    Kembali
                    <span class="svg-icon svg-icon-1hx svg-icon-gray-500 me-1">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                            stroke="currentColor" class="w-6 h-6">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3"></path>
                        </svg>

                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" data-bs-toggle="tab" href="#tab_sumatif">Sumatif</a>
            </li>
            {{-- <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#tab_formatif">Formatif</a>
            </li> --}}
            <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#tab_absensi">Absensi</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#tab_pengumuman">Pengumuman</a>
            </li>
        </ul>

        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="tab_sumatif" role="tabpanel">
                {{-- Tugas --}}
                <div class="row g-6 g-xl-9" data-masonry='{"percentPosition": true }'>

                    @forelse ($tugasItems as $tugas)
                    <div class="col-12 col-xl-4">
                        <!--begin::Card-->
                        @if ($tugas->tipe == 'kuis' && $tugas->mahasiswa_tugas->tanggal_selesai_test == null)
                        <a data-bs-toggle="modal" data-bs-target="#kt_modal_{{ $tugas->kode }}"
                            class="card border-hover-{{ \App\Models\Tugas::TIPE_OPTION[$tugas->tipe]['warna'] }} border-{{ \App\Models\Tugas::TIPE_OPTION[$tugas->tipe]['warna'] }} shadow-sm card-dashed">
                            @else
                            <a href="{{ route('tugas.proses', $tugas->kode) }}"
                                class="card border-hover-{{ \App\Models\Tugas::TIPE_OPTION[$tugas->tipe]['warna'] }} border-{{ \App\Models\Tugas::TIPE_OPTION[$tugas->tipe]['warna'] }} shadow-sm card-dashed">
                                @endif
                                <!--begin::Card header-->
                                <div class="card-header border-0 pt-9">
                                    <!--begin::Card Title-->
                                    <div class="card-title m-0">
                                        <!--begin::Avatar-->
                                        <div class="symbol symbol-50px w-50px bg-light">
                                            {{-- <img src="/assets/media/svg/brand-logos/plurk.svg" alt="image"
                                                class="p-3" /> --}}
                                            <x-smart-image
                                                src="{{ public_path(\App\Models\Tugas::TIPE_OPTION[$tugas->tipe]['icon']) }}"
                                                height="100px" />
                                        </div>
                                        <!--end::Avatar-->
                                    </div>
                                    <!--end::Car Title-->
                                    <!--begin::Card toolbar-->
                                    <div class="card-toolbar">
                                        @if (auth()->user()->isMahasiswa())
                                        @if ($tugas->mahasiswa_tugas)
                                        @if ($tugas->mahasiswa_tugas->file_tugas ||
                                        $tugas->mahasiswa_tugas->tanggal_kumpul)
                                        <span
                                            class="badge badge-light-primary fw-bolder me-2 px-4 py-3">Submitted</span>
                                        @endif
                                        @endif
                                        @endif
                                        <span
                                            class="badge badge-light-{{ \App\Models\Tugas::TIPE_OPTION[$tugas->tipe]['warna'] }} fw-bolder me-auto px-4 py-3">{{
                                            Str::ucfirst($tugas->tipe ?? '-') }}</span>
                                    </div>
                                    <!--end::Card toolbar-->
                                </div>
                                <!--end:: Card header-->
                                <!--begin:: Card body-->
                                <div class="card-body p-9">
                                    <!--begin::Name-->
                                    <div class="fs-3 fw-bolder text-dark">{{ $tugas->judul ?? '-' }}</div>
                                    <!--end::Name-->
                                    <!--begin::Description-->
                                    <p class="text-gray-400 fw-bold fs-5 mt-1 mb-7">Kode #<span class="text-primary">{{
                                            $tugas->kode ?? '-' }}</span></p>
                                    <!--end::Description-->
                                    <!--begin::Info-->
                                    <div class="d-flex flex-wrap mb-5">
                                        <!--begin::Budget-->
                                        <div
                                            class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-7 mb-3">
                                            <div class="fs-6 text-gray-800 fw-bolder">Aspek Penilaian</div>
                                            <div class="fw-bold text-gray-400">{{ $tugas->kategori->nama }}</div>
                                        </div>
                                        <!--end::Budget-->
                                        @if (auth()->user()->isMahasiswa())
                                        @if ($tugas->mahasiswa_tugas && $tugas->mahasiswa_tugas->tanggal_kumpul)
                                        <!--begin::Budget-->
                                        <div
                                            class="border border-primary bg-light-primary border-dashed rounded min-w-125px py-3 px-4 mb-3">
                                            <div class="fs-6 text-gray-800 fw-bolder">Poin Anda</div>
                                            <div class="fw-bold text-gray-400">
                                                {{ $tugas->mahasiswa_tugas->total_poin }}
                                            </div>
                                        </div>
                                        <!--end::Budget-->
                                        @endif
                                        @endif
                                    </div>
                                    <!--end::Info-->
                                    <!--begin::Progress-->
                                    <div class="h-4px w-100 bg-light mb-5">
                                        <div class="bg-{{ \App\Models\Tugas::TIPE_OPTION[$tugas->tipe]['warna'] }} rounded h-4px"
                                            role="progressbar" style="width: 100%" aria-valuenow="50" aria-valuemin="0"
                                            aria-valuemax="100"></div>
                                    </div>
                                    <!--end::Progress-->
                                    @if ($tugas->tipe != 'input')
                                    <div>
                                        <p class="text-gray-400 fw-bold">Status:
                                            @if (!$tugas->status_ujian && $tugas->mulai_pada)
                                            <span class="badge badge-danger">Ditutup</span>
                                            @elseif ($tugas->status_ujian && $tugas->mulai_pada)
                                            <span class="badge badge-primary">Dibuka
                                                {{ $tugas->mulai_pada ? Date::parse($tugas->mulai_pada)->format('l, d M
                                                Y') : '-' }}</span>
                                            @else
                                            <span class="badge badge-warning">Belum Dibuka</span>
                                            @endif
                                        </p>
                                        <p class="text-gray-400 fw-bold">Deadline:
                                            <span class="text-{{ $tugas->color }}">
                                                {{ $tugas->batas_akhir ? Date::parse($tugas->batas_akhir)->format('l, d
                                                M Y H:i') : '-' }}
                                            </span>
                                        </p>
                                        @if ($tugas->tipe == 'rubrik' || $tugas->tipe == 'rubrik2')
                                        <p class="text-gray-400 fw-bold">Dosen Penilai:
                                            {!! App\Services\MahasiswaService::getDosenPenilai($tugas->id, auth()->id()) !!}
                                        </p>
                                        @endif
                                    </div>
                                    @endif
                                </div>
                                <!--end:: Card body-->
                            </a>
                            @if ($tugas->tipe == 'kuis')
                            <div class="modal fade" tabindex="-1" id="kt_modal_{{ $tugas->kode }}">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Yakin akan memulai test?</h5>

                                            <!--begin::Close-->
                                            <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                                data-bs-dismiss="modal" aria-label="Close">
                                                <span class="svg-icon svg-icon-2x"></span>
                                            </div>
                                            <!--end::Close-->
                                        </div>

                                        <div class="modal-body">
                                            <div class="alert alert-danger d-flex align-items-center">
                                                <!--begin::Svg Icon | path: icons/duotune/general/gen048.svg-->
                                                <span class="svg-icon svg-icon-2hx svg-icon-danger me-4">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none">
                                                        <path opacity="0.3"
                                                            d="M20.5543 4.37824L12.1798 2.02473C12.0626 1.99176 11.9376 1.99176 11.8203 2.02473L3.44572 4.37824C3.18118 4.45258 3 4.6807 3 4.93945V13.569C3 14.6914 3.48509 15.8404 4.4417 16.984C5.17231 17.8575 6.18314 18.7345 7.446 19.5909C9.56752 21.0295 11.6566 21.912 11.7445 21.9488C11.8258 21.9829 11.9129 22 12.0001 22C12.0872 22 12.1744 21.983 12.2557 21.9488C12.3435 21.912 14.4326 21.0295 16.5541 19.5909C17.8169 18.7345 18.8277 17.8575 19.5584 16.984C20.515 15.8404 21 14.6914 21 13.569V4.93945C21 4.6807 20.8189 4.45258 20.5543 4.37824Z"
                                                            fill="black"></path>
                                                        <path
                                                            d="M10.5606 11.3042L9.57283 10.3018C9.28174 10.0065 8.80522 10.0065 8.51412 10.3018C8.22897 10.5912 8.22897 11.0559 8.51412 11.3452L10.4182 13.2773C10.8099 13.6747 11.451 13.6747 11.8427 13.2773L15.4859 9.58051C15.771 9.29117 15.771 8.82648 15.4859 8.53714C15.1948 8.24176 14.7183 8.24176 14.4272 8.53714L11.7002 11.3042C11.3869 11.6221 10.874 11.6221 10.5606 11.3042Z"
                                                            fill="black"></path>
                                                    </svg>
                                                </span>
                                                <!--end::Svg Icon-->
                                                <div class="d-flex flex-column">
                                                    <h4 class="mb-1 text-danger">Peringatan</h4>
                                                    <ul>
                                                        <li>Durasi pengerjaan test <b>{{ $tugas->durasi ?? '-' }}
                                                                MENIT</b></li>
                                                        <li>Waktu pengerjaan test terhitung ketika anda memulai test
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-light"
                                                data-bs-dismiss="modal">Tutup</button>
                                            <a href="{{ route('tugas.proses', $tugas->kode) }}" type="button"
                                                class="btn btn-primary">Ya, Mulai</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <!--end::Card-->
                    </div>
                    @empty
                    <div class="col-12">
                        Belum ada tugas.
                    </div>
                    @endforelse
                </div>
                <div class="mt-10 mb-10">
                    {{ $tugasItems->links() }}
                </div>
            </div>
            <div class="tab-pane fade" id="tab_pengumuman" role="tabpanel">
                {{-- Pengumuman --}}
                @if (!auth()->user()->isMahasiswa())
                <div class="mb-10">
                    <form method="post" action="{{ route('pengumuman.simpan', $kelas->id) }}"
                        enctype='multipart/form-data'>
                        @csrf
                        <div id="isi_editor"></div>
                        <input type="hidden" id="isi_html" name="isi"></input>
                        <input type="file" name="lampiran" class="form-control form-control-solid mt-5" id="">
                        <!--begin::Submit-->
                        <div class="d-flex justify-content-end ">
                            <button type="submit" class="btn btn-primary mt-5">Simpan</button>
                        </div>
                        <!--end::Submit-->
                    </form>
                </div>
                @endif

                @forelse ($pengumumanItems as $pengumuman)
                <div class="card bgi-no-repeat card-xl-stretch mb-xl-8 mb-5"
                    style="background-position: right top; background-size: 30% auto; background-image: url(/assets/media/svg/shapes/abstract-2.svg">
                    <!--begin::Body-->
                    @if (!auth()->user()->isMahasiswa())
                    <div style="top: 0; right: 0; position: absolute">
                        <a href="{{ route('pengumuman.hapus', $pengumuman->kode) }}"
                            class="btn btn-icon btn-color-danger btn-active-color-danger"
                            onclick="return confirm('Yakin akan menghapus?');">
                            <!--begin::Svg Icon | path: icons/duotune/general/gen023.svg-->
                            <span class="svg-icon svg-icon-2x">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                    class="bi bi-x-circle-fill" viewBox="0 0 16 16">
                                    <path
                                        d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293 5.354 4.646z" />
                                </svg>
                            </span>
                            <!--end::Svg Icon-->
                        </a>
                    </div>
                    @endif
                    <div class="card-body">
                        <a href="#" class="card-title fw-bolder text-dark fs-4">{!! $pengumuman->isi ?? '-' !!}</a>
                        <div>
                            <span class="fw-bolder text-muted">{{ Date::parse($pengumuman->created_at)->diffForHumans()
                                }}</span>

                        </div>
                        @if ($pengumuman->lampiran)
                        <a href="{{ url('storage/' . $pengumuman->lampiran ?? '') }}"
                            class="btn btn-sm bg-gray-300 mt-5">Unduh lampiran</a>
                        @endif
                        {{-- <p class="text-dark-75 fw-bold fs-5 m-0">Great blog posts don’t just happen Even the best
                            bloggers need it</p> --}}
                    </div>
                    <!--end::Body-->
                </div>
                @empty
                Belum ada pengumuman.
                @endforelse
            </div>

            {{-- <div class="tab-pane fade" id="tab_formatif" role="tabpanel">
                <!--begin::Search-->
                <div class="d-flex align-items-center position-relative my-1">
                    <span class="svg-icon svg-icon-1 position-absolute ms-4">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                            fill="none">
                            <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1"
                                transform="rotate(45 17.0365 15.1223)" fill="currentColor"></rect>
                            <path
                                d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                fill="currentColor"></path>
                        </svg>
                    </span>
                    <input type="text" data-kt-filter="search" class="form-control form-control-solid w-250px ps-14"
                        placeholder="Cari..." />
                </div>
                <!--end::Search-->
                <table id="kt_datatable_example_1" class="table table-striped table-row-bordered gy-5 gs-7">
                    <thead>
                        <tr class="fw-bold fs-6 text-gray-800">
                            <th width="10">No</th>
                            @if (auth()->user()->isMahasiswa())
                            <th class="min-w-100px">Total Nilai</th>
                            @endif
                            <th class="min-w-100px">Judul</th>
                            <th class="min-w-100px">Ruang</th>
                            <th class="min-w-100px">Semester</th>
                            <th class="min-w-100px">Blok</th>
                            <th class="min-w-100px">Tutor</th>
                            <th class="min-w-100px">Hari, Tanggal, Waktu</th>
                            @if (auth()->user()->isDosen())
                            <th class="min-w-100px">Kelompok (Peserta)</th>
                            <th class="min-w-100px">Status</th>
                            <th class="min-w-100px"></th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($sgds as $sgd)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            @if (auth()->user()->isMahasiswa())
                            <td class="text-center">
                                @php
                                    $mahasiswa = \App\Models\Mahasiswa::query()
                                        ->where('kelas_id', $kelas?->id)
                                        ->where('user_id', auth()->id())
                                        ->first();
                                    $totalNilaiSgd = \App\Models\NilaiSgd::query()
                                        ->where('sgd_id', $sgd?->id)
                                        ->where('mahasiswa_id', $mahasiswa?->id)
                                        ->sum('nilai');
                                    $keterangan = \App\Models\NilaiSgd::query()
                                        ->where('sgd_id', $sgd?->id)
                                        ->where('mahasiswa_id', $mahasiswa?->id)
                                        ->whereNotNull('keterangan')
                                        ->orderBy('id', 'asc')
                                        ->first();
                                @endphp
                                @if ($totalNilaiSgd)
                                    <h6>
                                        {{ $totalNilaiSgd }}
                                    </h6>
                                    <br>
                                    <button class="btn btn-sm btn-info" data-bs-toggle="modal" data-bs-target="#modal_detail_nilai{{ $sgd?->id }}">
                                        Detail Nilai
                                    </button>

                                    <div class="modal fade" tabindex="-1" id="modal_detail_nilai{{ $sgd?->id }}">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Detail Nilai {{ $sgd?->judul }}</h5>

                                                    <!--begin::Close-->
                                                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                                                        aria-label="Close">
                                                        <span class="svg-icon svg-icon-2x">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                                fill="none">
                                                                <rect opacity="0.5" x="7.05025" y="15.5356" width="12" height="2" rx="1"
                                                                    transform="rotate(-45 7.05025 15.5356)" fill="black" />
                                                                <rect x="8.46447" y="7.05029" width="12" height="2" rx="1"
                                                                    transform="rotate(45 8.46447 7.05029)" fill="black" />
                                                            </svg>
                                                        </span>
                                                    </div>
                                                    <!--end::Close-->
                                                </div>

                                                <div class="modal-body">
                                                    <table class="table table-striped">
                                                        @foreach ($sgd?->kriteria_sgds as $kriteriaSgd)
                                                        @php
                                                            $nilaiSgd = \App\Models\NilaiSgd::query()
                                                                ->where('sgd_id', $sgd?->id)
                                                                ->where('kriteria_sgd_id', $kriteriaSgd?->id)
                                                                ->where('mahasiswa_id', $mahasiswa?->id)
                                                                ->first();
                                                        @endphp
                                                        <tr>
                                                            <th class="text-start text-dark fw-bolder"><span>{{ $kriteriaSgd?->kriteria }}</span></th>
                                                            <td class="text-end">
                                                                @if ($nilaiSgd)
                                                                <span class="badge badge-{{ \App\Models\NilaiSgd::NILAI_SELECT[$nilaiSgd->nilai]['warna'] }} fw-bolder me-auto px-4 py-3">
                                                                    {{ $nilaiSgd?->nilai }} - {{ \App\Models\NilaiSgd::NILAI_SELECT[$nilaiSgd->nilai]['ket'] }}
                                                                </span>
                                                                @else
                                                                <span class="badge badge-secondary fw-bolder me-auto px-4 py-3">
                                                                    Belum Dinilai
                                                                </span>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                        <tr>
                                                            <th class="text-start text-dark fw-bolder"><span>Keterangan</span></th>
                                                            <td class="text-end">{{ $keterangan?->keterangan }}</td>
                                                        </tr>
                                                    </table>
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Tutup</button>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <span class="badge badge-secondary fw-bolder me-auto px-4 py-3 mb-2">
                                        Belum Dinilai
                                    </span>
                                @endif
                            </td>
                            @endif
                            <td>{{ $sgd?->judul ?? '-' }}</td>
                            <td>{{ $sgd?->ruang ?? '-' }}</td>
                            <td>{{ $sgd?->semester ?? '-' }}</td>
                            <td>{{ $sgd?->kelas?->blok?->nama ?? '-' }}</td>
                            <td>{{ $sgd?->user?->name ?? '-' }}</td>
                            <td>{{ Date::parse($sgd?->tanggal)->format('l, d F Y, H:i') ?? '-' }}</td>
                            @if (auth()->user()->isDosen())
                            <td>{{ $sgd?->kelompok?->nama }} ({{ $sgd?->kelompok?->mahasiswas?->count() ?? '0' }})</td>
                            <td>{{ $sgd?->status ?? '-' }}</td>
                            <td>
                                <a href="{{ route('sgd.detail', $sgd->kode) }}"
                                    class="btn btn-sm btn-info">
                                    Detail
                                </a>
                            </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div> --}}

            <div class="tab-pane fade" id="tab_absensi" role="tabpanel">
                {{-- Berita Acara --}}
                <!--begin::Search-->
                <div class="d-flex align-items-center position-relative my-1">
                    <span class="svg-icon svg-icon-1 position-absolute ms-4">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                            fill="none">
                            <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1"
                                transform="rotate(45 17.0365 15.1223)" fill="currentColor"></rect>
                            <path
                                d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                fill="currentColor"></path>
                        </svg>
                    </span>
                    <input type="text" data-kt-filter="search2" class="form-control form-control-solid w-250px ps-14"
                        placeholder="Cari..." />
                </div>
                <!--end::Search-->
                <table id="kt_datatable_example_2" class="table table-striped table-row-bordered gy-5 gs-7">
                    <thead>
                        <tr class="fw-bold fs-6 text-gray-800">
                            <th width="10">No</th>
                            @if (auth()->user()->isMahasiswa())
                            <th class="min-w-100px">Status Kehadiran</th>
                            @endif
                            <th class="min-w-100px">Materi</th>
                            <th class="min-w-100px">Judul Topik</th>
                            <th class="min-w-100px">Semester</th>
                            <th class="min-w-100px">Blok</th>
                            <th class="min-w-100px">Metode Pembelajaran</th>
                            <th class="min-w-100px">Dosen Pengampu</th>
                            <th class="min-w-100px">Hari, Tanggal, Waktu</th>
                            @if (auth()->user()->isDosen())
                            <th class="min-w-100px">Kelompok (Peserta)</th>
                            <th class="min-w-100px">Status</th>
                            <th class="min-w-100px"></th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($beritaAcaras as $beritaAcara)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            @if (auth()->user()->isMahasiswa())
                            <td>
                                @php
                                $mahasiswa = \App\Models\Mahasiswa::query()
                                ->where('kelas_id', $kelas?->id)
                                ->where('user_id', auth()->id())
                                ->first();
                                $absensi = \App\Models\Absensi::query()
                                ->where('berita_acara_id', $beritaAcara?->id)
                                ->where('mahasiswa_id', $mahasiswa?->id)
                                ->first();
                                @endphp
                                @if ($absensi)
                                <span
                                    class="badge badge-{{ \App\Models\Absensi::STATUS_SELECT[$absensi->status]['warna'] }} fw-bolder me-auto px-4 py-3  mb-2">
                                    {{ $absensi?->status }} <br><br> ({{ $absensi?->created_at->format('d/M/Y H:i:s')
                                    }})
                                </span>
                                <br>
                                @php
                                $feedback = \App\Models\Feedback::query()
                                ->where('berita_acara_id', $beritaAcara?->id)
                                ->where('user_id', $mahasiswa?->user_id)
                                ->first();
                                @endphp
                                <button class="btn btn-sm btn-secondary" data-bs-toggle="modal"
                                    data-bs-target="#modal_feedback{{ $absensi?->id }}">
                                    {{ $feedback ? 'Ubah' : 'Beri' }} Feedback
                                </button>

                                <div class="modal fade" tabindex="-1" id="modal_feedback{{ $absensi?->id }}">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Feedback untuk {{ $beritaAcara?->judul }}</h5>

                                                <!--begin::Close-->
                                                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                                    data-bs-dismiss="modal" aria-label="Close">
                                                    <span class="svg-icon svg-icon-2x">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                            viewBox="0 0 24 24" fill="none">
                                                            <rect opacity="0.5" x="7.05025" y="15.5356" width="12"
                                                                height="2" rx="1"
                                                                transform="rotate(-45 7.05025 15.5356)" fill="black" />
                                                            <rect x="8.46447" y="7.05029" width="12" height="2" rx="1"
                                                                transform="rotate(45 8.46447 7.05029)" fill="black" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <!--end::Close-->
                                            </div>

                                            <form method="POST" action="{{ route('feedback.simpan') }}"
                                                enctype="multipart/form-data">
                                                @csrf
                                                <div class="modal-body">
                                                    <input type="hidden" name="user_id"
                                                        value="{{ $mahasiswa?->user_id }}">
                                                    <input type="hidden" name="berita_acara_id"
                                                        value="{{ $beritaAcara?->id }}">

                                                    <div class="mb-10">
                                                        <label class="form-label">Feedback</label>
                                                        <textarea name="feedback"
                                                            class="form-control form-control-solid" required>{{ $feedback?->feedback }}</textarea>
                                                    </div>
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                @else
                                <span class="badge badge-secondary fw-bolder me-auto px-4 py-3 mb-2">
                                    Belum Absen
                                </span>
                                @if ($beritaAcara?->isBuka())
                                <a href="{{ route('absensi.scan', [$beritaAcara->kode, $mahasiswa?->id]) }}"
                                    class="btn btn-sm btn-info mb-2">
                                    Absen Sekarang
                                </a>
                                @endif
                                @endif
                            </td>
                            @endif
                            <td>
                                @if ($beritaAcara?->file)
                                <a href="{{ url('storage') . '/' .  $beritaAcara?->file }}" class="btn btn-sm btn-primary" target="_blank">Lihat file</a>
                                @endif
                            </td>
                            <td>{{ $beritaAcara?->judul ?? '-' }}</td>
                            <td>{{ $beritaAcara?->semester ?? '-' }}</td>
                            <td>{{ $beritaAcara?->kelas?->blok?->nama ?? '-' }}</td>
                            <td>{{ $beritaAcara?->metode ?? '-' }}</td>
                            <td>{{ $beritaAcara?->user?->name ?? '-' }}</td>
                            <td>{{ Date::parse($beritaAcara?->tanggal)->format('l, d F Y, H:i') ?? '-' }}</td>
                            @if (auth()->user()->isDosen())
                            <td>{{ $beritaAcara?->kelompok?->nama }} ({{ $beritaAcara?->kelompok?->mahasiswas?->count() ?? '0' }})</td>
                            <td>{{ $beritaAcara?->status ?? '-' }}</td>
                            <td>
                                <a href="{{ route('beritaAcara.detail', $beritaAcara->kode) }}"
                                    class="btn btn-sm btn-info">
                                    Detail
                                </a>
                            </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>


    </div>
    <!--end::Post-->
</div>
<!--end::Container-->
@endsection

@section('styles')
<link href="/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
<script src="/assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js"></script>
<script src="/js/quill/image-resize.min.js"></script>
<script src="/js/quill/image-drop.min.js"></script>
<script>
    var toolbarOptions = [
            ['bold', 'italic', 'underline', 'strike'], // toggled buttons
            ['blockquote', 'code-block'],

            [{
                'header': 1
            }, {
                'header': 2
            }], // custom button values
            [{
                'list': 'ordered'
            }, {
                'list': 'bullet'
            }],
            [{
                'script': 'sub'
            }, {
                'script': 'super'
            }], // superscript/subscript
            [{
                'indent': '-1'
            }, {
                'indent': '+1'
            }], // outdent/indent
            [{
                'direction': 'rtl'
            }], // text direction

            [{
                'size': ['small', false, 'large', 'huge']
            }], // custom dropdown
            [{
                'header': [1, 2, 3, 4, 5, 6, false]
            }],

            [{
                'color': []
            }, {
                'background': []
            }], // dropdown with defaults from theme
            [{
                'font': []
            }],
            [{
                'align': []
            }],
            ['image', 'video', 'link'],

            ['clean'] // remove formatting button
        ];
        var quill = new Quill('#isi_editor', {
            theme: 'snow',
            placeholder: 'Tulis pengumuman disini...',
            modules: {
                toolbar: toolbarOptions,
                imageResize: {
                    displaySize: true
                },
                imageDrop: true
            },
        });
        quill.on('text-change', function(delta, oldDelta, source) {
            document.getElementById("isi_html").value = quill.root.innerHTML;
        });
</script>
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>

<script src="/assets/plugins/custom/datatables/datatables.bundle.js"></script>
<script>
    "use strict";

        // Class definition
        var KTDatatablesButtons = function() {
            // Shared variables
            var table;
            var datatable;

            // Private functions
            var initDatatable = function() {
                // Set date data order
                const tableRows = table.querySelectorAll('tbody tr');

                tableRows.forEach(row => {
                    const dateRow = row.querySelectorAll('td');
                    const realDate = moment(dateRow[3].innerHTML, "DD MMM YYYY, LT")
                .format(); // select date from 4th column in table
                    dateRow[3].setAttribute('data-order', realDate);
                });


                // Init datatable --- more info on datatables: https://datatables.net/manual/
                datatable = $(table).DataTable({
                    "info": false,
                    'order': [],
                    'pageLength': 10,
                });
            }

            // Search Datatable --- official docs reference: https://datatables.net/reference/api/search()
            var handleSearchDatatable = () => {
                const filterSearch = document.querySelector('[data-kt-filter="search"]');
                filterSearch.addEventListener('keyup', function(e) {
                    datatable.search(e.target.value).draw();
                });
            }

            // Public methods
            return {
                init: function() {
                    table = document.querySelector('#kt_datatable_example_1');

                    if (!table) {
                        return;
                    }

                    initDatatable();
                    handleSearchDatatable();
                }
            };
        }();

        var KTDatatablesButtons2 = function() {
            // Shared variables
            var table;
            var datatable;

            // Private functions
            var initDatatable = function() {
                // Set date data order
                const tableRows = table.querySelectorAll('tbody tr');

                tableRows.forEach(row => {
                    const dateRow = row.querySelectorAll('td');
                    const realDate = moment(dateRow[3].innerHTML, "DD MMM YYYY, LT")
                .format(); // select date from 4th column in table
                    dateRow[3].setAttribute('data-order', realDate);
                });


                // Init datatable --- more info on datatables: https://datatables.net/manual/
                datatable = $(table).DataTable({
                    "info": false,
                    'order': [],
                    'pageLength': 10,
                });
            }

            // Search Datatable --- official docs reference: https://datatables.net/reference/api/search()
            var handleSearchDatatable = () => {
                const filterSearch = document.querySelector('[data-kt-filter="search2"]');
                filterSearch.addEventListener('keyup', function(e) {
                    datatable.search(e.target.value).draw();
                });
            }

            // Public methods
            return {
                init: function() {
                    table = document.querySelector('#kt_datatable_example_2');

                    if (!table) {
                        return;
                    }

                    initDatatable();
                    handleSearchDatatable();
                }
            };
        }();

        // On document ready
        KTUtil.onDOMContentLoaded(function() {
            KTDatatablesButtons.init();
            KTDatatablesButtons2.init();
        });
</script>
@endsection
