@extends('layouts.admin')

@section('title')
    Penilaian Sumatif
@endsection

@section('content')
    <!--begin::Toolbar-->
    <div class="toolbar py-5 py-lg-5" id="kt_toolbar">
        <!--begin::Container-->
        <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column me-3">
                <!--begin::Title-->
                <h1 class="d-flex text-dark fw-bolder my-1 fs-3">Penilaian Sumatif</h1>
                <!--end::Title-->
            </div>
            <!--end::Page title-->
            <div class="d-flex align-items-center py-2 py-md-1">
                <div>
                    <a href="{{ route('folder.show', $kelas->folder->kode) }}" class="btn btn-secondary fw-bolder">
                        Kembali
                        <span class="svg-icon svg-icon-2hx svg-icon-gray-500 me-1">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                stroke="currentColor" class="w-6 h-6">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                    d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3" />
                            </svg>

                        </span>
                    </a>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Container-->
    <div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
        <div class="content flex-row-fluid" id="kt_content">

            @include('admin.kelas.partials._detail')

            @include('admin.kelas.partials._nav')

            <!--begin::Contacts App- Getting Started-->
            @livewire('kelas.penilaian-sumatif-livewire', ['kelas' => $kelas])
            <!--end::Contacts App- Getting Started-->
        </div>
        <!--end::Post-->
    </div>
    <!--end::Container-->
@endsection

@section('styles')
@endsection

@section('scripts')
@endsection
