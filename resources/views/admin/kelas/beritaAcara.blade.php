@extends('layouts.admin')

@section('title')
Berita Acara
@endsection

@section('content')
<!--begin::Toolbar-->
<div class="toolbar py-5 py-lg-5" id="kt_toolbar">
    <!--begin::Container-->
    <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
        <!--begin::Page title-->
        <div class="page-title d-flex flex-column me-3">
            <!--begin::Title-->
            <h1 class="d-flex text-dark fw-bolder my-1 fs-3">Berita Acara</h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <div class="d-flex align-items-center py-2 py-md-1">
            <div>
                <a href="{{ route('folder.show', $kelas->folder->kode) }}" class="btn btn-secondary fw-bolder">
                    Kembali
                    <span class="svg-icon svg-icon-2hx svg-icon-gray-500 me-1">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                            stroke="currentColor" class="w-6 h-6">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3" />
                        </svg>

                    </span>
                </a>
            </div>
        </div>
    </div>
    <!--end::Container-->
</div>
<!--end::Toolbar-->
<!--begin::Container-->
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
    <div class="content flex-row-fluid" id="kt_content">

        @include('admin.kelas.partials._detail')

        @include('admin.kelas.partials._nav')

        @include('layouts.partials.messages')

        <div class="row">
            <div class="col-12 mb-10">
                <form method="POST" action="{{ route('kuliah.simpan') }}">
                    @csrf
                    <div class="input-group">
                        <span class="input-group-text" id="basic-addon3" style="border: none">
                            <i class="las la-wallet fs-1"></i>
                        </span>
                        <input type="hidden" name="kelas_id" value="{{ $kelas->id }}">
                        <input type="hidden" name="tipe" value="berita acara">
                        <input type="text" name="nama" class="form-control form-control-solid"
                            placeholder="Nama kelompok berita acara" required />
                        <button type="submit" class="btn btn-icon btn-primary">
                            <!--begin::Svg Icon | path: icons/duotune/general/gen035.svg-->
                            <span class="svg-icon svg-icon-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none">
                                    <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black" />
                                    <rect x="10.8891" y="17.8033" width="12" height="2" rx="1"
                                        transform="rotate(-90 10.8891 17.8033)" fill="black" />
                                    <rect x="6.01041" y="10.9247" width="12" height="2" rx="1" fill="black" />
                                </svg>
                            </span>
                            <!--end::Svg Icon-->
                        </button>
                    </div>
                </form>
            </div>

            <!--begin::Content-->
            <div class="col-12">

                @foreach ($kelas->kuliahs_berita_acara as $kuliah)
                <!--begin::Kategori-->
                <div class="card card-flush mb-5 crd-shadow bg-light">
                    <!--begin::Card header-->
                    <div class="card-header align-items-center py-5 gap-2 gap-md-5" style="background: #1c285e;">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label text-white fw-bolder fs-3 mb-1">{{ $kuliah->nama ?? '-' }}</span>
                        </h3>
                        <!--begin::Card toolbar-->
                        <div class="card-toolbar flex-row-fluid justify-content-end gap-5">
                            <div>
                                <a href="{{ route('beritaAcara.tambah', $kuliah->kode) }}" class="btn btn-secondary rounded-50"
                                    data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                    <span class="svg-icon svg-icon-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                            fill="none">
                                            <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black" />
                                            <rect x="10.8891" y="17.8033" width="12" height="2" rx="1"
                                                transform="rotate(-90 10.8891 17.8033)" fill="black" />
                                            <rect x="6.01041" y="10.9247" width="12" height="2" rx="1" fill="black" />
                                        </svg>
                                    </span>

                                    Tambah
                                </a>
                            </div>

                            <button type="button" class="btn btn-sm btn-icon btn-color-primary btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                <!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
                                <span class="svg-icon svg-icon-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px"
                                        viewBox="0 0 24 24">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="5" y="5" width="5" height="5" rx="1" fill="#000000" />
                                            <rect x="14" y="5" width="5" height="5" rx="1" fill="#000000"
                                                opacity="0.3" />
                                            <rect x="5" y="14" width="5" height="5" rx="1" fill="#000000"
                                                opacity="0.3" />
                                            <rect x="14" y="14" width="5" height="5" rx="1" fill="#000000"
                                                opacity="0.3" />
                                        </g>
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                            </button>
                            <!--begin::Menu 3-->
                            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px py-3"
                                data-kt-menu="true">
                                <!--begin::Heading-->
                                <div class="menu-item px-3">
                                    <div class="menu-content text-muted pb-2 px-3 fs-7 text-uppercase">Aksi</div>
                                </div>
                                <!--end::Heading-->
                                <!--begin::Menu item-->
                                <div class="menu-item px-3">
                                    <a href="#" class="menu-link px-3" data-bs-toggle="modal"
                                        data-bs-target="#modal_edit_kuliah{{ $kuliah->id }}">Edit</a>
                                </div>
                                <div class="menu-item px-3">
                                    <a href="{{ route('kuliah.hapus', $kuliah->kode) }}"
                                        onclick="return confirm('Yakin akan menghapus?');"
                                        class="menu-link px-3 text-danger">Hapus</a>
                                </div>
                                <!--end::Menu item-->
                            </div>

                            <div class="modal fade" tabindex="-1" id="modal_edit_kuliah{{ $kuliah->id }}">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Edit Kelompok</h5>

                                            <!--begin::Close-->
                                            <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                                data-bs-dismiss="modal" aria-label="Close">
                                                <span class="svg-icon svg-icon-2x">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none">
                                                        <rect opacity="0.5" x="7.05025" y="15.5356" width="12"
                                                            height="2" rx="1" transform="rotate(-45 7.05025 15.5356)"
                                                            fill="black" />
                                                        <rect x="8.46447" y="7.05029" width="12" height="2" rx="1"
                                                            transform="rotate(45 8.46447 7.05029)" fill="black" />
                                                    </svg>
                                                </span>
                                            </div>
                                            <!--end::Close-->
                                        </div>

                                        <form method="POST"
                                            action="{{ route('kuliah.update', $kuliah->id) }}"
                                            enctype="multipart/form-data">
                                            @csrf
                                            <div class="modal-body">
                                                <!--begin::Input group-->
                                                <div class="input-group input-group-solid mb-5">
                                                    <span class="input-group-text" id="basic-addon2">
                                                        <span class="svg-icon svg-icon-1">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                height="24" viewBox="0 0 24 24" fill="none">
                                                                <path opacity="0.3"
                                                                    d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM11.7 17.7L16 14C16.4 13.6 16.4 12.9 16 12.5C15.6 12.1 15.4 12.6 15 13L11 16L9 15C8.6 14.6 8.4 14.1 8 14.5C7.6 14.9 8.1 15.6 8.5 16L10.3 17.7C10.5 17.9 10.8 18 11 18C11.2 18 11.5 17.9 11.7 17.7Z"
                                                                    fill="black" />
                                                                <path
                                                                    d="M10.4343 15.4343L9.25 14.25C8.83579 13.8358 8.16421 13.8358 7.75 14.25C7.33579 14.6642 7.33579 15.3358 7.75 15.75L10.2929 18.2929C10.6834 18.6834 11.3166 18.6834 11.7071 18.2929L16.25 13.75C16.6642 13.3358 16.6642 12.6642 16.25 12.25C15.8358 11.8358 15.1642 11.8358 14.75 12.25L11.5657 15.4343C11.2533 15.7467 10.7467 15.7467 10.4343 15.4343Z"
                                                                    fill="black" />
                                                                <path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z"
                                                                    fill="black" />
                                                            </svg>
                                                        </span>
                                                    </span>
                                                    <input type="text" name="nama" class="form-control" value="{{ $kuliah->nama }}" required />
                                                </div>
                                                <!--end::Input group-->
                                            </div>

                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">Simpan</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Card toolbar-->
                    </div>
                    <!--end::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body pt-0">
                        <!--begin::Table-->
                        <table class="table align-middle table-row-dashed fs-6 gy-5">
                            <!--begin::Table body-->
                            <tbody class="fw-bold text-gray-600">
                                <!--begin::Table row-->
                                @foreach ($kuliah->berita_acaras->sortByDesc('tanggal') as $beritaAcara)
                                <tr>
                                    <!--begin::Customer-->
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <!--begin:: Avatar -->
                                            <div class="symbol symbol-40px me-4">
                                                <div class="symbol-label fs-2 fw-semibold bg-primary text-inverse-primary">BA</div>
                                            </div>
                                            <!--end::Avatar-->
                                            <div class="ms-5">
                                                <a href="#"
                                                    class="text-gray-800 fw-bolder text-hover-primary mb-1 fs-6 text-start pe-0">
                                                    {{ $beritaAcara?->judul ?? '-' }}</a>
                                                <br>
                                                <small class="text-muted">
                                                    {{ $beritaAcara?->user?->name }}
                                                </small>
                                            </div>
                                        </div>
                                    </td>
                                    <!--end::Customer-->
                                    <td class="text-end pe-0">
                                        <span class="fw-bolder">
                                            {{ $beritaAcara?->metode }}
                                        </span>
                                        <br>
                                        <small class="text-muted">
                                            {{ $beritaAcara?->tanggal ? Date::parse($beritaAcara?->tanggal)->format('l, d M Y H:i') : '-' }}
                                        </small>
                                    </td>
                                    <!--begin::Action-->
                                    <td class="text-end">

                                        @if ($beritaAcara?->status != 'Tutup')
                                        <a href="{{ route('beritaAcara.ubahStatus', $beritaAcara?->kode) }}"
                                            class="btn btn-sm btn-light-danger">
                                            <span class="svg-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                    height="24" viewBox="0 0 24 24" fill="none">
                                                    <rect opacity="0.5" x="7.05025" y="15.5356" width="12" height="2"
                                                        rx="1" transform="rotate(-45 7.05025 15.5356)" fill="black" />
                                                    <rect x="8.46447" y="7.05029" width="12" height="2" rx="1"
                                                        transform="rotate(45 8.46447 7.05029)" fill="black" />
                                                </svg></span>
                                            Tutup</a>
                                        @else
                                        <a href="{{ route('beritaAcara.ubahStatus', $beritaAcara?->kode) }}"
                                            class="btn btn-sm btn-light-primary">
                                            <span class="svg-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                    height="24" viewBox="0 0 24 24" fill="none">
                                                    <path
                                                        d="M9.89557 13.4982L7.79487 11.2651C7.26967 10.7068 6.38251 10.7068 5.85731 11.2651C5.37559 11.7772 5.37559 12.5757 5.85731 13.0878L9.74989 17.2257C10.1448 17.6455 10.8118 17.6455 11.2066 17.2257L18.1427 9.85252C18.6244 9.34044 18.6244 8.54191 18.1427 8.02984C17.6175 7.47154 16.7303 7.47154 16.2051 8.02984L11.061 13.4982C10.7451 13.834 10.2115 13.834 9.89557 13.4982Z"
                                                        fill="black" />
                                                </svg></span>
                                            Buka</a>
                                        @endif
                                        <a href="{{ route('beritaAcara.detail', $beritaAcara?->kode) }}"
                                            class="btn btn-sm btn-primary"><i class="bi bi-eye fs-4 me-2"></i>
                                            Detail</a>

                                        <a href="#" class="btn btn-sm btn-light btn-active-light-primary"
                                            data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Aksi
                                            <!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
                                            <span class="svg-icon svg-icon-5 m-0">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                    viewBox="0 0 24 24" fill="none">
                                                    <path
                                                        d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                                                        fill="black" />
                                                </svg>
                                            </span>
                                            <!--end::Svg Icon-->
                                        </a>
                                        <!--begin::Menu-->
                                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4"
                                            data-kt-menu="true">
                                            <!--begin::Menu item-->
                                            <div class="menu-item px-3">
                                                <a href="{{ route('beritaAcara.export', $beritaAcara?->kode) }}"
                                                    class="menu-link px-3">Export</a>
                                            </div>
                                            <!--end::Menu item-->
                                            <!--begin::Menu item-->
                                            <div class="menu-item px-3">
                                                <a href="{{ route('beritaAcara.edit', $beritaAcara?->kode) }}"
                                                    class="menu-link px-3">Edit</a>
                                            </div>
                                            <!--end::Menu item-->
                                            <!--begin::Menu item-->
                                            <div class="menu-item px-3">
                                                <a href="{{ route('beritaAcara.hapus', $beritaAcara?->kode) }}"
                                                    onclick="return confirm('Yakin akan menghapus?');"
                                                    class="menu-link px-3 text-danger">Hapus</a>
                                            </div>
                                            <!--end::Menu item-->
                                        </div>
                                        <!--end::Menu-->
                                    </td>
                                    <!--end::Action-->
                                </tr>
                                @endforeach
                                <!--end::Table row-->
                            </tbody>
                            <!--end::Table body-->

                            <table class="table align-middle table-row-dashed fs-6 gy-5">
                                <!--begin::Table body-->
                                <tbody class="fw-bold text-gray-600">
                                    <!--begin::Table row-->
                                    @foreach ($kuliah->sgds as $sgd)
                                    <tr>
                                        <!--begin::Customer-->
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <!--begin:: Avatar -->
                                                <div class="symbol symbol-40px me-4">
                                                    <div class="symbol-label fs-2 fw-semibold bg-success text-inverse-success">PN</div>
                                                </div>
                                                <!--end::Avatar-->
                                                <div class="ms-5">
                                                    <a href="#"
                                                        class="text-gray-800 fw-bolder text-hover-primary mb-1 fs-6 text-start pe-0">
                                                        {{ $sgd?->judul ?? '-' }}</a>
                                                    <br>
                                                    <small class="text-muted">
                                                        {{ $sgd?->user?->name }}
                                                    </small>
                                                </div>
                                            </div>
                                        </td>
                                        <!--end::Customer-->
                                        <td class="text-end pe-0">
                                            <span class="fw-bolder">{{ $sgd?->ruang }}</span>
                                            <br>
                                            <small class="text-muted">
                                                {{ $sgd?->tanggal ? Date::parse($sgd?->tanggal)->format('l, d M Y H:i') : '-' }}
                                            </small>
                                        </td>
                                        <!--begin::Action-->
                                        <td class="text-end">

                                            @if ($sgd?->status != 'Tutup')
                                            <a href="{{ route('sgd.ubahStatus', $sgd?->kode) }}"
                                                class="btn btn-sm btn-light-danger">
                                                <span class="svg-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                        height="24" viewBox="0 0 24 24" fill="none">
                                                        <rect opacity="0.5" x="7.05025" y="15.5356" width="12" height="2"
                                                            rx="1" transform="rotate(-45 7.05025 15.5356)" fill="black" />
                                                        <rect x="8.46447" y="7.05029" width="12" height="2" rx="1"
                                                            transform="rotate(45 8.46447 7.05029)" fill="black" />
                                                    </svg></span>
                                                Tutup</a>
                                            @else
                                            <a href="{{ route('sgd.ubahStatus', $sgd?->kode) }}"
                                                class="btn btn-sm btn-light-primary">
                                                <span class="svg-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                        height="24" viewBox="0 0 24 24" fill="none">
                                                        <path
                                                            d="M9.89557 13.4982L7.79487 11.2651C7.26967 10.7068 6.38251 10.7068 5.85731 11.2651C5.37559 11.7772 5.37559 12.5757 5.85731 13.0878L9.74989 17.2257C10.1448 17.6455 10.8118 17.6455 11.2066 17.2257L18.1427 9.85252C18.6244 9.34044 18.6244 8.54191 18.1427 8.02984C17.6175 7.47154 16.7303 7.47154 16.2051 8.02984L11.061 13.4982C10.7451 13.834 10.2115 13.834 9.89557 13.4982Z"
                                                            fill="black" />
                                                    </svg></span>
                                                Buka</a>
                                            @endif
                                            <a href="{{ route('sgd.detail', $sgd?->kode) }}"
                                                class="btn btn-sm btn-success"><i class="bi bi-eye fs-4 me-2"></i>
                                                Detail</a>

                                            <a href="#" class="btn btn-sm btn-light btn-active-light-primary"
                                                data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Aksi
                                                <!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
                                                <span class="svg-icon svg-icon-5 m-0">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none">
                                                        <path
                                                            d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                                                            fill="black" />
                                                    </svg>
                                                </span>
                                                <!--end::Svg Icon-->
                                            </a>
                                            <!--begin::Menu-->
                                            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4"
                                                data-kt-menu="true">
                                                <!--begin::Menu item-->
                                                <div class="menu-item px-3">
                                                    <a href="{{ route('sgd.export', $sgd?->kode) }}"
                                                        class="menu-link px-3">Export</a>
                                                </div>
                                                <!--end::Menu item-->
                                                <!--begin::Menu item-->
                                                <div class="menu-item px-3">
                                                    <a href="{{ route('sgd.edit', $sgd?->kode) }}"
                                                        class="menu-link px-3">Edit</a>
                                                </div>
                                                <!--end::Menu item-->
                                                <!--begin::Menu item-->
                                                <div class="menu-item px-3">
                                                    <a href="{{ route('sgd.hapus', $sgd?->kode) }}"
                                                        onclick="return confirm('Yakin akan menghapus?');"
                                                        class="menu-link px-3 text-danger">Hapus</a>
                                                </div>
                                                <!--end::Menu item-->
                                            </div>
                                            <!--end::Menu-->
                                        </td>
                                        <!--end::Action-->
                                    </tr>
                                    @endforeach
                                    <!--end::Table row-->
                                </tbody>
                                <!--end::Table body-->
                            </table>
                            <!--end::Table-->
                        </table>
                        <!--end::Table-->
                    </div>
                    <!--end::Card body-->
                </div>
                <!--end::Kategori-->
                @endforeach


            </div>
            <!--end::Content-->
        </div>
    </div>
    <!--end::Post-->
</div>
<!--end::Container-->
@endsection

@section('styles')
@endsection

@section('scripts')
@endsection
