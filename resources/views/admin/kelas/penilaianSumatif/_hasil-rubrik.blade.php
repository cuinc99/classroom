@if ($pertanyaans->count() > 0)
    @foreach ($pertanyaans as $pertanyaan)
        <div class="card card-flush py-4 mt-10">
            <!--begin::Card body-->
            <div class="card-body pt-5">

                <!--begin::Input group-->
                <div class="mb-10 fv-row">
                    <!--begin::Label-->
                    <label class="form-label fs-2x"><span class="opacity-50">{{ $loop->iteration }}.</span>
                        {!! $pertanyaan->pertanyaan ?? '-' !!}</label>
                    <br>
                    <span class="text-muted">{!! $pertanyaan->deskripsi !!}</span>
                    <!--end::Label-->
                    @if ($mahasiswaTugas->tanggal_kumpul)
                        <div class="d-flex flex-wrap mb-5 mt-10">
                            <!--begin::Due-->
                            <div class="border border-primary border-dashed rounded min-w-125px py-3 px-4 me-7 mb-3">
                                <div class="fs-6 text-gray-800 fw-bolder">POIN ANDA / MAKSIMAL POIN</div>
                                <div class="fw-bold">
                                    @php
                                        $poin = \App\Models\JawabanTugas::where('mahasiswa_id', $mahasiswa->id)
                                            ->where('tugas_id', $tugas->id)
                                            ->where('pertanyaan_id', $pertanyaan->id)
                                            ->first();
                                    @endphp
                                    <span class="fs-2x fw-bolder">{{ $poin?->poin ?? '-' }}
                                    </span>/ <span class=" text-gray-400">{{ $pertanyaan->maks_poin }}</span>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="d-flex flex-wrap mb-5 mt-10">
                            <!--begin::Due-->
                            <div class="border border-primary border-dashed rounded min-w-125px py-3 px-4 me-7 mb-3">
                                <div class="fs-6 text-gray-800 fw-bolder">MAKSIMAL POIN</div>
                                <div class="fw-bold text-gray-400">{{ $pertanyaan->maks_poin }}</div>
                            </div>
                        </div>
                    @endif
                </div>
                <!--end::Input group-->

            </div>
            <!--end::Card header-->
        </div>
    @endforeach
@endif
