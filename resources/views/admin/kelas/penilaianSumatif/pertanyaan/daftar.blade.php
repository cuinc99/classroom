@extends('layouts.admin')

@section('title')
    Daftar Pertanyaan
@endsection

@section('content')
    <!--begin::Toolbar-->
    <div class="toolbar py-5 py-lg-5">
        <!--begin::Container-->
        <div class="container-xxl d-flex flex-stack flex-wrap">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column me-3">
                <!--begin::Title-->
                <h1 class="d-flex text-dark fw-bolder my-1 fs-3">Daftar Pertanyaan</h1>
                <!--end::Title-->
            </div>
            <!--end::Page title-->
            <div class="d-flex align-items-center py-2 py-md-1">
                <div>
                    <a href="{{ route('kelas.penilaianSumatif', $kelas->kode) }}" class="btn btn-sm btn-secondary fw-bolder">
                        Kembali
                        <span class="svg-icon svg-icon-1hx svg-icon-gray-500 me-1">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                stroke="currentColor" class="w-6 h-6">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                    d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3" />
                            </svg>

                        </span>
                    </a>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Container-->
    <div class="d-flex flex-column-fluid align-items-start container-xxl">
        <div class="content flex-row-fluid">
            @if ($tugas->tipe == 'rubrik')
                @include('admin.kelas.penilaianSumatif.pertanyaan._rubrik-pertayaan')
            @endif

            @if ($tugas->tipe == 'rubrik2')
                @include('admin.kelas.penilaianSumatif.pertanyaan._rubrik2-pertayaan')
            @endif

            @if ($tugas->tipe == 'kuis')
                @livewire('kelas.tugas.pertanyaan.kuis', ['kelas' => $kelas, 'kategori' => $kategori, 'tugas' => $tugas])
            @endif
        </div>
        <!--end::Post-->
    </div>
    <!--end::Container-->
@endsection

@section('styles')
@endsection

@section('scripts')
    <script src="/assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js"></script>
    <script src="/js/quill/image-resize.min.js"></script>
    <script src="/js/quill/image-drop.min.js"></script>
    <script>
        var toolbarOptions = [
            ['bold', 'italic', 'underline', 'strike'], // toggled buttons
            ['blockquote', 'code-block'],

            [{
                'header': 1
            }, {
                'header': 2
            }], // custom button values
            [{
                'list': 'ordered'
            }, {
                'list': 'bullet'
            }],
            [{
                'script': 'sub'
            }, {
                'script': 'super'
            }], // superscript/subscript
            [{
                'indent': '-1'
            }, {
                'indent': '+1'
            }], // outdent/indent
            [{
                'direction': 'rtl'
            }], // text direction

            [{
                'size': ['small', false, 'large', 'huge']
            }], // custom dropdown
            [{
                'header': [1, 2, 3, 4, 5, 6, false]
            }],

            [{
                'color': []
            }, {
                'background': []
            }], // dropdown with defaults from theme
            [{
                'font': []
            }],
            [{
                'align': []
            }],
            ['image', 'video', 'link'],

            ['clean'] // remove formatting button
        ];
        var quill = new Quill('#isi_editor', {
            theme: 'snow',
            placeholder: 'Tulis deskripsi disini...',
            modules: {
                toolbar: toolbarOptions,
                imageResize: {
                    displaySize: true
                },
                imageDrop: true
            },
        });
        quill.on('text-change', function(delta, oldDelta, source) {
            document.getElementById("isi_html").value = quill.root.innerHTML;
        });
    </script>
    @stack('scripts')
@endsection
