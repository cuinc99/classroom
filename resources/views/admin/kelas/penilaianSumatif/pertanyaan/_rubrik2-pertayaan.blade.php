<div class="d-flex flex-column gap-7 gap-lg-10">
    <!--begin::Inventory-->
    <div class="card card-flush py-4">
        <!--begin::Card body-->
        <div class="card-body pt-5">
            <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x mb-5 fs-6">
                <li class="nav-item me-0 mb-md-2">
                    <a class="nav-link active btn btn-flex btn-active-light-info" data-bs-toggle="tab"
                        href="#kt_tab_pane_4">
                        <span class="svg-icon svg-icon-2hx">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none">
                                <path opacity="0.3"
                                    d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM16 13.5L12.5 13V10C12.5 9.4 12.6 9.5 12 9.5C11.4 9.5 11.5 9.4 11.5 10L11 13L8 13.5C7.4 13.5 7 13.4 7 14C7 14.6 7.4 14.5 8 14.5H11V18C11 18.6 11.4 19 12 19C12.6 19 12.5 18.6 12.5 18V14.5L16 14C16.6 14 17 14.6 17 14C17 13.4 16.6 13.5 16 13.5Z"
                                    fill="black" />
                                <rect x="11" y="19" width="10" height="2" rx="1" transform="rotate(-90 11 19)"
                                    fill="black" />
                                <rect x="7" y="13" width="10" height="2" rx="1" fill="black" />
                                <path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="black" />
                            </svg>
                        </span>
                        <span class="d-flex flex-column align-items-start">
                            <span class="fs-4 fw-bolder">Tambah</span>
                            <span class="fs-7">Buat kriteria baru</span>
                        </span>
                    </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link btn btn-flex btn-active-light-danger" data-bs-toggle="tab" href="#kt_tab_pane_5">
                        <span class="svg-icon svg-icon-2hx">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none">
                                <path opacity="0.3"
                                    d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22Z"
                                    fill="black" />
                                <g clip-path="url(#clip0_787_1289)">
                                    <path
                                        d="M9.56133 15.7161C9.72781 15.5251 9.5922 15.227 9.33885 15.227H8.58033C8.57539 15.1519 8.57262 15.0763 8.57262 15C8.57262 13.1101 10.1101 11.5726 12 11.5726C12.7576 11.5726 13.4585 11.8198 14.0265 12.2377C14.2106 12.3731 14.4732 12.3609 14.6216 12.1872L15.1671 11.5491C15.3072 11.3852 15.2931 11.1382 15.1235 11.005C14.2353 10.3077 13.1468 9.92944 12 9.92944C10.6456 9.92944 9.37229 10.4569 8.41458 11.4146C7.4569 12.3723 6.92945 13.6456 6.92945 15C6.92945 15.0759 6.93135 15.1516 6.93465 15.2269H6.29574C6.0424 15.2269 5.90677 15.5251 6.07326 15.7161L7.51455 17.3693L7.81729 17.7166L8.90421 16.4698L9.56133 15.7161Z"
                                        fill="black" />
                                    <path
                                        d="M17.9268 14.7516L16.8518 13.5185L16.1828 12.7511L15.2276 13.8468L14.4388 14.7516C14.2723 14.9426 14.4079 15.2407 14.6612 15.2407H15.4189C15.2949 17.0187 13.809 18.4274 12.0001 18.4274C11.347 18.4274 10.736 18.2437 10.216 17.9253C10.0338 17.8138 9.79309 17.8362 9.6543 17.9985L9.10058 18.6463C8.95391 18.8179 8.97742 19.0782 9.16428 19.2048C9.99513 19.7678 10.9743 20.0706 12.0001 20.0706C13.3545 20.0706 14.6278 19.5432 15.5855 18.5855C16.4863 17.6847 17.0063 16.5047 17.0649 15.2407H17.7043C17.9577 15.2407 18.0933 14.9426 17.9268 14.7516Z"
                                        fill="black" />
                                </g>
                                <path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="black" />
                            </svg>
                        </span>
                        <span class="d-flex flex-column align-items-start">
                            <span class="fs-4 fw-bolder">Salin</span>
                            <span class="fs-7">Gunakan rubrik dari tugas yang ada</span>
                        </span>
                    </a>
                </li>
            </ul>

            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="kt_tab_pane_4" role="tabpanel">
                    <form action="{{ route('pertanyaan.simpanRubrik', $tugas->kode) }}" method="POST">
                        @csrf
                        <div class="mb-10 fv-row">
                            <!--begin::Label-->
                            <label
                                class="required form-label">{{ \App\Models\Tugas::TIPE_OPTION[$tugas->tipe]['judul'] }}</label>
                            <!--end::Label-->
                            <!--begin::Input-->
                            <div class="input-group input-group-solid mb-5">
                                <span class="input-group-text" id="basic-addon2">
                                    <span class="svg-icon svg-icon-1">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none">
                                            <path opacity="0.3"
                                                d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                                                fill="black" />
                                            <path
                                                d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                                                fill="black" />
                                        </svg>
                                    </span>
                                </span>
                                <input type="text" name="pertanyaan" class="form-control"
                                    placeholder="Tulis Kriteria" required />
                            </div>

                            <!--end::Input-->
                        </div>
                        <!--end::Input group-->
                        <div class="d-flex justify-content-end mt-10">
                            <!--begin::Button-->
                            <button type="submit" class="btn btn-primary">
                                <span class="indicator-label">Simpan</span>
                            </button>
                            <!--end::Button-->
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade" id="kt_tab_pane_5" role="tabpanel">
                    <form action="{{ route('pertanyaan.simpanRubrik', $tugas->kode) }}" method="POST">
                        @csrf
                        <div class="input-group input-group-solid flex-nowrap">
                            <span class="input-group-text">
                                <span class="svg-icon svg-icon-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none">
                                        <path opacity="0.3"
                                            d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                                            fill="black" />
                                        <path
                                            d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                                            fill="black" />
                                    </svg>
                                </span>
                            </span>
                            <div class="overflow-hidden flex-grow-1">
                                <select name="tugas_id"
                                    class="form-select form-select-solid rounded-start-0 border-start"
                                    data-control="select2" data-placeholder="Pilih Rubrik" required>
                                    <option></option>
                                    @foreach ($tugasItems as $tugasItem)
                                        <option value="{{ $tugasItem->id }}">Salin Rubrik dari Tugas
                                            {{ $tugasItem->judul }} (Kelas {{ $tugasItem->kelas->nama }})</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end mt-10">
                            <!--begin::Button-->
                            <a href="{{ route('kelas.penilaianSumatif', $kelas->kode) }}" class="btn btn-light me-5">Kembali</a>
                            <!--end::Button-->
                            <!--begin::Button-->
                            <button type="submit" class="btn btn-primary">
                                <span class="indicator-label">Simpan</span>
                            </button>
                            <!--end::Button-->
                        </div>
                    </form>
                </div>
            </div>
            <!--begin::Input group-->

        </div>
        <!--end::Card header-->
    </div>
    <!--end::Inventory-->
</div>


@if ($pertanyaans->count() > 0)
    <div class="card-body pt-0 mt-10">
        <!--begin::Table-->
        <table class="table align-middle table-row-dashed fs-6 gy-5">
            <thead>
                <!--begin::Table row-->
                <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                    <th class="min-w-100px">Kriteria</th>
                    {{-- <th class="text-end">Maksimal Poin</th> --}}
                    <th class="text-end">Tanggal Buat</th>
                    <th class="text-end"></th>
                </tr>
                <!--end::Table row-->
            </thead>
            <!--begin::Table body-->
            <tbody class="fw-bold text-gray-600">
                <!--begin::Table row-->
                @foreach ($pertanyaans as $pertanyaanItem)
                    <tr>
                        <!--begin::Customer-->
                        <td>
                            <a href="#"
                                class="text-gray-800 fw-bolder text-hover-primary mb-1 fs-6 text-start pe-0">{!! $pertanyaanItem->pertanyaan ?? '-' !!}</a>
                        </td>
                        {{-- <td class="text-end">
                            <span class="fw-bolder">{{ $pertanyaanItem->maks_poin }}</span>
                        </td> --}}
                        <!--end::Customer-->
                        <td class="text-end">
                            <span
                                class="fw-bolder">{{ Date::parse($pertanyaanItem->created_at)->diffForHumans() }}</span>
                        </td>
                        <!--begin::Action-->
                        <td class="text-end">
                            {{-- <a href="{{ route('pertanyaan.daftar', [$kelas->kode, $kategori->kode, $tugas->kode]) }}" class="btn btn-sm btn-light-{{ \App\Models\Tugas::TIPE_OPTION[$tugas->tipe]['warna'] }}"><i class="bi bi-plus fs-4 me-2"></i> Buat {{ Str::ucfirst($tugas->tipe) }}</a> --}}
                            <a href="#" class="btn btn-sm btn-light btn-active-light-primary"
                                data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Aksi
                                <!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
                                <span class="svg-icon svg-icon-5 m-0">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none">
                                        <path
                                            d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                                            fill="black" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                            </a>
                            <!--begin::Menu-->
                            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4"
                                data-kt-menu="true">
                                <!--begin::Menu item-->
                                <div class="menu-item px-3">
                                    <a class="menu-link px-3" data-bs-toggle="modal" data-bs-target="#edit_kriteria{{ $pertanyaanItem->id }}">Edit</a>
                                </div>
                                <!--end::Menu item-->
                                <!--begin::Menu item-->
                                <div class="menu-item px-3">
                                    <a href="{{ route('pertanyaan.hapus', $pertanyaanItem->kode) }}"
                                        onclick="return confirm('Yakin akan menghapus?');"
                                        class="menu-link text-danger px-3">Hapus</a>
                                </div>
                                <!--end::Menu item-->
                            </div>
                            <!--end::Menu-->
                        </td>
                        <!--end::Action-->
                    </tr>
                    <div class="modal fade" tabindex="-1" id="edit_kriteria{{ $pertanyaanItem->id }}">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Edit</h5>

                                    <!--begin::Close-->
                                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                        data-bs-dismiss="modal" aria-label="Close">
                                        <span class="svg-icon svg-icon-2x"></span>
                                    </div>
                                    <!--end::Close-->
                                </div>

                                <form action="{{ route('pertanyaan.updateRubrik', $pertanyaanItem->id) }}" method="POST">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="mb-10 fv-row">
                                            <!--begin::Label-->
                                            <label
                                                class="required form-label">{{ \App\Models\Tugas::TIPE_OPTION[$tugas->tipe]['judul'] }}</label>
                                            <!--end::Label-->
                                            <!--begin::Input-->
                                            <div class="input-group input-group-solid mb-5">
                                                <span class="input-group-text" id="basic-addon2">
                                                    <span class="svg-icon svg-icon-1">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                            viewBox="0 0 24 24" fill="none">
                                                            <path opacity="0.3"
                                                                d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                                                                fill="black" />
                                                            <path
                                                                d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                                                                fill="black" />
                                                        </svg>
                                                    </span>
                                                </span>
                                                <input type="text" name="pertanyaan"
                                                    value="{{ $pertanyaanItem->pertanyaan }}"
                                                    class="form-control" required />
                                            </div>

                                            <!--end::Input-->
                                        </div>
                                        <!--end::Input group-->
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Tutup</button>
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach
                <!--end::Table row-->
            </tbody>
            <!--end::Table body-->
        </table>
        <!--end::Table-->
    </div>
@endif
