@if (!$mahasiswaTugas->tanggal_kumpul)
    {{-- <div class="d-flex flex-column text-dark-75">
        <span class="font-weight-bolder font-size-sm">Sisa Waktu</span>
        <span class="font-weight-bolder font-size-h5"><span class="text-dark-50 font-weight-bold"
                id="timer-element">-</span></span>
    </div> --}}
    <div class="card card-xl-stretch mb-xl-8 mb-5">
        <!--begin::Body-->
        <div class="card-body d-flex align-items-center pt-3 pb-0">
            <div class="d-flex flex-column flex-grow-1 py-2 py-lg-13 me-2">
                <span class="fw-bold text-muted fs-5 mb-2">Waktu Mulai</span>
                <a href="#" class="fw-bolder text-dark text-hover-primary mb-5"
                    style="font-size: 13pt">{{ Date::parse($mahasiswaTugas->tanggal_mulai_test)->format('d M Y, H:i:s') }}</a>
                <span class="fw-bold text-muted fs-5 mb-2">Sisa Waktu</span>
                <a href="#" class="fw-bolder text-dark" id="timer-element" style="font-size: 13pt"></a>
            </div>
            <img src="/assets/media/svg/avatars/029-boy-11.svg" alt="" class="align-self-end h-100px">
        </div>
        <!--end::Body-->
    </div>

    <form action="{{ route('tugas.kumpulkanKuis', [$tugas->id, $mahasiswa->id]) }}" method="post" id="myForm">
        @csrf
        <div class="d-flex flex-column gap-7 gap-lg-10">


            @foreach ($pertanyaans as $pertanyaan)
                <div class="card card-flush py-4 bg-light shadow-md">
                    <!--begin::Card body-->
                    <div class="card-body pt-5">
                        <!--begin::Input group-->
                        <div class="mb-10 fv-row">
                            <!--begin::Label-->
                            <label class="form-label"><small class="opacity-50">Soal ke
                                    {{ $loop->iteration }}.</small>
                                {!! $pertanyaan->pertanyaan ?? '-' !!}</label>
                            <!--end::Label-->
                            <div data-kt-buttons="true">
                                @foreach ($pertanyaan->jawabans->when($tugas->acak_jawaban, fn($q) => $q->shuffle()) as $jawaban)
                                @livewire('ujian', [
                                    'tugasId' => $tugas->id,
                                    'mahasiswaId' => $mahasiswa->id,
                                    'pertanyaanId' => $pertanyaan->id,
                                    'jawabanId' => $jawaban->id,
                                    'index' => $loop->iteration,
                                ])
                                @endforeach
                            </div>
                        </div>
                        <!--end::Input group-->

                    </div>
                    <!--end::Card header-->
                </div>
            @endforeach
        </div>
        <div class="d-flex justify-content-end mt-10 mb-10">
            <!--begin::Button-->
            <!--end::Button-->
            <!--begin::Button-->
            <a class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal_confirm">
                <span class="indicator-label">Kumpulkan</span>
            </a>
            <button type="submit" id="btn" style="display: none"></button>
            <!--end::Button-->
        </div>
        <div class="modal" role="dialog" id="modal_confirm">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Yakin akan mengumpulkan test?</h5>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-light" data-bs-dismiss="modal" aria-label="Close">Tutup</a>
                        <button type="submit" class="btn btn-danger">Ya, Kumpulkan</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    @push('child-scripts')
        <script>
            var closeAtB = '{{ $mahasiswaTugas->tanggal_selesai_test }}';
            var closeAt = convertDateForIos(closeAtB);
            var nowB = '{{ Carbon\Carbon::now() }}';
            var now = convertDateForIos(nowB);

            function convertDateForIos(date) {
                var arr = date.split(/[- :]/);
                date = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
                return date;
            }
        </script>
        <script src="/js/ujian_countdown_timer.js"></script>
    @endpush
    <div class="modal" role="dialog" id="timeWarningModal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-danger">Peringatan!</h5>
                </div>
                <div class="modal-body">
                    <p>Test akan segera berakhir, anda berada dalam 2 menit terakhir waktu test.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal" aria-label="Close">Ok</button>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="card">
        <!--begin::Card body-->
        <div class="card-body p-0">
            <!--begin::Wrapper-->
            <div class="card-px text-center py-20 my-10">
                <!--begin::Title-->
                <h2 class="fs-2x text-muted mb-10">Poin Anda</h2>
                <h2 class="fs-3x fw-bolder mb-10">{{ $mahasiswaTugas->total_poin }}</h2>
                <!--end::Title-->
                <!--begin::Description-->
                <p class="text-gray-400 fs-4 fw-bold mb-10"></p>
                <!--end::Description-->
                <!--begin::Action-->
                <a href="{{ route('kelas.show', $tugas->kelas->kode) }}" class="btn btn-primary">Kembali</a>
                <!--end::Action-->
            </div>
            <!--end::Wrapper-->
            <!--begin::Illustration-->
            <div class="text-center px-4">
                <img class="mw-100 mh-300px" alt="" src="/assets/media/illustrations/sketchy-1/7.png" />
            </div>
            <!--end::Illustration-->
        </div>
        <!--end::Card body-->
    </div>
@endif
