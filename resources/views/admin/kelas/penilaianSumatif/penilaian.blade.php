@extends('layouts.admin')

@section('title')
    Informasi
@endsection

@section('content')
    <!--begin::Toolbar-->
    <div class="toolbar py-5 py-lg-5">
        <!--begin::Container-->
        <div class="container-xxl d-flex flex-stack flex-wrap">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column me-3">
                <!--begin::Title-->
                <h1 class="d-flex text-dark fw-bolder my-1 fs-3">
                    {{ $mahasiswaTugas->tanggal_kumpul ? 'Anda telah memberi penilaian tugas ini!' : 'Proses Penilaian' }}
                </h1>
                <!--end::Title-->
            </div>
            <!--end::Page title-->
            @if ($mahasiswaTugas->file_tugas)
                <div class="d-flex align-items-center py-2 py-md-1">
                    <h1 class="d-flex text-dark fw-bolder my-1 fs-3 me-10">
                        {{ $mahasiswaTugas->tanggal_kumpul ? 'Poin Final' : 'Poin Sementara' }} : {{ $mahasiswaTugas->total_poin }}
                    </h1>
                    <!--begin::Button-->
                    <button
                        class="btn btn-primary fw-bolder btn-hover-scale" data-bs-toggle="modal" data-bs-target="#kt_modal_2">
                        <!--begin::Svg Icon | path: assets/media/icons/duotune/arrows/arr087.svg-->
                        <span class="svg-icon svg-icon-muted svg-icon-2hx"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                height="24" viewBox="0 0 24 24" fill="none">
                                <path opacity="0.3"
                                    d="M14 2H6C4.89543 2 4 2.89543 4 4V20C4 21.1046 4.89543 22 6 22H18C19.1046 22 20 21.1046 20 20V8L14 2Z"
                                    fill="black" />
                                <path d="M20 8L14 2V6C14 7.10457 14.8954 8 16 8H20Z" fill="black" />
                                <path
                                    d="M10.3629 14.0084L8.92108 12.6429C8.57518 12.3153 8.03352 12.3153 7.68761 12.6429C7.31405 12.9967 7.31405 13.5915 7.68761 13.9453L10.2254 16.3488C10.6111 16.714 11.215 16.714 11.6007 16.3488L16.3124 11.8865C16.6859 11.5327 16.6859 10.9379 16.3124 10.5841C15.9665 10.2565 15.4248 10.2565 15.0789 10.5841L11.4631 14.0084C11.1546 14.3006 10.6715 14.3006 10.3629 14.0084Z"
                                    fill="black" />
                            </svg></span>
                        <!--end::Svg Icon-->
                        Lihat File Tugas
                    </button>
                    <!--end::Button-->
                    <div class="modal bg-white fade" tabindex="-1" id="kt_modal_2">
                        <div class="modal-dialog modal-fullscreen">
                            <div class="modal-content shadow-none">
                                <div class="modal-header">
                                    <h5 class="modal-title">Preview File Tugas</h5>

                                    <!--begin::Close-->
                                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                                        <span class="svg-icon svg-icon-2x"></span>
                                    </div>
                                    <!--end::Close-->
                                </div>

                                <div class="modal-body">
                                    <embed src="{{ url('storage/' . $mahasiswaTugas->file_tugas ?? '-') }}" width="100%" height="100%" type="application/pdf">
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Tutup</button>
                                    <a href="{{ url('storage/' . $mahasiswaTugas->file_tugas) }}" target="_blank" class="btn btn-primary">Download</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <!--end::Container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Container-->
    <div class="d-flex flex-column-fluid align-items-start container-xxl">
        <div class="content flex-row-fluid">

            @if ($mahasiswaTugas->tanggal_kumpul)
                <div class="card mb-10">
                    <!--begin::Card body-->
                    <div class="card-body p-0">
                        <!--begin::Wrapper-->
                        <div class="card-px text-center py-20 my-10">
                            <!--begin::Title-->
                            <h2 class="fs-2x text-muted mb-10">{{ $mahasiswa->user->username }} - {{ $mahasiswa->user->name }} <br> Memperoleh Poin</h2>
                            <h2 class="fs-3x fw-bolder mb-10">{{ $mahasiswaTugas->total_poin }}</h2>
                            <!--end::Description-->
                        </div>
                        <!--end::Wrapper-->
                        <!--begin::Illustration-->
                        <div class="text-center px-4">
                            <img class="mw-100 mh-300px" alt="" src="/assets/media/illustrations/sketchy-1/7.png" />
                        </div>
                        <!--end::Illustration-->
                    </div>
                    <!--end::Card body-->
                </div>
            @endif

                <form action="{{ route('tugas.penilaianSimpan', [$tugas->id, $mahasiswa->id]) }}" method="post">
                    @csrf
                    <div class="d-flex flex-column gap-7 gap-lg-10">
                        <!--begin::Inventory-->

                        @foreach ($pertanyaans as $pertanyaan)
                            <input type="hidden" name="pertanyaans[]" value="{{ $pertanyaan->id }}">
                            <div class="card card-flush py-4">
                                <!--begin::Card body-->
                                <div class="card-body pt-5">

                                    <!--begin::Input group-->
                                    <div class="mb-10 fv-row">
                                        <!--begin::Label-->
                                        <label class="form-label fs-2x"><span
                                                class="opacity-50">{{ $loop->iteration }}.</span>
                                            {!! $pertanyaan->pertanyaan ?? '-' !!}</label>
                                        <br>
                                        <span class="text-muted">{!! $pertanyaan->deskripsi !!}</span>
                                        <!--end::Label-->
                                        <div class="mb-10 fv-row mt-10">
                                            <!--begin::Input-->
                                            <div class="input-group input-group-solid mb-5">
                                                <input type="number" name="poins[{{ $pertanyaan->id }}]"
                                                    class="form-control" placeholder="Input Poin" aria-label="Input Poin"
                                                    max="{{ $pertanyaan->maks_poin }}" aria-describedby="basic-addon2" value="{{ \App\Models\JawabanTugas::where('mahasiswa_id', $mahasiswa->id)->where('tugas_id', $tugas->id)->where('pertanyaan_id', $pertanyaan->id)->first()->poin ?? '' }}"
                                                    required />
                                                <span class="input-group-text" id="basic-addon2">
                                                    /{{ $pertanyaan->maks_poin }}
                                                </span>
                                            </div>

                                            <!--end::Input-->
                                        </div>
                                    </div>
                                    <!--end::Input group-->

                                </div>
                                <!--end::Card header-->
                            </div>
                        @endforeach

                        <!--end::Inventory-->
                    </div>
                    <div class="d-flex justify-content-end mt-10 mb-10">
                        <!--begin::Button-->
                        <a href="{{ route('tugas.proses', $tugas->kode) }}" class="btn btn-light me-5">Kembali</a>
                        <!--end::Button-->
                        <!--begin::Button-->
                        <button type="submit" name="simpan" class="btn btn-info">
                            <span class="indicator-label">Simpan</span>
                        </button>
                        {{-- <button type="submit" name="kumpul" class="btn btn-primary" onclick="return confirm('Yakin akan mengumpulkan?');">
                            <span class="indicator-label">Kumpulkan</span>
                        </button> --}}
                        <!--end::Button-->
                    </div>
                </form>


        </div>
        <!--end::Post-->
    </div>
    <!--end::Container-->
@endsection

@section('styles')
@endsection

@section('scripts')
@endsection
