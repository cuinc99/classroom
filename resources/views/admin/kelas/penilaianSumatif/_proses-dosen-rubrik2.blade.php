<div class="card mb-10">
    <div class="card-body">
        <table class="table align-middle table-row-dashed">
            <!--begin::Table head-->
            <thead>
                <!--begin::Table row-->
                <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                    <th style="width: 50px">Kriteria</th>
                    <th style="width: 50px"></th>
                    <th class="min-w-70px">Keterangan</th>
                </tr>
                <!--end::Table row-->
            </thead>
            <!--end::Table head-->
            <!--begin::Table body-->
            <tbody class="fw-bold text-gray-600">
                @foreach ($pertanyaans as $pertanyaan)
                    <tr>
                        <td>
                            <b>K {{ $loop->iteration }}</b>
                        </td>
                        <td>
                            <span class="svg-icon svg-icon-2 me-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3" />
                                  </svg>
                            </span>
                        </td>
                        <td class="min-w-70px text-muted">{{ $pertanyaan->pertanyaan }}</td>
                    </tr>
                    @endforeach
                </tbody>
        </table>
    </div>
</div>
<div class="card">
    <div class="card-body table-responsive2">
        <table class="table align-middle table-row-dashed fs-6 gy-5">
            <!--begin::Table head-->
            <thead>
                <!--begin::Table row-->
                <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                    <th width="150px" class="text-end min-w-100px"></th>
                    <th class="min-w-175px">Mahasiswa</th>
                    @foreach ($pertanyaans as $pertanyaan)
                    <th class="text-end min-w-70px">K {{ $loop->iteration }}</th>
                    @endforeach
                    <th class="text-end min-w-70px">Poin</th>
                    <th class="text-end min-w-100px"></th>
                </tr>
                <!--end::Table row-->
            </thead>
            <!--end::Table head-->
            <!--begin::Table body-->
            <tbody class="fw-bold text-gray-600">
                <!--begin::Table row-->
                @foreach ($mahasiswas as $mahasiswa)
                    @php
                        $isSudahDinilai = $mahasiswa->mahasiswa_tugas($tugas->id)->tanggal_kumpul;
                    @endphp
                    <tr>
                        <td>
                            <a href="{{ route('tugas.chat', [$tugas->kode, $mahasiswa->kode]) }}"
                                class="btn btn-light-primary"><i class="bi bi-chat-square-text-fill fs-4 me-2"></i>
                                Chat</a>
                        </td>
                        <!--begin::Customer-->
                        <td>
                            <div class="d-flex align-items-center">
                                <!--begin:: Avatar -->
                                <div class="symbol symbol-circle symbol-50px overflow-hidden me-3">
                                    <x-smart-image src="{{ public_path('/img/user.png') }}" height="100px" />
                                </div>
                                <!--end::Avatar-->
                                <div class="ms-5">
                                    <a href="#"
                                        class="text-gray-800 fw-bolder mb-1 fs-6 text-start pe-0">{{ $mahasiswa->user->name ?? '' }}</a>
                                    <span class="text-muted fw-bold d-block fs-7">NIM :
                                        {{ $mahasiswa->user->username ?? '-' }}</span>
                                </div>
                            </div>
                        </td>
                        @foreach ($pertanyaans as $pertanyaan)
                            @php
                                $poin = \App\Models\JawabanTugas::where('mahasiswa_id', $mahasiswa->id)->where('tugas_id', $tugas->id)->where('pertanyaan_id', $pertanyaan->id)->first();
                            @endphp
                            <td class="text-end">{{ $poin?->poin ?? '-' }}</td>
                        @endforeach

                        <td class="text-end">
                            @if ($isSudahDinilai)
                            <span class="badge badge-success fw-bolder my-2">{{ number_format($mahasiswa->mahasiswa_tugas($tugas->id)->total_poin, 2) ?? '0' }}</span>
                            @else
                            <span class="badge badge-danger fw-bolder my-2">0</span>
                            @endif
                        </td>

                        <td class="text-end">
                            @if ($isSudahDinilai)
                            <button data-bs-toggle="modal" data-bs-target="#modal_penilaian{{ $tugas->id }}{{ $mahasiswa?->id }}"
                                class="btn btn-sm btn-info">Ubah Penilaian</button>
                            @else
                            <button data-bs-toggle="modal" data-bs-target="#modal_penilaian{{ $tugas->id }}{{ $mahasiswa?->id }}"
                                class="btn btn-sm btn-primary">Beri Penilaian</button>
                            @endif
                        </td>
                    </tr>

                    <div class="modal fade"
                    id="modal_penilaian{{ $tugas->id }}{{ $mahasiswa?->id }}" tabindex="-1" role="dialog"
                    aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Nilai Mahasiswa {{ $mahasiswa?->user?->name }}</h5>

                                    <!--begin::Close-->
                                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                        data-bs-dismiss="modal" aria-label="Close">
                                        <span class="svg-icon svg-icon-2x">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                height="24" viewBox="0 0 24 24" fill="none">
                                                <rect opacity="0.5" x="7.05025" y="15.5356" width="12"
                                                    height="2" rx="1"
                                                    transform="rotate(-45 7.05025 15.5356)"
                                                    fill="black" />
                                                <rect x="8.46447" y="7.05029" width="12" height="2"
                                                    rx="1" transform="rotate(45 8.46447 7.05029)"
                                                    fill="black" />
                                            </svg>
                                        </span>
                                    </div>
                                    <!--end::Close-->
                                </div>

                                <form method="POST" action="{{ route('tugas.penilaianSimpan', [$tugas->id, $mahasiswa->id]) }}"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="modal-body text-start">
                                        @foreach ($tugas->pertanyaanTugas as $pertanyaan)
                                        @php
                                            $jawabanTugas = \App\Models\JawabanTugas::query()
                                                ->where('tugas_id', $tugas?->id)
                                                ->where('mahasiswa_id', $mahasiswa?->id)
                                                ->where('pertanyaan_id', $pertanyaan?->id)
                                                ->first();
                                        @endphp
                                        <div class="mb-10">
                                            <input type="hidden" name="pertanyaans[]" value="{{ $pertanyaan?->id }}">
                                            <label class="required form-label">{{ $pertanyaan?->pertanyaan }}</label>
                                            @foreach (\App\Models\Tugas::NILAI_SELECT as $key => $nilai)
                                            <div class="form-check form-check-custom form-check-solid mb-2">
                                                <input class="form-check-input" name="poins[{{ $pertanyaan->id }}]" type="radio" value="{{ $key }}" id="flexRadioDefault{{ $key }}{{ $pertanyaan->id }}" @checked($jawabanTugas?->poin == $key) required />
                                                <label class="form-check-label" for="flexRadioDefault{{ $key }}{{ $pertanyaan->id }}">
                                                    {{ $nilai['ket'] }} ({{ $key }})
                                                </label>
                                            </div>
                                            @endforeach
                                        </div>
                                        @endforeach
                                    </div>

                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach
            </tbody>
            <!--end::Table body-->
        </table>
    </div>
</div>
