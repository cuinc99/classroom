<div class="card mb-10">
    <div class="card-body">
        <table class="table align-middle table-row-dashed">
            <!--begin::Table head-->
            <thead>
                <!--begin::Table row-->
                <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                    <th style="width: 50px">Poin</th>
                    <th style="width: 50px"></th>
                    <th class="min-w-70px">Keterangan</th>
                </tr>
                <!--end::Table row-->
            </thead>
            <!--end::Table head-->
            <!--begin::Table body-->
            <tbody class="fw-bold text-gray-600">
                @foreach ($pertanyaans as $pertanyaan)
                    <tr>
                        <td>
                            <b>P {{ $loop->iteration }}</b>
                        </td>
                        <td>
                            <span class="svg-icon svg-icon-2 me-3">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3" />
                                  </svg>
                            </span>
                        </td>
                        <td class="min-w-70px text-muted">{{ $pertanyaan->pertanyaan }}</td>
                    </tr>
                    @endforeach
                </tbody>
        </table>
    </div>
</div>
<div class="card">
    <div class="card-body pt-0 table-responsive">
        <table class="table align-middle table-row-dashed fs-6 gy-5">
            <!--begin::Table head-->
            <thead>
                <!--begin::Table row-->
                <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                    <th width="150px" class="text-end min-w-100px"></th>
                    <th class="min-w-175px">Mahasiswa</th>
                    @foreach ($pertanyaans as $pertanyaan)
                    <th class="text-end min-w-70px">P {{ $loop->iteration }}</th>
                    @endforeach
                    <th class="text-end min-w-70px">Poin</th>
                    <th class="text-end min-w-100px"></th>
                </tr>
                <!--end::Table row-->
            </thead>
            <!--end::Table head-->
            <!--begin::Table body-->
            <tbody class="fw-bold text-gray-600">
                <!--begin::Table row-->
                @foreach ($mahasiswas as $mahasiswa)
                    <tr>
                        <td>
                            <a href="{{ route('tugas.chat', [$tugas->kode, $mahasiswa->kode]) }}"
                                class="btn btn-light-primary"><i class="bi bi-chat-square-text-fill fs-4 me-2"></i>
                                Chat</a>
                        </td>
                        <!--begin::Customer-->
                        <td>
                            <div class="d-flex align-items-center">
                                <!--begin:: Avatar -->
                                <div class="symbol symbol-circle symbol-50px overflow-hidden me-3">
                                    <x-smart-image src="{{ public_path('/img/user.png') }}" height="100px" />
                                </div>
                                <!--end::Avatar-->
                                <div class="ms-5">
                                    <a href="#"
                                        class="text-gray-800 fw-bolder mb-1 fs-6 text-start pe-0">{{ $mahasiswa->user->name ?? '' }}</a>
                                    <span class="text-muted fw-bold d-block fs-7">NIM :
                                        {{ $mahasiswa->user->username ?? '-' }}</span>
                                </div>
                            </div>
                        </td>
                        @foreach ($pertanyaans as $pertanyaan)
                        @php
                            $poin = \App\Models\JawabanTugas::where('mahasiswa_id', $mahasiswa->id)->where('tugas_id', $tugas->id)->where('pertanyaan_id', $pertanyaan->id)->first();
                        @endphp
                        <td class="text-end">{{ $poin?->poin ?? '-' }}</td>
                        @endforeach
                        <td class="text-end">
                            <span
                                class="badge badge-success fw-bolder my-2">{{ number_format($mahasiswa->mahasiswa_tugas($tugas->id)->total_poin, 2) ?? '0' }}</span>
                            @if ($mahasiswa->mahasiswa_tugas($tugas->id)->tanggal_kumpul == null &&
                                $mahasiswa->mahasiswa_tugas($tugas->id)->total_poin != 0)
                                <span class="badge badge-light-danger fw-bolder my-2">
                                    Belum Dikumpulkan
                                </span>
                            @endif
                        </td>
                        <!--begin::Action-->
                        <td class="text-end">
                            @if (!$mahasiswa->mahasiswa_tugas($tugas->id)->file_tugas && $tugas->tipe == 'rubrik')
                                <a href="#" class="btn btn-sm btn-danger">Tugas Belum Dikumpulkan</a>
                            @elseif ($mahasiswa->mahasiswa_tugas($tugas->id)->tanggal_kumpul &&
                                $mahasiswa->mahasiswa_tugas($tugas->id)->file_tugas)
                                <a href="{{ route('tugas.penilaian', [$tugas->kode, $mahasiswa->kode]) }}"
                                    class="btn btn-sm btn-info">Penilaian Selesai - Edit Nilai</a>
                            @else
                                @php
                                    $totPoin = App\Models\MahasiswaTugas::whereMahasiswaId($mahasiswa->id)->whereTugasId($tugas->id)->firstOrFail();
                                @endphp
                                @if ($totPoin->total_poin)
                                <a href="{{ route('tugas.penilaianKumpul', [$tugas->id, $mahasiswa->id]) }}"
                                    class="btn btn-sm btn-success mb-2" onclick="return confirm('Yakin akan mengumpulkan?');">Kumpulkan</a> <br>
                                @endif
                                <a href="{{ route('tugas.penilaian', [$tugas->kode, $mahasiswa->kode]) }}"
                                    class="btn btn-sm btn-primary">Beri Penilaian</a>
                            @endif
                            <!--begin::Menu-->
                        </td>
                        <!--end::Action-->
                    </tr>
                @endforeach
            </tbody>
            <!--end::Table body-->
        </table>
    </div>
</div>
