@if ($mahasiswaTugas->tanggal_kumpul)
    <div class="card">
        <!--begin::Card body-->
        <div class="card-body p-0">
            <!--begin::Wrapper-->
            <div class="card-px text-center py-20 my-10">
                <!--begin::Title-->
                <h2 class="fs-2x text-muted mb-10">Poin Anda</h2>
                <h2 class="fs-3x fw-bolder mb-10">{{ $mahasiswaTugas->total_poin }}</h2>
                <!--end::Title-->
                <!--begin::Description-->
                <p class="text-gray-400 fs-4 fw-bold mb-10"></p>
                <!--end::Description-->
            </div>
            <!--end::Wrapper-->
            <!--begin::Illustration-->
            <div class="text-center px-4">
                <img class="mw-100 mh-300px" alt="" src="/assets/media/illustrations/sketchy-1/7.png" />
            </div>
            <!--end::Illustration-->
        </div>
        <!--end::Card body-->
    </div>
@endif

@foreach ($pertanyaans as $pertanyaan)
    <div class="card card-flush py-4 mb-5 bg-light shadow-md">
        <!--begin::Card body-->
        <div class="card-body pt-5">

            <!--begin::Input group-->
            <div class="mb-10 fv-row">
                <!--begin::Label-->
                <label class="form-label fs-2x"><span class="opacity-50">{{ $loop->iteration }}.</span>
                    {!! $pertanyaan->pertanyaan ?? '-' !!}</label>
                <br>
                <span class="text-muted">{!! $pertanyaan->deskripsi !!}</span>
                <!--end::Label-->
                @if ($mahasiswaTugas->tanggal_kumpul)
                    @php
                        $poin = \App\Models\JawabanTugas::where('mahasiswa_id', $mahasiswa->id)
                            ->where('tugas_id', $tugas->id)
                            ->where('pertanyaan_id', $pertanyaan->id)
                            ->first();
                    @endphp
                    <div class="d-flex flex-wrap mb-5 mt-10">
                        <!--begin::Due-->
                        <div class="border border-primary border-dashed rounded min-w-125px py-3 px-4 me-7 mb-3">
                            <div class="fs-6 text-gray-800 fw-bolder">POIN ANDA / MAKSIMAL POIN</div>
                            <div class="fw-bold">
                                @php
                                    $poin = \App\Models\JawabanTugas::where('mahasiswa_id', $mahasiswa->id)
                                        ->where('tugas_id', $tugas->id)
                                        ->where('pertanyaan_id', $pertanyaan->id)
                                        ->first();
                                @endphp
                                <span class="fs-2x fw-bolder">{{ $poin?->poin ?? '-' }}
                                </span>/ <span class=" text-gray-400">{{ $pertanyaan->maks_poin }}</span>
                            </div>
                            @if ($poin)
                            <span class="badge badge-{{ \App\Models\Tugas::NILAI_SELECT[$poin?->poin]['warna'] }} fw-bolder me-auto px-4 py-3">
                                {{ \App\Models\Tugas::NILAI_SELECT[$poin?->poin]['ket'] }}
                            </span>
                            @endif
                        </div>
                    </div>
                @else
                    <div class="d-flex flex-wrap mb-5 mt-10">
                        <!--begin::Due-->
                        <div class="border border-primary border-dashed rounded min-w-125px py-3 px-4 me-7 mb-3">
                            <div class="fs-6 text-gray-800 fw-bolder">POIN ANDA / MAKSIMAL POIN</div>
                            <div class="fw-bold">
                                @php
                                    $poin = \App\Models\JawabanTugas::where('mahasiswa_id', $mahasiswa->id)
                                        ->where('tugas_id', $tugas->id)
                                        ->where('pertanyaan_id', $pertanyaan->id)
                                        ->first();
                                @endphp
                                <span class="fs-2x fw-bolder">{{ $poin?->poin ?? '-' }}
                                </span>/ <span class=" text-gray-400">{{ $pertanyaan->maks_poin }}</span>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <!--end::Input group-->

        </div>
        <!--end::Card header-->
    </div>
@endforeach
