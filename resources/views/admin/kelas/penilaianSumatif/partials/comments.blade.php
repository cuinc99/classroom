
@foreach ($comments as $comment)
    <!--begin::Comment-->
    <div class="mb-9 {{ $comment->parent_id ? 'ms-10' : '' }}">
        <!--begin::Card-->
        <div class="card card-bordered w-100 {{ $comment->parent_id ? 'bg-light-info' : '' }}">
            <!--begin::Body-->
            <div class="card-body">
                <!--begin::Wrapper-->
                <div class="mb-8 w-100 d-flex flex-stack">
                    <!--begin::Container-->
                    <div class="d-flex align-items-center f">
                        <!--begin::Author-->
                        <div class="symbol symbol-50px me-5">
                            {{-- @if ($comment->user)
                            <img src="{{ $comment->user->avatar }}" alt="{{ $comment->user->name }}" />
                            @else
                            @endif --}}
                            <img src="/img/user.png" alt="guest">
                        </div>
                        <!--end::Author-->
                        <!--begin::Info-->
                        <div class="text-gray-600 d-flex flex-column fw-bold fs-5 text-dark">
                            <!--begin::Text-->
                            <div class="d-flex align-items-center">
                                <!--begin::Username-->
                                <a href="#"
                                    class="text-gray-800 fw-bolder text-hover-primary fs-5 me-3">{{ $comment->user ? $comment->user->name : 'Guest' }}</a>
                                <!--end::Username-->
                            </div>
                            <!--end::Text-->
                            <!--begin::Date-->
                            <span
                                class="text-muted fw-bold fs-6">{{ Date::parse($comment->created_at)->diffForHumans() }}</span>
                            <!--end::Date-->
                        </div>
                        <!--end::Info-->
                    </div>
                    <!--end::Container-->
                    <!--begin::Actions-->
                    {{-- <div class="m-0">
                    <button class="p-0 btn btn-color-gray-400 btn-active-color-primary fw-bolder">Reply</button>
                </div> --}}
                    <!--end::Actions-->
                </div>
                <!--end::Wrapper-->
                <!--begin::Desc-->
                <p class="m-0 text-gray-700 fw-normal fs-5">{{ $comment->comment }}</p>
                <!--end::Desc-->
                @if (!$comment->parent_id)
                    <form method="post" action="{{ route('reply.add') }}" class="mt-10"
                        style="display: block;">
                        @csrf
                        <div class="d-flex">
                            <input type="text" class="form-control form-control-solid me-3 flex-grow-1" name="comment"
                                placeholder="Balas komentar ini" />
                            <input type="hidden" name="mahasiswa_tugas_id" value="{{ $mahasiswa_tugas_id }}" />
                            <input type="hidden" name="comment_id" value="{{ $comment->id }}" />

                            <button type="submit" class="flex-shrink-0 btn btn-light fw-bolder">Balas</button>
                        </div>
                    </form>
                @endif
            </div>
            <!--end::Body-->
        </div>
        <!--end::Card-->
    </div>
    <!--end::Comment-->
    @include('admin.kelas.penilaianSumatif.partials.comments', [
        'comments' => $comment->replies,
    ])
@endforeach
