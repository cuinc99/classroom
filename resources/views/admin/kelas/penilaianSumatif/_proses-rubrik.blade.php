<div class="card card-xl-stretch bg-body border-0 mb-5 mb-xl-0">
    <!--begin::Body-->
    <div class="card-body d-flex flex-column flex-lg-row flex-stack p-lg-15">
        <!--begin::Info-->
        @if ($tugas->instruksi || $tugas->dokumen_instruksi)
            <div
                class="d-flex flex-column justify-content-center align-items-center align-items-lg-start me-10 text-center text-lg-start">
                <!--begin::Title-->
                <h5 class="line-height-lg mb-5">
                    <span>Instruksi</span>
                </h5>
                <!--end::Title-->
                <div class="fs-4 text-muted mb-7">
                    {!! $tugas->instruksi ?? 'Tidak ada instruksi, unduh file dibawah ini' !!}</div>
                @if ($tugas->dokumen_instruksi)
                    <a href='{{ url('storage/' . $tugas->dokumen_instruksi) }}'
                        class="btn btn-success fw-bold px-6 py-3">Unduh File Instruksi</a>
                @endif
            </div>
            <img src="/assets/media/illustrations/sketchy-1/10.png" alt=""
                class="mw-200px mw-lg-350px mt-lg-n10" />
        @endif
    </div>
    <!--end::Body-->
</div>
<form action="{{ route('tugas.kumpulkanRubrik', [$tugas->id, $mahasiswa->id]) }}" method="POST"
    enctype="multipart/form-data" class="mb-10">
    @csrf
    <div class="card">
        <!--begin::Card body-->
        <div class="card-body p-0">
            <!--begin::Wrapper-->
            <div class="card-px text-center py-20 my-10">
                @if ($mahasiswaTugas->file_tugas)
                    <small class="text-success">Anda telah mengumpulkan tugas</small> <br>
                    <a class="btn btn-info mb-5" data-bs-toggle="modal" data-bs-target="#preview_tugas">Lihat
                        File Tugas</a>
                    <div class="modal bg-white fade" tabindex="-1" id="preview_tugas">
                        <div class="modal-dialog modal-fullscreen">
                            <div class="modal-content shadow-none">
                                <div class="modal-header">
                                    <h5 class="modal-title">Preview File Tugas</h5>

                                    <!--begin::Close-->
                                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                        data-bs-dismiss="modal" aria-label="Close">
                                        <span class="svg-icon svg-icon-2x"></span>
                                    </div>
                                    <!--end::Close-->
                                </div>

                                <div class="modal-body">
                                    <embed src="{{ url('storage/' . $mahasiswaTugas->file_tugas) }}" width="100%"
                                        height="100%" type="application/pdf">
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Tutup</button>
                                    <a href="{{ url('storage/' . $mahasiswaTugas->file_tugas) }}" target="_blank"
                                        class="btn btn-primary">Download</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <!--begin::Title-->
                <div class="form-group">
                    <label class="form-label">Pilih File Tugas</label>
                    <input type="file" name="tugas" accept="application/pdf" class="filepond" id="tugas">
                </div>
                <!--end::Title-->
                <!--begin::Description-->
                <p class="text-gray-400 fs-4 fw-bold mb-10"></p>
                <!--end::Description-->
                <!--begin::Action-->
                <!--begin::Button-->
                <button type="submit" class="btn btn-primary">
                    @if ($mahasiswaTugas->file_tugas)
                        <span class="indicator-label">Kumpulkan Ulang</span>
                    @else
                        <span class="indicator-label">Kumpulkan</span>
                    @endif
                </button>

                <!--end::Button-->
                <!--end::Action-->
            </div>
            <!--end::Wrapper-->
            <!--begin::Illustration-->
            <div class="text-center px-4">
                <img class="mw-100 mh-300px" alt="" src="/assets/media/illustrations/sketchy-1/4.png" />
            </div>
            <!--end::Illustration-->
        </div>
        <!--end::Card body-->
    </div>
</form>
@if ($mahasiswaTugas->tanggal_kumpul)
    <div class="card">
        <!--begin::Card body-->
        <div class="card-body p-0">
            <!--begin::Wrapper-->
            <div class="card-px text-center py-20 my-10">
                <!--begin::Title-->
                <h2 class="fs-2x text-muted mb-10">Poin Anda</h2>
                <h2 class="fs-3x fw-bolder mb-10">{{ $mahasiswaTugas->total_poin }}</h2>
                <!--end::Title-->
                <!--begin::Description-->
                <p class="text-gray-400 fs-4 fw-bold mb-10"></p>
                <!--end::Description-->
            </div>
            <!--end::Wrapper-->
            <!--begin::Illustration-->
            <div class="text-center px-4">
                <img class="mw-100 mh-300px" alt="" src="/assets/media/illustrations/sketchy-1/7.png" />
            </div>
            <!--end::Illustration-->
        </div>
        <!--end::Card body-->
    </div>
@endif

@foreach ($pertanyaans as $pertanyaan)
    <div class="card card-flush py-4 mb-5 bg-light shadow-md">
        <!--begin::Card body-->
        <div class="card-body pt-5">

            <!--begin::Input group-->
            <div class="mb-10 fv-row">
                <!--begin::Label-->
                <label class="form-label fs-2x"><span class="opacity-50">{{ $loop->iteration }}.</span>
                    {!! $pertanyaan->pertanyaan ?? '-' !!}</label>
                <br>
                <span class="text-muted">{!! $pertanyaan->deskripsi !!}</span>
                <!--end::Label-->
                @if ($mahasiswaTugas->tanggal_kumpul)
                    <div class="d-flex flex-wrap mb-5 mt-10">
                        <!--begin::Due-->
                        <div class="border border-primary border-dashed rounded min-w-125px py-3 px-4 me-7 mb-3">
                            <div class="fs-6 text-gray-800 fw-bolder">POIN ANDA / MAKSIMAL POIN</div>
                            <div class="fw-bold">
                                @php
                                    $poin = \App\Models\JawabanTugas::where('mahasiswa_id', $mahasiswa->id)
                                        ->where('tugas_id', $tugas->id)
                                        ->where('pertanyaan_id', $pertanyaan->id)
                                        ->first();
                                @endphp
                                <span class="fs-2x fw-bolder">{{ $poin?->poin ?? '-' }}
                                </span>/ <span class=" text-gray-400">{{ $pertanyaan->maks_poin }}</span>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="d-flex flex-wrap mb-5 mt-10">
                        <!--begin::Due-->
                        <div class="border border-primary border-dashed rounded min-w-125px py-3 px-4 me-7 mb-3">
                            <div class="fs-6 text-gray-800 fw-bolder">POIN ANDA / MAKSIMAL POIN</div>
                            <div class="fw-bold">
                                @php
                                    $poin = \App\Models\JawabanTugas::where('mahasiswa_id', $mahasiswa->id)
                                        ->where('tugas_id', $tugas->id)
                                        ->where('pertanyaan_id', $pertanyaan->id)
                                        ->first();
                                @endphp
                                <span class="fs-2x fw-bolder">{{ $poin?->poin ?? '-' }}
                                </span>/ <span class=" text-gray-400">{{ $pertanyaan->maks_poin }}</span>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <!--end::Input group-->

        </div>
        <!--end::Card header-->
    </div>
@endforeach
