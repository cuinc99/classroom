@extends('layouts.admin')

@section('title')
    Hasil {{ Str::ucfirst($tugas->tipe) }}
@endsection

@section('content')
    <!--begin::Toolbar-->
    <div class="toolbar py-5 py-lg-5">
        <!--begin::Container-->
        <div class="container-xxl d-flex flex-stack flex-wrap">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column me-3">
                <!--begin::Title-->
                <h1 class="d-flex text-dark fw-bolder my-1 fs-3">Hasil {{ Str::ucfirst($tugas->tipe) }} - ({{ $mahasiswa->user->username }}) {{ $mahasiswa->user->name }}</h1>
                <!--end::Title-->
            </div>
            <!--end::Page title-->
            <div class="d-flex align-items-center py-2 py-md-1">
                <div>
                    <a href="{{ route('tugas.detail', $tugas->kode) }}" class="btn btn-sm btn-secondary fw-bolder">
                        Kembali
                        <span class="svg-icon svg-icon-1hx svg-icon-gray-500 me-1">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3" />
                              </svg>

                        </span>
                    </a>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Container-->
    <div class="d-flex flex-column-fluid align-items-start container-xxl">
        <div class="content flex-row-fluid">

            @if ($tugas->tipe == 'rubrik')
                @include('admin.kelas.penilaianSumatif._hasil-rubrik')
            @endif

            @if ($tugas->tipe == 'rubrik2')
                @include('admin.kelas.penilaianSumatif._hasil-rubrik2')
            @endif

            @if ($tugas->tipe == 'kuis')
                @include('admin.kelas.penilaianSumatif._hasil-kuis')
            @endif

        </div>
        <!--end::Post-->
    </div>
    <!--end::Container-->
@endsection

@section('styles')
@endsection

@section('scripts')
@endsection
