@extends('layouts.admin')

@section('title')
    Chat
@endsection

@section('content')
<div class="toolbar py-5 py-lg-5">
    <!--begin::Container-->
    <div class="container-xxl d-flex flex-stack flex-wrap">
        <!--begin::Page title-->
        <div class="page-title d-flex flex-column me-3">
            <!--begin::Title-->
            <h1 class="d-flex text-dark fw-bolder my-1 fs-3">Chat dengan {{ $mahasiswa->user->name }}</h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <div class="d-flex align-items-center py-2 py-md-1">
            <div>
                @if (auth()->user()->isAdmin())
                <a href="{{ route('tugas.detail', $tugas->kode) }}" class="btn btn-sm btn-secondary fw-bolder">
                    Kembali
                    <span class="svg-icon svg-icon-1hx svg-icon-gray-500 me-1">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3" />
                          </svg>

                    </span>
                </a>
                @elseif (auth()->user()->isDosen())
                <a href="{{ route('tugas.proses', $tugas->kode) }}" class="btn btn-sm btn-secondary fw-bolder">
                    Kembali
                    <span class="svg-icon svg-icon-1hx svg-icon-gray-500 me-1">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3" />
                          </svg>

                    </span>
                </a>
                @endif
            </div>
        </div>
        {{-- <div class="d-flex align-items-center py-2 py-md-1">
            <!--begin::Button-->
            <a href="{{ route('tugas.proses', $tugas->kode) }}" class="btn btn-primary fw-bolder btn-hover-scale" id="kt_toolbar_primary_button">
                <!--begin::Svg Icon | path: assets/media/icons/duotune/arrows/arr087.svg-->
                <span class="svg-icon svg-icon-muted svg-icon-1"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <path d="M17.6 4L9.6 12L17.6 20H13.6L6.3 12.7C5.9 12.3 5.9 11.7 6.3 11.3L13.6 4H17.6Z" fill="black"/>
                    </svg></span>
                <!--end::Svg Icon-->
                Kembali
                </a>
            <!--end::Button-->
        </div> --}}
    </div>
    <!--end::Container-->
</div>
    <!--begin::Container-->
    <div class="d-flex flex-column-fluid align-items-start container-xxl">
        <div class="content flex-row-fluid">
            <div class="mt-10">
                <form method="post" action="{{ route('comment.add') }}">
                    @csrf
                    <textarea class="placeholder-gray-600 form-control form-control-solid fw-bolder fs-4 ps-9 pt-7" rows="6" name="comment"
                        placeholder="Beri komentar anda"></textarea>
                    <input type="hidden" name="mahasiswa_tugas_id" value="{{ $mahasiswaTugas->id }}" />
                    <!--begin::Submit-->
                    <button type="submit"
                        class="mb-20 btn btn-primary mt-n20 position-relative float-end me-7">Kirim</button>
                    <!--end::Submit-->
                </form>
            </div>
            <!--end::Input group-->

            <div class="mb-15">
                @include('admin.kelas.penilaianSumatif.partials.comments', [
                    'comments' => $mahasiswaTugas->comments,
                    'mahasiswa_tugas_id' => $mahasiswaTugas->id,
                ])
            </div>

        </div>
        <!--end::Post-->
    </div>
    <!--end::Container-->
@endsection

@section('styles')
@endsection

@section('scripts')
@endsection
