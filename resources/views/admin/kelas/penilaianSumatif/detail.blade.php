@extends('layouts.admin')

@section('title')
    Detail Tugas
@endsection

@section('content')
    <!--begin::Toolbar-->
    <div class="toolbar py-5 py-lg-5">
        <!--begin::Container-->
        <div class="container-xxl d-flex flex-stack flex-wrap">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column me-3">
                <!--begin::Title-->
                <h1 class="d-flex text-dark fw-bolder my-1 fs-3">Detail Tugas</h1>
                <!--end::Title-->
            </div>
            <!--end::Page title-->
            <div class="d-flex align-items-center py-2 py-md-1">
                <div>
                    <a href="{{ route('kelas.penilaianSumatif', $tugas->kelas->kode) }}" class="btn btn-sm btn-secondary fw-bolder">
                        Kembali
                        <span class="svg-icon svg-icon-1hx svg-icon-gray-500 me-1">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                stroke="currentColor" class="w-6 h-6">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                    d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3" />
                            </svg>

                        </span>
                    </a>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Container-->
    <div class="d-flex flex-column-fluid align-items-start container-xxl">
        <div class="content flex-row-fluid">
            <div class="row">
                <div class="col-12 col-md-3">
                    <div class="card card-flush bg-light mb-5" data-kt-sticky="true"
                        data-kt-sticky-name="docs-sticky-summary" data-kt-sticky-offset="{default: false, xl: '200px'}"
                        data-kt-sticky-width="{lg: '250px', xl: '300px'}" data-kt-sticky-left="auto"
                        data-kt-sticky-top="100px" data-kt-sticky-animation="false" data-kt-sticky-zindex="95">
                        <!--begin::Header-->
                        <div class="card-header py-2">
                            <!--begin::Title-->
                            <h3 class="card-title align-items-start flex-column">
                                <span
                                    class="badge badge-{{ \App\Models\Tugas::TIPE_OPTION[$tugas->tipe]['warna'] }} fw-bolder my-2">{{ Str::ucfirst($tugas->tipe) }}</span>
                                <span class="card-label fw-bolder text-dark">Detail Informasi</span>
                            </h3>
                            <!--end::Title-->
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div class="card-body pt-2">
                            <div class="d-flex flex-stack">
                                <!--begin::Wrapper-->
                                <div class="d-flex align-items-center me-3">
                                    <!--begin::Icon-->
                                    <span class="svg-icon svg-icon-2 me-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none">
                                            <path opacity="0.3"
                                                d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                                                fill="black" />
                                            <path
                                                d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                                                fill="black" />
                                        </svg>
                                    </span>
                                    <!--end::Icon-->
                                    <!--begin::Section-->
                                    <div class="flex-grow-1">
                                        <span class="text-gray-400 fw-bold d-block fs-7">Nama Tugas</span>
                                        <a href="#"
                                            class="text-gray-800 text-hover-primary fs-5 fw-bolder lh-0">{{ $tugas->judul ?? '-' }}</a>
                                    </div>
                                    <!--end::Section-->
                                </div>
                                <!--end::Wrapper-->
                            </div>
                            @if ($tugas->batas_akhir)
                                <div class="separator separator-dashed my-3"></div>
                                <div class="d-flex flex-stack">
                                    <!--begin::Wrapper-->
                                    <div class="d-flex align-items-center me-3">
                                        <!--begin::Icon-->
                                        <span class="svg-icon svg-icon-2 me-3">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="21"
                                                viewBox="0 0 20 21" fill="none">
                                                <path opacity="0.3"
                                                    d="M19 3.40002C18.4 3.40002 18 3.80002 18 4.40002V8.40002H14V4.40002C14 3.80002 13.6 3.40002 13 3.40002C12.4 3.40002 12 3.80002 12 4.40002V8.40002H8V4.40002C8 3.80002 7.6 3.40002 7 3.40002C6.4 3.40002 6 3.80002 6 4.40002V8.40002H2V4.40002C2 3.80002 1.6 3.40002 1 3.40002C0.4 3.40002 0 3.80002 0 4.40002V19.4C0 20 0.4 20.4 1 20.4H19C19.6 20.4 20 20 20 19.4V4.40002C20 3.80002 19.6 3.40002 19 3.40002ZM18 10.4V13.4H14V10.4H18ZM12 10.4V13.4H8V10.4H12ZM12 15.4V18.4H8V15.4H12ZM6 10.4V13.4H2V10.4H6ZM2 15.4H6V18.4H2V15.4ZM14 18.4V15.4H18V18.4H14Z"
                                                    fill="black" />
                                                <path
                                                    d="M19 0.400024H1C0.4 0.400024 0 0.800024 0 1.40002V4.40002C0 5.00002 0.4 5.40002 1 5.40002H19C19.6 5.40002 20 5.00002 20 4.40002V1.40002C20 0.800024 19.6 0.400024 19 0.400024Z"
                                                    fill="black" />
                                            </svg>
                                        </span>
                                        <!--end::Icon-->
                                        <!--begin::Section-->
                                        <div class="flex-grow-1">
                                            <span class="text-gray-400 fw-bold d-block fs-7">Deadline</span>
                                            <a href="#" class="text-gray-800 text-hover-primary fs-5 fw-bolder lh-0">
                                                <span class="text-{{ $tugas->color }}">
                                                    {{ Date::parse($tugas->batas_akhir)->format('l, d M Y H:i') }}
                                                </span>
                                            </a>
                                        </div>
                                        <!--end::Section-->
                                    </div>
                                    <!--end::Wrapper-->
                                </div>
                            @endif
                            <div class="separator separator-dashed my-3"></div>
                            <div class="d-flex flex-stack">
                                <!--begin::Wrapper-->
                                <div class="d-flex align-items-center me-3">
                                    <!--begin::Icon-->
                                    <span class="svg-icon svg-icon-2 me-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none">
                                            <path opacity="0.3"
                                                d="M18 10V20C18 20.6 18.4 21 19 21C19.6 21 20 20.6 20 20V10H18Z"
                                                fill="black" />
                                            <path opacity="0.3"
                                                d="M11 10V17H6V10H4V20C4 20.6 4.4 21 5 21H12C12.6 21 13 20.6 13 20V10H11Z"
                                                fill="black" />
                                            <path opacity="0.3" d="M10 10C10 11.1 9.1 12 8 12C6.9 12 6 11.1 6 10H10Z"
                                                fill="black" />
                                            <path opacity="0.3" d="M18 10C18 11.1 17.1 12 16 12C14.9 12 14 11.1 14 10H18Z"
                                                fill="black" />
                                            <path opacity="0.3" d="M14 4H10V10H14V4Z" fill="black" />
                                            <path opacity="0.3" d="M17 4H20L22 10H18L17 4Z" fill="black" />
                                            <path opacity="0.3" d="M7 4H4L2 10H6L7 4Z" fill="black" />
                                            <path
                                                d="M6 10C6 11.1 5.1 12 4 12C2.9 12 2 11.1 2 10H6ZM10 10C10 11.1 10.9 12 12 12C13.1 12 14 11.1 14 10H10ZM18 10C18 11.1 18.9 12 20 12C21.1 12 22 11.1 22 10H18ZM19 2H5C4.4 2 4 2.4 4 3V4H20V3C20 2.4 19.6 2 19 2ZM12 17C12 16.4 11.6 16 11 16H6C5.4 16 5 16.4 5 17C5 17.6 5.4 18 6 18H11C11.6 18 12 17.6 12 17Z"
                                                fill="black" />
                                        </svg>
                                    </span>
                                    <!--end::Icon-->
                                    <!--begin::Section-->
                                    <div class="flex-grow-1">
                                        <span class="text-gray-400 fw-bold d-block fs-7">Nama Kelas</span>
                                        <a href="#"
                                            class="text-gray-800 text-hover-primary fs-5 fw-bolder lh-0">{{ $tugas->kelas->nama ?? '-' }}</a>
                                    </div>
                                    <!--end::Section-->
                                </div>
                                <!--end::Wrapper-->
                            </div>
                            <div class="separator separator-dashed my-3"></div>
                            <div class="d-flex flex-stack">
                                <!--begin::Wrapper-->
                                <div class="d-flex align-items-center me-3">
                                    <!--begin::Icon-->
                                    <span class="svg-icon svg-icon-2 me-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none">
                                            <path
                                                d="M16.0173 9H15.3945C14.2833 9 13.263 9.61425 12.7431 10.5963L12.154 11.7091C12.0645 11.8781 12.1072 12.0868 12.2559 12.2071L12.6402 12.5183C13.2631 13.0225 13.7556 13.6691 14.0764 14.4035L14.2321 14.7601C14.2957 14.9058 14.4396 15 14.5987 15H18.6747C19.7297 15 20.4057 13.8774 19.912 12.945L18.6686 10.5963C18.1487 9.61425 17.1285 9 16.0173 9Z"
                                                fill="black" />
                                            <rect opacity="0.3" x="14" y="4" width="4"
                                                height="4" rx="2" fill="black" />
                                            <path
                                                d="M4.65486 14.8559C5.40389 13.1224 7.11161 12 9 12C10.8884 12 12.5961 13.1224 13.3451 14.8559L14.793 18.2067C15.3636 19.5271 14.3955 21 12.9571 21H5.04292C3.60453 21 2.63644 19.5271 3.20698 18.2067L4.65486 14.8559Z"
                                                fill="black" />
                                            <rect opacity="0.3" x="6" y="5" width="6"
                                                height="6" rx="3" fill="black" />
                                        </svg>
                                    </span>
                                    <!--end::Icon-->
                                    <!--begin::Section-->
                                    <div class="flex-grow-1">
                                        <span class="text-gray-400 fw-bold d-block fs-7">Jumlah Mahasiswa</span>
                                        <a href="#"
                                            class="text-gray-800 text-hover-primary fs-5 fw-bolder lh-0">{{ $mahasiswas->count() }}
                                            Orang</a>
                                    </div>
                                    <!--end::Section-->
                                </div>
                                <!--end::Wrapper-->
                            </div>
                            <div class="separator separator-dashed my-3"></div>
                            <div class="d-flex flex-stack">
                                <!--begin::Wrapper-->
                                <div class="d-flex align-items-center me-3">
                                    <!--begin::Icon-->
                                    <span class="svg-icon svg-icon-2 me-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none">
                                            <rect x="8" y="9" width="3" height="10"
                                                rx="1.5" fill="black" />
                                            <rect opacity="0.5" x="13" y="5" width="3"
                                                height="14" rx="1.5" fill="black" />
                                            <rect x="18" y="11" width="3" height="8"
                                                rx="1.5" fill="black" />
                                            <rect x="3" y="13" width="3" height="6"
                                                rx="1.5" fill="black" />
                                        </svg>
                                    </span>
                                    <!--end::Icon-->
                                    <!--begin::Section-->
                                    <div class="flex-grow-1">
                                        <span class="text-gray-400 fw-bold d-block fs-7">Nama Blok</span>
                                        <a href="#"
                                            class="text-gray-800 text-hover-primary fs-5 fw-bolder lh-0">{{ $tugas->kelas->blok->nama }}</a>
                                    </div>
                                    <!--end::Section-->
                                </div>
                                <!--end::Wrapper-->
                            </div>
                            <div class="separator separator-dashed my-3"></div>
                            <div class="d-flex flex-stack">
                                <!--begin::Wrapper-->
                                <div class="d-flex align-items-center me-3">
                                    <!--begin::Icon-->
                                    <span class="svg-icon svg-icon-2 me-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none">
                                            <path opacity="0.3"
                                                d="M13.625 22H9.625V3C9.625 2.4 10.025 2 10.625 2H12.625C13.225 2 13.625 2.4 13.625 3V22Z"
                                                fill="black" />
                                            <path
                                                d="M19.625 10H12.625V4H19.625L21.025 6.09998C21.325 6.59998 21.325 7.30005 21.025 7.80005L19.625 10Z"
                                                fill="black" />
                                            <path
                                                d="M3.62499 16H10.625V10H3.62499L2.225 12.1001C1.925 12.6001 1.925 13.3 2.225 13.8L3.62499 16Z"
                                                fill="black" />
                                        </svg>
                                    </span>
                                    <!--end::Icon-->
                                    <!--begin::Section-->
                                    <div class="flex-grow-1">
                                        <span class="text-gray-400 fw-bold d-block fs-7">Semester</span>
                                        <a href="#"
                                            class="text-gray-800 text-hover-primary fs-5 fw-bolder lh-0">{{ $tugas->kelas->blok->semester }}</a>
                                    </div>
                                    <!--end::Section-->
                                </div>
                                <!--end::Wrapper-->
                            </div>

                        </div>
                        <!--end::Body-->
                    </div>
                </div>
                <div class="col-12 col-md-9">
                    <div class="card">
                        <div class="card-body pt-0">
                            <div class="table-responsive">
                                <table class="table align-middle table-row-dashed fs-6 gy-5">
                                    <!--begin::Table head-->
                                    <thead>
                                        <!--begin::Table row-->
                                        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                                            <th class="text-end min-w-100px"></th>
                                            <th class="min-w-175px">Mahasiswa</th>
                                            <th class="min-w-175px text-end">Status</th>
                                            <th class="text-end min-w-70px">Poin</th>
                                        </tr>
                                        <!--end::Table row-->
                                    </thead>
                                    <!--end::Table head-->
                                    <!--begin::Table body-->
                                    <tbody class="fw-bold text-gray-600">
                                        <!--begin::Table row-->
                                        @foreach ($mahasiswas as $mahasiswa)
                                            <tr>
                                                <td>
                                                    <a href="{{ route('tugas.chat', [$tugas->kode, $mahasiswa->kode]) }}"
                                                        class="btn btn-sm btn-light-primary"><i
                                                            class="bi bi-chat-square-text-fill fs-4 me-2"></i> Chat</a>
                                                    @if ($tugas->tipe != 'input')
                                                        @if ($mahasiswa->mahasiswa_tugas($tugas->id)->tanggal_kumpul != null)
                                                            <a href="{{ route('tugas.hasil', [$tugas->kode, $mahasiswa->kode]) }}"
                                                                class="btn btn-sm btn-light-{{ \App\Models\Tugas::TIPE_OPTION[$tugas->tipe]['warna'] }}"><i
                                                                    class="bi bi-activity fs-4 me-2"></i> Hasil
                                                                {{ Str::ucfirst($tugas->tipe) }}</a>

                                                            @if ($tugas->tipe == 'kuis')
                                                                <a href="{{ route('tugas.ujianUlang', [$tugas->kode, $mahasiswa->kode]) }}"
                                                                    onclick="return confirm('Yakin akan mengulang ujian? hasil ujian sebelumnya akan terhapus!');"
                                                                    class="btn btn-sm btn-light-info"><i
                                                                        class="bi bi-joystick fs-4 me-2"></i> Ujian
                                                                    Ulang</a>
                                                            @endif
                                                        @endif
                                                    @endif
                                                </td>
                                                <!--begin::Customer-->
                                                <td>
                                                    <div class="d-flex align-items-center">
                                                        <!--begin:: Avatar -->
                                                        <div class="symbol symbol-circle symbol-50px overflow-hidden me-3">
                                                            <x-smart-image src="{{ public_path('/img/user.png') }}"
                                                                height="100px" />
                                                        </div>
                                                        <!--end::Avatar-->
                                                        <div class="ms-5">
                                                            <a href="#"
                                                                class="text-gray-800 fw-bolder mb-1 fs-6 text-start pe-0">{{ $mahasiswa->user->name ?? '' }}</a>
                                                            <span class="text-muted fw-bold d-block fs-7">NIM :
                                                                {{ $mahasiswa->user->username ?? '-' }}</span>
                                                            @if ($tugas->tipe == 'rubrik')
                                                                <span class="text-muted fw-bold d-block fs-7">Penilai
                                                                    :</span>
                                                                {!! App\Services\MahasiswaService::getDosenPenilai($tugas->id, $mahasiswa->user->id) !!}
                                                            @endif
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="text-end">
                                                    @if ($tugas->tipe == 'rubrik')
                                                        @if ($mahasiswa->mahasiswa_tugas($tugas->id)->tanggal_kumpul &&
                                                            $mahasiswa->mahasiswa_tugas($tugas->id)->file_tugas)
                                                            <span
                                                                class="badge badge-success fw-bolder my-2">Finished</span>
                                                        @elseif($mahasiswa->mahasiswa_tugas($tugas->id)->file_tugas)
                                                            <span class="badge badge-info fw-bolder my-2">Submitted</span>
                                                        @else
                                                            <span class="badge badge-warning fw-bolder my-2">Pending</span>
                                                        @endif
                                                    @else
                                                        @if ($mahasiswa->mahasiswa_tugas($tugas->id)->tanggal_kumpul)
                                                            <span
                                                                class="badge badge-success fw-bolder my-2">Finished</span>
                                                        @else
                                                            <span class="badge badge-warning fw-bolder my-2">Pending</span>
                                                        @endif
                                                    @endif
                                                </td>
                                                <td class="text-end">
                                                    @if ($mahasiswa->mahasiswa_tugas($tugas->id)->tanggal_kumpul == null)
                                                        <span
                                                            class="badge badge-danger fw-bolder my-2">{{ number_format($mahasiswa->mahasiswa_tugas($tugas->id)->total_poin, 2) ?? '0' }}</span>
                                                    @else
                                                        <span
                                                            class="badge badge-success fw-bolder my-2">{{ number_format($mahasiswa->mahasiswa_tugas($tugas->id)->total_poin, 2) ?? '0' }}</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <!--end::Table body-->
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--end::Post-->
    </div>
    <!--end::Container-->
@endsection

@section('styles')
@endsection

@section('scripts')
@endsection
