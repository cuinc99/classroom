@extends('layouts.admin')

@section('title')
    Informasi
@endsection

@section('content')
    <!--begin::Toolbar-->
    <div class="toolbar py-5 py-lg-5">
        <!--begin::Container-->
        <div class="container-xxl d-flex flex-stack flex-wrap">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column me-3">
                <!--begin::Title-->
                @if ($tugas->status_ujian || $mahasiswaTugas->tanggal_kumpul)
                    @if ($tugas->tipe == 'kuis')
                        <h1 class="d-flex text-dark fw-bolder my-1 fs-3">
                            {{ $mahasiswaTugas->tanggal_kumpul ? 'Anda telah menyelesaikan tugas ini!' : 'Proses Pengerjaan Tugas' }}
                        </h1>
                    @endif
                    @if ($tugas->tipe == 'rubrik')
                        <h1 class="d-flex text-dark fw-bolder my-1 fs-3">
                            {{ $mahasiswaTugas->tanggal_kumpul ? 'Dosen telah menyelesaikan penginputan nilai!': 'Proses Penginputan Nilai oleh Dosen' }}
                        </h1>
                        @if ($tugas->batas_akhir && !$mahasiswaTugas->tanggal_kumpul)
                        Deadline :
                        <span class="text-{{ $tugas->color }}">
                            {{ Date::parse($tugas->batas_akhir)->format('l, d M Y H:i') }}
                            @if ($tugas->late)
                            <span class="mx-3">|</span>
                            {{ $tugas->late }}
                        </span>
                            @endif
                        @endif
                    @endif
                    @if ($tugas->tipe == 'rubrik2')
                        <h1 class="d-flex text-dark fw-bolder my-1 fs-3">
                            {{ $mahasiswaTugas->tanggal_kumpul ? 'Dosen telah menyelesaikan penilaian!': 'Proses penilaian oleh Dosen' }}
                        </h1>
                    @endif
                    @if ($tugas->tipe == 'input')
                        <h1 class="d-flex text-dark fw-bolder my-1 fs-3">
                            {{ $mahasiswaTugas->tanggal_kumpul ? 'Admin telah menyelesaikan penginputan nilai!': 'Menunggu Penginputan Nilai oleh Dosen' }}
                        </h1>
                    @endif
                @else
                    <h1 class="d-flex text-dark fw-bolder my-1 fs-3">
                        Informasi
                    </h1>
                @endif
                <!--end::Title-->
            </div>
            <!--end::Page title-->
            <div class="d-flex align-items-center py-2 py-md-1">
                <div>
                    <a href="{{ route('kelas.show', $tugas->kelas->kode) }}" class="btn btn-sm btn-secondary fw-bolder">
                        Kembali
                        <span class="svg-icon svg-icon-1hx svg-icon-gray-500 me-1">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                stroke="currentColor" class="w-6 h-6">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                    d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3" />
                            </svg>

                        </span>
                    </a>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Container-->
    <div class="flex-column-fluid align-items-start container-xxl">
        <div class="content flex-row-fluid">
            @if ($tugas->status_ujian || $mahasiswaTugas->tanggal_kumpul)
                @if ($tugas->tipe == 'rubrik')
                    @include('admin.kelas.penilaianSumatif._proses-rubrik')
                @endif
                @if ($tugas->tipe == 'rubrik2')
                    @include('admin.kelas.penilaianSumatif._proses-rubrik2')
                @endif
                @if ($tugas->tipe == 'kuis')
                    @include('admin.kelas.penilaianSumatif._proses-kuis')
                @endif
                @if ($tugas->tipe == 'input')
                    @include('admin.kelas.penilaianSumatif._proses-input')
                @endif
            @endif
        </div>
        <!--end::Post-->

        @if ($tugas->tipe != 'kuis')
        <div class="mt-10">
            <h2 class="mb-6 text-gray-800 fs-1 w-bolder">Chat</h2>

            <form method="post" action="{{ route('comment.add') }}">
                @csrf
                <textarea class="placeholder-gray-600 form-control form-control-solid fw-bolder fs-4 ps-9 pt-7" rows="6" name="comment"
                    placeholder="Beri komentar anda"></textarea>
                <input type="hidden" name="mahasiswa_tugas_id" value="{{ $mahasiswaTugas->id }}" />
                <!--begin::Submit-->
                <button type="submit" class="mb-20 btn btn-primary mt-n20 position-relative float-end me-7">Kirim</button>
                <!--end::Submit-->
            </form>
        </div>
        <!--end::Input group-->

        <div class="mb-15">
            @include('admin.kelas.penilaianSumatif.partials.comments', [
                'comments' => $mahasiswaTugas->comments,
                'mahasiswa_tugas_id' => $mahasiswaTugas->id,
            ])
        </div>
        @endif
    </div>
    <!--end::Container-->
@endsection

@section('styles')
    <!--begin::Page Custom Styles(used by this page)-->
    <link href="https://unpkg.com/filepond/dist/filepond.css" rel="stylesheet">
    <!--end::Page Custom Styles-->
    <style>
        /* .filepond--panel-root{
                background-color: white;
            } */

    </style>
@endsection

@section('scripts')
    <script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
    <script src="https://unpkg.com/filepond/dist/filepond.js"></script>
    <script>
        const inputElement = document.querySelector('input[id="tugas"]');
        FilePond.registerPlugin(
            FilePondPluginFileValidateType,
        );

        FilePond.create(inputElement, {
            credits: false,
            labelIdle: `Drag & Drop your file or <span class="filepond--label-action">Browse</span>`,
        });

        FilePond.setOptions({
            server: {
                url: '/tugas/upload-tugas',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            }
        });
    </script>
    <!--end::Page Scripts-->
    @stack('child-scripts')
@endsection
