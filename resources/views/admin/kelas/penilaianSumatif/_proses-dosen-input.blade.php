<div class="card">
    <form action="{{ route('tugas.inputNilaiSimpan', [$tugas->id]) }}" method="post">
        @csrf
        <div class="card-body pt-0">
            <table class="table align-middle table-row-dashed fs-6 gy-5">
                <!--begin::Table head-->
                <thead>
                    <!--begin::Table row-->
                    <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                        <th width="150px" class="text-end min-w-100px"></th>
                        <th class="min-w-175px">Mahasiswa</th>
                        <th width="150px" class="text-end">Nilai</th>
                    </tr>
                    <!--end::Table row-->
                </thead>
                <!--end::Table head-->
                <!--begin::Table body-->
                <tbody class="fw-bold text-gray-600">
                    <!--begin::Table row-->
                    @foreach ($mahasiswas as $mahasiswa)
                        <tr>
                            <td>
                                <a href="{{ route('tugas.chat', [$tugas->kode, $mahasiswa->kode]) }}"
                                    class="btn btn-light-primary"><i class="bi bi-chat-square-text-fill fs-4 me-2"></i>
                                    Chat</a>
                            </td>
                            <!--begin::Customer-->
                            <td>
                                <div class="d-flex align-items-center">
                                    <!--begin:: Avatar -->
                                    <div class="symbol symbol-circle symbol-50px overflow-hidden me-3">
                                        <x-smart-image src="{{ public_path('/img/user.png') }}" height="100px" />
                                    </div>
                                    <!--end::Avatar-->
                                    <div class="ms-5">
                                        <a href="#"
                                            class="text-gray-800 fw-bolder text-hover-primary mb-1 fs-6 text-start pe-0">{{ $mahasiswa->user->name ?? '' }}</a>
                                        <span class="text-muted fw-bold d-block fs-7">NIM :
                                            {{ $mahasiswa->user->username ?? '-' }}</span>
                                    </div>
                                </div>
                            </td>
                            <td class="text-end">
                                <div class="input-group">
                                    <span class="input-group-text" id="basic-addon1" style="border: none">
                                        <!--begin::Svg Icon | path: icons/duotune/communication/com006.svg-->
                                        <i class="fas fa-envelope-open-text fs-4"></i>
                                        <!--end::Svg Icon-->
                                    </span>
                                    <input type="number" name="nilais[{{ $mahasiswa->id }}]"
                                        class="form-control form-control-solid"
                                        value="{{ number_format($mahasiswa->mahasiswa_tugas($tugas->id)->total_poin, 2) ?? '0' }}"
                                        placeholder="Input Nilai" aria-label="Input Nilai"
                                        aria-describedby="basic-addon1"
                                        step="0.01"
                                        min="0"/>
                                </div>
                                <input type="hidden" name="ids[]" value="{{ $mahasiswa->id }}">
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <!--end::Table body-->
            </table>
        </div>
        <div class="card-footer text-end">
            <a href="{{ route('kelas.penilaianSumatif', $tugas->kelas->kode) }}" class="btn btn-light me-2">Kembali</a>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
    </form>

</div>
