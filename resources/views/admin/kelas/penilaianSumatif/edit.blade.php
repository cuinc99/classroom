@extends('layouts.admin')

@section('title')
    Edit Tugas
@endsection

@section('content')
    <!--begin::Toolbar-->
    <div class="toolbar py-5 py-lg-5">
        <!--begin::Container-->
        <div class="container-xxl d-flex flex-stack flex-wrap">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column me-3">
                <!--begin::Title-->
                <h1 class="d-flex text-dark fw-bolder my-1 fs-3">Edit Tugas</h1>
                <!--end::Title-->
            </div>
            <!--end::Page title-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Container-->
    <div class="d-flex flex-column-fluid align-items-start container-xxl">
        <div class="content flex-row-fluid">
            <form method="POST" action="{{ route('tugas.update', $tugas->kode) }}" enctype="multipart/form-data">
                @csrf
                <div class="d-flex flex-column gap-7 gap-lg-10">
                    <!--begin::Inventory-->
                    <div class="card card-flush py-4">
                        <!--begin::Card body-->
                        <div class="card-body pt-5">
                            <!--begin::Input group-->
                            <div class="mb-10 fv-row">
                                <!--begin::Label-->
                                <label class="required form-label">Judul</label>
                                <!--end::Label-->
                                <div class="input-group input-group-solid mb-5">
                                    <span class="input-group-text" id="basic-addon2">
                                        <span class="svg-icon svg-icon-1">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                viewBox="0 0 24 24" fill="none">
                                                <path opacity="0.3"
                                                    d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                                                    fill="black" />
                                                <path
                                                    d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                                                    fill="black" />
                                            </svg>
                                        </span>
                                    </span>
                                    <input type="text" name="judul" value="{{ $tugas->judul }}" class="form-control"
                                        placeholder="Judul" required />
                                </div>
                                <!--begin::Input-->
                                <!--end::Input-->
                            </div>
                            <!--end::Input group-->
                            <!--begin::Input group-->
                            <div class="mb-10 fv-row">
                                <!--begin::Label-->
                                <label class="form-label">Instruksi</label>
                                <!--end::Label-->
                                <div id="isi_editor">
                                    {!! $tugas->instruksi !!}
                                </div>
                                <input type="hidden" id="isi_html" name="instruksi"
                                    value="{{ $tugas->instruksi }}"></input>
                                <!--begin::Input-->
                                <!--end::Input-->
                            </div>
                            <div class="mb-10 fv-row">
                                <!--begin::Label-->
                                <label class="form-label">Deadline</label>
                                <!--end::Label-->
                                <div class="input-group input-group-solid mb-5">
                                    <span class="input-group-text" id="basic-addon2">
                                        <span class="svg-icon svg-icon-1">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                viewBox="0 0 24 24" fill="none">
                                                <path opacity="0.3"
                                                    d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                                                    fill="black" />
                                                <path
                                                    d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                                                    fill="black" />
                                            </svg>
                                        </span>
                                    </span>
                                    <input type="text" name="batas_akhir" id="batas_akhir"
                                        value="{{ $tugas->batas_akhir }}" class="form-control" />
                                </div>
                                <!--begin::Input-->
                                <!--end::Input-->
                            </div>
                            @if ($tugas->kategori->is_persentase)
                            <div class="mb-10 fv-row">
                                <!--begin::Label-->
                                <label class="form-label">Persentase Nilai (%)</label>
                                <!--end::Label-->
                                <div class="input-group input-group-solid mb-5">
                                    <span class="input-group-text" id="basic-addon2">
                                        <span class="svg-icon svg-icon-1">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                viewBox="0 0 24 24" fill="none">
                                                <path opacity="0.3"
                                                    d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                                                    fill="black" />
                                                <path
                                                    d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                                                    fill="black" />
                                            </svg>
                                        </span>
                                    </span>
                                    <input type="number" name="persentase_nilai" value="{{ $tugas->persentase_nilai }}" id="persentase_nilai" class="form-control" />
                                </div>
                                <!--begin::Input-->
                                <!--end::Input-->
                            </div>
                            @endif
                            @if ($tugas->tipe == 'rubrik' || $tugas->tipe == 'rubrik2')
                                @if ($tugas->tipe == 'rubrik')
                                <div class="mb-10 fv-row">
                                    <!--begin::Label-->
                                    <label class="form-label">Dokumen Instruksi</label>
                                    <!--end::Label-->
                                    <div class="input-group input-group-solid mb-5">
                                        <span class="input-group-text" id="basic-addon2">
                                            <span class="svg-icon svg-icon-1">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                    viewBox="0 0 24 24" fill="none">
                                                    <path opacity="0.3"
                                                        d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                                                        fill="black" />
                                                    <path
                                                        d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                                                        fill="black" />
                                                </svg>
                                            </span>
                                        </span>
                                        <input type="file" name="dokumen_instruksi" class="form-control" />
                                        @if ($tugas->dokumen_instruksi)
                                            <a href="{{ url('storage/' . $tugas->dokumen_instruksi) }}" class="btn btn-success">
                                                Unduh Dokumen
                                            </a>
                                            <a href="{{ route('tugas.hapusDokumen', $tugas->kode) }}" class="btn btn-danger" onclick="return confirm('Yakin akan menghapus?');">
                                                Hapus
                                            </a>
                                        @endif
                                    </div>
                                    <!--begin::Input-->
                                    <!--end::Input-->
                                </div>
                                @endif

                                <label class="form-label">Dosen Penilai</label>
                                @foreach ($tugas->dosen_penilai as $dosen)
                                    <div class="row fv-row mb-10">
                                        <!--begin::Col-->
                                        <div class="col-lg-5 fv-row">
                                            <select name="user_ids[]"
                                                class="form-select form-select-solid rounded-0 border-start border-end"
                                                data-placeholder="Pilih Dosen" required>
                                                <option>Pilih Dosen</option>
                                                @foreach ($users as $item)
                                                    <option value="{{ $item->id }}"
                                                        {{ $dosen->user_id == $item->id ? 'selected' : '' }}>
                                                        {!! $item->name !!}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <!--end::Col-->
                                        <!--begin::Col-->
                                        <div class="col-lg-6 fv-row">
                                            <select name="kelompok_ids[]"
                                                class="form-select form-select-solid rounded-0 border-start border-end"
                                                data-placeholder="Pilih Kelompok" required>
                                                <option>Pilih Kelompok</option>
                                                @foreach ($kelompoks as $item)
                                                    <option value="{{ $item->id }}"
                                                        {{ $dosen->kelompok_id == $item->id ? 'selected' : '' }}>
                                                        {!! $item->nama !!}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <!--end::Col-->
                                        <!--begin::Col-->
                                        <div class="col-lg-1 fv-row">
                                            <a href="{{ route('dosen.hapus', $dosen->kode) }}" onclick="return confirm('Yakin akan menghapus?');" class="svg-icon svg-icon-danger svg-icon-3hx">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                    viewBox="0 0 24 24" fill="none">
                                                    <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10"
                                                        fill="black" />
                                                    <rect x="7" y="15.3137" width="12" height="2" rx="1"
                                                        transform="rotate(-45 7 15.3137)" fill="black" />
                                                    <rect x="8.41422" y="7" width="12" height="2" rx="1"
                                                        transform="rotate(45 8.41422 7)" fill="black" />
                                                </svg>
                                            </a>
                                        </div>
                                        <!--end::Col-->
                                    </div>
                                @endforeach

                                <div x-data="handler()">
                                    <div class="mb-5 fv-row">
                                        <a class="btn btn-primary rounded-50" @click="addNewField()">
                                            <span class="svg-icon svg-icon-2">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                    viewBox="0 0 24 24" fill="none">
                                                    <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5"
                                                        fill="black" />
                                                    <rect x="10.8891" y="17.8033" width="12" height="2" rx="1"
                                                        transform="rotate(-90 10.8891 17.8033)" fill="black" />
                                                    <rect x="6.01041" y="10.9247" width="12" height="2" rx="1"
                                                        fill="black" />
                                                </svg>
                                            </span>
                                            Tambah Dosen Penilai</a>
                                    </div>
                                    <template x-for="(field, index) in fields" :key="index">
                                        <div class="row fv-row">
                                            <!--begin::Col-->
                                            <div class="col-lg-5 fv-row">
                                                <div class="input-group input-group-solid mb-5">
                                                    <span class="input-group-text" id="basic-addon2">
                                                        <span class="svg-icon svg-icon-1">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                                viewBox="0 0 24 24" fill="none">
                                                                <path opacity="0.3"
                                                                    d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                                                                    fill="black" />
                                                                <path
                                                                    d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                                                                    fill="black" />
                                                            </svg>
                                                        </span>
                                                    </span>
                                                    <select x-model="field.user_ids" name="user_ids[]"
                                                        class="form-select form-select-solid rounded-0 border-start border-end"
                                                        data-control="select2" data-placeholder="Pilih Dosen" required>
                                                        <option>Pilih Dosen</option>
                                                        @foreach ($users as $item)
                                                            <option value="{{ $item->id }}">{!! $item->name !!}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <!--end::Col-->
                                            <!--begin::Col-->
                                            <div class="col-lg-6 fv-row">
                                                <div class="input-group input-group-solid mb-5">
                                                    <span class="input-group-text" id="basic-addon2">
                                                        <span class="svg-icon svg-icon-1">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                                viewBox="0 0 24 24" fill="none">
                                                                <path opacity="0.3"
                                                                    d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                                                                    fill="black" />
                                                                <path
                                                                    d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                                                                    fill="black" />
                                                            </svg>
                                                        </span>
                                                    </span>
                                                    <select x-model="field.kelompok_ids" name="kelompok_ids[]"
                                                        class="form-select form-select-solid rounded-0 border-start border-end"
                                                        data-control="select2" data-placeholder="Pilih Kelompok" required>
                                                        <option>Pilih Kelompok</option>
                                                        @foreach ($kelompoks as $item)
                                                            <option value="{{ $item->id }}">{!! $item->nama !!}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <!--end::Col-->
                                            <!--begin::Col-->
                                            <div class="col-lg-1 fv-row">
                                                <span class="svg-icon svg-icon-danger svg-icon-3hx"
                                                    @click="removeField(index)">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none">
                                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10"
                                                            fill="black" />
                                                        <rect x="7" y="15.3137" width="12" height="2" rx="1"
                                                            transform="rotate(-45 7 15.3137)" fill="black" />
                                                        <rect x="8.41422" y="7" width="12" height="2" rx="1"
                                                            transform="rotate(45 8.41422 7)" fill="black" />
                                                    </svg>
                                                </span>
                                            </div>
                                            <!--end::Col-->
                                        </div>
                                    </template>
                                </div>
                            @endif

                            @if ($tugas->tipe == 'kuis')
                                <div class="mb-10 fv-row">
                                    <!--begin::Label-->
                                    <label class="required form-label">Durasi Test</label>
                                    <!--end::Label-->
                                    <div class="input-group input-group-solid mb-5">
                                        <span class="input-group-text" id="basic-addon2">
                                            <span class="svg-icon svg-icon-1">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                    viewBox="0 0 24 24" fill="none">
                                                    <path opacity="0.3"
                                                        d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                                                        fill="black" />
                                                    <path
                                                        d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                                                        fill="black" />
                                                </svg>
                                            </span>
                                        </span>
                                        <input type="number" name="durasi" value="{{ $tugas->durasi }}" min="1" class="form-control" placeholder="Durasis dalam satuan Menit, Cth: 30"
                                            required />
                                    </div>
                                    <!--begin::Input-->
                                    <!--end::Input-->
                                </div>
                                <div class="fv-row mb-10">
                                    <!--begin::Label-->
                                    <label class="form-label">Acak Soal</label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    <div class="form-check form-check-custom form-check-solid mb-2">
                                        <input class="form-check-input" type="radio" id="acak_soal_ya" name="acak_soal"
                                            value="1" {{ $tugas->acak_soal == 1 ? 'checked' : '' }} />
                                        <label class="form-check-label" for="acak_soal_ya">Ya</label>
                                    </div>
                                    <div class="form-check form-check-custom form-check-solid mb-2">
                                        <input class="form-check-input" type="radio" id="acak_soal_tidak" name="acak_soal"
                                            value="0" {{ $tugas->acak_soal == 0 ? 'checked' : '' }} />
                                        <label class="form-check-label" for="acak_soal_tidak">Tidak</label>
                                    </div>
                                    <!--end::Input-->
                                    <!--begin::Description-->
                                    <div class="text-muted fs-7">Centang YA untuk mengacak soal.</div>
                                    <!--end::Description-->
                                </div>
                                <!--end::Input group-->
                                <!--begin::Input group-->
                                <div class="fv-row mb-10">
                                    <!--begin::Label-->
                                    <label class="form-label">Acak Jawaban</label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    <div class="form-check form-check-custom form-check-solid mb-2">
                                        <input class="form-check-input" type="radio" id="acak_jawaban_ya"
                                            name="acak_jawaban" value="1"
                                            {{ $tugas->acak_jawaban == 1 ? 'checked' : '' }} />
                                        <label class="form-check-label" for="acak_jawaban_ya">Ya</label>
                                    </div>
                                    <div class="form-check form-check-custom form-check-solid mb-2">
                                        <input class="form-check-input" type="radio" id="acak_jawaban_tidak"
                                            name="acak_jawaban" value="0"
                                            {{ $tugas->acak_jawaban == 0 ? 'checked' : '' }} />
                                        <label class="form-check-label" for="acak_jawaban_tidak">Tidak</label>
                                    </div>
                                    <!--end::Input-->
                                    <!--begin::Description-->
                                    <div class="text-muted fs-7">Centang YA untuk mengacak jawaban.</div>
                                    <!--end::Description-->
                                </div>
                                <!--end::Input group-->
                            @endif

                            @if ($tugas->tipe == 'input')
                                <!--begin::Input group-->
                                <div class="fv-row mb-10">
                                    <!--begin::Label-->
                                    <label class="form-label">Sembunyikan Nilai?</label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    <div class="form-check form-check-custom form-check-solid mb-2">
                                        <input class="form-check-input" type="radio" id="hidden_ya"
                                            name="hidden" value="1"
                                            {{ $tugas->hidden == 1 ? 'checked' : '' }} />
                                        <label class="form-check-label" for="hidden_ya">Ya</label>
                                    </div>
                                    <div class="form-check form-check-custom form-check-solid mb-2">
                                        <input class="form-check-input" type="radio" id="hidden_tidak"
                                            name="hidden" value="0"
                                            {{ $tugas->hidden == 0 ? 'checked' : '' }} />
                                        <label class="form-check-label" for="hidden_tidak">Tidak</label>
                                    </div>
                                    <!--end::Input-->
                                    <!--begin::Description-->
                                    <div class="text-muted fs-7">Centang YA untuk menyembunyikan nilai dimahasiswa.</div>
                                    <!--end::Description-->
                                </div>
                                <!--end::Input group-->
                            @endif
                        </div>
                        <!--end::Card header-->
                    </div>
                    <!--end::Inventory-->
                </div>
                <div class="d-flex justify-content-end mt-10">
                    <!--begin::Button-->
                    <a href="{{ route('kelas.penilaianSumatif', $kelas->kode) }}" class="btn btn-light me-5">Batal</a>
                    <!--end::Button-->
                    <!--begin::Button-->
                    <button type="submit" class="btn btn-primary">
                        <span class="indicator-label">Simpan</span>
                    </button>
                    <!--end::Button-->
                </div>
            </form>
        </div>
        <!--end::Post-->
    </div>
    <!--end::Container-->
@endsection

@section('styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section('scripts')
    {{-- @stack('scripts') --}}
    <script src="/assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js"></script>
    <script src="/js/quill/image-resize.min.js"></script>
    <script src="/js/quill/image-drop.min.js"></script>
    <script>
        var toolbarOptions = [
            ['bold', 'italic', 'underline', 'strike'], // toggled buttons
            ['blockquote', 'code-block'],

            [{
                'header': 1
            }, {
                'header': 2
            }], // custom button values
            [{
                'list': 'ordered'
            }, {
                'list': 'bullet'
            }],
            [{
                'script': 'sub'
            }, {
                'script': 'super'
            }], // superscript/subscript
            [{
                'indent': '-1'
            }, {
                'indent': '+1'
            }], // outdent/indent
            [{
                'direction': 'rtl'
            }], // text direction

            [{
                'size': ['small', false, 'large', 'huge']
            }], // custom dropdown
            [{
                'header': [1, 2, 3, 4, 5, 6, false]
            }],

            [{
                'color': []
            }, {
                'background': []
            }], // dropdown with defaults from theme
            [{
                'font': []
            }],
            [{
                'align': []
            }],
            ['image', 'video', 'link'],

            ['clean'] // remove formatting button
        ];
        var quill = new Quill('#isi_editor', {
            theme: 'snow',
            placeholder: 'Tulis instruksi disini...',
            modules: {
                toolbar: toolbarOptions,
                imageResize: {
                    displaySize: true
                },
                imageDrop: true
            },
        });
        quill.on('text-change', function(delta, oldDelta, source) {
            document.getElementById("isi_html").value = quill.root.innerHTML;
        });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        $("#batas_akhir").flatpickr({
            enableTime: true,
            dateFormat: "Y-m-d H:i",
            altInput: true,
            altFormat: "F j, Y H:i",
        });
    </script>
    <script>
        function handler() {
            return {
                fields: [],
                addNewField() {
                    this.fields.push({
                        user_ids: '',
                        kelompok_ids: ''
                    });
                },
                removeField(index) {
                    this.fields.splice(index, 1);
                }
            }
        }
    </script>
@endsection
