@if (!$mahasiswaTugas->tanggal_kumpul)
    <div class="card">
        <!--begin::Card body-->
        <div class="card-body p-0">
            <!--begin::Wrapper-->
            <div class="card-px text-center py-20 my-10">
                <!--begin::Action-->
                <a href="{{ route('kelas.show', $tugas->kelas->kode) }}" class="btn btn-primary">Kembali</a>
                <!--end::Action-->
            </div>
            <!--end::Wrapper-->
            <!--begin::Illustration-->
            <div class="text-center px-4">
                <img class="mw-100 mh-300px" alt="" src="/assets/media/illustrations/sketchy-1/4.png" />
            </div>
            <!--end::Illustration-->
        </div>
        <!--end::Card body-->
    </div>
@else
    <div class="card">
        <!--begin::Card body-->
        <div class="card-body p-0">
            <!--begin::Wrapper-->
            <div class="card-px text-center py-20 my-10">
                <!--begin::Title-->
                <h2 class="fs-2x text-muted mb-10">Poin Anda</h2>
                <h2 class="fs-3x fw-bolder mb-10">{{ $mahasiswaTugas->total_poin }}</h2>
                <!--end::Title-->
                <!--begin::Description-->
                <p class="text-gray-400 fs-4 fw-bold mb-10"></p>
                <!--end::Description-->
                <!--begin::Action-->
                <a href="{{ route('kelas.show', $tugas->kelas->kode) }}" class="btn btn-primary">Kembali</a>
                <!--end::Action-->
            </div>
            <!--end::Wrapper-->
            <!--begin::Illustration-->
            <div class="text-center px-4">
                <img class="mw-100 mh-300px" alt="" src="/assets/media/illustrations/sketchy-1/7.png" />
            </div>
            <!--end::Illustration-->
        </div>
        <!--end::Card body-->
    </div>
@endif
