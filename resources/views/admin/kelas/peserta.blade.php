@extends('layouts.admin')

@section('title')
    Peserta
@endsection

@section('content')
    <!--begin::Toolbar-->
    <div class="toolbar py-5 py-lg-5" id="kt_toolbar">
        <!--begin::Container-->
        <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column me-3">
                <!--begin::Title-->
                <h1 class="d-flex text-dark fw-bolder my-1 fs-3">Peserta</h1>
                <!--end::Title-->
            </div>
            <!--end::Page title-->
            <div class="d-flex align-items-center py-2 py-md-1">
                <div>
                    <button class="btn btn-primary fw-bolder me-2" data-bs-toggle="modal" data-bs-target="#modal_import_peserta">
                        <!--begin::Svg Icon | path: icons/duotune/general/gen031.svg-->
                        <span class="svg-icon svg-icon-2hx svg-icon-gray-500 me-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path opacity="0.3"
                                    d="M5 16C3.3 16 2 14.7 2 13C2 11.3 3.3 10 5 10H5.1C5 9.7 5 9.3 5 9C5 6.2 7.2 4 10 4C11.9 4 13.5 5 14.3 6.5C14.8 6.2 15.4 6 16 6C17.7 6 19 7.3 19 9C19 9.4 18.9 9.7 18.8 10C18.9 10 18.9 10 19 10C20.7 10 22 11.3 22 13C22 14.7 20.7 16 19 16H5ZM8 13.6H16L12.7 10.3C12.3 9.89999 11.7 9.89999 11.3 10.3L8 13.6Z"
                                    fill="black" />
                                <path d="M11 13.6V19C11 19.6 11.4 20 12 20C12.6 20 13 19.6 13 19V13.6H11Z" fill="black" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                        Import
                    </button>
                    <a href="{{ route('folder.show', $kelas->folder->kode) }}" class="btn btn-secondary fw-bolder">
                        Kembali
                        <span class="svg-icon svg-icon-2hx svg-icon-gray-500 me-1">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                stroke="currentColor" class="w-6 h-6">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                    d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3" />
                            </svg>

                        </span>
                    </a>
                </div>
            </div>
            <div class="modal fade" tabindex="-1" id="modal_import_peserta">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Import Peserta</h5>

                            <!--begin::Close-->
                            <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                                aria-label="Close">
                                <span class="svg-icon svg-icon-2x">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none">
                                        <rect opacity="0.5" x="7.05025" y="15.5356" width="12" height="2" rx="1"
                                            transform="rotate(-45 7.05025 15.5356)" fill="black" />
                                        <rect x="8.46447" y="7.05029" width="12" height="2" rx="1"
                                            transform="rotate(45 8.46447 7.05029)" fill="black" />
                                    </svg>
                                </span>
                            </div>
                            <!--end::Close-->
                        </div>

                        <form method="POST" action="{{ route('kelas.pesertaImport', $kelas->kode) }}"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                <!--begin::Input group-->
                                <div class="input-group input-group-solid mb-5">
                                    <span class="input-group-text" id="basic-addon2">
                                        <span class="svg-icon svg-icon-1">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                viewBox="0 0 24 24" fill="none">
                                                <path opacity="0.3"
                                                    d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM11.7 17.7L16 14C16.4 13.6 16.4 12.9 16 12.5C15.6 12.1 15.4 12.6 15 13L11 16L9 15C8.6 14.6 8.4 14.1 8 14.5C7.6 14.9 8.1 15.6 8.5 16L10.3 17.7C10.5 17.9 10.8 18 11 18C11.2 18 11.5 17.9 11.7 17.7Z"
                                                    fill="black" />
                                                <path
                                                    d="M10.4343 15.4343L9.25 14.25C8.83579 13.8358 8.16421 13.8358 7.75 14.25C7.33579 14.6642 7.33579 15.3358 7.75 15.75L10.2929 18.2929C10.6834 18.6834 11.3166 18.6834 11.7071 18.2929L16.25 13.75C16.6642 13.3358 16.6642 12.6642 16.25 12.25C15.8358 11.8358 15.1642 11.8358 14.75 12.25L11.5657 15.4343C11.2533 15.7467 10.7467 15.7467 10.4343 15.4343Z"
                                                    fill="black" />
                                                <path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="black" />
                                            </svg>
                                        </span>
                                    </span>
                                    <input type="file" name="file" accept=".xlsx" class="form-control" required />
                                </div>
                                <!--end::Input group-->
                            </div>

                            <div class="modal-footer">
                                <a href="/file/template-import-peserta.xlsx" class="btn btn-light">Unduh Template Excel</a>
                                <button type="submit" class="btn btn-primary">Import</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Container-->
    <div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
        <div class="content flex-row-fluid" id="kt_content">

            @include('admin.kelas.partials._detail')

            @include('admin.kelas.partials._nav')

            <div class="row">
                <div class="col-12 mb-10">
                    <form method="POST" action="{{ route('kelompok.simpan') }}">
                        @csrf
                        <div class="input-group">
                            <span class="input-group-text" id="basic-addon3" style="border: none">
                                <i class="las la-wallet fs-1"></i>
                            </span>
                            <input type="hidden" name="kelas_id" value="{{ $kelas->id }}">
                            <input type="text" name="nama" class="form-control form-control-solid"
                                placeholder="Nama kelompok" required/>
                            <button type="submit" class="btn btn-icon btn-primary">
                                <!--begin::Svg Icon | path: icons/duotune/general/gen035.svg-->
                                <span class="svg-icon svg-icon-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none">
                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black" />
                                        <rect x="10.8891" y="17.8033" width="12" height="2" rx="1"
                                            transform="rotate(-90 10.8891 17.8033)" fill="black" />
                                        <rect x="6.01041" y="10.9247" width="12" height="2" rx="1" fill="black" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row" data-masonry='{"percentPosition": true }'>
                @foreach ($kelas->kelompoks as $kelompok)
                    <div class="col-12 col-md-4">
                        <div class="card mb-5 card-flush h-xl-100 crd-shadow bg-light">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5 pb-5" style="background: #1c285e;">
                                <h3 class="card-title align-items-start flex-column">
                                    <span class="card-label fw-bolder text-white">{{ $kelompok->nama ?? '-' }}</span>
                                    <span class="text-muted mt-1 fw-bold fs-7">Total Mahasiswa :
                                        {{ $kelompok->mahasiswas->count() }}</span>
                                </h3>
                                <div class="card-toolbar">
                                    <!--begin::Menu-->
                                    <button type="button"
                                        class="btn btn-sm btn-icon btn-color-primary btn-active-light-primary"
                                        data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
                                        <span class="svg-icon svg-icon-2">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px"
                                                viewBox="0 0 24 24">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="5" y="5" width="5" height="5" rx="1" fill="#000000" />
                                                    <rect x="14" y="5" width="5" height="5" rx="1" fill="#000000"
                                                        opacity="0.3" />
                                                    <rect x="5" y="14" width="5" height="5" rx="1" fill="#000000"
                                                        opacity="0.3" />
                                                    <rect x="14" y="14" width="5" height="5" rx="1" fill="#000000"
                                                        opacity="0.3" />
                                                </g>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                    </button>
                                    <!--begin::Menu 3-->
                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px py-3"
                                        data-kt-menu="true">
                                        <!--begin::Heading-->
                                        <div class="menu-item px-3">
                                            <div class="menu-content text-muted pb-2 px-3 fs-7 text-uppercase">Aksi</div>
                                        </div>
                                        <!--end::Heading-->
                                        <!--begin::Menu item-->
                                        <div class="menu-item px-3">
                                            <a href="#" class="menu-link px-3" data-bs-toggle="modal"
                                                data-bs-target="#modal_tambah_mahasiswa{{ $kelompok->id }}">Tambah
                                                Mahasiswa</a>
                                        </div>
                                        <div class="menu-item px-3">
                                            <a href="#" class="menu-link px-3" data-bs-toggle="modal"
                                                data-bs-target="#modal_edit_kelompok{{ $kelompok->id }}">Edit</a>
                                        </div>
                                        @if ($kelompok->berita_acaras->count() == 0 && $kelompok->sgds->count() == 0)
                                        <div class="menu-item px-3">
                                            <a href="{{ route('kelompok.hapus', $kelompok->kode) }}" onclick="return confirm('Yakin akan menghapus?');" class="menu-link px-3 text-danger">Hapus</a>
                                        </div>
                                        @endif
                                        <!--end::Menu item-->
                                    </div>
                                    <!--end::Menu 3-->
                                    <!--end::Menu-->
                                </div>
                                {{-- modal edit kelompok --}}
                                <div class="modal fade" tabindex="-1" id="modal_edit_kelompok{{ $kelompok->id }}">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Edit Kelompok</h5>

                                                <!--begin::Close-->
                                                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                                    data-bs-dismiss="modal" aria-label="Close">
                                                    <span class="svg-icon svg-icon-2x">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                            viewBox="0 0 24 24" fill="none">
                                                            <rect opacity="0.5" x="7.05025" y="15.5356" width="12"
                                                                height="2" rx="1" transform="rotate(-45 7.05025 15.5356)"
                                                                fill="black" />
                                                            <rect x="8.46447" y="7.05029" width="12" height="2" rx="1"
                                                                transform="rotate(45 8.46447 7.05029)" fill="black" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <!--end::Close-->
                                            </div>

                                            <form method="POST"
                                                action="{{ route('kelompok.kelompokUpdate', $kelompok->id) }}"
                                                enctype="multipart/form-data">
                                                @csrf
                                                <div class="modal-body">
                                                    <!--begin::Input group-->
                                                    <div class="input-group input-group-solid mb-5">
                                                        <span class="input-group-text" id="basic-addon2">
                                                            <span class="svg-icon svg-icon-1">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                    height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path opacity="0.3"
                                                                        d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM11.7 17.7L16 14C16.4 13.6 16.4 12.9 16 12.5C15.6 12.1 15.4 12.6 15 13L11 16L9 15C8.6 14.6 8.4 14.1 8 14.5C7.6 14.9 8.1 15.6 8.5 16L10.3 17.7C10.5 17.9 10.8 18 11 18C11.2 18 11.5 17.9 11.7 17.7Z"
                                                                        fill="black" />
                                                                    <path
                                                                        d="M10.4343 15.4343L9.25 14.25C8.83579 13.8358 8.16421 13.8358 7.75 14.25C7.33579 14.6642 7.33579 15.3358 7.75 15.75L10.2929 18.2929C10.6834 18.6834 11.3166 18.6834 11.7071 18.2929L16.25 13.75C16.6642 13.3358 16.6642 12.6642 16.25 12.25C15.8358 11.8358 15.1642 11.8358 14.75 12.25L11.5657 15.4343C11.2533 15.7467 10.7467 15.7467 10.4343 15.4343Z"
                                                                        fill="black" />
                                                                    <path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z"
                                                                        fill="black" />
                                                                </svg>
                                                            </span>
                                                        </span>
                                                        <input type="text" name="nama" class="form-control"
                                                            value="{{ $kelompok->nama }}" required />
                                                    </div>
                                                    <!--end::Input group-->
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                {{-- modal edit kelompok --}}
                                <div class="modal fade" tabindex="-1" id="modal_tambah_mahasiswa{{ $kelompok->id }}">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Tambah Mahasiswa ke {{ $kelompok->nama }}</h5>

                                                <!--begin::Close-->
                                                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                                    data-bs-dismiss="modal" aria-label="Close">
                                                    <span class="svg-icon svg-icon-2x">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                            viewBox="0 0 24 24" fill="none">
                                                            <rect opacity="0.5" x="7.05025" y="15.5356" width="12"
                                                                height="2" rx="1" transform="rotate(-45 7.05025 15.5356)"
                                                                fill="black" />
                                                            <rect x="8.46447" y="7.05029" width="12" height="2" rx="1"
                                                                transform="rotate(45 8.46447 7.05029)" fill="black" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <!--end::Close-->
                                            </div>

                                            <form method="POST"
                                                action="{{ route('kelompok.tambahMahasiswaKelompok') }}"
                                                enctype="multipart/form-data">
                                                @csrf
                                                <div class="modal-body">
                                                    <input type="hidden" name="kelompok_id" value="{{ $kelompok->id }}">
                                                    @php
                                                        $mahasiswas = \App\Models\User::query()
                                                            ->where('user_type', 'mahasiswa')
                                                            ->get();
                                                    @endphp
                                                    <select name="user_id" class="form-select form-select-solid"
                                                        data-control="select2"
                                                        data-dropdown-parent="#modal_tambah_mahasiswa{{ $kelompok->id }}"
                                                        data-placeholder="Pilih Mahasiswa" data-allow-clear="true">
                                                        <option></option>
                                                        @foreach ($mahasiswas as $item)
                                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card-body pt-5">
                                <div class="hover-scroll-overlay-y pe-6 me-n6" style="height: 415px">
                                @forelse ($kelompok->mahasiswas as $mahasiswa)
                                    <!--begin::Item-->
                                    <div class="d-flex align-items-sm-center mb-7">
                                        <!--begin::Symbol-->
                                        <div class="symbol symbol-50px me-5">
                                            <x-smart-image src="{{ public_path('/img/user.png') }}" height="50px" />
                                        </div>
                                        <!--end::Symbol-->
                                        <!--begin::Section-->
                                        <div class="d-flex align-items-center flex-row-fluid flex-wrap">
                                            <div class="flex-grow-1 me-2">
                                                <a href="#"
                                                    class="text-gray-800 text-hover-primary fs-6 fw-bolder">{{ $mahasiswa->user->name ?? '-' }}</a>
                                                <span class="text-muted fw-bold d-block fs-7">NIM :
                                                    {{ $mahasiswa->user->username ?? '-' }}</span>
                                            </div>

                                            @if ($kelompok->berita_acaras->count() == 0 && $kelompok->sgds->count() == 0)
                                            <a href="{{ route('kelas.pesertaHapus', [$mahasiswa->id, $kelompok->id]) }}"
                                                onclick="return confirm('Yakin akan menghapus?');"
                                                data-bs-toggle="tooltip" data-bs-custom-class="tooltip-dark" data-bs-placement="right" title="Hapus">
                                                <span class="svg-icon svg-icon-danger svg-icon-1">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none">
                                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10"
                                                            fill="black" />
                                                        <rect x="7" y="15.3137" width="12" height="2" rx="1"
                                                            transform="rotate(-45 7 15.3137)" fill="black" />
                                                        <rect x="8.41422" y="7" width="12" height="2" rx="1"
                                                            transform="rotate(45 8.41422 7)" fill="black" />
                                                    </svg>
                                                </span>
                                            </a>
                                            @endif
                                        </div>
                                        <!--end::Section-->
                                    </div>
                                    <!--end::Item-->
                                @empty
                                    <small class="text-muted">Tidak ada mahasiswa</small>
                                @endforelse
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <!--end::Post-->
    </div>
    <!--end::Container-->
@endsection

@section('styles')
@endsection

@section('scripts')
    @stack('scripts')
    <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
@endsection
