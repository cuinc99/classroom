@extends('layouts.admin')

@section('title')
    Folder
@endsection

@section('content')
@livewire('folder.index', ['folderId' => NULL])
@endsection

@section('styles')
@endsection

@section('scripts')
    <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
@endsection
