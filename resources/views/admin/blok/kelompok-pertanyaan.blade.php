@extends('layouts.admin')

@section('title')
    Pertanyaan
@endsection

@section('content')
    <div id="kt_content_container" class="flex-column-fluid align-items-start container-xxl">
        <form method="POST" action="{{ route('pertanyaan.simpan') }}">
            @csrf
            <div class="d-flex flex-column gap-7 gap-lg-10">
                <!--begin::Inventory-->
                <div class="card card-flush py-4">
                    <div class="card-header border-0 pt-5">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="text-muted mt-1 fw-bold fs-7  mb-1">Nama Blok :
                                {{ $kelompokPertanyaan->blok->nama ?? '-' }}</span>
                            <span class="card-label fw-bolder fs-3">Nama Kelompok Soal :
                                {{ $kelompokPertanyaan->nama ?? '-' }}</span>
                        </h3>
                    </div>
                    <!--begin::Card body-->
                    <div class="card-body pt-5">
                        <!--begin::Input group-->
                        <input type="hidden" name="blok_id" value="{{ $kelompokPertanyaan->blok_id }}">
                        <input type="hidden" name="kelompok_pertanyaan_id" value="{{ $kelompokPertanyaan->id }}">
                        <div class="mb-10 fv-row">
                            <!--begin::Label-->
                            <label class="required form-label">Tulis Soal</label>
                            <!--end::Label-->
                            <!--begin::Input-->
                            <div id="isi_editor"></div>
                            <input type="hidden" id="isi_html" name="pertanyaan"></input>
                            <!--end::Input-->
                        </div>
                        <!--end::Input group-->

                        <div x-data="handler()">
                            <div class="mb-5 fv-row">
                                <a class="btn btn-success rounded-50" @click="addNewField()">
                                    <span class="svg-icon svg-icon-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                            fill="none">
                                            <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black" />
                                            <rect x="10.8891" y="17.8033" width="12" height="2" rx="1"
                                                transform="rotate(-90 10.8891 17.8033)" fill="black" />
                                            <rect x="6.01041" y="10.9247" width="12" height="2" rx="1" fill="black" />
                                        </svg>
                                    </span>
                                    Tambah Jawaban</a>
                            </div>
                            <template x-for="(field, index) in fields" :key="index">
                                <div class="row fv-row">
                                    <!--begin::Col-->
                                    <div class="col-lg-2 fv-row">
                                        <div class="input-group input-group-solid mb-5">
                                            <span class="input-group-text" id="basic-addon2">
                                                <span class="svg-icon svg-icon-1">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none">
                                                        <path opacity="0.3"
                                                            d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                                                            fill="black" />
                                                        <path
                                                            d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                                                            fill="black" />
                                                    </svg>
                                                </span>
                                            </span>
                                            <input type="number" x-model="field.poin" name="poin[]" class="form-control"
                                                placeholder="Poin" value="0" required />
                                        </div>
                                    </div>
                                    <!--end::Col-->
                                    <!--begin::Col-->
                                    <div class="col-lg-9 fv-row">
                                        <div class="input-group input-group-solid mb-5">
                                            <span class="input-group-text" id="basic-addon3">
                                                <span class="svg-icon svg-icon-1">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none">
                                                        <path opacity="0.3"
                                                            d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                                                            fill="black" />
                                                        <path
                                                            d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                                                            fill="black" />
                                                    </svg>
                                                </span>
                                            </span>
                                            <input type="text" x-model="field.jawaban" name="jawaban[]"
                                                class="form-control" placeholder="Tulis Jawaban" required />
                                        </div>
                                    </div>
                                    <!--end::Col-->
                                    <!--begin::Col-->
                                    <div class="col-lg-1 fv-row">
                                        <span class="svg-icon svg-icon-danger svg-icon-3hx" @click="removeField(index)">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                viewBox="0 0 24 24" fill="none">
                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10"
                                                    fill="black" />
                                                <rect x="7" y="15.3137" width="12" height="2" rx="1"
                                                    transform="rotate(-45 7 15.3137)" fill="black" />
                                                <rect x="8.41422" y="7" width="12" height="2" rx="1"
                                                    transform="rotate(45 8.41422 7)" fill="black" />
                                            </svg>
                                        </span>
                                    </div>
                                    <!--end::Col-->
                                </div>
                            </template>
                        </div>

                    </div>
                    <div class="card-footer d-flex justify-content-end">
                        <button type="submit" class="btn btn-primary">
                            <span class="indicator-label">Simpan</span>
                        </button>
                    </div>
                    <!--end::Card header-->
                </div>
                <!--end::Inventory-->
            </div>
        </form>

        @if ($pertanyaans->count() > 0)
            <div class="card-body pt-0 mt-10">
                <!--begin::Table-->
                <table class="table align-middle table-row-dashed fs-6 gy-5">
                    <thead>
                        <!--begin::Table row-->
                        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                            <th width="10">#</th>
                            <th class="min-w-100px">Pertanyaan</th>
                            {{-- <th class="text-end">Tanggal Buat</th> --}}
                            <th class="text-end"></th>
                        </tr>
                        <!--end::Table row-->
                    </thead>
                    <!--begin::Table body-->
                    <tbody class="fw-bold text-gray-600">
                        <!--begin::Table row-->
                        @foreach ($pertanyaans as $pertanyaanItem)
                            <tr>
                                <td>
                                    {{ $loop->iteration }}
                                </td>
                                <!--begin::Customer-->
                                <td>
                                    <a href="#"
                                        class="text-gray-800 fw-bolder mb-1 fs-6 text-start pe-0">{!! $pertanyaanItem->pertanyaan ?? '-' !!}</a>
                                    <div class="d-flex flex-column flex-grow-1 pe-8 mt-5">
                                        <!--begin::Stats-->
                                        <div class="d-flex flex-wrap">
                                            @foreach ($pertanyaanItem->jawabans as $jawabanItem)
                                                <!--begin::Stat-->
                                                <div
                                                    class="border {{ $jawabanItem->poin > 0 ? 'text-primary border-primary shadow' : 'border-gray-300' }} border rounded min-w-125px py-3 px-4 me-6 mb-3">
                                                    <!--begin::Number-->
                                                    <div class="d-flex align-items-center">
                                                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
                                                        <span
                                                            class="svg-icon svg-icon-3 {{ $jawabanItem->poin > 0 ? ' svg-icon-primary' : '' }} me-2">
                                                            @if ($jawabanItem->poin > 0)
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                    height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path opacity="0.3"
                                                                        d="M10.3 14.3L11 13.6L7.70002 10.3C7.30002 9.9 6.7 9.9 6.3 10.3C5.9 10.7 5.9 11.3 6.3 11.7L10.3 15.7C9.9 15.3 9.9 14.7 10.3 14.3Z"
                                                                        fill="black" />
                                                                    <path
                                                                        d="M22 12C22 17.5 17.5 22 12 22C6.5 22 2 17.5 2 12C2 6.5 6.5 2 12 2C17.5 2 22 6.5 22 12ZM11.7 15.7L17.7 9.70001C18.1 9.30001 18.1 8.69999 17.7 8.29999C17.3 7.89999 16.7 7.89999 16.3 8.29999L11 13.6L7.70001 10.3C7.30001 9.89999 6.69999 9.89999 6.29999 10.3C5.89999 10.7 5.89999 11.3 6.29999 11.7L10.3 15.7C10.5 15.9 10.8 16 11 16C11.2 16 11.5 15.9 11.7 15.7Z"
                                                                        fill="black" />
                                                                </svg>
                                                            @else
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                    height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path opacity="0.3"
                                                                        d="M12 10.6L14.8 7.8C15.2 7.4 15.8 7.4 16.2 7.8C16.6 8.2 16.6 8.80002 16.2 9.20002L13.4 12L12 10.6ZM10.6 12L7.8 14.8C7.4 15.2 7.4 15.8 7.8 16.2C8 16.4 8.3 16.5 8.5 16.5C8.7 16.5 8.99999 16.4 9.19999 16.2L12 13.4L10.6 12Z"
                                                                        fill="black" />
                                                                    <path
                                                                        d="M22 12C22 17.5 17.5 22 12 22C6.5 22 2 17.5 2 12C2 6.5 6.5 2 12 2C17.5 2 22 6.5 22 12ZM13.4 12L16.2 9.20001C16.6 8.80001 16.6 8.19999 16.2 7.79999C15.8 7.39999 15.2 7.39999 14.8 7.79999L12 10.6L9.2 7.79999C8.8 7.39999 8.2 7.39999 7.8 7.79999C7.4 8.19999 7.4 8.80001 7.8 9.20001L10.6 12L7.8 14.8C7.4 15.2 7.4 15.8 7.8 16.2C8 16.4 8.3 16.5 8.5 16.5C8.7 16.5 9 16.4 9.2 16.2L12 13.4L14.8 16.2C15 16.4 15.3 16.5 15.5 16.5C15.7 16.5 16 16.4 16.2 16.2C16.6 15.8 16.6 15.2 16.2 14.8L13.4 12Z"
                                                                        fill="black" />
                                                                </svg>
                                                            @endif
                                                        </span>
                                                        <!--end::Svg Icon-->
                                                        <div class="fs-2 fw-bolder">{{ $jawabanItem->poin ?? '0' }}
                                                        </div>
                                                    </div>
                                                    <!--end::Number-->
                                                    <!--begin::Label-->
                                                    <div class="fw-bold fs-6 text-gray-400">
                                                        {{ $jawabanItem->jawaban ?? '-' }}</div>
                                                    <!--end::Label-->
                                                </div>
                                                <!--end::Stat-->
                                            @endforeach
                                        </div>
                                        <!--end::Stats-->
                                    </div>
                                </td>
                                <!--end::Customer-->
                                {{-- <td class="text-end">
                                <span
                                    class="fw-bolder">{{ Date::parse($pertanyaanItem->created_at)->diffForHumans() }}</span>
                            </td> --}}
                                <!--begin::Action-->
                                <td class="text-end">
                                    <a href="{{ route('pertanyaan.edit', [$kelompokPertanyaan->kode, $pertanyaanItem->kode]) }}"
                                        data-bs-toggle="tooltip" data-bs-custom-class="tooltip-dark"
                                        data-bs-placement="top" title="Edit">
                                        <span class="svg-icon svg-icon-primary svg-icon-2hx">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                viewBox="0 0 24 24" fill="none">
                                                <path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M2 4.63158C2 3.1782 3.1782 2 4.63158 2H13.47C14.0155 2 14.278 2.66919 13.8778 3.04006L12.4556 4.35821C11.9009 4.87228 11.1726 5.15789 10.4163 5.15789H7.1579C6.05333 5.15789 5.15789 6.05333 5.15789 7.1579V16.8421C5.15789 17.9467 6.05333 18.8421 7.1579 18.8421H16.8421C17.9467 18.8421 18.8421 17.9467 18.8421 16.8421V13.7518C18.8421 12.927 19.1817 12.1387 19.7809 11.572L20.9878 10.4308C21.3703 10.0691 22 10.3403 22 10.8668V19.3684C22 20.8218 20.8218 22 19.3684 22H4.63158C3.1782 22 2 20.8218 2 19.3684V4.63158Z"
                                                    fill="black" />
                                                <path
                                                    d="M10.9256 11.1882C10.5351 10.7977 10.5351 10.1645 10.9256 9.77397L18.0669 2.6327C18.8479 1.85165 20.1143 1.85165 20.8953 2.6327L21.3665 3.10391C22.1476 3.88496 22.1476 5.15129 21.3665 5.93234L14.2252 13.0736C13.8347 13.4641 13.2016 13.4641 12.811 13.0736L10.9256 11.1882Z"
                                                    fill="black" />
                                                <path
                                                    d="M8.82343 12.0064L8.08852 14.3348C7.8655 15.0414 8.46151 15.7366 9.19388 15.6242L11.8974 15.2092C12.4642 15.1222 12.6916 14.4278 12.2861 14.0223L9.98595 11.7221C9.61452 11.3507 8.98154 11.5055 8.82343 12.0064Z"
                                                    fill="black" />
                                            </svg>
                                        </span>
                                    </a>
                                    @if ($pertanyaanItem->tugas_pertanyaans()->count() == 0)
                                        <a href="{{ route('pertanyaan.hapus', $pertanyaanItem->kode) }}"
                                            onclick="return confirm('Yakin akan menghapus?');" data-bs-toggle="tooltip"
                                            data-bs-custom-class="tooltip-dark" data-bs-placement="top" title="Hapus">
                                            <span class="svg-icon svg-icon-danger svg-icon-2hx">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                    viewBox="0 0 24 24" fill="none">
                                                    <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10"
                                                        fill="black" />
                                                    <rect x="7" y="15.3137" width="12" height="2" rx="1"
                                                        transform="rotate(-45 7 15.3137)" fill="black" />
                                                    <rect x="8.41422" y="7" width="12" height="2" rx="1"
                                                        transform="rotate(45 8.41422 7)" fill="black" />
                                                </svg>
                                            </span>
                                        </a>
                                        <!--end::Svg Icon-->
                                    @endif
                                </td>
                                <!--end::Action-->
                            </tr>
                        @endforeach
                        <!--end::Table row-->
                    </tbody>
                    <!--end::Table body-->
                </table>
                <!--end::Table-->
            </div>
        @endif

    </div>

@endsection

@section('styles')
@endsection

@section('scripts')
    <script src="/assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js"></script>
    <script src="/js/quill/image-resize.min.js"></script>
    <script src="/js/quill/image-drop.min.js"></script>
    <script>
        var toolbarOptions = [
            ['bold', 'italic', 'underline', 'strike'], // toggled buttons
            ['blockquote', 'code-block'],

            [{
                'header': 1
            }, {
                'header': 2
            }], // custom button values
            [{
                'list': 'ordered'
            }, {
                'list': 'bullet'
            }],
            [{
                'script': 'sub'
            }, {
                'script': 'super'
            }], // superscript/subscript
            [{
                'indent': '-1'
            }, {
                'indent': '+1'
            }], // outdent/indent
            [{
                'direction': 'rtl'
            }], // text direction

            [{
                'size': ['small', false, 'large', 'huge']
            }], // custom dropdown
            [{
                'header': [1, 2, 3, 4, 5, 6, false]
            }],

            [{
                'color': []
            }, {
                'background': []
            }], // dropdown with defaults from theme
            [{
                'font': []
            }],
            [{
                'align': []
            }],
            ['image', 'video', 'link'],

            ['clean'] // remove formatting button
        ];
        var quill = new Quill('#isi_editor', {
            theme: 'snow',
            placeholder: 'Tulis soal disini...',
            modules: {
                toolbar: toolbarOptions,
                imageResize: {
                    displaySize: true
                },
                imageDrop: true
            },
        });
        quill.on('text-change', function(delta, oldDelta, source) {
            document.getElementById("isi_html").value = quill.root.innerHTML;
        });
    </script>
    <script>
        function handler() {
            return {
                fields: [],
                addNewField() {
                    this.fields.push({
                        poin: '0',
                        jawaban: ''
                    });
                },
                removeField(index) {
                    this.fields.splice(index, 1);
                }
            }
        }
    </script>
@endsection
