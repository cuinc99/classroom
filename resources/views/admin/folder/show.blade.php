@extends('layouts.admin')

@section('title')
    Kelas
@endsection

@section('content')
    @livewire('folder.index', ['folderId' => $folder->id])
    @livewire('kelas.index', ['folderId' => $folder->id])
@endsection

@section('styles')
@endsection

@section('scripts')
    <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
@endsection
