@extends('layouts.admin')

@section('title')
    Beranda
@endsection

@section('content')
    @if (auth()->user()->isAssessment())
        @include('admin.home.assessment')
    @else
        @include('admin.home.dosen-mahasiswa')
    @endif
@endsection

@section('styles')
@endsection

@section('scripts')
<script>
    function handler() {
        return {
            fields: [],
            addNewField() {
                this.fields.push({
                    poin: '0',
                    jawaban: ''
                });
            },
            removeField(index) {
                this.fields.splice(index, 1);
            }
        }
    }
</script>
@stack('scripts')
@endsection
