@extends('layouts.admin')

@section('title', 'Detail Penilaian ' . $sgd->judul)

@section('content')
<div class="toolbar py-5 py-lg-5">
    <!--begin::Container-->
    <div class="container-xxl d-flex flex-stack flex-wrap">
        <!--begin::Page title-->
        <div class="page-title d-flex flex-column me-3">
            <!--begin::Title-->
            <h1 class="d-flex text-dark fw-bolder my-1 fs-3">Daftar Mahasiswa</h1>
            <!--end::Title-->
        </div>
        @if (auth()->user()->isDosen())
        <div class="d-flex align-items-center py-2 py-md-1">
            <button class="btn btn-sm btn-info me-2" data-bs-toggle="modal" data-bs-target="#modal_skenario">
                {{ $sgd->skenario ? 'Ubah' : 'Tambah' }} Skenario
            </button>

            <div class="modal fade" tabindex="-1" id="modal_skenario">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Skenario untuk {{ $sgd?->judul }}</h5>

                            <!--begin::Close-->
                            <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                                aria-label="Close">
                                <span class="svg-icon svg-icon-2x">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none">
                                        <rect opacity="0.5" x="7.05025" y="15.5356" width="12" height="2" rx="1"
                                            transform="rotate(-45 7.05025 15.5356)" fill="black" />
                                        <rect x="8.46447" y="7.05029" width="12" height="2" rx="1"
                                            transform="rotate(45 8.46447 7.05029)" fill="black" />
                                    </svg>
                                </span>
                            </div>
                            <!--end::Close-->
                        </div>

                        <form method="POST" action="{{ route('sgd.simpanSkenario', $sgd?->id) }}" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                <div class="mb-10">
                                    <label class="form-label">Skenario</label>
                                    <textarea name="skenario" class="form-control form-control-solid"
                                        required>{{ $sgd?->skenario }}</textarea>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div>
                <a href="{{ route('kelas.show', $sgd->kelas->kode) }}"
                    class="btn btn-sm btn-secondary fw-bolder">
                    Kembali
                    <span class="svg-icon svg-icon-1hx svg-icon-gray-500 me-1">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                            stroke="currentColor" class="w-6 h-6">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3" />
                        </svg>

                    </span>
                </a>
            </div>
        </div>
        @else
        <div class="d-flex align-items-center py-2 py-md-1">
            <a href="{{ route('sgd.export', $sgd?->kode) }}" class="btn btn-sm btn-primary me-2">
                Export Penilaian
            </a>
            <div>
                <a href="{{ route('kelas.penilaianFormatif', $sgd?->kelas->kode) }}"
                    class="btn btn-sm btn-secondary fw-bolder">
                    Kembali
                    <span class="svg-icon svg-icon-1hx svg-icon-gray-500 me-1">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                            stroke="currentColor" class="w-6 h-6">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3" />
                        </svg>

                    </span>
                </a>
            </div>
        </div>
        @endif
        <!--end::Page title-->
    </div>
    <!--end::Container-->
</div>
<div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
    <div class="content flex-row-fluid" id="kt_content">
        <div class="row">
            <div class="col-xl-4">
                <div class="card mb-6" data-kt-sticky="true">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped gy-7 gs-7">
                                <tr>
                                    <td width="35%">Semester</td>
                                    <td width="5">:</td>
                                    <td>{{ $sgd?->semester }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Blok</td>
                                    <td width="5">:</td>
                                    <td>{{ $sgd?->kelas?->blok?->nama }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Judul</td>
                                    <td width="5">:</td>
                                    <td>{{ $sgd?->judul }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Ruang</td>
                                    <td width="5">:</td>
                                    <td>{{ $sgd?->ruang }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Hari, Tanggal</td>
                                    <td width="5">:</td>
                                    <td>{{ Date::parse($sgd?->tanggal)?->format('l, d M Y') }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Waktu</td>
                                    <td width="5">:</td>
                                    <td>{{ $sgd?->tanggal?->format('H:i') }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Kelompok Peserta</td>
                                    <td width="5">:</td>
                                    <td>{{ $sgd?->kelompok?->nama }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Tutor</td>
                                    <td width="5">:</td>
                                    <td>{{ $sgd?->user?->name }}</td>
                                </tr>
                                <tr>
                                    <td width="35%">Skenario</td>
                                    <td width="5">:</td>
                                    <td>{{ $sgd?->skenario }}</td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
            </div>

            @php
                $absensi = \App\Models\Absensi::where('berita_acara_id', $sgd->id)->get();
            @endphp

            <div class="col-xl-8">
                <div class="card">
                    <!--begin::Body-->
                    <div class="card-body">
                        <div class="table-responsive">
                            <!--begin::Table-->
                            <table class="table table-striped table-bordered gy-7 gs-7">
                                <!--begin::Table head-->
                                <thead>
                                    <tr class="fw-bold fs-6 text-gray-800 border-bottom border-gray-200"">
                                        <th width="5" rowspan="2">No</th>
                                        <th class="min-w-150px" rowspan="2">Mahasiswa</th>
                                        <th class="text-center" colspan="{{ $sgd?->kriteria_sgds->count() }}">{{ $sgd?->judul }}</th>
                                        <th class="text-center" rowspan="2">Total Nilai</th>
                                        <th class="text-center" rowspan="2">Ket.</th>
                                        @if (auth()->user()->isDosen() && $sgd?->isBuka())
                                        <th class="min-w-100px"></th>
                                        @endif
                                    </tr>
                                    <tr>
                                        @foreach ($sgd?->kriteria_sgds as $kriteriaSgd)
                                        <th class="text-center">{{ $kriteriaSgd?->kriteria }}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <!--end::Table head-->
                                <!--begin::Table body-->
                                <tbody>

                                    @foreach ($sgd?->kelompok?->mahasiswas as $mahasiswa)
                                    @php
                                        $keterangan = \App\Models\NilaiSgd::query()
                                            ->where('sgd_id', $sgd?->id)
                                            ->where('mahasiswa_id', $mahasiswa?->id)
                                            ->whereNotNull('keterangan')
                                            ->orderBy('id', 'asc')
                                            ->first();
                                    @endphp

                                    <tr>
                                        <td>
                                            {{ $loop->iteration }}
                                        </td>
                                        <td>
                                            <span class="text-dark fw-bolder mb-1 fs-6">
                                                {{ $mahasiswa?->user?->name ?? '-' }}
                                            </span>
                                            <span class="text-muted fw-bold d-block">
                                                {{ $mahasiswa?->user?->username ?? '-' }}
                                            </span>
                                        </td>
                                        @php
                                            $totalNilai = 0;
                                        @endphp
                                        @foreach ($sgd?->kriteria_sgds as $kriteriaSgd)
                                        <td class="text-center">
                                            @php
                                            $nilaiSgd = \App\Models\NilaiSgd::query()
                                                ->where('sgd_id', $sgd?->id)
                                                ->where('kriteria_sgd_id', $kriteriaSgd?->id)
                                                ->where('mahasiswa_id', $mahasiswa?->id)
                                                ->first();
                                            @endphp
                                            @if ($nilaiSgd)
                                            <span class="badge badge-{{ \App\Models\NilaiSgd::NILAI_SELECT[$nilaiSgd?->nilai]['warna'] }} fw-bolder me-auto px-4 py-3">
                                                {{ $nilaiSgd?->nilai }} - {{ \App\Models\NilaiSgd::NILAI_SELECT[$nilaiSgd?->nilai]['ket'] }}
                                            </span>
                                            @else
                                            <span class="badge badge-secondary fw-bolder me-auto px-4 py-3">
                                                Belum Dinilai
                                            </span>
                                            @endif
                                        </td>
                                        @php
                                            $totalNilai += $nilaiSgd?->nilai;
                                        @endphp
                                        @endforeach
                                        <td class="text-center">
                                            {{ $totalNilai }}
                                        </td>
                                        <td>
                                            {{ $keterangan?->keterangan }}
                                        </td>
                                        @if (auth()->user()->isDosen() && $sgd?->isBuka())
                                        <td class="text-end">
                                            <button class="btn btn-sm btn-light-info" data-bs-toggle="modal"
                                                data-bs-target="#modal_nilai_sgd{{ $mahasiswa?->id }}{{ $sgd?->id }}">
                                                Beri Nilai
                                            </button>
                                        </td>

                                        <div class="modal fade" tabindex="-1"
                                            id="modal_nilai_sgd{{ $mahasiswa?->id }}{{ $sgd?->id }}">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Nilai Mahasiswa {{ $mahasiswa?->user?->name }}</h5>

                                                        <!--begin::Close-->
                                                        <div class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                                            data-bs-dismiss="modal" aria-label="Close">
                                                            <span class="svg-icon svg-icon-2x">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                    height="24" viewBox="0 0 24 24" fill="none">
                                                                    <rect opacity="0.5" x="7.05025" y="15.5356" width="12"
                                                                        height="2" rx="1"
                                                                        transform="rotate(-45 7.05025 15.5356)"
                                                                        fill="black" />
                                                                    <rect x="8.46447" y="7.05029" width="12" height="2"
                                                                        rx="1" transform="rotate(45 8.46447 7.05029)"
                                                                        fill="black" />
                                                                </svg>
                                                            </span>
                                                        </div>
                                                        <!--end::Close-->
                                                    </div>

                                                    <form method="POST" action="{{ route('nilaiSgd.simpan') }}"
                                                        enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="modal-body">
                                                            <input type="hidden" name="mahasiswa_id" value="{{ $mahasiswa?->id }}">
                                                            <input type="hidden" name="sgd_id" value="{{ $sgd?->id }}">

                                                            @foreach ($sgd?->kriteria_sgds as $kriteriaSgd)
                                                            @php
                                                                $nilaiSgd = \App\Models\NilaiSgd::query()
                                                                    ->where('sgd_id', $sgd?->id)
                                                                    ->where('kriteria_sgd_id', $kriteriaSgd?->id)
                                                                    ->where('mahasiswa_id', $mahasiswa?->id)
                                                                    ->first();
                                                            @endphp
                                                            <div class="mb-10">
                                                                <input type="hidden" name="kriteria_sgd_ids[]" value="{{ $kriteriaSgd?->id }}">
                                                                <label class="required form-label">{{ $kriteriaSgd?->kriteria }}</label>
                                                                <select name="nilais[]" class="form-select form-select-solid"
                                                                    data-control="select2"
                                                                    data-dropdown-parent="#modal_nilai_sgd{{ $mahasiswa?->id }}{{ $sgd?->id }}"
                                                                    data-placeholder="Pilih Nilai" data-allow-clear="true"
                                                                    required>
                                                                    <option></option>
                                                                    @foreach (\App\Models\NilaiSgd::NILAI_SELECT as $key => $nilai)
                                                                    <option value="{{ $key }}" @selected($nilaiSgd?->nilai == $key)>{{ $key }} - {{ $nilai['ket'] }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            @endforeach
                                                            <div class="mb-10">
                                                                <label class="form-label">Keterangan</label>
                                                                <textarea name="keterangan" class="form-control form-control-solid">{{ $keterangan?->keterangan }}</textarea>
                                                            </div>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    </tr>
                                    @endforeach

                                </tbody>
                                <!--end::Table body-->
                            </table>
                        </div>
                    </div>
                    <!--end::Body-->
                </div>
            </div>

        </div>
    </div>
    <!--end::Post-->
</div>
@endsection

@section('styles')
@endsection

@section('scripts')
@endsection
