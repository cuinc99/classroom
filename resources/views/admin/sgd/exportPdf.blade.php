<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pleno SGD</title>
    <style>
        table,
        th,
        td {
            border: 1px double black;
            border-collapse: collapse;
            padding: 5px;
        }

        .page-break {
            page-break-after: always;
        }
    </style>
</head>

<body>
    <div>
        <img src="{{ public_path().'/img/cop.png' }}" style="height: 90px; width: auto;" alt="">
    </div>
    <br />
    <br />

    <div style="text-align: center;">
        <b>TABEL PENILAIAN <i>SMALL GROUP DISCUSSION</i> (SGD)</b>
    </div>
    <br />

    <table width="100%" style="border: none;">
        <tr>
            <td width="35%" style="border: none;">BLOK</td>
            <td width="5" style="border: none;">:</td>
            <td style="border: none;">{{ $sgd?->kelas?->blok?->nama }}</td>
        </tr>
        <tr>
            <td width="35%" style="border: none;">KELOMPOK / RUANG</td>
            <td width="5" style="border: none;">:</td>
            <td style="border: none;">{{ $sgd?->judul }}</td>
        </tr>
        <tr>
            <td width="35%" style="border: none;">SEMESTER / KELAS</td>
            <td width="5" style="border: none;">:</td>
            <td style="border: none;">{{ $sgd?->semester }}</td>
        </tr>
        <tr>
            <td width="35%" style="border: none;">TUTOR</td>
            <td width="5" style="border: none;">:</td>
            <td style="border: none;">{{ $sgd?->user?->name }}</td>
        </tr>
        <tr>
            <td width="35%" style="border: none;">HARI, TANGGAL</td>
            <td width="5" style="border: none;">:</td>
            <td style="border: none;">{{ Date::parse($sgd?->tanggal)?->format('l, d M Y') }}</td>
        </tr>
        <tr>
            <td width="35%" style="border: none;">SKENARIO</td>
            <td width="5" style="border: none;">:</td>
            <td style="border: none;">{{ $sgd?->skenario }}</td>
        </tr>
    </table>
    <br />

    <table width="100%" style="font-size: 10pt;">
        <tr>
            <td widtd="5" rowspan="2" style="text-align: center;">No</td>
            <td widtd="25%" rowspan="2" style="text-align: center;">Nama Mahasiswa</td>
            <td widtd="15%" rowspan="2" style="text-align: center;">NIM</td>
            <td colspan="{{ $sgd?->kriteria_sgds->count() }}" style="text-align: center;">{{ $sgd?->judul }}</td>
            <td rowspan="2" style="text-align: center;">Total Nilai</td>
            <td rowspan="2" style="text-align: center;">Keterangan</td>
        </tr>
        <tr>
            @foreach ($sgd?->kriteria_sgds as $kriteriaSgd)
            <td style="text-align: center;">{{ $kriteriaSgd?->kriteria }}</td>
            @endforeach
        </tr>
        @foreach ($sgd?->kelompok?->mahasiswas as $mahasiswa)
        <tr>
            <td style="text-align: center;">{{ $loop->iteration }}</td>
            <td>{{ $mahasiswa?->user?->name }}</td>
            <td style="text-align: center;">{{ $mahasiswa?->user?->username }}</td>

            @php
            $totalNilai = 0;
            @endphp
            @foreach ($sgd?->kriteria_sgds as $kriteriaSgd)
            <td style="text-align: center;">
                @php
                $nilaiSgd = \App\Models\NilaiSgd::query()
                    ->where('sgd_id', $sgd?->id)
                    ->where('kriteria_sgd_id', $kriteriaSgd?->id)
                    ->where('mahasiswa_id', $mahasiswa?->id)
                    ->first();
                @endphp
                @if ($nilaiSgd)
                <span>
                    {{ $nilaiSgd?->nilai }} - {{ \App\Models\NilaiSgd::NILAI_SELECT[$nilaiSgd?->nilai]['ket'] }}
                </span>
                @else
                <span>
                    -
                </span>
                @endif
            </td>
            @php
            $totalNilai += $nilaiSgd?->nilai;
            @endphp
            @endforeach
            <td style="text-align: center;">
                {{ $totalNilai }}
            </td>
            <td>
                @php
                    $keterangan = \App\Models\NilaiSgd::query()
                        ->where('sgd_id', $sgd?->id)
                        ->where('mahasiswa_id', $mahasiswa?->id)
                        ->whereNotNull('keterangan')
                        ->orderBy('id', 'asc')
                        ->first();
                @endphp
                    {{ $keterangan?->keterangan }}
            </td>
        </tr>
        @endforeach
    </table>
    <br />
</body>

</html>
