@extends('layouts.admin')

@section('title')
    Edit Pleno SGD
@endsection

@section('content')
    <!--begin::Toolbar-->
    <div class="toolbar py-5 py-lg-5">
        <!--begin::Container-->
        <div class="container-xxl d-flex flex-stack flex-wrap">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column me-3">
                <!--begin::Title-->
                <h1 class="d-flex text-dark fw-bolder my-1 fs-3">Edit Pleno SGD</h1>
                <!--end::Title-->
            </div>
            <!--end::Page title-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Container-->
    <div class="d-flex flex-column-fluid align-items-start container-xxl">
        <div class="content flex-row-fluid">
            @include('layouts.partials.messages')

            @php
                $nilaiSgdExists = \App\Models\NilaiSgd::query()
                    ->where('sgd_id', $sgd?->id)
                    ->exists();
            @endphp

            <form method="POST" action="{{ route('sgd.update', $sgd?->id) }}" enctype="multipart/form-data">
                @csrf
                <div class="d-flex flex-column gap-7 gap-lg-10">
                    <!--begin::Inventory-->
                    <div class="card card-flush py-4">
                        <!--begin::Card body-->
                        <div class="card-body pt-5">
                            <div class="row">
                                <div class="col-xl-6">
                                    <div class="mb-10">
                                        <label class="required form-label">Semester</label>
                                        <input type="text" name="semester" value="{{ $sgd?->semester }}" class="form-control form-control-solid"
                                            placeholder="cth: 1 (Satu)" required />
                                    </div>

                                    <div class="mb-10">
                                        <label class="required form-label">Judul</label>
                                        <input type="text" name="judul" value="{{ $sgd?->judul }}" class="form-control form-control-solid"
                                           placeholder="cth: Kriteria Penilaian SGD Sesi I" required />
                                    </div>

                                    <div class="mb-10">
                                        <label class="required form-label">Ruang</label>
                                        <input type="text" name="ruang" value="{{ $sgd?->ruang }}" class="form-control form-control-solid"
                                            required />
                                    </div>

                                    <div class="mb-10">
                                        <label class="form-label">Skenario</label>
                                        <textarea name="skenario" class="form-control form-control-solid">{{ $sgd?->skenario }}</textarea>
                                    </div>

                                    <div class="mb-10">
                                        <label class="required form-label">Tanggal</label>
                                        <input type="datetime-local" name="tanggal" value="{{ $sgd?->tanggal }}"
                                            class="form-control form-control-solid" required />
                                    </div>

                                    <div class="mb-10">
                                        <label class="required form-label">Kelompok</label>
                                        <select name="kelompok_id" class="form-select form-select-solid"
                                            id="kelompok_id" data-control="select2"
                                             data-placeholder="Pilih Kelompok"
                                            data-allow-clear="true" required @disabled($nilaiSgdExists)>
                                            <option></option>
                                            @foreach ($kelas->kelompoks as $item)
                                            <option value="{{ $item->id }}" @selected($item->id == $sgd?->kelompok_id)>{{ $item->nama }} ({{
                                                $item->mahasiswas->count() }}
                                                Mahasiswa)</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="mb-10">
                                        <label class="required form-label">Tutor</label>
                                        <select name="user_id" class="form-select form-select-solid"
                                            data-control="select2"
                                            data-placeholder="Pilih Tutor" data-allow-clear="true" required @disabled($nilaiSgdExists)>
                                            <option></option>
                                            @foreach ($dosens as $item)
                                            <option value="{{ $item->id }}" @selected($item->id == $sgd?->user_id)>{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <label class="form-label">Kriteria</label>
                                    @foreach ($sgd->kriteria_sgds as $kriteriaSgd)
                                        <div class="row fv-row mb-10">
                                            <!--begin::Col-->
                                            <div class="col-10 fv-row">
                                                <div class="input-group input-group-solid mb-5">
                                                    <span class="input-group-text" id="basic-addon2">
                                                        <span class="svg-icon svg-icon-1">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                height="24" viewBox="0 0 24 24" fill="none">
                                                                <path opacity="0.3"
                                                                    d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                                                                    fill="black" />
                                                                <path
                                                                    d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                                                                    fill="black" />
                                                            </svg>
                                                        </span>
                                                    </span>
                                                    <textarea name="kriterias[]"
                                                        class="form-control form-control-solid" required @disabled($nilaiSgdExists)>{{ $kriteriaSgd?->kriteria }}</textarea>
                                                </div>
                                            </div>
                                            <!--end::Col-->
                                            <!--begin::Col-->
                                            @if (!$nilaiSgdExists)
                                            <div class="col-2 fv-row">
                                                <a href="{{ route('kriteriaSgd.hapus', $kriteriaSgd?->id) }}" onclick="return confirm('Yakin akan menghapus?');" class="svg-icon svg-icon-danger svg-icon-3hx">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none">
                                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10"
                                                            fill="black" />
                                                        <rect x="7" y="15.3137" width="12" height="2" rx="1"
                                                            transform="rotate(-45 7 15.3137)" fill="black" />
                                                        <rect x="8.41422" y="7" width="12" height="2" rx="1"
                                                            transform="rotate(45 8.41422 7)" fill="black" />
                                                    </svg>
                                                </a>
                                            </div>
                                            @endif
                                            <!--end::Col-->
                                        </div>
                                    @endforeach

                                    @if (!$nilaiSgdExists)
                                    <div x-data="handler()">
                                        <div class="mb-5 fv-row">
                                            <a class="btn btn-primary rounded-50" @click="addNewField()">
                                                <span class="svg-icon svg-icon-2">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none">
                                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5"
                                                            fill="black" />
                                                        <rect x="10.8891" y="17.8033" width="12" height="2" rx="1"
                                                            transform="rotate(-90 10.8891 17.8033)" fill="black" />
                                                        <rect x="6.01041" y="10.9247" width="12" height="2" rx="1"
                                                            fill="black" />
                                                    </svg>
                                                </span>
                                                Tambah Kriteria</a>
                                        </div>
                                        <template x-for="(field, index) in fields" :key="index">
                                            <div class="row fv-row">
                                                <!--begin::Col-->
                                                <div class="col-10 fv-row">
                                                    <div class="input-group input-group-solid mb-5">
                                                        <span class="input-group-text" id="basic-addon2">
                                                            <span class="svg-icon svg-icon-1">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                    height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path opacity="0.3"
                                                                        d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                                                                        fill="black" />
                                                                    <path
                                                                        d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                                                                        fill="black" />
                                                                </svg>
                                                            </span>
                                                        </span>
                                                        <textarea x-model="field.kriterias" name="kriterias[]"
                                                            class="form-control form-control-solid" required></textarea>
                                                    </div>
                                                </div>
                                                <!--end::Col-->
                                                <!--begin::Col-->
                                                <div class="col-2 fv-row">
                                                    <span class="svg-icon svg-icon-danger svg-icon-3hx"
                                                        @click="removeField(index)">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                            viewBox="0 0 24 24" fill="none">
                                                            <rect opacity="0.3" x="2" y="2" width="20" height="20"
                                                                rx="10" fill="black" />
                                                            <rect x="7" y="15.3137" width="12" height="2" rx="1"
                                                                transform="rotate(-45 7 15.3137)" fill="black" />
                                                            <rect x="8.41422" y="7" width="12" height="2" rx="1"
                                                                transform="rotate(45 8.41422 7)" fill="black" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <!--end::Col-->
                                            </div>
                                        </template>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!--end::Card header-->
                    </div>
                    <!--end::Inventory-->
                </div>
                <div class="d-flex justify-content-end mt-10">
                    <!--begin::Button-->
                    <a href="{{ route('kelas.sgd', $kelas->kode) }}" class="btn btn-light me-5">Batal</a>
                    <!--end::Button-->
                    <!--begin::Button-->
                    <button type="submit" class="btn btn-primary">
                        <span class="indicator-label">Simpan</span>
                    </button>
                    <!--end::Button-->
                </div>
            </form>
        </div>
        <!--end::Post-->
    </div>
    <!--end::Container-->
@endsection

@section('styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section('scripts')
    {{-- @stack('scripts') --}}
    <script src="/assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js"></script>
    <script src="/js/quill/image-resize.min.js"></script>
    <script src="/js/quill/image-drop.min.js"></script>
    <script>
        var toolbarOptions = [
            ['bold', 'italic', 'underline', 'strike'], // toggled buttons
            ['blockquote', 'code-block'],

            [{
                'header': 1
            }, {
                'header': 2
            }], // custom button values
            [{
                'list': 'ordered'
            }, {
                'list': 'bullet'
            }],
            [{
                'script': 'sub'
            }, {
                'script': 'super'
            }], // superscript/subscript
            [{
                'indent': '-1'
            }, {
                'indent': '+1'
            }], // outdent/indent
            [{
                'direction': 'rtl'
            }], // text direction

            [{
                'size': ['small', false, 'large', 'huge']
            }], // custom dropdown
            [{
                'header': [1, 2, 3, 4, 5, 6, false]
            }],

            [{
                'color': []
            }, {
                'background': []
            }], // dropdown with defaults from theme
            [{
                'font': []
            }],
            [{
                'align': []
            }],
            ['image', 'video', 'link'],

            ['clean'] // remove formatting button
        ];
        var quill = new Quill('#isi_editor', {
            theme: 'snow',
            placeholder: 'Tulis instruksi disini...',
            modules: {
                toolbar: toolbarOptions,
                imageResize: {
                    displaySize: true
                },
                imageDrop: true
            },
        });
        quill.on('text-change', function(delta, oldDelta, source) {
            document.getElementById("isi_html").value = quill.root.innerHTML;
        });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        $("#batas_akhir").flatpickr({
            enableTime: true,
            dateFormat: "Y-m-d H:i",
            altInput: true,
            altFormat: "F j, Y H:i",
        });
    </script>
    <script>
        function handler() {
            return {
                fields: [],
                addNewField() {
                    this.fields.push({
                        kriterias: '',
                    });
                },
                removeField(index) {
                    this.fields.splice(index, 1);
                }
            }
        }
    </script>
@endsection
