<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Blok
 *
 * @property int $id
 * @property string|null $tipe
 * @property string|null $tahap
 * @property string $kode
 * @property string $nama
 * @property int|null $sks
 * @property int|null $semester
 * @property int|null $syarat_mata_kuliah
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Kelas[] $kelas
 * @property-read int|null $kelas_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\KelompokPertanyaan[] $kelompok_pertanyaans
 * @property-read int|null $kelompok_pertanyaans_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Pertanyaan[] $pertanyaans
 * @property-read int|null $pertanyaans_count
 * @method static \Illuminate\Database\Eloquent\Builder|Blok newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Blok newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Blok query()
 * @method static \Illuminate\Database\Eloquent\Builder|Blok whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blok whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blok whereKode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blok whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blok whereSemester($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blok whereSks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blok whereSyaratMataKuliah($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blok whereTahap($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blok whereTipe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blok whereUpdatedAt($value)
 */
	class Blok extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Comment
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $parent_id
 * @property string $comment
 * @property int|null $commentable_id
 * @property string $commentable_type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Comment[] $replies
 * @property-read int|null $replies_count
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Comment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Comment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Comment query()
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereCommentableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereCommentableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereUserId($value)
 */
	class Comment extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Dosen
 *
 * @property int $id
 * @property string $kode
 * @property string $status
 * @property int|null $user_id
 * @property int|null $kelas_id
 * @property int|null $kelompok_id
 * @property int|null $tugas_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Kelompok|null $kelompok
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Dosen newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Dosen newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Dosen query()
 * @method static \Illuminate\Database\Eloquent\Builder|Dosen whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dosen whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dosen whereKelasId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dosen whereKelompokId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dosen whereKode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dosen whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dosen whereTugasId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dosen whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dosen whereUserId($value)
 */
	class Dosen extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Jawaban
 *
 * @property int $id
 * @property string $jawaban
 * @property int $poin
 * @property int|null $pertanyaan_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Jawaban newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Jawaban newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Jawaban query()
 * @method static \Illuminate\Database\Eloquent\Builder|Jawaban whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Jawaban whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Jawaban whereJawaban($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Jawaban wherePertanyaanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Jawaban wherePoin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Jawaban whereUpdatedAt($value)
 */
	class Jawaban extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\JawabanTugas
 *
 * @property int $id
 * @property int $poin
 * @property int|null $mahasiswa_id
 * @property int|null $tugas_id
 * @property int|null $pertanyaan_id
 * @property int|null $jawaban_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|JawabanTugas newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|JawabanTugas newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|JawabanTugas query()
 * @method static \Illuminate\Database\Eloquent\Builder|JawabanTugas whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JawabanTugas whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JawabanTugas whereJawabanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JawabanTugas whereMahasiswaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JawabanTugas wherePertanyaanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JawabanTugas wherePoin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JawabanTugas whereTugasId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JawabanTugas whereUpdatedAt($value)
 */
	class JawabanTugas extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Kategori
 *
 * @property int $id
 * @property string $kode
 * @property string $nama
 * @property int $persentase_nilai
 * @property int|null $kelas_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tugas[] $tugas
 * @property-read int|null $tugas_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tugas[] $tugasByOldest
 * @property-read int|null $tugas_by_oldest_count
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori query()
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori whereKelasId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori whereKode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori wherePersentaseNilai($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kategori whereUpdatedAt($value)
 */
	class Kategori extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Kelas
 *
 * @property int $id
 * @property string $kode
 * @property string $nama
 * @property string|null $deskripsi
 * @property int|null $user_id
 * @property int|null $blok_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Blok|null $blok
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Kategori[] $kategoris
 * @property-read int|null $kategoris_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Kelompok[] $kelompoks
 * @property-read int|null $kelompoks_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Mahasiswa[] $mahasiswas
 * @property-read int|null $mahasiswas_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Pengumuman[] $pengumumans
 * @property-read int|null $pengumumans_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tugas[] $tugas
 * @property-read int|null $tugas_count
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Kelas newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Kelas newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Kelas query()
 * @method static \Illuminate\Database\Eloquent\Builder|Kelas whereBlokId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kelas whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kelas whereDeskripsi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kelas whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kelas whereKode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kelas whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kelas whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kelas whereUserId($value)
 */
	class Kelas extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Kelompok
 *
 * @property int $id
 * @property string $kode
 * @property string $nama
 * @property int|null $kelas_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Kelas|null $kelas
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Mahasiswa[] $mahasiswas
 * @property-read int|null $mahasiswas_count
 * @method static \Illuminate\Database\Eloquent\Builder|Kelompok newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Kelompok newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Kelompok query()
 * @method static \Illuminate\Database\Eloquent\Builder|Kelompok whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kelompok whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kelompok whereKelasId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kelompok whereKode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kelompok whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Kelompok whereUpdatedAt($value)
 */
	class Kelompok extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\KelompokPertanyaan
 *
 * @property int $id
 * @property string $kode
 * @property string $nama
 * @property int|null $blok_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Blok|null $blok
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Pertanyaan[] $pertanyaans
 * @property-read int|null $pertanyaans_count
 * @method static \Illuminate\Database\Eloquent\Builder|KelompokPertanyaan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|KelompokPertanyaan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|KelompokPertanyaan query()
 * @method static \Illuminate\Database\Eloquent\Builder|KelompokPertanyaan whereBlokId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KelompokPertanyaan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KelompokPertanyaan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KelompokPertanyaan whereKode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KelompokPertanyaan whereNama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|KelompokPertanyaan whereUpdatedAt($value)
 */
	class KelompokPertanyaan extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Mahasiswa
 *
 * @property int $id
 * @property string $kode
 * @property int|null $user_id
 * @property int|null $kelas_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Kelompok[] $kelompoks
 * @property-read int|null $kelompoks_count
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Mahasiswa newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Mahasiswa newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Mahasiswa query()
 * @method static \Illuminate\Database\Eloquent\Builder|Mahasiswa whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Mahasiswa whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Mahasiswa whereKelasId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Mahasiswa whereKode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Mahasiswa whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Mahasiswa whereUserId($value)
 */
	class Mahasiswa extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\MahasiswaKelompok
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $mahasiswa_id
 * @property int|null $kelompok_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|MahasiswaKelompok newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MahasiswaKelompok newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MahasiswaKelompok query()
 * @method static \Illuminate\Database\Eloquent\Builder|MahasiswaKelompok whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MahasiswaKelompok whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MahasiswaKelompok whereKelompokId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MahasiswaKelompok whereMahasiswaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MahasiswaKelompok whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MahasiswaKelompok whereUserId($value)
 */
	class MahasiswaKelompok extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\MahasiswaTugas
 *
 * @property int $id
 * @property int $total_poin
 * @property string|null $tanggal_kumpul
 * @property string|null $file_tugas
 * @property int|null $mahasiswa_id
 * @property int|null $tugas_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $tanggal_mulai_test
 * @property string|null $tanggal_selesai_test
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Comment[] $comments
 * @property-read int|null $comments_count
 * @property-read \App\Models\Mahasiswa|null $mahasiswa
 * @method static \Illuminate\Database\Eloquent\Builder|MahasiswaTugas newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MahasiswaTugas newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MahasiswaTugas query()
 * @method static \Illuminate\Database\Eloquent\Builder|MahasiswaTugas whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MahasiswaTugas whereFileTugas($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MahasiswaTugas whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MahasiswaTugas whereMahasiswaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MahasiswaTugas whereTanggalKumpul($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MahasiswaTugas whereTanggalMulaiTest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MahasiswaTugas whereTanggalSelesaiTest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MahasiswaTugas whereTotalPoin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MahasiswaTugas whereTugasId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MahasiswaTugas whereUpdatedAt($value)
 */
	class MahasiswaTugas extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Pengumuman
 *
 * @property int $id
 * @property string $kode
 * @property string $isi
 * @property string|null $lampiran
 * @property int|null $kelas_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Kelas|null $kelas
 * @method static \Illuminate\Database\Eloquent\Builder|Pengumuman newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Pengumuman newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Pengumuman query()
 * @method static \Illuminate\Database\Eloquent\Builder|Pengumuman whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pengumuman whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pengumuman whereIsi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pengumuman whereKelasId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pengumuman whereKode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pengumuman whereLampiran($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pengumuman whereUpdatedAt($value)
 */
	class Pengumuman extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Pertanyaan
 *
 * @property int $id
 * @property string $kode
 * @property string $pertanyaan
 * @property string|null $deskripsi
 * @property int $maks_poin
 * @property string|null $tipe
 * @property int $parent_id
 * @property int|null $tugas_id
 * @property int|null $blok_id
 * @property int|null $kelompok_pertanyaan_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Blok|null $blok
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Jawaban[] $jawabans
 * @property-read int|null $jawabans_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TugasPertanyaan[] $tugas_pertanyaans
 * @property-read int|null $tugas_pertanyaans_count
 * @method static \Illuminate\Database\Eloquent\Builder|Pertanyaan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Pertanyaan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Pertanyaan query()
 * @method static \Illuminate\Database\Eloquent\Builder|Pertanyaan whereBlokId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pertanyaan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pertanyaan whereDeskripsi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pertanyaan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pertanyaan whereKelompokPertanyaanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pertanyaan whereKode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pertanyaan whereMaksPoin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pertanyaan whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pertanyaan wherePertanyaan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pertanyaan whereTipe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pertanyaan whereTugasId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pertanyaan whereUpdatedAt($value)
 */
	class Pertanyaan extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Tugas
 *
 * @property int $id
 * @property string $kode
 * @property string $judul
 * @property string|null $instruksi
 * @property string|null $dokumen_instruksi
 * @property int $maks_poin
 * @property string|null $batas_akhir
 * @property string|null $mulai_pada
 * @property int $status_ujian
 * @property string $tipe
 * @property int $acak_soal
 * @property int $acak_jawaban
 * @property int|null $kelas_id
 * @property int|null $kategori_id
 * @property int|null $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $durasi
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Dosen[] $dosen_penilai
 * @property-read int|null $dosen_penilai_count
 * @property-read \App\Models\Kategori|null $kategori
 * @property-read \App\Models\Kelas|null $kelas
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Pertanyaan[] $pertanyaanTugas
 * @property-read int|null $pertanyaan_tugas_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Pertanyaan[] $pertanyaans
 * @property-read int|null $pertanyaans_count
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Tugas newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tugas newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tugas query()
 * @method static \Illuminate\Database\Eloquent\Builder|Tugas whereAcakJawaban($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tugas whereAcakSoal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tugas whereBatasAkhir($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tugas whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tugas whereDokumenInstruksi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tugas whereDurasi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tugas whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tugas whereInstruksi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tugas whereJudul($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tugas whereKategoriId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tugas whereKelasId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tugas whereKode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tugas whereMaksPoin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tugas whereMulaiPada($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tugas whereStatusUjian($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tugas whereTipe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tugas whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tugas whereUserId($value)
 */
	class Tugas extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\TugasPertanyaan
 *
 * @property int $id
 * @property int|null $tugas_id
 * @property int|null $pertanyaan_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|TugasPertanyaan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TugasPertanyaan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TugasPertanyaan query()
 * @method static \Illuminate\Database\Eloquent\Builder|TugasPertanyaan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TugasPertanyaan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TugasPertanyaan wherePertanyaanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TugasPertanyaan whereTugasId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TugasPertanyaan whereUpdatedAt($value)
 */
	class TugasPertanyaan extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $username
 * @property string $password
 * @property string $user_type
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Kelas[] $kelasDosens
 * @property-read int|null $kelas_dosens_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Kelas[] $kelasMahasiswas
 * @property-read int|null $kelas_mahasiswas_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Kelompok[] $kelompokDosens
 * @property-read int|null $kelompok_dosens_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Kelompok[] $kelompokMahasiswas
 * @property-read int|null $kelompok_mahasiswas_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Kelompok[] $kelompoks
 * @property-read int|null $kelompoks_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Sanctum\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUserType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUsername($value)
 */
	class User extends \Eloquent {}
}

