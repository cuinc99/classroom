!(function (e) {
    var t = {};
    function n(r) {
        if (t[r]) return t[r].exports;
        var o = (t[r] = { i: r, l: !1, exports: {} });
        return e[r].call(o.exports, o, o.exports, n), (o.l = !0), o.exports;
    }
    (n.m = e),
        (n.c = t),
        (n.d = function (e, t, r) {
            n.o(e, t) ||
                Object.defineProperty(e, t, { enumerable: !0, get: r });
        }),
        (n.r = function (e) {
            "undefined" != typeof Symbol &&
                Symbol.toStringTag &&
                Object.defineProperty(e, Symbol.toStringTag, {
                    value: "Module",
                }),
                Object.defineProperty(e, "__esModule", { value: !0 });
        }),
        (n.t = function (e, t) {
            if ((1 & t && (e = n(e)), 8 & t)) return e;
            if (4 & t && "object" == typeof e && e && e.__esModule) return e;
            var r = Object.create(null);
            if (
                (n.r(r),
                Object.defineProperty(r, "default", {
                    enumerable: !0,
                    value: e,
                }),
                2 & t && "string" != typeof e)
            )
                for (var o in e)
                    n.d(
                        r,
                        o,
                        function (t) {
                            return e[t];
                        }.bind(null, o)
                    );
            return r;
        }),
        (n.n = function (e) {
            var t =
                e && e.__esModule
                    ? function () {
                          return e.default;
                      }
                    : function () {
                          return e;
                      };
            return n.d(t, "a", t), t;
        }),
        (n.o = function (e, t) {
            return Object.prototype.hasOwnProperty.call(e, t);
        }),
        (n.p = "/"),
        n((n.s = 36));
})({
    36: function (e, t, n) {
        e.exports = n(37);
    },
    37: function (e, t) {
        if ("undefined" != typeof closeAt) var n = new Date(closeAt).getTime();
        else n = new Date().getTime();
        var r = new Date(now).getTime();
        r -= 1e3;
        var o = document.getElementById("timer-element"),
            a = !1,
            i = setInterval(function () {
                var e = n - (r += 1e3),
                    t = (Math.floor(e / 864e5), Math.floor((e % 864e5) / 36e5)),
                    l = Math.floor((e % 36e5) / 6e4),
                    u = Math.floor((e % 6e4) / 1e3);
                (o.innerHTML = t + " jam " + l + " menit " + u + " detik"),
                    0 === t &&
                        l <= 1 &&
                        (o.classList.add("text-danger"),
                        o.classList.remove("text-dark"),
                        a || $("#timeWarningModal").modal("show"),
                        (a = !0)),
                    e < 0 &&
                        (clearInterval(i),
                        (document.getElementById("timer-element").innerHTML =
                            "WAKTU TEST HABIS"),
                        (document.getElementById('btn').click()));
            }, 1e3);
    },
});
