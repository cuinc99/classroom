<?php

namespace Database\Seeders;

use App\Models\Blok;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $path = 'database/usersclass.sql';
        DB::unprepared(file_get_contents($path));

        // User::create([
        //     'name' => 'Admin',
        //     'username' => 'admin',
        //     'password' => bcrypt('password'),
        //     'user_type' => 'admin',
        // ]);

        // User::create([
        //     'name' => 'Assessment',
        //     'username' => 'assessment',
        //     'password' => bcrypt('password'),
        //     'user_type' => 'assessment',
        // ]);

        // for ($i=1; $i <= 5; $i++) {
        //     User::create([
        //         'name' => 'Dosen ' . $i,
        //         'username' => 'dosen' . $i,
        //         'password' => bcrypt('password'),
        //         'user_type' => 'dosen',
        //     ]);
        // }

        // for ($i=1; $i <= 10; $i++) {
        //     User::create([
        //         'name' => 'Mahasiswa ' . $i,
        //         'username' => 'mahasiswa' . $i,
        //         'password' => bcrypt('password'),
        //         'user_type' => 'mahasiswa',
        //     ]);
        // }

        $bloks = [
            // semester 1
            [
                'id' => 1,
                'tahap' => '1',
                'tipe' => 'Sarjana',
                'kode' => '01PPD04',
                'nama' => 'Personal Profesional Development (PPD)',
                'sks' => 4,
                'semester' => 1,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 2,
                'tahap' => '2',
                'tipe' => 'Sarjana',
                'kode' => '02BMD05',
                'nama' => 'BIOMEDIK',
                'sks' => 5,
                'semester' => 1,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 3,
                'tahap' => '3',
                'tipe' => 'Sarjana',
                'kode' => '03SC04',
                'nama' => 'Sistem Cardiovascular I',
                'sks' => 4,
                'semester' => 1,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 4,
                'tahap' => '4',
                'tipe' => 'Sarjana',
                'kode' => '04SR05',
                'nama' => 'Sistem Respirasi I',
                'sks' => 5,
                'semester' => 1,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 5,
                'tipe' => 'Sarjana',
                'kode' => '01MKD02',
                'nama' => 'Bahasa Indonesia',
                'sks' => 2,
                'semester' => 1,
                'tahap' => NULL,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 6,
                'tipe' => 'Sarjana',
                'kode' => '01CSL02',
                'nama' => 'Clinical Skills Lab 1',
                'sks' => 2,
                'semester' => 1,
                'tahap' => NULL,
                'syarat_mata_kuliah' => NULL,
            ],

            // semester 2
            [
                'id' => 7,
                'tahap' => '1',
                'tipe' => 'Sarjana',
                'kode' => '05NMS05',
                'nama' => 'Sistem Neuromusculoskeletal (NMS) I',
                'sks' => 5,
                'semester' => 2,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 8,
                'tahap' => '2',
                'tipe' => 'Sarjana',
                'kode' => '06SU04',
                'nama' => 'Sistem Urogenital I',
                'sks' => 4,
                'semester' => 2,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 9,
                'tahap' => '3',
                'tipe' => 'Sarjana',
                'kode' => '07SR05',
                'nama' => 'Sistem Reproduksi I',
                'sks' => 5,
                'semester' => 2,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 10,
                'tahap' => '4',
                'tipe' => 'Sarjana',
                'kode' => '08SD05',
                'nama' => 'Sistem Digestive I',
                'sks' => 5,
                'semester' => 2,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 11,
                'tipe' => 'Sarjana',
                'kode' => '02MKD02',
                'nama' => 'Bahasa Inggris',
                'sks' => 2,
                'semester' => 2,
                'tahap' => NULL,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 12,
                'tipe' => 'Sarjana',
                'kode' => '02CSL02',
                'nama' => 'Clinical Skills Lab 2',
                'sks' => 2,
                'semester' => 2,
                'tahap' => NULL,
                'syarat_mata_kuliah' => NULL,
            ],

            // semester 3
            [
                'id' => 13,
                'tahap' => '1',
                'tipe' => 'Sarjana',
                'kode' => '09SEM05',
                'nama' => 'Sistem Endokrin & Metabolisme',
                'sks' => 5,
                'semester' => 3,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 14,
                'tahap' => '2',
                'tipe' => 'Sarjana',
                'kode' => '10SHI04',
                'nama' => 'Sistem Hematologi & Imunologi',
                'sks' => 4,
                'semester' => 3,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 15,
                'tahap' => '3',
                'tipe' => 'Sarjana',
                'kode' => '11SC04',
                'nama' => 'Sistem Cardiovascular II',
                'sks' => 4,
                'semester' => 3,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 16,
                'tahap' => '4',
                'tipe' => 'Sarjana',
                'kode' => '12SR05',
                'nama' => 'Sistem Respirasi II',
                'sks' => 5,
                'semester' => 3,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 17,
                'tipe' => 'Sarjana',
                'kode' => '03MKD02',
                'nama' => 'Pendidikan Pancasila dan Kewarganegaraan',
                'sks' => 2,
                'semester' => 3,
                'tahap' => NULL,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 18,
                'tipe' => 'Sarjana',
                'kode' => '03CSL02',
                'nama' => 'Clinical Skills Lab 3',
                'sks' => 2,
                'semester' => 3,
                'tahap' => NULL,
                'syarat_mata_kuliah' => NULL,
            ],

            // semester 4
            [
                'id' => 19,
                'tahap' => '1',
                'tipe' => 'Sarjana',
                'kode' => '13NMS05',
                'nama' => 'Sistem Neuromusculoskeletal (NMS) II',
                'sks' => 5,
                'semester' => 4,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 20,
                'tahap' => '2',
                'tipe' => 'Sarjana',
                'kode' => '14SU04',
                'nama' => 'Sistem Urogenital II',
                'sks' => 4,
                'semester' => 4,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 21,
                'tahap' => '3',
                'tipe' => 'Sarjana',
                'kode' => '15SR05',
                'nama' => 'Sistem Reproduksi II',
                'sks' => 5,
                'semester' => 4,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 22,
                'tahap' => '4',
                'tipe' => 'Sarjana',
                'kode' => '16SD05',
                'nama' => 'Sistem Digestive II',
                'sks' => 5,
                'semester' => 4,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 23,
                'tipe' => 'Sarjana',
                'kode' => '04MKD02',
                'nama' => 'Pendidikan Agama',
                'sks' => 2,
                'semester' => 4,
                'tahap' => NULL,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 24,
                'tipe' => 'Sarjana',
                'kode' => '04CSL02',
                'nama' => 'Clinical Skills Lab 4',
                'sks' => 2,
                'semester' => 4,
                'tahap' => NULL,
                'syarat_mata_kuliah' => NULL,
            ],

            // semester 5
            [
                'id' => 25,
                'tahap' => '1',
                'tipe' => 'Sarjana',
                'kode' => '17MT05',
                'nama' => 'Mata & THT',
                'sks' => 5,
                'semester' => 5,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 26,
                'tahap' => '2',
                'tipe' => 'Sarjana',
                'kode' => '18INT04',
                'nama' => 'Integument',
                'sks' => 4,
                'semester' => 5,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 27,
                'tahap' => '3',
                'tipe' => 'Sarjana',
                'kode' => '19PSI04',
                'nama' => 'Psikiatri',
                'sks' => 4,
                'semester' => 5,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 28,
                'tahap' => '4',
                'tipe' => 'Sarjana',
                'kode' => '20TBG05',
                'nama' => 'Tumbang Kembang & Geriatri',
                'sks' => 5,
                'semester' => 5,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 29,
                'tipe' => 'Sarjana',
                'kode' => '05CSL02',
                'nama' => 'Clinical Skills Lab 5',
                'sks' => 2,
                'semester' => 5,
                'tahap' => NULL,
                'syarat_mata_kuliah' => NULL,
            ],

            // semester 6
            [
                'id' => 30,
                'tahap' => '1',
                'tipe' => 'Sarjana',
                'kode' => '21PK05',
                'nama' => 'Penelitian Kesehatan ',
                'sks' => 5,
                'semester' => 6,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 31,
                'tahap' => '2',
                'tipe' => 'Sarjana',
                'kode' => '22KEKE04',
                'nama' => 'Kedokteran Keluarga ',
                'sks' => 4,
                'semester' => 6,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 32,
                'tahap' => '3',
                'tipe' => 'Sarjana',
                'kode' => '23KEKO05',
                'nama' => 'Kedokteran Komunitas',
                'sks' => 5,
                'semester' => 6,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 33,
                'tahap' => '4',
                'tipe' => 'Sarjana',
                'kode' => '24KSPR05',
                'nama' => 'Kesehatan Pariwisata',
                'sks' => 5,
                'semester' => 6,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 34,
                'tipe' => 'Sarjana',
                'kode' => '06CSL02',
                'nama' => 'Clinical Skills Lab 6',
                'sks' => 2,
                'semester' => 6,
                'tahap' => NULL,
                'syarat_mata_kuliah' => NULL,
            ],

            // semester 7
            [
                'id' => 35,
                'tahap' => '1',
                'tipe' => 'Sarjana',
                'kode' => '25EMR05',
                'nama' => 'Emergency',
                'sks' => 5,
                'semester' => 7,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 36,
                'tahap' => '2',
                'tipe' => 'Sarjana',
                'kode' => '26MDC04',
                'nama' => 'Medicolegal',
                'sks' => 4,
                'semester' => 7,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 37,
                'tahap' => '3-4 (1)',
                'tipe' => 'Sarjana',
                'kode' => '27EHK02',
                'nama' => 'Elektif Hukum Kesehatan',
                'sks' => 3,
                'semester' => 7,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 38,
                'tahap' => '3-4 (1)',
                'tipe' => 'Sarjana',
                'kode' => '28EKH04',
                'nama' => 'Elektif Kesehatan Haji',
                'sks' => 3,
                'semester' => 7,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 39,
                'tahap' => '3-4 (1)',
                'tipe' => 'Sarjana',
                'kode' => '29EPB04',
                'nama' => 'Elektif Penanggulangan Bencana',
                'sks' => 3,
                'semester' => 7,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 40,
                'tipe' => 'Sarjana',
                'kode' => '07CSL02',
                'nama' => 'Clinical Skills Lab 7',
                'sks' => 2,
                'semester' => 7,
                'tahap' => NULL,
                'syarat_mata_kuliah' => NULL,
            ],
            [
                'id' => 41,
                'tipe' => 'Sarjana',
                'kode' => '30KTI04',
                'nama' => 'KTI',
                'sks' => 4,
                'semester' => 7,
                'tahap' => NULL,
                'syarat_mata_kuliah' => NULL,
            ],
        ];
        Blok::insert($bloks);

    }
}
