<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pertanyaans', function (Blueprint $table) {
            $table->id();
            $table->string('kode')->unique();
            $table->longText('pertanyaan');
            $table->longText('deskripsi')->nullable();
            $table->integer('maks_poin')->default(10);
            $table->string('tipe')->nullable(); // rubrik, kuis
            $table->bigInteger('parent_id')->default(0);

            $table->unsignedBigInteger('tugas_id')->nullable();
            $table->foreign('tugas_id')
                ->references('id')
                ->on('tugas')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->unsignedBigInteger('blok_id')->nullable();
            $table->foreign('blok_id')
                ->references('id')
                ->on('bloks')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->unsignedBigInteger('kelompok_pertanyaan_id')->nullable();
            $table->foreign('kelompok_pertanyaan_id')
                ->references('id')
                ->on('kelompok_pertanyaans')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pertanyaans');
    }
};
