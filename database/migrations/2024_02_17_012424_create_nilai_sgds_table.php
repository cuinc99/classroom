<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('nilai_sgds', function (Blueprint $table) {
            $table->id();
            $table->integer('nilai')->default(0);
            $table->text('keterangan')->nullable();

            $table->unsignedBigInteger('mahasiswa_id')->nullable();
            $table->foreign('mahasiswa_id')
                ->references('id')
                ->on('mahasiswas')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->unsignedBigInteger('kriteria_sgd_id')->nullable();
            $table->foreign('kriteria_sgd_id')
                ->references('id')
                ->on('kriteria_sgds')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->unsignedBigInteger('sgd_id')->nullable();
            $table->foreign('sgd_id')
                ->references('id')
                ->on('sgds')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('nilai_sgds');
    }
};
