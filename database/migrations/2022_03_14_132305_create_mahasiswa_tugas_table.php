<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswa_tugas', function (Blueprint $table) {
            $table->id();
            $table->integer('total_poin')->default(0);
            $table->dateTime('tanggal_kumpul')->nullable();
            $table->dateTime('tanggal_mulai_test')->nullable();
            $table->dateTime('tanggal_selesai_test')->nullable();
            $table->string('file_tugas')->nullable();

            $table->unsignedBigInteger('mahasiswa_id')->nullable();
            $table->foreign('mahasiswa_id')
                ->references('id')
                ->on('mahasiswas')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->unsignedBigInteger('tugas_id')->nullable();
                $table->foreign('tugas_id')
                    ->references('id')
                    ->on('tugas')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswa_tugas');
    }
};
