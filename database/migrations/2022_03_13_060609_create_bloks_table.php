<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bloks', function (Blueprint $table) {
            $table->id();
            $table->string('tipe')->nullable();
            $table->string('tahap')->nullable();
            $table->string('kode');
            $table->string('nama');
            $table->integer('sks')->nullable();
            $table->integer('semester')->nullable();
            $table->integer('syarat_mata_kuliah')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bloks');
    }
};
