<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tugas', function (Blueprint $table) {
            $table->id();
            $table->string('kode')->unique();
            $table->string('judul');
            $table->longText('instruksi')->nullable();
            $table->longText('dokumen_instruksi')->nullable();
            $table->integer('maks_poin')->default(100);
            $table->dateTime('batas_akhir')->nullable();
            $table->dateTime('mulai_pada')->nullable();
            $table->boolean('status_ujian')->default(false);
            $table->string('tipe'); // rubrik or kuis
            $table->boolean('acak_soal')->default(false);
            $table->boolean('acak_jawaban')->default(false);
            $table->boolean('hidden')->default(false);
            $table->integer('durasi')->nullable();
            $table->integer('persentase_nilai')->nullable()->default(0);

            $table->unsignedBigInteger('kelas_id')->nullable();
            $table->foreign('kelas_id')
                ->references('id')
                ->on('kelas')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->unsignedBigInteger('kategori_id')->nullable();
                $table->foreign('kategori_id')
                    ->references('id')
                    ->on('kategoris')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tugas');
    }
};
