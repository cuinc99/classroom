<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jawaban_tugas', function (Blueprint $table) {
            $table->id();
            $table->integer('poin')->default(0);

            // $table->unsignedBigInteger('user_id')->nullable();
            // $table->foreign('user_id')
            //     ->references('id')
            //     ->on('users')
            //     ->onUpdate('cascade')
            //     ->onDelete('cascade');

            $table->unsignedBigInteger('mahasiswa_id')->nullable();
            $table->foreign('mahasiswa_id')
                ->references('id')
                ->on('mahasiswas')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->unsignedBigInteger('tugas_id')->nullable();
            $table->foreign('tugas_id')
                ->references('id')
                ->on('tugas')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->unsignedBigInteger('pertanyaan_id')->nullable();
            $table->foreign('pertanyaan_id')
                ->references('id')
                ->on('pertanyaans')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->unsignedBigInteger('jawaban_id')->nullable();
            $table->foreign('jawaban_id')
                ->references('id')
                ->on('jawabans')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jawaban_tugas');
    }
};
