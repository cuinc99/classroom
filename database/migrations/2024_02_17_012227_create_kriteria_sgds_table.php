<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('kriteria_sgds', function (Blueprint $table) {
            $table->id();
            $table->string('kriteria')->nullable();

            $table->unsignedBigInteger('sgd_id')->nullable();
            $table->foreign('sgd_id')
                ->references('id')
                ->on('sgds')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('kriteria_sgds');
    }
};
