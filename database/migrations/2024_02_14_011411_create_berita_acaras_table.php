<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('berita_acaras', function (Blueprint $table) {
            $table->id();
            $table->string('kode')->unique();
            $table->string('semester')->nullable();
            $table->string('metode')->nullable();
            $table->string('judul')->nullable();
            $table->datetime('tanggal')->nullable();
            $table->longText('saran')->nullable();
            $table->string('status')->default('Tutup');
            $table->longText('file')->nullable();

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->unsignedBigInteger('kelompok_id')->nullable();
            $table->foreign('kelompok_id')
                ->references('id')
                ->on('kelompoks')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->unsignedBigInteger('kuliah_id')->nullable();
            $table->foreign('kuliah_id')
                ->references('id')
                ->on('kuliahs')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->unsignedBigInteger('kelas_id')->nullable();
            $table->foreign('kelas_id')
                ->references('id')
                ->on('kelas')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('berita_acaras');
    }
};
