<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dosens', function (Blueprint $table) {
            $table->unsignedBigInteger('berita_acara_id')
                ->after('tugas_id')
                ->nullable();
            $table->foreign('berita_acara_id')
                ->references('id')
                ->on('berita_acaras')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->unsignedBigInteger('sgd_id')
                ->after('berita_acara_id')
                ->nullable();
            $table->foreign('sgd_id')
                ->references('id')
                ->on('sgds')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dosens', function (Blueprint $table) {
            //
        });
    }
};
