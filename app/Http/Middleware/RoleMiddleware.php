<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RoleMiddleware
{
    public function handle(Request $request, Closure $next, ...$roles)
    {
        // Cek apakah pengguna terautentikasi dan memiliki salah satu role yang diberikan
        if (!$request->user() || !in_array($request->user()->user_type, $roles)) {
            abort(401);
        }

        return $next($request);
    }
}
