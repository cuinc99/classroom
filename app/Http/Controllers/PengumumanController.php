<?php

namespace App\Http\Controllers;

use App\Models\Pengumuman;
use Illuminate\Http\Request;

class PengumumanController extends Controller
{
    public function simpan(Request $request, $kelasId)
    {
        $rr = $request->all();
        $rr['kelas_id'] = $kelasId;

        if ($request->hasFile('lampiran')) {
            $file = $request->file('lampiran');
            $path = $file->storeAs('lampiran-pengumuman', $file->getClientOriginalName(), 'public');
            $rr['lampiran'] = $path;
        }

        Pengumuman::create($rr);
        return back();
    }

    public function hapus($kode)
    {
        $pengumuman = Pengumuman::whereKode($kode)->firstOrFail();

        $pengumuman->delete();

        return back();
    }
}
