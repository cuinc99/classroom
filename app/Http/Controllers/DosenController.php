<?php

namespace App\Http\Controllers;

use App\Models\Dosen;
use Illuminate\Http\Request;

class DosenController extends Controller
{
    public function hapus($kode)
    {
        $dosen = Dosen::whereKode($kode)->firstOrFail();
        $dosen->delete();

        return back();
    }
}
