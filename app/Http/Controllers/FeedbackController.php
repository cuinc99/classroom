<?php

namespace App\Http\Controllers;

use App\Models\BeritaAcara;
use App\Models\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    public function simpan(Request $request, BeritaAcara $beritaAcara)
    {
        $data = $request->except('_token');

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $name = date('dmYHis') . '_' . $file->getClientOriginalName();
            $path = $file->storeAs('file-materi', $name, 'public');

            $beritaAcara->update(['file' => $path]);
        }

        Feedback::updateOrCreate(
            [
                'berita_acara_id' => $request->berita_acara_id,
                'user_id' => $request->user_id,
            ],
            $request->except(['_token', 'file'])
        );

        return back();
    }
}
