<?php

namespace App\Http\Controllers;

use App\Models\Absensi;
use App\Models\Mahasiswa;
use Illuminate\Http\Request;
use App\Models\MahasiswaKelompok;
use App\Models\MahasiswaTugas;
use App\Models\NilaiSgd;

class MahasiswaController extends Controller
{
    public function hapus($kode)
    {
        $mahasiswa = Mahasiswa::whereKode($kode)->firstOrFail();
        MahasiswaKelompok::whereMahasiswaId($mahasiswa->id)->delete();
        MahasiswaTugas::whereMahasiswaId($mahasiswa->id)->delete();
        Absensi::whereMahasiswaId($mahasiswa->id)->delete();
        NilaiSgd::whereMahasiswaId($mahasiswa->id)->delete();
        $mahasiswa->delete();

        return back();
    }
}
