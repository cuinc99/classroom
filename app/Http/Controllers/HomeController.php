<?php

namespace App\Http\Controllers;

use App\Models\Blok;
use App\Models\Pertanyaan;
use App\Models\Tugas;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $user = auth()->user();

        if ($user->isAdmin()) {
            return view('admin.kelas.index');
        } elseif ($user->isAssessment()) {
            for ($i = 1; $i <= 7; $i++) {
                $bloks[] = Blok::where('semester', $i)->get();
            }
            return view('admin.home', compact('bloks'));
        } else {
            $search = $request->q;
            $kelasItems = $user->kelasSaya()
                    ->when($search != '', function ($q) use ($search) {
                        $q->where('nama', 'like', '%' . $search . '%');
                    })
                    ->count() > 0
                ? $user->kelasSaya()
                    ->toQuery()
                    ->when($search != '', function ($q) use ($search) {
                        $q->where('nama', 'like', '%' . $search . '%');
                    })
                    ->latest()
                    ->paginate(10)
                : collect();

            return view('admin.home', compact('kelasItems', 'search'));
        }
    }
}
