<?php

namespace App\Http\Controllers;

use Excel;
use App\Models\Sgd;
use App\Models\User;
use App\Models\Dosen;
use App\Models\Kelas;
use App\Models\Tugas;
use App\Models\Absensi;
use App\Models\Kelompok;
use App\Models\NilaiSgd;
use App\Models\Mahasiswa;
use App\Models\BeritaAcara;
use Illuminate\Support\Str;
use App\Exports\NilaiExport;
use Illuminate\Http\Request;
use App\Imports\PesertaImport;
use App\Models\MahasiswaTugas;
use Barryvdh\DomPDF\Facade\Pdf;
use App\Models\MahasiswaKelompok;

class KelasController extends Controller
{
    public function index()
    {
        return view('admin.kelas.index');
    }

    public function show($kode)
    {
        $kelas = Kelas::where('kode', $kode)->firstOrFail();

        $tugas = Tugas::whereKelasId($kelas->id)->whereStatusUjian(1);

        if (auth()->user()->isDosen()) {
            $tugas->whereIn('tipe', ['rubrik', 'rubrik2']);
        } else {
            $tugas->whereIn('tipe', ['rubrik', 'rubrik2', 'kuis', 'input'])->where('hidden', false);
        }

        $pengumumanItems = $kelas->pengumumans;

        if (auth()->user()->isDosen()) {
            $kelompokIds = Dosen::whereKelasId($kelas->id)->whereUserId(auth()->id())->pluck('kelompok_id');
            $tugasIds = Dosen::whereKelasId($kelas->id)->whereUserId(auth()->id())->pluck('tugas_id');
            $tugasItems = $tugas->whereIn('id', $tugasIds)->latest()->paginate(10);
            $beritaAcaras = BeritaAcara::with('feedbacks')
                ->where('kelas_id', $kelas->id)
                ->where('user_id', auth()->id())
                ->latest('tanggal')
                ->get();
            $sgds = Sgd::with('kriteria_sgds')
                ->where('kelas_id', $kelas->id)
                ->where('user_id', auth()->id())
                ->latest('tanggal')
                ->get();
        } else {
            $kelompokIds = Mahasiswa::whereKelasId($kelas->id)->whereUserId(auth()->id())->with('kelompoks')->first()?->kelompoks->pluck('id');
            $tugasItems = $tugas->latest()->paginate(10);
            $beritaAcaras = BeritaAcara::with('feedbacks')
                ->where('kelas_id', $kelas->id)
                ->whereIn('kelompok_id', $kelompokIds)
                ->latest('tanggal')
                ->get();
            $sgds = Sgd::with('kriteria_sgds')
                ->where('kelas_id', $kelas->id)
                ->whereIn('kelompok_id', $kelompokIds)
                ->latest('tanggal')
                ->get();
        }

        $kelompokItems = Kelompok::whereIn('id', $kelompokIds)->get();

        return view('admin.kelas.show', compact('kelas', 'pengumumanItems', 'tugasItems', 'kelompokItems', 'kelompokIds', 'beritaAcaras', 'sgds'));
    }

    public function detail($kode)
    {
        abort_if(!auth()->user()->isAdmin(), 401);

        $kelas = Kelas::where('kode', $kode)->firstOrFail();

        $pengumumanItems = $kelas->pengumumans;

        return view('admin.kelas.detail', compact('kelas', 'pengumumanItems'));
    }

    public function penilaianSumatif($kode)
    {
        abort_if(!auth()->user()->isAdmin(), 401);

        $kelas = Kelas::where('kode', $kode)->firstOrFail();

        return view('admin.kelas.penilaianSumatif', compact('kelas'));
    }

    public function penilaianFormatif($kode)
    {
        abort_if(!auth()->user()->isAdmin(), 401);

        $kelas = Kelas::where('kode', $kode)->firstOrFail();

        return view('admin.kelas.penilaianFormatif', compact('kelas'));
    }

    public function peserta($kode)
    {
        abort_if(!auth()->user()->isAdmin(), 401);

        $kelas = Kelas::where('kode', $kode)->firstOrFail();

        return view('admin.kelas.peserta', compact('kelas'));
    }

    public function nilaiSumatif($kode)
    {
        abort_if(!auth()->user()->isAdmin(), 401);

        $kelas = Kelas::where('kode', $kode)->firstOrFail();

        $mahasiswas = Mahasiswa::with('user')
            ->whereKelasId($kelas->id)
            ->get();

        return view('admin.kelas.nilaiSumatif', compact('kelas', 'mahasiswas'));
    }

    public function nilaiFormatif($kode)
    {
        abort_if(!auth()->user()->isAdmin(), 401);

        $kelas = Kelas::where('kode', $kode)->firstOrFail();

        $mahasiswas = Mahasiswa::with('user')
            ->whereKelasId($kelas->id)
            ->get();

        return view('admin.kelas.nilaiFormatif', compact('kelas', 'mahasiswas'));
    }

    public function nilaiExport($kode)
    {
        $kelas = Kelas::where('kode', $kode)->firstOrFail();

        $mahasiswas = Mahasiswa::with('user')
            ->whereKelasId($kelas->id)
            ->get();

        $filename = 'rekap_nilai_' . Str::slug($kelas->nama) . '.xlsx';

        return Excel::download(new NilaiExport($kelas, $mahasiswas), $filename);
    }

    public function kehadiran($kode)
    {
        abort_if(!auth()->user()->isAdmin(), 401);

        $kelas = Kelas::where('kode', $kode)->firstOrFail();

        $mahasiswas = Mahasiswa::with('user')
            ->whereKelasId($kelas->id)
            ->get();

        return view('admin.kelas.kehadiran', compact('kelas', 'mahasiswas'));
    }

    public function kehadiranExport($kode)
    {
        $kelas = Kelas::whereKode($kode)->firstOrFail();

        // return view('admin.exports.kehadiran-pdf', compact('kelas'));

        $pdf = Pdf::setPaper('a4', 'portrait')
            ->loadView('admin.exports.kehadiran-pdf', compact('kelas'));

        return $pdf->download(time() . '_kehadiran_' . $kelas?->nama . '.pdf');
    }

    public function pesertaImport(Request $request, $kode)
    {
        Excel::import(new PesertaImport($kode), $request->file('file'));
        return back();
    }

    public function pesertaHapus($mahasiswaId, $kelompokId)
    {
        $mahasiswaKelompok = MahasiswaKelompok::whereMahasiswaId($mahasiswaId)->whereKelompokId($kelompokId)->firstOrFail();
        $mahasiswaKelompok->delete();
        // Absensi::where('mahasiswa_id', $mahasiswaId)->delete();

        return back();
    }

    public function mahasiswaHapus(Request $request)
    {

        if ($request->ids) {
            $ids = $request->ids;

            foreach ($ids as $id) {
                MahasiswaKelompok::whereMahasiswaId($id)->delete();
                MahasiswaTugas::whereMahasiswaId($id)->delete();
                Mahasiswa::whereId($id)->delete();
                Absensi::whereMahasiswaId($id)->delete();
                NilaiSgd::whereMahasiswaId($id)->delete();
            }
        }
        return;
    }

    public function beritaAcara($kode)
    {
        abort_if(!auth()->user()->isAdmin(), 401);

        $kelas = Kelas::where('kode', $kode)->firstOrFail();

        // $dosenIds = Dosen::query()
        //     ->where('kelas_id', $kelas?->id)
        //     ->where('status', 'penilai')
        //     ->get()
        //     ->pluck('user_id');

        $dosens = User::query()
            ->where('user_type', 'dosen')
        // ->whereIn('id', $dosenIds)
            ->get();

        // $beritaAcaras = BeritaAcara::with('feedbacks')
        //     ->whereKelasId($kelas->id)
        //     ->orderBy('tanggal', 'desc')
        //     ->get();

        return view('admin.kelas.beritaAcara', compact('kelas', 'dosens'));
    }

    public function sgd($kode)
    {
        abort_if(!auth()->user()->isAdmin(), 401);

        $kelas = Kelas::where('kode', $kode)->firstOrFail();

        // $dosenIds = Dosen::query()
        //     ->where('kelas_id', $kelas?->id)
        //     ->where('status', 'penilai')
        //     ->get()
        //     ->pluck('user_id');

        $dosens = User::query()
            ->where('user_type', 'dosen')
        // ->whereIn('id', $dosenIds)
            ->get();

        $sgds = Sgd::with('kriteria_sgds')
            ->whereKelasId($kelas->id)
            ->latest('tanggal')
            ->get();

        return view('admin.kelas.sgd', compact('kelas', 'dosens', 'sgds'));
    }
}
