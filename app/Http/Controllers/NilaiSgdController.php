<?php

namespace App\Http\Controllers;

use App\Models\Absensi;
use App\Models\BeritaAcara;
use App\Models\Mahasiswa;
use App\Models\NilaiSgd;
use Illuminate\Http\Request;

class NilaiSgdController extends Controller
{
    public function simpan(Request $request)
    {
        foreach ($request->kriteria_sgd_ids as $key => $kriteriaSgdId) {
            NilaiSgd::updateOrCreate(
                [
                    'sgd_id' => $request->sgd_id,
                    'kriteria_sgd_id' => $kriteriaSgdId,
                    'mahasiswa_id' => $request->mahasiswa_id,
                ],
                [
                    'nilai' => $request->nilais[$key],
                    'sgd_id' => $request->sgd_id,
                    'kriteria_sgd_id' => $kriteriaSgdId,
                    'mahasiswa_id' => $request->mahasiswa_id,
                    'keterangan' => $key == 0 ? $request->keterangan : null,
                ],
            );
        }

        return back();
    }

}
