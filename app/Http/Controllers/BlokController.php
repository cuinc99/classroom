<?php

namespace App\Http\Controllers;

use App\Models\Blok;
use App\Models\KelompokPertanyaan;
use Illuminate\Http\Request;

class BlokController extends Controller
{
    public function index($kode)
    {
        $blok = Blok::whereKode($kode)->firstOrFail();

        $kelompokPertanyaans = $blok->kelompok_pertanyaans;

        return view('admin.blok.index', compact('blok', 'kelompokPertanyaans'));
    }

    public function hapus($kode)
    {
        $blok = Blok::whereKode($kode)->firstOrFail();
        KelompokPertanyaan::whereBlokId($blok->id)->delete();
        $blok->delete();

        return back();
    }
}
