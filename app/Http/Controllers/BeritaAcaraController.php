<?php

namespace App\Http\Controllers;

use App\Models\Absensi;
use App\Models\BeritaAcara;
use App\Models\Dosen;
use App\Models\Feedback;
use App\Models\Kuliah;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;

class BeritaAcaraController extends Controller
{
    public function tambah($kodeKuliah)
    {
        $kuliah = Kuliah::whereKode($kodeKuliah)->first();

        $metode = \App\Models\BeritaAcara::query()
            ->select('metode')
            ->groupBy('metode')
            ->get()
            ->pluck('metode');

        return view('admin.beritaAcara.tambah', compact('kuliah', 'metode'));
    }

    public function simpan(Request $request)
    {
        $beritaAcara = BeritaAcara::create($request->except('_token'));

        Dosen::create([
            'status' => 'absensi',
            'user_id' => $request->user_id,
            'kelas_id' => $request->kelas_id,
            'kelompok_id' => $request->kelompok_id,
            'berita_acara_id' => $beritaAcara->id,
        ]);

        session()->flash('success', 'Berita acara berhasil ditambahkan');

        return to_route('kelas.beritaAcara', $beritaAcara?->kelas->kode);
    }

    public function detail($kode)
    {
        $beritaAcara = BeritaAcara::whereKode($kode)->firstOrFail();

        return view('admin.beritaAcara.detail', compact('beritaAcara'));
    }

    public function edit($kode)
    {
        $beritaAcara = BeritaAcara::whereKode($kode)->first();

        $metode = \App\Models\BeritaAcara::query()
            ->select('metode')
            ->groupBy('metode')
            ->get()
            ->pluck('metode');

        return view('admin.beritaAcara.edit', compact('beritaAcara', 'metode'));
    }

    public function update(Request $request, $id)
    {
        $beritaAcara = BeritaAcara::findOrFail($id);

        $absensiExists = Absensi::query()
            ->where('berita_acara_id', $beritaAcara?->id)
            ->exists();

        if (!$absensiExists) {
            $dosen = Dosen::where('berita_acara_id', $beritaAcara?->id)->first();
            $dosen->update(['user_id' => $request->user_id]);
        }

        $beritaAcara->update($request->except('_token'));

        session()->flash('success', 'Berita acara berhasil diubah');

        return to_route('kelas.beritaAcara', $beritaAcara?->kelas->kode);
    }

    public function ubahStatus($kode)
    {
        $beritaAcara = BeritaAcara::whereKode($kode)->firstOrFail();

        $status = $beritaAcara->status == 'Tutup' ? 'Buka' : 'Tutup';

        $beritaAcara->update(['status' => $status]);

        return back();
    }

    public function hapus($kode)
    {
        $beritaAcara = BeritaAcara::whereKode($kode)->firstOrFail();

        Absensi::where('berita_acara_id', $beritaAcara->id)->delete();
        Feedback::where('berita_acara_id', $beritaAcara->id)->delete();
        Dosen::where('berita_acara_id', $beritaAcara->id)->delete();

        $beritaAcara->delete();

        session()->flash('success', 'Berita acara berhasil dihapus');

        return back();
    }

    public function export($kode)
    {
        $beritaAcara = BeritaAcara::whereKode($kode)->firstOrFail();

        // return view('admin.beritaAcara.exportPdf', compact('beritaAcara'));

        $pdf = Pdf::setPaper('a4', 'portrait')
            ->loadView('admin.beritaAcara.exportPdf', compact('beritaAcara'));

        return $pdf->download('Berita Acara_' . $beritaAcara?->judul . '_' . $beritaAcara?->tanggal . '.pdf');
    }
}
