<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Kelompok;
use App\Models\Mahasiswa;
use Illuminate\Http\Request;
use App\Models\MahasiswaKelompok;
use App\Services\MahasiswaService;

class KelompokController extends Controller
{
    public function simpan(Request $request)
    {
        Kelompok::firstOrCreate(
            [
                'nama' => $request->nama,
                'kelas_id' => $request->kelas_id,
            ],
            [
                'nama' => $request->nama,
                'kelas_id' => $request->kelas_id,
            ]
        );

        return back();
    }

    public function hapus($kode)
    {
        $kelompok = Kelompok::whereKode($kode)->firstOrFail();

        MahasiswaKelompok::whereKelompokId($kelompok->id)->delete();
        $kelompok->delete();

        return back();
    }

    public function kelompokUpdate(Request $request, $id)
    {
        $kelompok = Kelompok::findOrFail($id);

        $kelompok->update([
            'nama' => $request->nama
        ]);

        return back();
    }

    public function tambahMahasiswaKelompok(Request $request)
    {
        $user = User::findOrFail($request->user_id);
        $kelompok = Kelompok::findOrFail($request->kelompok_id);

        $mahasiswa = Mahasiswa::firstOrCreate(
            [
                'user_id' => $user->id,
                'kelas_id' => $kelompok->kelas->id,
            ],
            [
                'user_id' => $user->id,
                'kelas_id' => $kelompok->kelas->id
            ]
        );

        MahasiswaService::setMahasiswaToTugas($mahasiswa->id, $kelompok->kelas->id);

        $mahasiswaKelompok = MahasiswaKelompok::whereUserId($user->id)->whereMahasiswaId($mahasiswa->id)->whereKelompokId($kelompok->id)->first();
        if ($mahasiswaKelompok) {
            return back();
        }

        MahasiswaKelompok::create([
            'user_id' => $user->id,
            'mahasiswa_id' => $mahasiswa->id,
            'kelompok_id' => $kelompok->id,
        ]);

        return back();
    }
}
