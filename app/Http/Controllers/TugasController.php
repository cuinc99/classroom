<?php

namespace App\Http\Controllers;

use App\Exports\MahasiswaExport;
use App\Imports\InputNilaiImport;
use App\Models\Dosen;
use App\Models\JawabanTugas;
use App\Models\Kategori;
use App\Models\Kelas;
use App\Models\Kelompok;
use App\Models\Mahasiswa;
use App\Models\MahasiswaTugas;
use App\Models\Pertanyaan;
use App\Models\Tugas;
use App\Models\User;
use App\Services\MahasiswaService;
use App\Services\NilaiService;
use Carbon\Carbon;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TugasController extends Controller
{
    public function tambah($kodeKelas, $kodeKategori)
    {
        $kelas = Kelas::where('kode', $kodeKelas)->firstOrFail();
        $kategori = Kategori::where('kode', $kodeKategori)->firstOrFail();
        $users = User::whereUserType('dosen')->orderBy('name', 'asc')->get();
        $kelompoks = Kelompok::whereKelasId($kelas->id)->get();

        return view('admin.kelas.penilaianSumatif.tambah', compact('kelas', 'kategori', 'users', 'kelompoks'));
    }

    public function simpan(Request $request, $kodeKelas, $kodeKategori)
    {
        $kelas = Kelas::where('kode', $kodeKelas)->firstOrFail();
        $kategori = Kategori::where('kode', $kodeKategori)->firstOrFail();

        $tugas = $request->except(['user_ids', 'kelompok_ids']);
        $tugas['kelas_id'] = $kelas->id;
        $tugas['kategori_id'] = $kategori->id;
        $tugas['user_id'] = auth()->id();
        $tugas['status_ujian'] = $request->tipe == 'input' ? 1 : 0;
        $tugas['mulai_pada'] = $request->tipe == 'input' ? now() : null;

        if ($request->hasFile('dokumen_instruksi')) {
            $file = $request->file('dokumen_instruksi');
            $path = $file->storeAs('dokumen-instruksi', $file->getClientOriginalName(), 'public');
            $tugas['dokumen_instruksi'] = $path;
        }

        $tugasSaved = Tugas::create($tugas);

        if ($request->tipe == 'rubrik' || $request->tipe == 'rubrik2') {
            if ($request->user_ids && $request->kelompok_ids) {
                foreach ($request->user_ids as $key => $userId) {
                    Dosen::create([
                        'status' => 'penilai',
                        'user_id' => $userId,
                        'kelas_id' => $kelas->id,
                        'kelompok_id' => $request->kelompok_ids[$key],
                        'tugas_id' => $tugasSaved->id,
                    ]);
                }
            }
        }

        MahasiswaService::setTugasToMahasiswa($tugasSaved->id, $kelas->id);

        session()->flash('success', 'Tugas berhasil ditambahkan.');

        return redirect()->route('kelas.penilaianSumatif', $kelas->kode);
    }

    public function edit($kodeKelas, $kodeTugas)
    {
        $kelas = Kelas::whereKode($kodeKelas)->firstOrFail();
        $tugas = Tugas::whereKode($kodeTugas)->firstOrFail();
        $users = User::whereUserType('dosen')->orderBy('name', 'asc')->get();
        $kelompoks = Kelompok::whereKelasId($kelas->id)->get();

        return view('admin.kelas.penilaianSumatif.edit', compact('kelas', 'tugas', 'users', 'kelompoks'));
    }

    public function update(Request $request, $kodeTugas)
    {
        $tugas = Tugas::whereKode($kodeTugas)->firstOrFail();
        $rr = $request->except(['user_ids', 'kelompok_ids']);

        if ($request->hasFile('dokumen_instruksi')) {
            $file = $request->file('dokumen_instruksi');
            $path = $file->storeAs('dokumen-instruksi', $file->getClientOriginalName(), 'public');
            $rr['dokumen_instruksi'] = $path;
        } else {
            $rr['dokumen_instruksi'] = $tugas->dokumen_instruksi;
        }

        $tugas->update($rr);

        if ($tugas->tipe == 'rubrik' || $tugas->tipe == 'rubrik2') {
            if ($request->user_ids && $request->kelompok_ids) {
                foreach ($request->user_ids as $key => $userId) {
                    Dosen::updateOrCreate(
                        [
                            'user_id' => $userId,
                            'kelas_id' => $tugas->kelas->id,
                            'kelompok_id' => $request->kelompok_ids[$key],
                            'tugas_id' => $tugas->id,
                        ],
                        [
                            'status' => 'penilai',
                            'user_id' => $userId,
                            'kelas_id' => $tugas->kelas->id,
                            'kelompok_id' => $request->kelompok_ids[$key],
                            'tugas_id' => $tugas->id,
                        ]
                    );
                }
            }
        }

        MahasiswaService::setTugasToMahasiswa($tugas->id, $tugas->kelas->id);

        session()->flash('success', 'Tugas berhasil diubah.');

        return redirect()->route('kelas.penilaianSumatif', $tugas->kelas->kode);
    }

    public function proses($kodeTugas)
    {
        if (auth()->user()->isMahasiswa()) {
            $tugas = Tugas::whereKode($kodeTugas)->firstOrFail();

            $mahasiswa = Mahasiswa::whereKelasId($tugas->kelas->id)->whereUserId(auth()->id())->firstOrFail();

            $mahasiswaTugas = MahasiswaTugas::whereTugasId($tugas->id)->whereMahasiswaId($mahasiswa->id)->firstOrFail();

            if ($tugas->tipe == 'kuis') {
                $pertanyaans = $tugas->acak_soal ? $tugas->pertanyaans->shuffle() : $tugas->pertanyaans()->oldest()->get();

                if ($mahasiswaTugas->tanggal_selesai_test == null) {
                    $mahasiswaTugas->update([
                        'tanggal_mulai_test' => Carbon::now(),
                        'tanggal_selesai_test' => Carbon::now()->addMinutes($tugas->durasi),
                    ]);

                    foreach ($pertanyaans as $pertanyaan) {
                        JawabanTugas::create([
                            'poin' => 0,
                            'mahasiswa_id' => $mahasiswa->id,
                            'tugas_id' => $tugas->id,
                            'pertanyaan_id' => $pertanyaan->id,
                            'jawaban_id' => null,
                        ]);
                    }
                }

                if ($mahasiswaTugas->tanggal_kumpul) {

                    $pertanyaanIds = $tugas->pertanyaans->pluck('id');

                    $total_poin = NilaiService::getTotalPoinKuis($mahasiswa->id, $tugas->id, $pertanyaanIds);

                    MahasiswaTugas::whereMahasiswaId($mahasiswa->id)->whereTugasId($tugas->id)->update([
                        'total_poin' => $total_poin,
                    ]);
                }
            } else {
                $pertanyaans = $tugas->pertanyaanTugas()->get();
            }

            return view('admin.kelas.penilaianSumatif.proses', compact('tugas', 'pertanyaans', 'mahasiswa', 'mahasiswaTugas'));
        } else {
            $tugas = Tugas::whereKode($kodeTugas)->firstOrFail();

            if (auth()->user()->isAdmin()) {
                $mahasiswas = Mahasiswa::whereKelasId($tugas->kelas_id)->get();
            } else {
                $kelompokIds = Dosen::whereUserId(auth()->id())->whereKelasId($tugas->kelas_id)->whereTugasId($tugas->id)->pluck('kelompok_id');
                $mahasiswas = Kelompok::with('mahasiswas')->whereIn('id', $kelompokIds)->get()->pluck('mahasiswas')->collapse();
            }

            $pertanyaans = $tugas->pertanyaanTugas()->get();

            return view('admin.kelas.penilaianSumatif.proses-dosen', compact('tugas', 'mahasiswas', 'pertanyaans'));
        }
    }

    public function kumpulkanKuis(Request $request, $tugasId, $mahasiswaId)
    {
        $pertanyaanIds = Tugas::where('id', $tugasId)->first()->pertanyaans->pluck('id');

        $total_poin = NilaiService::getTotalPoinKuis($mahasiswaId, $tugasId, $pertanyaanIds);

        MahasiswaTugas::whereMahasiswaId($mahasiswaId)->whereTugasId($tugasId)->update([
            'total_poin' => $total_poin,
            'tanggal_kumpul' => Carbon::now(),
        ]);

        return back();
    }

    public function kumpulkanRubrik(Request $request, $tugasId, $mahasiswaId)
    {
        if ($request->tugas != '') {
            MahasiswaTugas::whereMahasiswaId($mahasiswaId)->whereTugasId($tugasId)->update([
                'file_tugas' => $request->tugas,
            ]);
        }
        return back();
    }

    public function uploadTugas(Request $request)
    {
        if ($request->hasfile('tugas')) {
            $tugas = $request->file('tugas');
            $tugas_name = $tugas->getClientOriginalName();
            $path = $tugas->storeAs('tugas/' . auth()->id(), $tugas_name, 'public');

            return $path;
        }

        return '';
    }

    public function penilaian($kodeTugas, $mahasiswaKode)
    {
        $tugas = Tugas::whereKode($kodeTugas)->firstOrFail();

        $pertanyaans = $tugas->pertanyaanTugas()->get();

        $mahasiswa = Mahasiswa::whereKode($mahasiswaKode)->firstOrFail();

        $mahasiswaTugas = MahasiswaTugas::whereTugasId($tugas->id)->whereMahasiswaId($mahasiswa->id)->firstOrFail();

        return view('admin.kelas.penilaianSumatif.penilaian', compact('tugas', 'pertanyaans', 'mahasiswa', 'mahasiswaTugas'));
    }

    public function penilaianSimpan(Request $request, $tugasId, $mahasiswaId)
    {
        $tugas = Tugas::whereId($tugasId)->firstOrFail();

        DB::transaction(function () use ($tugas, $tugasId, $mahasiswaId) {
            foreach (request()->pertanyaans as $pertanyaanId) {
                $poin = request()->poins[$pertanyaanId];
                JawabanTugas::updateOrCreate(
                    [
                        'mahasiswa_id' => $mahasiswaId,
                        'tugas_id' => $tugasId,
                        'pertanyaan_id' => $pertanyaanId,
                    ],
                    [
                        'poin' => $poin,
                        'mahasiswa_id' => $mahasiswaId,
                        'tugas_id' => $tugasId,
                        'pertanyaan_id' => $pertanyaanId,
                    ]
                );
            }

            $total_poin = JawabanTugas::whereMahasiswaId($mahasiswaId)->whereTugasId($tugasId)->get()->unique('pertanyaan_id')->sum('poin');

            if ($tugas->tipe == 'rubrik2') {
                $maksPoin = $tugas->pertanyaanTugas->count() * 5;

                $total_poin = $total_poin / $maksPoin;
            }

            MahasiswaTugas::whereMahasiswaId($mahasiswaId)->whereTugasId($tugasId)->update([
                'total_poin' => $total_poin,
                'tanggal_kumpul' => $tugas->tipe == 'rubrik2' ? now() : null,
            ]);
        });

        return back();
    }

    public function penilaianKumpul($tugasId, $mahasiswaId)
    {
        $total_poin = JawabanTugas::whereMahasiswaId($mahasiswaId)->whereTugasId($tugasId)->sum('poin');

        MahasiswaTugas::whereMahasiswaId($mahasiswaId)->whereTugasId($tugasId)->update([
            'total_poin' => $total_poin,
            'tanggal_kumpul' => now(),
        ]);

        return back();
    }

    public function inputNilaiSimpan(Request $request, $tugasId)
    {
        foreach ($request->ids as $id) {
            MahasiswaTugas::whereMahasiswaId($id)->whereTugasId($tugasId)->update([
                'total_poin' => $request->nilais[$id] ?? 0,
                'tanggal_kumpul' => now(),
            ]);
        }

        return back();
    }

    public function inputNilaiImport(Request $request, $tugasId)
    {
        Excel::import(new InputNilaiImport($tugasId), $request->file('file'));
        return back();
    }

    public function chat($kodeTugas, $kodeMahasiswa)
    {
        $tugas = Tugas::whereKode($kodeTugas)->firstOrFail();

        $mahasiswa = Mahasiswa::whereKode($kodeMahasiswa)->firstOrFail();

        $mahasiswaTugas = MahasiswaTugas::whereTugasId($tugas->id)->whereMahasiswaId($mahasiswa->id)->firstOrFail();

        return view('admin.kelas.penilaianSumatif.chat', compact('mahasiswaTugas', 'tugas', 'mahasiswa'));
    }

    public function buka($kode)
    {
        $tugas = Tugas::whereKode($kode)->firstOrFail();
        $tugas->update([
            'status_ujian' => 1,
            'mulai_pada' => now(),
        ]);

        return back();
    }

    public function tutup($kode)
    {
        $tugas = Tugas::whereKode($kode)->firstOrFail();
        $tugas->update([
            'status_ujian' => 0,
        ]);

        return back();
    }

    public function hapusDokumen($kode)
    {
        $tugas = Tugas::whereKode($kode)->firstOrFail();
        $tugas->update([
            'dokumen_instruksi' => null,
        ]);

        return back();
    }

    public function detail($kode)
    {
        $tugas = Tugas::whereKode($kode)->firstOrFail();

        $mahasiswas = Mahasiswa::whereKelasId($tugas->kelas_id)->get();

        return view('admin.kelas.penilaianSumatif.detail', compact('tugas', 'mahasiswas'));
    }

    public function hasil($kodeTugas, $kodeMahasiswa)
    {
        $tugas = Tugas::whereKode($kodeTugas)->firstOrFail();
        $mahasiswa = Mahasiswa::whereKode($kodeMahasiswa)->firstOrFail();
        $mahasiswaTugas = MahasiswaTugas::whereTugasId($tugas->id)->whereMahasiswaId($mahasiswa->id)->firstOrFail();

        if ($tugas->tipe == 'kuis') {
            $jawabanTugas = JawabanTugas::whereMahasiswaId($mahasiswa->id)->whereTugasId($tugas->id)->get();
            $pertanyaans = Pertanyaan::whereIn('id', $jawabanTugas->pluck('pertanyaan_id'))->get();

            // $total_poin = NilaiService::getTotalPoinKuis($mahasiswa->id, $tugas->id, $pertanyaanIds);

            MahasiswaTugas::whereMahasiswaId($mahasiswa->id)->whereTugasId($tugas->id)->update([
                'total_poin' => $jawabanTugas->sum('poin'),
            ]);
        } else {
            $pertanyaans = $tugas->pertanyaanTugas()->get();
        }

        return view('admin.kelas.penilaianSumatif.hasil', compact('tugas', 'mahasiswa', 'pertanyaans', 'mahasiswaTugas'));
    }

    public function ujianUlang($kodeTugas, $kodeMahasiswa)
    {
        $tugas = Tugas::whereKode($kodeTugas)->firstOrFail();
        $mahasiswa = Mahasiswa::whereKode($kodeMahasiswa)->firstOrFail();
        $mahasiswaTugas = MahasiswaTugas::whereTugasId($tugas->id)->whereMahasiswaId($mahasiswa->id)->firstOrFail();

        $pertanyaans = $tugas->pertanyaans()->get();

        $mahasiswaTugas->update([
            'total_poin' => 0,
            'tanggal_kumpul' => null,
            'tanggal_mulai_test' => null,
            'tanggal_selesai_test' => null,
        ]);

        foreach ($pertanyaans as $pertanyaan) {
            $jawabanTugas = JawabanTugas::query()
                ->where('mahasiswa_id', $mahasiswa->id)
                ->where('tugas_id', $tugas->id)
                ->where('pertanyaan_id', $pertanyaan->id)
                ->first();

            if ($jawabanTugas) {
                # code...
                $jawabanTugas->delete();
            }
        }

        return back();
    }

    public function exportMahasiswa($tugasKode)
    {
        $tugas = Tugas::where('kode', $tugasKode)->firstOrFail();
        $mahasiswas = Mahasiswa::whereKelasId($tugas->kelas_id)->get();
        return Excel::download(new MahasiswaExport($mahasiswas), 'form-penilaian-mahasiswa-tugas-' . $tugas->judul . '.xlsx');
    }
}
