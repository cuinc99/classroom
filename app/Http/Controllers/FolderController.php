<?php

namespace App\Http\Controllers;

use App\Models\Folder;
use Illuminate\Http\Request;

class FolderController extends Controller
{
    public function show($kode)
    {
        $folder = Folder::where('kode', $kode)->firstOrFail();
        return view('admin.folder.show', compact('folder'));
    }
}
