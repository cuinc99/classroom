<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\MahasiswaTugas;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        $comment = new Comment;

        $comment->comment = $request->comment;

        $comment->user()->associate($request->user());

        $mahasiswaTugas = MahasiswaTugas::find($request->mahasiswa_tugas_id);

        $mahasiswaTugas->comments()->save($comment);

        return back();
    }

    public function replyStore(Request $request)
    {
        $reply = new Comment();

        $reply->comment = $request->get('comment');

        $reply->user()->associate($request->user());

        $reply->parent_id = $request->get('comment_id');

        $mahasiswaTugas = MahasiswaTugas::find($request->get('mahasiswa_tugas_id'));

        $mahasiswaTugas->comments()->save($reply);

        return back();
    }
}
