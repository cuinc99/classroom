<?php

namespace App\Http\Controllers;

use App\Models\Sgd;
use App\Models\User;
use App\Models\Dosen;
use App\Models\Kelas;
use App\Models\Kuliah;
use App\Models\NilaiSgd;
use App\Models\KriteriaSgd;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;

class SgdController extends Controller
{
    public function tambah($kodeKuliah)
    {
        $kuliah = Kuliah::whereKode($kodeKuliah)->first();

        return view('admin.sgd.tambah', compact('kuliah'));
    }

    public function simpan(Request $request)
    {
        if (!$request->kriterias && !$request->copy_sgd_id) {
            session()->flash('error', 'Kriteria wajib ditambahkan');
            return back();
        }

        $sgd = Sgd::create($request->except(['_token', 'kriterias', 'copy_sgd_id']));

        if ($request->copy_sgd_id) {
            foreach (KriteriaSgd::where('sgd_id', $request->copy_sgd_id)->pluck('kriteria') as $kriteria) {
                KriteriaSgd::create([
                    'kriteria' => $kriteria,
                    'sgd_id' => $sgd->id,
                ]);
            }
        }

        if ($request->kriterias) {
            foreach ($request->kriterias as $kriteria) {
                KriteriaSgd::create([
                    'kriteria' => $kriteria,
                    'sgd_id' => $sgd->id,
                ]);
            }
        }

        Dosen::create([
            'status' => 'sgd',
            'user_id' => $request->user_id,
            'kelas_id' => $request->kelas_id,
            'kelompok_id' => $request->kelompok_id,
            'sgd_id' => $sgd->id,
        ]);

        session()->flash('success', 'Penilaian berhasil ditambahkan');

        return to_route('kelas.penilaianFormatif', $sgd?->kelas->kode);
    }

    public function simpanSkenario(Request $request, Sgd $sgd)
    {
        $sgd->update(['skenario' => $request->skenario]);

        return back();
    }

    public function detail($kode)
    {
        $sgd = Sgd::whereKode($kode)->firstOrFail();

        return view('admin.sgd.detail', compact('sgd'));
    }

    public function edit($kode)
    {
        $sgd = Sgd::whereKode($kode)->firstOrFail();

        $kelas = Kelas::where('id', $sgd?->kelas_id)->first();

        // $dosenIds = Dosen::query()
        //     ->where('kelas_id', $kelas?->id)
        //     ->where('status', 'penilai')
        //     ->get()
        //     ->pluck('user_id');

        $dosens = User::query()
            ->where('user_type', 'dosen')
        // ->whereIn('id', $dosenIds)
            ->get();

        return view('admin.sgd.edit', compact('sgd', 'kelas', 'dosens'));
    }

    public function update(Request $request, $id)
    {
        $sgd = Sgd::findOrFail($id);

        $nilaiSgdExists = NilaiSgd::query()
            ->where('sgd_id', $sgd?->id)
            ->exists();

        if (!$nilaiSgdExists && !$request->kriterias) {
            session()->flash('error', 'Kriteria wajib ditambahkan');
            return back();
        } else {
            if ($request->kriterias) {
                KriteriaSgd::where('sgd_id', $sgd?->id)->delete();

                foreach ($request->kriterias as $kriteria) {
                    KriteriaSgd::create([
                        'kriteria' => $kriteria,
                        'sgd_id' => $sgd->id,
                    ]);
                }
            }
        }


        if (!$nilaiSgdExists) {
            $dosen = Dosen::where('sgd_id', $sgd?->id)->first();
            $dosen->update(['user_id' => $request->user_id]);
        }

        $sgd->update($request->except(['_token', 'kriterias']));

        session()->flash('success', 'Penilaian berhasil diubah');

        return to_route('kelas.penilaianFormatif', $sgd?->kelas->kode);
    }

    public function ubahStatus($kode)
    {
        $sgd = Sgd::whereKode($kode)->firstOrFail();

        $status = $sgd->status == 'Tutup' ? 'Buka' : 'Tutup';

        $sgd->update(['status' => $status]);

        return back();
    }

    public function hapus($kode)
    {
        $sgd = Sgd::whereKode($kode)->firstOrFail();

        KriteriaSgd::where('sgd_id', $sgd->id)->delete();
        NilaiSgd::where('sgd_id', $sgd->id)->delete();
        Dosen::where('sgd_id', $sgd->id)->delete();

        $sgd->delete();

        session()->flash('success', 'Penilaian berhasil dihapus');

        return back();
    }

    public function export($kode)
    {
        $sgd = Sgd::whereKode($kode)->firstOrFail();

        // return view('admin.sgd.exportPdf', compact('sgd'));

        $pdf = Pdf::setPaper('legal', 'landscape') // f4 = array(0,0,609.4488,935.433)
            ->loadView('admin.sgd.exportPdf', compact('sgd'));

        return $pdf->download('Penilaian_' . $sgd?->judul . '_' . $sgd?->tanggal . '.pdf');
    }
}
