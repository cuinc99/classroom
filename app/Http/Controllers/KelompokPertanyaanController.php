<?php

namespace App\Http\Controllers;

use App\Models\Pertanyaan;
use Illuminate\Http\Request;
use App\Models\KelompokPertanyaan;

class KelompokPertanyaanController extends Controller
{
    public function index($kode)
    {
        $kelompokPertanyaan = KelompokPertanyaan::whereKode($kode)->firstOrFail();
        $pertanyaans = Pertanyaan::query()
            ->whereParentId('parent_id', 0)
            ->whereTipe('kuis')
            ->whereBlokId($kelompokPertanyaan->blok_id)
            ->whereKelompokPertanyaanId($kelompokPertanyaan->id)
            ->latest()
            ->get();

        return view('admin.blok.kelompok-pertanyaan', compact('kelompokPertanyaan', 'pertanyaans'));
    }

    public function simpan(Request $request)
    {
        KelompokPertanyaan::create($request->all());
        return back();
    }

    public function update(Request $request, $id)
    {
        $kelompokPertanyaan = KelompokPertanyaan::findOrFail($id);

        $kelompokPertanyaan->update([
            'nama' => $request->nama
        ]);

        return back();
    }

    public function hapus($kode)
    {
        $kelompokPertanyaan = KelompokPertanyaan::whereKode($kode)->firstOrFail();
        $kelompokPertanyaan->delete();

        return back();
    }
}
