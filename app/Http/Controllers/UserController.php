<?php

namespace App\Http\Controllers;

use Excel;
use App\Models\User;
use App\Imports\UsersImport;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request, $userType)
    {
        $cari = $request->q;
        $users = User::query()
            ->whereUserType($userType)
            ->when($cari != NULL, function ($q) use ($cari) {
                $q->where('name', 'like', '%' . $cari . '%');
                $q->orWhere('username', 'like', '%' . $cari . '%');
            })
            ->latest()
            ->paginate(10);

        return view('admin.users.index', compact('users', 'userType'));
    }

    public function simpan(Request $request)
    {
        $request->validate([
            'username' => 'required|unique:users',
            'name' => 'required',
            'password' => 'required',
        ]);
        $rr = $request->all();
        $rr['password'] = bcrypt($request->password);
        User::create($rr);
        return back();
    }

    public function update(Request $request, User $user)
    {
        $request->validate([
            'username' => 'required|unique:users,username,'. $user->id,
            'name' => 'required',
        ]);
        $rr = $request->all();
        if ($request->password != '') {
            $rr['password'] = bcrypt($request->password);
        } else  {
            $rr['password'] = $user->password;
        }

        $user->update($rr);
        return back();
    }

    public function editProfil()
    {
        $user = auth()->user();
        return view('admin.users.edit', compact('user'));
    }

    public function simpanProfil(Request $request)
    {
        $user = auth()->user();

        if ($user->isMahasiswa()) {
            $request->validate([
                'name' => 'required',
            ]);
        } else {
            $request->validate([
                'username' => 'required|unique:users,username,'. $user->id,
                'name' => 'required',
            ]);
        }


        $rr = $request->all();

        if ($request->password != NULL) {
            $rr['password'] = bcrypt($request->password);
        } else {
            $rr['password'] = $user->password;
        }

        $user->update($rr);
        return back();
    }

    public function import(Request $request)
    {
        Excel::import(new UsersImport, $request->file('file'));
        return back();
    }

    public function hapus($kode)
    {
        $user = User::whereKode($kode)->firstOrFail();
        $user->delete();
        return back();
    }

    public function loginAs($kode)
    {
        $user = User::whereKode($kode)->firstOrFail();
        auth()->loginUsingId($user->id);
        return redirect()->route('home');
    }
}
