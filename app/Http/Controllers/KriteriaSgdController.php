<?php

namespace App\Http\Controllers;

use App\Models\Absensi;
use App\Models\BeritaAcara;
use App\Models\KriteriaSgd;
use App\Models\Mahasiswa;
use App\Models\NilaiSgd;
use Illuminate\Http\Request;

class KriteriaSgdController extends Controller
{
    public function hapus($id)
    {
        $kriteriaSgd = KriteriaSgd::whereId($id)->firstOrFail();

        NilaiSgd::where('kriteria_sgd_id', $kriteriaSgd->id)->delete();

        $kriteriaSgd->delete();

        session()->flash('success', 'Kriteria berhasil dihapus');

        return back();
    }
}
