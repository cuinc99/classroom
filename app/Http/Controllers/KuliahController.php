<?php

namespace App\Http\Controllers;

use App\Models\Kuliah;
use Illuminate\Http\Request;

class KuliahController extends Controller
{
    public function simpan(Request $request)
    {
        Kuliah::firstOrCreate(
            [
                'nama' => $request->nama,
                'tipe' => $request->tipe,
                'kelas_id' => $request->kelas_id,
            ],
            [
                'nama' => $request->nama,
                'tipe' => $request->tipe,
                'kelas_id' => $request->kelas_id,
            ]
        );

        return back();
    }

    public function hapus($kode)
    {
        $kuliah = Kuliah::whereKode($kode)->firstOrFail();
        $kuliah->delete();

        return back();
    }

    public function update(Request $request, $id)
    {
        $kuliah = Kuliah::findOrFail($id);

        $kuliah->update([
            'nama' => $request->nama
        ]);

        return back();
    }
}
