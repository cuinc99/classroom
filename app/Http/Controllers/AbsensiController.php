<?php

namespace App\Http\Controllers;

use App\Models\Absensi;
use App\Models\BeritaAcara;
use App\Models\Mahasiswa;
use Illuminate\Http\Request;

class AbsensiController extends Controller
{
    public function simpan(Request $request)
    {
        Absensi::updateOrCreate(
            [
                'berita_acara_id' => $request->berita_acara_id,
                'mahasiswa_id' => $request->mahasiswa_id,
            ],
            $request->except('_token')
        );

        return back();
    }

    public function scan($beritaAcaraKode, $mahasiswaId)
    {
        $beritaAcara = BeritaAcara::where('kode', $beritaAcaraKode)->first();

        $absensi = Absensi::query()
            ->where('berita_acara_id', $beritaAcara?->id)
            ->where('mahasiswa_id', $mahasiswaId)
            ->first();

        if ($absensi) {
            return to_route('kelas.show', $beritaAcara?->kelas?->kode);
        }

        return view('admin.beritaAcara.scan', compact('beritaAcara', 'mahasiswaId'));
    }

    public function setHadir($beritaAcaraKode, $mahasiswaId)
    {
        $beritaAcara = BeritaAcara::where('kode', $beritaAcaraKode)->first();

        if ($beritaAcara?->isBuka()) {
            Absensi::updateOrCreate(
                [
                    'berita_acara_id' => $beritaAcara?->id,
                    'mahasiswa_id' => $mahasiswaId,
                ],
                [
                    'berita_acara_id' => $beritaAcara?->id,
                    'mahasiswa_id' => $mahasiswaId,
                    'status' => 'Hadir',
                ]
            );
        }

        return;
    }
}
