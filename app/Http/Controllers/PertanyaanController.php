<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use App\Models\Tugas;
use App\Models\Jawaban;
use App\Models\Kategori;
use App\Models\Pertanyaan;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class PertanyaanController extends Controller
{
    public function daftar($kodeKelas, $kodeKategori, $kodeTugas)
    {
        $kelas = Kelas::where('kode', $kodeKelas)->firstOrFail();
        $kategori = Kategori::where('kode', $kodeKategori)->firstOrFail();
        $tugas = Tugas::where('kode', $kodeTugas)->firstOrFail();

        if ($tugas->tipe == 'rubrik') {
            $tugasItems = Tugas::where('tipe', 'rubrik')->whereNot('id', $tugas->id)->whereNotNull('kelas_id')->latest()->get();
            $pertanyaans = Pertanyaan::where('tugas_id', $tugas->id)->get();
            return view('admin.kelas.penilaianSumatif.pertanyaan.daftar', compact('pertanyaans', 'kelas', 'kategori', 'tugas', 'tugasItems'));
        }

        if ($tugas->tipe == 'rubrik2') {
            $tugasItems = Tugas::where('tipe', 'rubrik2')->whereNot('id', $tugas->id)->whereNotNull('kelas_id')->latest()->get();
            $pertanyaans = Pertanyaan::where('tugas_id', $tugas->id)->get();
            return view('admin.kelas.penilaianSumatif.pertanyaan.daftar', compact('pertanyaans', 'kelas', 'kategori', 'tugas', 'tugasItems'));
        }

        return view('admin.kelas.penilaianSumatif.pertanyaan.daftar', compact('kelas', 'kategori', 'tugas'));
    }

    public function simpanRubrik(Request $request, $kodeTugas)
    {
        $tugasBaru = Tugas::where('kode', $kodeTugas)->firstOrFail();
        if ($request->tugas_id) {
            $tugas = Tugas::findOrFail($request->tugas_id);

            $pertanyaans = $tugas->pertanyaanTugas;

            if ($pertanyaans->count() > 0) {
                foreach ($pertanyaans as  $pertanyaan) {
                    $pertanyaanBaru = $pertanyaan->replicate();
                    $pertanyaanBaru->kode = Str::uuid();
                    $pertanyaanBaru->tugas_id = $tugasBaru->id;
                    $pertanyaanBaru->save();
                }
            } else {
                return back();
            }
        } else {
            $pertanyaan = $request->all();
            $pertanyaan['tugas_id'] = $tugasBaru->id;
            $pertanyaan['tipe'] = $tugasBaru->tipe;
            $pertanyaan['maks_poin'] = $tugasBaru->tipe == 'rubrik' ? 10 : 5;
            Pertanyaan::create($pertanyaan);
        }

        return back();
    }

    public function updateRubrik(Request $request, $id)
    {
        $pertanyaan = Pertanyaan::findOrFail($id);

        $pertanyaan->update($request->all());

        return back();
    }

    public function simpan(Request $request)
    {
        $pertanyaan = Pertanyaan::create([
            'blok_id' => $request->blok_id,
            'kelompok_pertanyaan_id' => $request->kelompok_pertanyaan_id,
            'pertanyaan' => $request->pertanyaan,
            'tipe' => 'kuis',
            'parent_id' => 0,
        ]);

        foreach ($request->poin as $key => $poin) {
            Jawaban::create([
                'jawaban' => $request->jawaban[$key],
                'poin' => $poin,
                'pertanyaan_id' => $pertanyaan->id,
            ]);
        }

        return back();
    }

    public function hapus($kode)
    {
        $pertanyaan = Pertanyaan::whereKode($kode)->firstOrFail();

        foreach ($pertanyaan->jawabans as $jawaban) {
            $jawaban->delete();
        }

        $pertanyaan->delete();


        return back();
    }

    public function edit($kelompokPertanyaabKode, $kode)
    {
        $pertanyaan = Pertanyaan::whereKode($kode)->firstOrFail();

        return view('admin.blok.edit-pertanyaan', compact('pertanyaan', 'kelompokPertanyaabKode'));
    }

    public function update(Request $request, $kode)
    {
        $pertanyaan = Pertanyaan::whereKode($kode)->firstOrFail();

        $pertanyaan->update([
            'pertanyaan' => $request->pertanyaan,
        ]);

        foreach ($request->poin as $key => $poin) {
            $jawabanId = $request->jawaban_id[$key] ?? NULL;
            if ($jawabanId != NULL) {
                $jawaban = Jawaban::findOrFail($jawabanId);
                $jawaban->update([
                    'jawaban' => $request->jawaban[$key],
                    'poin' => $poin,
                ]);
            } else {
                Jawaban::create([
                    'jawaban' => $request->jawaban[$key],
                    'poin' => $poin,
                    'pertanyaan_id' => $pertanyaan->id,
                ]);
            }
        }

        return back();
    }

    public function hapusJawaban($kode, $jawabanId)
    {
        $jawaban = Jawaban::findOrFail($jawabanId);

        $jawaban->delete();

        return back();
    }
}
