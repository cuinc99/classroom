<?php

namespace App\Http\Livewire;

use App\Models\Tugas;
use App\Models\Jawaban;
use Livewire\Component;
use App\Models\Mahasiswa;
use App\Models\Pertanyaan;
use App\Models\JawabanTugas;

class Ujian extends Component
{
    public $tugas;
    public $mahasiswa;
    public $pertanyaan;
    public $jawaban;
    public $index;

    public function mount($tugasId, $mahasiswaId, $pertanyaanId, $jawabanId, $index)
    {
        $this->tugas = Tugas::findOrFail($tugasId);
        $this->mahasiswa = Mahasiswa::findOrFail($mahasiswaId);
        $this->pertanyaan = Pertanyaan::findOrFail($pertanyaanId);
        $this->jawaban = Jawaban::findOrFail($jawabanId);
        $this->index = $index;
    }

    public function render()
    {
        return view('livewire.ujian');
    }

    public function updateJawaban($pertanyaanId, $jawabanId, $poin)
    {
        $jawabanTugas = JawabanTugas::query()
            ->whereTugasId($this->tugas->id)
            ->whereMahasiswaId($this->mahasiswa->id)
            ->wherePertanyaanId($pertanyaanId)
            ->firstOrFail();

        $jawabanTugas->update([
            'poin' => $poin,
            'jawaban_id' => $jawabanId,
        ]);
    }
}
