<?php

namespace App\Http\Livewire\Kelas;

use App\Models\Blok;
use App\Models\Dosen;
use App\Models\Kelas;
use App\Models\Folder;
use Livewire\Component;
use Illuminate\Http\Request;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $user;
    public $kelas;
    public $kelasId;
    public $folder;
    public $folders;
    public $search;

    public function mount(Request $request, $folderId)
    {
        $this->user = auth()->user();
        $this->folder = Folder::findOrFail($folderId);
        $this->folders = Folder::latest()->get();
        $this->search = $request->q;
    }

    public function render()
    {
        $kelasItems = Kelas::query()
                ->where('folder_id', $this->folder->id)
                ->when($this->search != '', function($q) {
                    $q->where('nama', 'like', '%' . $this->search . '%');
                })
                ->latest()
                ->paginate(10);

        for ($i = 1; $i <= 7; $i++) {
            $bloks[] = Blok::where('semester', $i)->get();
        }

        return view('livewire.kelas.index', compact('kelasItems', 'bloks'));
    }

    protected $rules = [
        'kelas.nama' => 'required',
        'kelas.blok_id' => 'required',
        'kelas.deskripsi' => 'sometimes',
        'kelas.folder_id' => 'sometimes',
        'kelas.active' => 'sometimes',
    ];

    protected $messages = [
        'kelas.nama.required' => 'Nama kelas harus diisi.',
    ];

    public function updated($property)
    {
        $this->validateOnly($property);
    }

    public function create()
    {
        $this->reset(['kelas', 'kelasId']);
    }

    public function edit($kelasId)
    {
        $this->kelasId = $kelasId;
        $this->kelas = Kelas::findOrFail($kelasId);
    }

    public function close()
    {
        $this->reset(['kelas', 'kelasId']);
    }

    public function save()
    {
        $this->validate();

        if (!is_null($this->kelasId)) {
            $this->kelas->save();
            session()->flash('success', 'Kelas berhasil diubah.');
        } else {
            $kelas = $this->kelas;
            $kelas['user_id'] = $this->user->id;
            $kelas['folder_id'] = $this->folder->id;
            $savedKelas = Kelas::create($kelas);
            Dosen::create([
                'status' => 'admin',
                'user_id' => $this->user->id,
                'kelas_id' => $savedKelas->id,
            ]);
            session()->flash('success', 'Kelas berhasil ditambahkan.');
            $this->reset(['kelas', 'kelasId']);
        }
        return redirect()->route('folder.show', $this->folder->kode);
    }

    public function delete($kelasId)
    {
        Kelas::findOrFail($kelasId)->delete();
        $this->reset(['kelas', 'kelasId']);
        return redirect()->route('folder.show', $this->folder->kode);
    }
}
