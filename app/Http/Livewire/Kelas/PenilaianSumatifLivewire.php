<?php

namespace App\Http\Livewire\Kelas;

use App\Models\Kelas;
use Livewire\Component;
use App\Models\Kategori;
use App\Models\MahasiswaTugas;
use App\Models\Tugas as TugasModel;

class PenilaianSumatifLivewire extends Component
{
    public Kelas $kelas;
    public $kategori;

    public function render()
    {
        $kategoriItems = Kategori::where('kelas_id', $this->kelas->id)->get();

        return view('livewire.kelas.penilaian-sumatif-livewire', compact('kategoriItems'));
    }

    protected $rules = [
        'kategori.nama' => 'required',
        'kategori.persentase_nilai' => 'required',
    ];

    protected $messages = [
        'kelas.nama.required' => 'Nama kategori harus diisi.',
        'kelas.persentase_nilai.required' => 'Persentase nilai harus diisi.',
    ];

    public function updated($property)
    {
        $this->validateOnly($property);
    }

    public function saveKategori()
    {
        $this->validate();

        $kategori = $this->kategori;
        $kategori['kelas_id'] = $this->kelas->id;
        Kategori::create($kategori);

        session()->flash('success', 'Kategori berhasil ditambahkan.');

        $this->reset(['kategori']);
    }

    public function deleteKategori($kategoriId)
    {
        Kategori::findOrFail($kategoriId)->delete();
    }

    public function deleteTugas($tugasiId)
    {
        MahasiswaTugas::whereTugasId($tugasiId)->delete();
        TugasModel::findOrFail($tugasiId)->delete();
    }
}
