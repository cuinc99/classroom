<?php

namespace App\Http\Livewire\Kelas;

use App\Models\Kelas;
use Livewire\Component;
use App\Models\Kategori;
use App\Models\Kuliah;
use App\Models\MahasiswaTugas;
use App\Models\Tugas as TugasModel;

class PenilaianFormatifLivewire extends Component
{
    public Kelas $kelas;
    public $kategori;

    public function render()
    {
        $kuliahItems = Kuliah::where('kelas_id', $this->kelas->id)->get();

        return view('livewire.kelas.penilaian-formatif-livewire', compact('kuliahItems'));
    }


    public function deleteKategori($kategoriId)
    {
        Kategori::findOrFail($kategoriId)->delete();
    }

    public function deleteTugas($tugasiId)
    {
        MahasiswaTugas::whereTugasId($tugasiId)->delete();
        TugasModel::findOrFail($tugasiId)->delete();
    }
}
