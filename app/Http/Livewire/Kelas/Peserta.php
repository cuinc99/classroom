<?php

namespace App\Http\Livewire\Kelas;

use App\Models\Kelas;
use App\Models\Kelompok;
use Livewire\Component;

class Peserta extends Component
{
    public Kelas $kelas;
    public $kelompok;

    public function render()
    {
        $kelompokItems = Kelompok::where('kelas_id', $this->kelas->id)->get();

        return view('livewire.kelas.peserta', compact('kelompokItems'));
    }

    protected $rules = [
        'kelompok.nama' => 'required',
    ];

    protected $messages = [
        'kelompok.nama.required' => 'Nama kelompok harus diisi.',
    ];

    public function updated($property)
    {
        $this->validateOnly($property);
    }

    public function saveKelompok()
    {
        $this->validate();

        $kelompok = $this->kelompok;
        $kelompok['kelas_id'] = $this->kelas->id;
        Kelompok::create($kelompok);

        session()->flash('success', 'Kelompok berhasil ditambahkan.');

        $this->emit('kelompokAdded');

        $this->reset(['kelompok']);
    }

    public function deleteKelompok($kelompokId)
    {
        Kelompok::findOrFail($kelompokId)->delete();
    }
}
