<?php

namespace App\Http\Livewire\Kelas\Peserta;

use App\Models\User;
use App\Models\Kelas;
use Livewire\Component;
use App\Models\Kelompok;
use App\Models\Mahasiswa as MahasiswaModel;
use App\Models\MahasiswaTugas;
use App\Models\Tugas;
use App\Services\MahasiswaService;

class Mahasiswa extends Component
{
    public Kelas $kelas;
    public $mahasiswa;

    protected $listeners = ['kelompokAdded' => 'render'];

    public function render()
    {
        $getMahasiswa = MahasiswaModel::where('kelas_id', $this->kelas->id);
        $userItems = User::where('user_type', 'mahasiswa')->whereNotIn('id', $getMahasiswa->pluck('user_id')->toArray())->get();
        $kelompokItems = Kelompok::where('kelas_id', $this->kelas->id)->get();
        $mahasiswaItems = $getMahasiswa->get();

        return view('livewire.kelas.peserta.mahasiswa', compact('userItems', 'kelompokItems', 'mahasiswaItems'));
    }

    protected $rules = [
        'mahasiswa.kelompok_id' => 'required',
        'mahasiswa.user_ids' => 'required',
    ];

    protected $messages = [
        'mahasiswa.user_ids.required' => 'Mahasiswa harus dipilih.',
        'mahasiswa.kelompok_id.required' => 'Kelompok harus dipilih.',
    ];

    public function updated($property)
    {
        $this->validateOnly($property);
    }

    public function saveMahasiswa()
    {
        $this->validate();

        foreach ($this->mahasiswa['user_ids'] as $id) {
            $mahasiswaSaved = MahasiswaModel::create([
                'user_id' => $id,
                'kelas_id' => $this->kelas->id,
                'kelompok_id' => $this->mahasiswa['kelompok_id'],
            ]);

            MahasiswaService::setMahasiswaToTugas($mahasiswaSaved->id, $this->kelas->id);
        }


        session()->flash('success', 'Mahasiswa berhasil ditambahkan.');

        $this->reset(['mahasiswa']);
        $this->emit('setUsersMahasiswaSelect', []);
        return redirect()->route('kelas.peserta', $this->kelas->kode);
    }

    public function deleteMahasiswa($mahasiswaId)
    {
        MahasiswaModel::findOrFail($mahasiswaId)->delete();
    }
}
