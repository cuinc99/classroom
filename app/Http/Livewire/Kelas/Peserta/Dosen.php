<?php

namespace App\Http\Livewire\Kelas\Peserta;

use App\Models\User;
use App\Models\Kelas;
use Livewire\Component;
use App\Models\Dosen as DosenModel;
use App\Models\Kelompok;

class Dosen extends Component
{
    public Kelas $kelas;
    public $dosen;

    protected $listeners = ['kelompokAdded' => 'render'];

    public function render()
    {
        $getDosen = DosenModel::where('kelas_id', $this->kelas->id)->where('status', '!=' ,'admin');
        $userItems = User::where('user_type', 'dosen')->get();
        $kelompokItems = Kelompok::where('kelas_id', $this->kelas->id)->get();
        $dosenItems = $getDosen->get();

        return view('livewire.kelas.peserta.dosen', compact('userItems', 'kelompokItems','dosenItems'));
    }

    protected $rules = [
        'dosen.kelompok_id' => 'required',
        'dosen.user_ids' => 'required',
    ];

    protected $messages = [
        'dosen.user_ids.required' => 'Dosen harus dipilih.',
        'dosen.kelompok_id.required' => 'Kelompok harus dipilih.',
    ];

    public function updated($property)
    {
        $this->validateOnly($property);
    }

    public function saveDosen()
    {
        $this->validate();

        foreach ($this->dosen['user_ids'] as $id) {
            DosenModel::create([
                'status' => $this->dosen['kelompok_id'] == 'main' ? 'main' : 'penilai',
                'user_id' => $id,
                'kelas_id' => $this->kelas->id,
                'kelompok_id' => $this->dosen['kelompok_id'] == 'main' ? NULL : $this->dosen['kelompok_id'],
            ]);
        }


        session()->flash('success', 'Dosen berhasil ditambahkan.');

        $this->reset(['dosen']);
        $this->emit('setUsersSelect', []);
        return redirect()->route('kelas.peserta', $this->kelas->kode);
    }

    public function deleteDosen($dosenId)
    {
        DosenModel::findOrFail($dosenId)->delete();
    }
}
