<?php

namespace App\Http\Livewire\Kelas\Tugas\Pertanyaan;

use App\Models\Jawaban;
use App\Models\Kelas;
use App\Models\Tugas;
use Livewire\Component;
use App\Models\Kategori;
use App\Models\KelompokPertanyaan;
use App\Models\Pertanyaan;
use App\Models\TugasPertanyaan;

class Kuis extends Component
{
    public Kelas $kelas;
    public Kategori $kategori;
    public Tugas $tugas;
    public $kelompok_pertanyaan_id;

    public function render()
    {
        $kelompokPertanyaans = KelompokPertanyaan::whereBlokId($this->kelas->blok_id)->latest()->get();
        $pertanyaans = $this->tugas->pertanyaans;

        return view('livewire.kelas.tugas.pertanyaan.kuis', compact('pertanyaans', 'kelompokPertanyaans'));
    }

    protected $rules = [
        'kelompok_pertanyaan_id' => 'required',
    ];

    public function updated($property)
    {
        $this->validateOnly($property);
    }

    public function save()
    {
        $kelompokPertanyaan = KelompokPertanyaan::findOrFail($this->kelompok_pertanyaan_id);

        foreach ($kelompokPertanyaan->pertanyaans as $pertanyaan) {
            TugasPertanyaan::updateOrcreate(
                [
                    'tugas_id' => $this->tugas->id,
                    'pertanyaan_id' => $pertanyaan->id,
                ],
                [
                    'tugas_id' => $this->tugas->id,
                    'pertanyaan_id' => $pertanyaan->id,
                ]
            );
        }

        session()->flash('success', 'Pertanyaan berhasil ditambahkan.');
        $this->reset(['kelompok_pertanyaan_id']);
        return redirect()->route('pertanyaan.daftar', [$this->kelas->kode, $this->kategori->kode, $this->tugas->kode]);
    }

    public function deletePertanyaan($pertanyaaniId)
    {
        TugasPertanyaan::findOrFail($pertanyaaniId)->delete();
        session()->flash('success', 'Pertanyaan berhasil dihapus.');
        return redirect()->route('pertanyaan.daftar', [$this->kelas->kode, $this->kategori->kode, $this->tugas->kode]);
    }
}
