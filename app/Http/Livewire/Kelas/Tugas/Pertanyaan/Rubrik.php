<?php

namespace App\Http\Livewire\Kelas\Tugas\Pertanyaan;

use App\Models\Jawaban;
use App\Models\Kelas;
use App\Models\Tugas;
use Livewire\Component;
use App\Models\Kategori;
use App\Models\Pertanyaan;

class Rubrik extends Component
{
    public Kelas $kelas;
    public Kategori $kategori;
    public Tugas $tugas;
    public $pertanyaan;

    public function render()
    {
        $pertanyaans = Pertanyaan::where('tugas_id', $this->tugas->id)->latest()->get();
        return view('livewire.kelas.tugas.pertanyaan.rubrik', compact('pertanyaans'));
    }

    protected $rules = [
        'pertanyaan.pertanyaan' => 'required',
        'pertanyaan.maks_poin' => 'required',
        'pertanyaan.deskripsi' => 'sometimes',
    ];

    protected $messages = [
        'pertanyaan.judul.required' => 'Judul kriteria harus diisi.',
        'pertanyaan.maks_poin.required' => 'Maksimal poin harus diisi.',
    ];

    public function updated($property)
    {
        $this->validateOnly($property);
    }

    public function saveRubrik()
    {
        $pertanyaan = $this->pertanyaan;
        $pertanyaan['tugas_id'] = $this->tugas->id;
        Pertanyaan::create($pertanyaan);

        session()->flash('success', 'Kriteria berhasil ditambahkan.');
        $this->reset(['pertanyaan']);
    }

    public function deletePertanyaan($pertanyaaniId)
    {
        Pertanyaan::findOrFail($pertanyaaniId)->delete();
    }
}
