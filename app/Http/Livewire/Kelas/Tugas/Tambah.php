<?php

namespace App\Http\Livewire\Kelas\Tugas;

use App\Models\Kelas;
use App\Models\Tugas;
use Livewire\Component;
use App\Models\Kategori;
use Livewire\WithFileUploads;
use App\Services\MahasiswaService;

class Tambah extends Component
{
    use WithFileUploads;

    public Kelas $kelas;
    public Kategori $kategori;
    public $tugas;

    public function render()
    {
        return view('livewire.kelas.tugas.tambah');
    }

    protected $rules = [
        'tugas.judul' => 'required',
        'tugas.instruksi' => 'sometimes',
        'tugas.maks_poin' => 'sometimes',
        // 'tugas.batas_akhir' => 'sometimes',
        'tugas.tipe' => 'required',
        'kelas.acak_soal' => 'sometimes',
        'kelas.acak_jawaban' => 'sometimes',
        'kelas.dokumen_instruksi' => 'sometimes',
    ];

    protected $messages = [
        'tugas.judul.required' => 'Judul tugas harus diisi.',
        'tugas.tipe.required' => 'Tipe tugas harus diisi.',
    ];

    public function updated($property)
    {
        $this->validateOnly($property);
    }

    public function save()
    {
        $tugas = $this->tugas;
        $tugas['kelas_id'] = $this->kelas->id;
        $tugas['kategori_id'] = $this->kategori->id;
        $tugas['user_id'] = auth()->id();
        $tugas['status_ujian'] = $tugas['tipe'] == 'kuis' ? 0 : 1;
        $tugas['mulai_pada'] = $tugas['tipe'] == 'kuis' ? NULL : now();

        if (isset($this->tugas['dokumen_instruksi'])) {
            $path = $this->tugas['dokumen_instruksi']->store('dokumen_instruksi', 'public');

            $tugas['dokumen_instruksi'] = $path;
        }

        $tugasSaved = Tugas::create($tugas);

        MahasiswaService::setTugasToMahasiswa($tugasSaved->id, $this->kelas->id);

        session()->flash('success', 'Tugas berhasil ditambahkan.');
        $this->reset(['tugas']);
        redirect()->route('kelas.penilaianSumatif', $this->kelas->kode);
    }
}
