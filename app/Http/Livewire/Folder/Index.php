<?php

namespace App\Http\Livewire\Folder;

use App\Models\Folder;
use Livewire\Component;
use Illuminate\Http\Request;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $folder;
    public $folderId;
    public $currentFolderId;
    public $currentFolder;
    public $search;

    public function mount(Request $request, $folderId)
    {
        $this->currentFolderId = $folderId;
        if ($this->currentFolderId) {
            $this->currentFolder = Folder::findOrFail($this->currentFolderId);
        }
        $this->search = $request->q;
    }

    public function render()
    {
        $folders = Folder::query()
            ->when($this->currentFolderId, fn ($q) => $q->where('parent_id', $this->currentFolderId))
            ->when($this->currentFolderId == NULL, fn ($q) => $q->where('parent_id', $this->currentFolderId))
            ->when($this->search != '', function($q) {
                $q->where('nama', 'like', '%' . $this->search . '%');
            })
            ->latest()
            ->paginate(10);
        return view('livewire.folder.index', compact('folders'));
    }

    protected $rules = [
        'folder.nama' => 'required',
    ];

    protected $messages = [
        'folder.nama.required' => 'Nama folder harus diisi.',
    ];

    public function updated($property)
    {
        $this->validateOnly($property);
    }

    public function create()
    {
        $this->reset(['folder', 'folderId']);
    }

    public function edit($folderId)
    {
        $this->folderId = $folderId;
        $this->folder = Folder::findOrFail($folderId);
    }

    public function close()
    {
        $this->reset(['folder', 'folderId']);
    }

    public function save()
    {
        $this->validate();

        if (!is_null($this->folderId)) {
            $this->folder->save();
            session()->flash('success', 'Folder berhasil diubah.');
        } else {
            $folder = $this->folder;
            $folder['parent_id'] = $this->currentFolderId ?? NULL;
            Folder::create($folder);
            session()->flash('success', 'Folder berhasil ditambahkan.');
            $this->reset(['folder', 'folderId']);
        }
        if ($this->currentFolderId) {
            return redirect()->route('folder.show', $this->currentFolder->kode);
        } else {
            return redirect()->route('home');
        }

    }

    public function delete($folderId)
    {

        $folder = Folder::findOrFail($folderId);
        $folder->kelas()->each(fn ($kls) => $kls->update(['folder_id' => 1]));
        $folder->delete();
        $this->reset(['folder', 'folderId']);
        return back();
    }
}
