<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class NilaiExport implements FromView, ShouldAutoSize
{
    public $kelas;
    public $mahasiswas;

    public function __construct($kelas, $mahasiswas)
    {
        $this->kelas = $kelas;
        $this->mahasiswas = $mahasiswas;
    }

    public function view(): View
    {
        return view('admin.exports.nilai-xls', [
            'kelas' => $this->kelas,
            'mahasiswas' => $this->mahasiswas
        ]);
    }

    // public function registerEvents(): array
    // {
    //     return [
    //         AfterSheet::class => function (AfterSheet $event) {
    //             $headRange = 'A1:C1';
    //             $headStyle = [
    //                 'font' => [
    //                     'bold' => true,
    //                 ],
    //                 'fill' => [
    //                     'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
    //                     'startColor' => [
    //                         'rgb' => '28bf13',
    //                     ],
    //                 ],
    //             ];
    //             $event->sheet->getDelegate()->getStyle($headRange)->applyFromArray($headStyle);
    //         },
    //     ];
    // }
}
