<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MahasiswaExport implements FromView, ShouldAutoSize
{
    public $mahasiswas;

    public function __construct($mahasiswas)
    {
        $this->mahasiswas = $mahasiswas;
    }

    public function view(): View
    {
        return view('admin.exports.form-penilaian-mahasiswa', [
            'mahasiswas' => $this->mahasiswas
        ]);
    }
}
