<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Kuliah extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($kuliah) {
            $kuliah->kode = Str::random(1) . Str::random(1) . rand(10,99) . Str::random(1) . Str::random(1);
        });
    }

    public function kelas(): BelongsTo
    {
        return $this->belongsTo(Kelas::class, 'kelas_id');
    }

    public function berita_acaras(): HasMany
    {
        return $this->hasMany(BeritaAcara::class, 'kuliah_id');
    }

    public function sgds(): HasMany
    {
        return $this->hasMany(Sgd::class, 'kuliah_id')->latest();
    }

    public function sgdsByOldest(): HasMany
    {
        return $this->hasMany(Tugas::class, 'kategori_id')->oldest();
    }
}
