<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BeritaAcara extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $casts = [
        'tanggal' => 'datetime',
        'dosen_pengampu' => 'array',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($berita_acara) {
            $berita_acara->kode = Str::random(1) . Str::random(1) . rand(10,99) . Str::random(1) . Str::random(1);
        });
    }

    const METODE_SELECT = ['Kuliah', 'Jurnal Reading', 'SGD', 'Pleno', 'CSL'];

    const STATUS_SELECT = [
        'Buka' => [
            'warna' => 'success'
        ],
        'Tutup' => [
            'warna' => 'danger'
        ],
    ];

    public function getDosenPengampuListAttribute()
    {
        $dosen = [];

        $dosen[] = User::whereId($this->user_id)->value('name') . ' (Utama)';

        if ($this->dosen_pengampu) {
            foreach ($this->dosen_pengampu as $value) {
                $dosen[] = User::whereId($value)->value('name');
            }
        }

        return $dosen;
    }

    public function isBuka()
    {
        return $this->status == 'Buka';
    }

    public function isTutup()
    {
        return $this->status == 'Tutup';
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function kelas(): BelongsTo
    {
        return $this->belongsTo(Kelas::class, 'kelas_id');
    }

    public function kelompok(): BelongsTo
    {
        return $this->belongsTo(Kelompok::class, 'kelompok_id');
    }

    public function kuliah(): BelongsTo
    {
        return $this->belongsTo(Kuliah::class, 'kuliah_id');
    }

    public function feedbacks(): HasMany
    {
        return $this->hasMany(Feedback::class, 'berita_acara_id');
    }

    public function absensis(): HasMany
    {
        return $this->hasMany(Absensi::class, 'berita_acara_id');
    }
}
