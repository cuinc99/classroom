<?php

namespace App\Models;

use App\Models\Pertanyaan;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Jenssegers\Date\Date;

class Tugas extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $casts = [
        'batas_akhir' => 'datetime',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($tugas) {
            $tugas->kode = Str::random(1) . Str::random(1) . rand(10, 99) . Str::random(1) . Str::random(1);
        });
    }

    const TIPE_OPTION = [
        'rubrik' => [
            'judul' => 'Kriteria',
            'warna' => 'info',
            'icon' => '/img/rubrik.png',
        ],
        'rubrik2' => [
            'judul' => 'Kriteria',
            'warna' => 'primary',
            'icon' => '/img/rubrik.png',
        ],
        'kuis' => [
            'judul' => 'Soal',
            'warna' => 'warning',
            'icon' => '/img/kuis.png',
        ],
        'input' => [
            'judul' => 'Soal',
            'warna' => 'danger',
            'icon' => '/img/input.png',
        ],
    ];

    const NILAI_SELECT = [
        0 => [
            'ket' => 'Tidak dijawab',
            'warna' => 'secondary'
        ],
        1 => [
            'ket' => 'Sangat Kurang',
            'warna' => 'danger'
        ],
        2 => [
            'ket' => 'Kurang',
            'warna' => 'warning'
        ],
        3 => [
            'ket' => 'Cukup',
            'warna' => 'info'
        ],
        4 => [
            'ket' => 'Baik',
            'warna' => 'success'
        ],
        5 => [
            'ket' => 'Sangat Baik',
            'warna' => 'primary'
        ],
    ];

    public function getColorAttribute()
    {
        if ($this->batas_akhir < now()) {
            return 'danger';
        }

        return;
    }

    public function getLateAttribute()
    {
        $date = Date::parse($this->batas_akhir);
        if ($this->batas_akhir < now()) {
            $diff = date_diff(now(), $date);
            return 'Telat ' . $diff->format("%R%a hari");
        }

        return;
    }

    public function pertanyaans(): BelongsToMany
    {
        return $this->belongsToMany(Pertanyaan::class, 'tugas_pertanyaans', 'tugas_id', 'pertanyaan_id')->withPivot('id');
    }

    public function pertanyaanTugas(): HasMany
    {
        return $this->hasMany(Pertanyaan::class, 'tugas_id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function kelas(): BelongsTo
    {
        return $this->belongsTo(Kelas::class, 'kelas_id');
    }

    public function kategori(): BelongsTo
    {
        return $this->belongsTo(Kategori::class, 'kategori_id');
    }

    public function mahasiswa_tugas(): HasOne
    {
        $mahasiswa = Mahasiswa::where('kelas_id', $this->kelas->id)->where('user_id', auth()->id())->firstOrFail();
        return $this->hasOne(MahasiswaTugas::class, 'tugas_id')->where('mahasiswa_id', $mahasiswa->id);
    }

    public function dosen_penilai(): HasMany
    {
        return $this->hasMany(Dosen::class, 'tugas_id');
    }
}
