<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Kelompok extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($kelompok) {
            $kelompok->kode = Str::random(1) . Str::random(1) . rand(10,99) . Str::random(1) . Str::random(1);
        });
    }

    public function mahasiswas(): BelongsToMany
    {
        return $this->belongsToMany(Mahasiswa::class, 'mahasiswa_kelompoks', 'kelompok_id', 'mahasiswa_id');
    }

    public function kelas(): BelongsTo
    {
        return $this->belongsTo(Kelas::class, 'kelas_id');
    }

    public function berita_acaras(): HasMany
    {
        return $this->hasMany(BeritaAcara::class, 'kelompok_id');
    }

    public function sgds(): HasMany
    {
        return $this->hasMany(Sgd::class, 'kelompok_id');
    }
}
