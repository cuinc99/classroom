<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class KelompokPertanyaan extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($kelompokPertanyaan) {
            $kelompokPertanyaan->kode = Str::random(1) . Str::random(1) . rand(10,99) . Str::random(1) . Str::random(1);
        });
    }

    public function pertanyaans(): HasMany
    {
        return $this->hasMany(Pertanyaan::class, 'kelompok_pertanyaan_id');
    }

    public function blok(): BelongsTo
    {
        return $this->belongsTo(Blok::class, 'blok_id');
    }
}
