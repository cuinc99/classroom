<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Blok extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function pertanyaans(): HasMany
    {
        return $this->hasMany(Pertanyaan::class, 'blok_id');
    }

    public function kelompok_pertanyaans(): HasMany
    {
        return $this->hasMany(KelompokPertanyaan::class, 'blok_id');
    }

    public function kelas(): HasMany
    {
        return $this->hasMany(Kelas::class, 'blok_id');
    }
}
