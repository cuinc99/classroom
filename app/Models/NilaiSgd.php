<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class NilaiSgd extends Model
{
    use HasFactory;

    protected $guarded = [];

    const NILAI_SELECT = [
        0 => [
            'ket' => 'Sangat Kurang',
            'warna' => 'danger'
        ],
        1 => [
            'ket' => 'Kurang',
            'warna' => 'warning'
        ],
        2 => [
            'ket' => 'Cukup',
            'warna' => 'info'
        ],
        3 => [
            'ket' => 'Baik',
            'warna' => 'success'
        ],
        4 => [
            'ket' => 'Sangat Baik',
            'warna' => 'primary'
        ],
    ];

    public function kriteria_sgd(): BelongsTo
    {
        return $this->belongsTo(KriteriaSgd::class, 'kriteria_sgd_id');
    }

    public function mahasiswa(): BelongsTo
    {
        return $this->belongsTo(Mahasiswa::class, 'mahasiswa_id');
    }
}
