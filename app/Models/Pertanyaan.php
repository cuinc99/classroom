<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pertanyaan extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($tugas) {
            $tugas->kode = Str::uuid();
        });
    }

    public function blok(): BelongsTo
    {
        return $this->belongsTo(Blok::class, 'blok_id');
    }

    public function jawabans(): HasMany
    {
        return $this->hasMany(Jawaban::class, 'pertanyaan_id');
    }

    public function deletePertanyaan($pertanyaanId)
    {
        return;
    }

    public function tugas_pertanyaans(): HasMany
    {
        return $this->hasMany(TugasPertanyaan::class, 'pertanyaan_id');
    }
}
