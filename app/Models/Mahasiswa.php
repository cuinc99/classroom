<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Mahasiswa extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($mahasiswa) {
            $mahasiswa->kode = Str::uuid();
        });
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    // public function kelompok(): BelongsTo
    // {
    //     return $this->belongsTo(Kelompok::class, 'kelompok_id');
    // }

    public function mahasiswa_tugas($tugasId)
    {
        return MahasiswaTugas::whereMahasiswaId($this->id)->whereTugasId($tugasId)->firstOrFail();
    }

    public function kelompoks(): BelongsToMany
    {
        return $this->belongsToMany(Kelompok::class, 'mahasiswa_kelompoks', 'mahasiswa_id', 'kelompok_id');
    }

    public function berita_acaras()
    {
        $kelompokIds = $this->kelompoks->pluck('id');
        $beritaAcara = BeritaAcara::query()
            ->whereIn('kelompok_id', $kelompokIds)
            ->latest('tanggal')
            ->get();

        return $beritaAcara;
    }
}
