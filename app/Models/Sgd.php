<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Sgd extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $casts = [
        'tanggal' => 'datetime',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($berita_acara) {
            $berita_acara->kode = Str::random(1) . Str::random(1) . rand(10,99) . Str::random(1) . Str::random(1);
        });
    }

    const STATUS_SELECT = [
        'Buka' => [
            'warna' => 'success'
        ],
        'Tutup' => [
            'warna' => 'danger'
        ],
    ];

    public function isBuka()
    {
        return $this->status == 'Buka';
    }

    public function isTutup()
    {
        return $this->status == 'Tutup';
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function kelas(): BelongsTo
    {
        return $this->belongsTo(Kelas::class, 'kelas_id');
    }

    public function kelompok(): BelongsTo
    {
        return $this->belongsTo(Kelompok::class, 'kelompok_id');
    }

    public function kuliah(): BelongsTo
    {
        return $this->belongsTo(Kuliah::class, 'kuliah_id');
    }

    public function kriteria_sgds(): HasMany
    {
        return $this->hasMany(KriteriaSgd::class, 'sgd_id');
    }
}
