<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Kategori extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($kategori) {
            $kategori->kode = Str::random(1) . Str::random(1) . rand(10,99) . Str::random(1) . Str::random(1);
        });
    }

    public function tugas(): HasMany
    {
        return $this->hasMany(Tugas::class, 'kategori_id')->latest();
    }

    public function tugasByOldest(): HasMany
    {
        return $this->hasMany(Tugas::class, 'kategori_id')->oldest();
    }
}
