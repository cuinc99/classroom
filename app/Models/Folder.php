<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Folder extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $appends = ['nama_lengkap'];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($folder) {
            $folder->kode = Str::random(1) . Str::random(1) . rand(10, 99) . Str::random(1) . Str::random(1);
        });

        static::deleting(function ($folder) {
            foreach ($folder->child_folder as $child) {
                $child->delete();
            }
        });
    }

    public function getNamaLengkapAttribute()
    {
        if (!$this->parent_id) {
            return $this->nama;
        }

        $folders = Folder::all();
        $folder = Folder::find($this->id);
        $name = [$folder->nama];

        foreach ($folders as $value) {
            if ($folder->parent_id) {
                $folder = Folder::find($folder->parent_id);
                $name[] = $folder->nama;
            }
        }

        return implode(" / ", array_reverse($name));
    }

    public function kelas(): HasMany
    {
        return $this->hasMany(Kelas::class, 'folder_id');
    }

    public function parent_folder(): BelongsTo
    {
        return $this->belongsTo(Folder::class, 'parent_id');
    }

    public function child_folder(): HasMany
    {
        return $this->hasMany(Folder::class, 'parent_id');
    }
}
