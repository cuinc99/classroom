<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Kelas extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($kelas) {
            $kelas->kode = Str::random(1) . Str::random(1) . rand(10,99) . Str::random(1) . Str::random(1);
        });
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function blok(): BelongsTo
    {
        return $this->belongsTo(Blok::class);
    }

    public function folder(): BelongsTo
    {
        return $this->belongsTo(Folder::class);
    }

    public function kategoris(): HasMany
    {
        return $this->hasMany(Kategori::class, 'kelas_id');
    }

    public function tugas(): HasMany
    {
        return $this->hasMany(Tugas::class, 'kelas_id');
    }

    public function mahasiswas(): HasMany
    {
        return $this->hasMany(Mahasiswa::class, 'kelas_id');
    }

    public function pengumumans(): HasMany
    {
        return $this->hasMany(Pengumuman::class, 'kelas_id')->latest();
    }

    public function kelompoks(): HasMany
    {
        return $this->hasMany(Kelompok::class, 'kelas_id')->latest();
    }

    // public function kuliahs(): HasMany
    // {
    //     return $this->hasMany(Kuliah::class, 'kelas_id')->latest();
    // }

    public function kuliahs_penilaian_formatif(): HasMany
    {
        return $this->hasMany(Kuliah::class, 'kelas_id')->where('tipe', 'penilaian formatif')->latest();
    }

    public function kuliahs_berita_acara(): HasMany
    {
        return $this->hasMany(Kuliah::class, 'kelas_id')->where('tipe', 'berita acara')->latest();
    }

    public function sgds(): HasMany
    {
        return $this->hasMany(Sgd::class, 'kelas_id')->latest();
    }
}
