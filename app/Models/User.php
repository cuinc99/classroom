<?php

namespace App\Models;

use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'username',
        'password',
        'user_type',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($user) {
            $user->kode = Str::uuid();
        });
    }

    public function isAdmin()
    {
        return $this->user_type == 'admin';
    }

    public function isAssessment()
    {
        return $this->user_type == 'assessment';
    }

    public function isDosen()
    {
        return $this->user_type == 'dosen';
    }

    public function isMahasiswa()
    {
        return $this->user_type == 'mahasiswa';
    }

    public function kelasDosens()
    {
        return $this->belongsToMany(Kelas::class, 'dosens', 'user_id', 'kelas_id')->whereActive(1);
    }

    public function kelasMahasiswas()
    {
        return $this->belongsToMany(Kelas::class, 'mahasiswas', 'user_id', 'kelas_id')->whereActive(1);
    }

    public function kelasSaya()
    {
        if (auth()->user()->isMahasiswa()) {
            return $this->kelasMahasiswas;
        } else {
            return $this->kelasDosens;
        }
    }

    public function kelompokDosens()
    {
        return $this->belongsToMany(Kelompok::class, 'dosens', 'user_id', 'kelompok_id');
    }

    public function kelompokMahasiswas()
    {
        return $this->belongsToMany(Kelompok::class, 'mahasiswas', 'user_id', 'kelompok_id');
    }

    public function kelompokSaya()
    {
        if (auth()->user()->isMahasiswa()) {
            return $this->kelompokMahasiswas;
        } elseif (auth()->user()->isDosen()) {
            return $this->kelompokDosens;
        } else {
            return;
        }
    }

    public function tugasSaya()
    {
        $kelasIds = auth()->user()->kelasSaya()->pluck('id')->toArray();

        if (auth()->user()->isMahasiswa()) {
            return Tugas::whereIn('kelas_id', $kelasIds);
        } else {
            return Tugas::whereIn('kelas_id', $kelasIds)->whereIn('tipe', ['rubrik']);
        }
    }

    public function pengumumanSaya()
    {
        $kelasIds = auth()->user()->kelasSaya()->pluck('id')->toArray();

        return Pengumuman::whereIn('kelas_id', $kelasIds)->latest();
    }

    public function kelompoks(): BelongsToMany
    {
        return $this->belongsToMany(Kelompok::class, 'mahasiswa_kelompoks', 'user_id', 'kelompok_id');
    }
}
