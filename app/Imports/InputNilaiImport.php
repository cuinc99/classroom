<?php

namespace App\Imports;

use App\Models\MahasiswaTugas;
use Maatwebsite\Excel\Concerns\ToModel;

class InputNilaiImport implements ToModel
{
    public $tugasId;

    public function __construct($tugasId)
    {
        $this->tugasId = $tugasId;
    }

    public function model(array $row)
    {
        if (($row[0] == 'No')) {
            return NULL;
        }

        if (($row[4] == '')) {
            return NULL;
        }

        MahasiswaTugas::whereMahasiswaId($row[1])->whereTugasId($this->tugasId)->update([
            'total_poin' => $row[4] ?? 0,
            'tanggal_kumpul' => now(),
        ]);
    }
}
