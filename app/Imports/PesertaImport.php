<?php

namespace App\Imports;

use App\Models\User;
use App\Models\Kelas;
use App\Models\Kelompok;
use App\Models\Mahasiswa;
use App\Models\MahasiswaKelompok;
use App\Services\MahasiswaService;
use Maatwebsite\Excel\Concerns\ToModel;

class PesertaImport implements ToModel
{
    public $kode;

    public function __construct($kode)
    {
        $this->kode = $kode;
    }

    public function model(array $row)
    {
        if (($row[0] == 'NIM')) {
            return NULL;
        }

        $user = User::firstOrCreate(
            [
                'username' => $row[0]
            ],
            [
                'username' => $row[0],
                'name' => $row[1],
                'user_type' => 'mahasiswa',
                'password' => bcrypt('password'),
            ]
        );

        $kelas = Kelas::whereKode($this->kode)->first();
        if (!$kelas) {
            return NULL;
        }

        $kelompok = Kelompok::firstOrCreate(
            [
                'nama' => $row[2],
                'kelas_id' => $kelas->id,
            ],
            [
                'nama' => $row[2],
                'kelas_id' => $kelas->id
            ]
        );

        $mahasiswa = Mahasiswa::firstOrCreate(
            [
                'user_id' => $user->id,
                'kelas_id' => $kelas->id,
            ],
            [
                'user_id' => $user->id,
                'kelas_id' => $kelas->id
            ]
        );

        MahasiswaService::setMahasiswaToTugas($mahasiswa->id, $kelas->id);

        $mahasiswaKelompok = MahasiswaKelompok::whereUserId($user->id)->whereMahasiswaId($mahasiswa->id)->whereKelompokId($kelompok->id)->first();
        if ($mahasiswaKelompok) {
            return NULL;
        }

        return MahasiswaKelompok::create([
            'user_id' => $user->id,
            'mahasiswa_id' => $mahasiswa->id,
            'kelompok_id' => $kelompok->id,
        ]);
    }
}
