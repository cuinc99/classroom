<?php

namespace App\Imports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\ToModel;

class UsersImport implements ToModel
{
    public function model(array $row)
    {
        if (($row[0] == 'NIM')) {
            return NULL;
        }

        $user = User::whereUsername($row[0])->first();
        if ($user) {
            return NULL;
        }

        return User::create([
            'username' => $row[0],
            'name' => $row[1],
            'password' => bcrypt($row[2]),
            'user_type' => 'mahasiswa',
        ]);
    }
}
