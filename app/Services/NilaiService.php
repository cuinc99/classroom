<?php

namespace App\Services;

use App\Models\JawabanTugas;

class NilaiService
{
    static public function getNilaiHuruf($total)
    {
        if ($total == 0) {
            return 'E';
        }

        if ($total >= 80 && $total <= 100) {
            return 'A';
        } elseif ($total >= 70 && $total <= 79.99) {
            return 'B';
        } elseif ($total >= 60 && $total <= 69.99) {
            return 'C';
        } elseif ($total >= 50 && $total <= 59.99) {
            return 'D';
        } elseif ($total <= 49.99) {
            return 'E';
        } else {
            return '-';
        }


        // switch ($total) {
        //     case $total >= 76 && $total <= 100:
        //         return 'A';
        //         break;

        //     case $total >= 66 && $total <= 75:
        //         return 'B';
        //         break;

        //     case $total >= 56 && $total <= 65:
        //         return 'C';
        //         break;

        //     case $total >= 46 && $total <= 55:
        //         return 'D';
        //         break;

        //     case $total <= 45:
        //         return 'E';
        //         break;

        //     default:
        //         return 'E';
        // }
    }

    static public function getTotalPoinKuis($mahasiswaId, $tugasId, $pertanyaanIds)
    {
        // return $jawabanTugasFix = JawabanTugas::query()
        //     ->whereTugasId($tugasId)
        //     ->whereMahasiswaId($mahasiswaId)
        //     ->whereIn('pertanyaan_id', $pertanyaanIds)
        //     ->pluck('pertanyaan_id');

        // $jawabanTugasFix = JawabanTugas::query()
        //     ->whereTugasId($tugasId)
        //     ->whereMahasiswaId($mahasiswaId)
        //     ->whereIn('pertanyaan_id', $pertanyaanIds)
        //     ->get()
        //     ->pluck('id')->toArray();

        // JawabanTugas::query()
        //     ->whereTugasId($tugasId)
        //     ->whereMahasiswaId($mahasiswaId)
        //     ->whereNotIn('id', $jawabanTugasFix)
        //     ->delete();

        // $total_poin = JawabanTugas::whereMahasiswaId($mahasiswaId)->whereTugasId($tugasId)->whereIn('pertanyaan_id', $pertanyaanIds)->sum('poin');
        $total_poin = JawabanTugas::whereMahasiswaId($mahasiswaId)->whereTugasId($tugasId)->sum('poin');

        return $total_poin;
    }
}
