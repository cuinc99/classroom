<?php

namespace App\Services;

use App\Models\Dosen;
use App\Models\Tugas;
use App\Models\Mahasiswa;
use App\Models\MahasiswaTugas;

class MahasiswaService
{
    static public function setMahasiswaToTugas($mahasiswaId, $kelasId)
    {
        $tugasItems = Tugas::where('kelas_id', $kelasId)->get();
        foreach ($tugasItems as $tugas) {
            MahasiswaTugas::firstOrCreate(
                [
                    'mahasiswa_id' => $mahasiswaId,
                    'tugas_id' => $tugas->id,
                ],
                [
                    'total_poin' => 0,
                    'mahasiswa_id' => $mahasiswaId,
                    'tugas_id' => $tugas->id,
                ]
            );
        }
    }

    static public function setTugasToMahasiswa($tugasId, $kelasId)
    {
        $mahasiswaItems = Mahasiswa::where('kelas_id', $kelasId)->get();
        foreach ($mahasiswaItems as $mahasiswa) {
            MahasiswaTugas::firstOrCreate(
                [
                    'mahasiswa_id' => $mahasiswa->id,
                    'tugas_id' => $tugasId,
                ],
                [
                    'total_poin' => 0,
                    'mahasiswa_id' => $mahasiswa->id,
                    'tugas_id' => $tugasId,
                ]
            );
        }
    }

    static public function getDosenPenilai($tugasId, $userId)
    {
        $tugas = Tugas::findOrFail($tugasId);

        if (auth()->user()->isDosen()) {
            $kelompokIds = Dosen::whereKelasId($tugas->kelas->id)->whereUserId($userId)->pluck('kelompok_id');
        } else {
            $kelompokIds = Mahasiswa::whereKelasId($tugas->kelas->id)->whereUserId($userId)->with('kelompoks')->first()->kelompoks->pluck('id');
        }


        $penilai = [];
        foreach ($tugas->dosen_penilai->whereIn('kelompok_id', $kelompokIds) as $dosen) {
            $penilai[] = $dosen->user->name;
        }

        if ($penilai) {
            return '<span class="text-primary">' . implode(', ', $penilai) . '</span>';
        }
        return '<span class="text-danger">Belum ada dosen penilai</span>';

    }
}
